<?php
 

/**
 * Send a POST requst using cURL
 * @param string $url to request
 * @param array $post values to send
 * @param array $options for cURL
 * @return string
 */
function curl_call($url,$method, $data)
{
	global $CFG;
	 require_once($CFG->dirroot.'/depths/mapper/utility.php');
 print_message("curl_call:".$url." method:".$method,"curl_call");
 print_message("curl_call send data:".$data,"curl_call");
# headers and data (this is API dependent, some uses XML)
$headers = array(
'Accept: application/json',
'Content-Type: application/json',
);

$handle = curl_init();
curl_setopt($handle, CURLOPT_URL, $url);
curl_setopt($handle, CURLOPT_HTTPHEADER, $headers);
curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
curl_setopt($handle, CURLOPT_SSL_VERIFYHOST, false);
curl_setopt($handle, CURLOPT_SSL_VERIFYPEER, false);
 
switch($method)
{

case 'GET':
break;

case 'POST':
 
curl_setopt($handle, CURLOPT_POST, true);
curl_setopt($handle, CURLOPT_POSTFIELDS, $data);
break;

case 'PUT':
curl_setopt($handle, CURLOPT_CUSTOMREQUEST, 'PUT');
//curl_setopt($handle, CURLOPT_POSTFIELDS,"id=".urlencode("someid"), $data);
curl_setopt($handle, CURLOPT_POSTFIELDS, $data);
break;

case 'DELETE':
curl_setopt($handle, CURLOPT_CUSTOMREQUEST, 'DELETE');
break;
}
 
$response = curl_exec($handle);
 
$code = curl_getinfo($handle, CURLINFO_HTTP_CODE);
 
print_message("curl_call received answer:".$response,"curl_call");
	return $response;
}
?>