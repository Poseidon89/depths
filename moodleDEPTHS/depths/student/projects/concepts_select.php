<?php  


    require_once(dirname(__FILE__).'/../../../config.php');
    require_once($CFG->dirroot.'/depths/mapper/utility.php');
     require_once($CFG->dirroot.'/depths/lib/depths_settingslib.php'); 
    require_once($CFG->dirroot.'/depths/lib/depths_lib.php');
    require_once($CFG->dirroot.'/depths/lib/depths_tagslib.php');
    require_once("$CFG->libdir/rsslib.php");
 	 $reload=optional_param ('reload', 0, PARAM_BOOL);
    $calledFrom = optional_param('calledFrom', '', PARAM_TEXT);
	 //////////////////////////////////
	global $SESSION; 

  
    $id = optional_param('id', 0, PARAM_INT);           // Course Module ID
    $tab  = optional_param('tab', GLOSSARY_NO_VIEW, PARAM_ALPHA);    // browsing entries by categories?
    
    $displayformat = optional_param('displayformat',-1, PARAM_INT);  // override of the glossary display format
 
    $mode       = optional_param('mode', '', PARAM_ALPHA);           // term entry cat date available search author approval
 
    $hook       = optional_param('hook', '', PARAM_CLEAN);           // the term, entry, cat, etc... to look for based on mode
   
    $fullsearch = optional_param('fullsearch', 0,PARAM_INT);         // full search (concept and definition) when searching?
   
    $sortkey    = optional_param('sortkey', '', PARAM_ALPHA);// Sorted view: CREATION | UPDATE | FIRSTNAME | LASTNAME...
    
    $sortorder  = optional_param('sortorder', 'ASC', PARAM_ALPHA);   // it defines the order of the sorting (ASC or DESC)
    
    $offset     = optional_param('offset', 0,PARAM_INT);             // entries to bypass (for paging purposes)
     
    $page       = optional_param('page', 0,PARAM_INT);               // Page to show (for paging purposes)
    
    $show       = optional_param('show', '', PARAM_ALPHA);           // [ concept | alias ] => mode=term hook=$show
    
    $problemuri = optional_param('problemuri','',PARAM_URL);
    
    if($problemuri!=null){

   require_course_login($course->id, true, $cm);
    $context = get_context_instance(CONTEXT_MODULE, $cm->id);
 

    $navlinks = array();
    $navlinks[] = array('name' => "Related concepts", 'link' => "concepts_select.php", 'type' => 'activity');
    $navlinks[] = array('name' => format_string($glossary->name), 'link' => "view.php?id=$id", 'type' => 'activityinstance');

 
        $navigation = build_navigation($navlinks);
        print_header_simple(format_string(/*$glossary->name*/"Related concepts"), "", $navigation, "", "", true,
            /*update_module_button($cm->id, $course->id, $strglossary),*/ navmenu($course, $cm));
 
 
    echo '<br />';
	
		 
	
       	print_box("Please select the concepts related to the problem", 'generalbox', 'intro');
       	$json = getAllDomainConcepts();
       	$json=substr($json,1,strlen($json)-2);
       	print_message("problem uri to get concepts:".$problemuri,"concepts_select");
       	$selectedConceptsArray=getRecommendedConceptsForDesignProblem($problemuri);
        
       	  $array_json=json_decode($json);
           	echo "<center><form name='addconcepts' id='addconcepts'><table><tr>";  
       	   $concepts_json=$array_json->concepts;
        	 $i=0;
       	 foreach($concepts_json as $k=>$v){
       	 	 
       	 		 $i=$i+1;
       	 		$conceptName=$v->concept;
       	 		$conceptUri=$v->uri;
       	 		if(in_array($conceptUri,$selectedConceptsArray)){
       	 			print_message("This concept is in array:".$conceptUri,"relatedConcepts");
       	 			echo '<td><input type="checkbox" checked="checked" name="checkbox" id="'.$conceptUri.'" value="'.$conceptUri.'">'.$conceptName.'</input></td>	';
       	 		}else{
       	 		//$conceptFreq=$v->freq;
       	 		//echo "conceptName:".$conceptName;
       	 		echo '<td><input type="checkbox" name="checkbox" id="'.$conceptUri.'" value="'.$conceptUri.'">'.$conceptName.'</input></td>	';
       	 		}
       	 	 if($i==4){
       	 	 	echo "</tr><tr>";
       	 	 	$i=0;
       	 	 }
       
       	 }
       	 echo "</tr></table>
       	 <br><input type='submit' value='Save and close' onClick='sendSelectedConcepts(\"xxxa\")';>
       	 </form></center>";
       	 
       	  //$val=$array_json[0];
       	  //echo "ss".$val['concepts'];
       	  
       	  
       //	echo "<br>json:".$json;
       
       	
 
    print_footer($course);
	}

?>
