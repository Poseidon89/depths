<?php  // $Id: tabs.php,v 1.1 2008/02/20 10:26:33 cvsadmin Exp $
    if (!isset($sortorder)) {
        $sortorder = '';
    }    
    if (!isset($sortkey)) {
        $sortkey = '';
    }
    
    //make sure variables are properly cleaned
    $sortkey   = clean_param($sortkey, PARAM_ALPHA);// Sorted view: CREATION | UPDATE | FIRSTNAME | LASTNAME...
    $sortorder = clean_param($sortorder, PARAM_ALPHA);   // it defines the order of the sorting (ASC or DESC)

    $toolsrow = array();
    $browserow = array();
    $inactive = array();
    $activated = array();

    if (!has_capability('mod/glossary:approve', $context) && $tab == GLOSSARY_APPROVAL_VIEW) {
    /// Non-teachers going to approval view go to defaulttab
        $tab = $defaulttab;
    }


    $browserow[] = new tabobject(DEPTHS_CATEGORY_VIEW, 
                                 $CFG->wwwroot.'/depths/student/projects/projects_view.php?id='.$id.'&amp;calledFrom='.$calledFrom.'&amp;mode=available&amp;problemuri='.urlencode($problemuri),
                                 get_string('standardview', 'depths'));

    $browserow[] = new tabobject(DEPTHS_CATEGORY_VIEW,
    $CFG->wwwroot.'/depths/student/projects/projects_view.php?id='.$id.'&amp;calledFrom='.$calledFrom.'&amp;mode=myprojects&amp;problemuri='.urlencode($problemuri),
    get_string('myprojectsview', 'depths'));
    
    $browserow[] = new tabobject(DEPTHS_CATEGORY_VIEW, 
                                 $CFG->wwwroot.'/depths/student/projects/projects_view.php?id='.$id.'&amp;calledFrom='.$calledFrom.'&amp;mode=thisproblem&amp;problemuri='.urlencode($problemuri), 
                                 get_string('thisproblemview', 'depths'));
/*
    $browserow[] = new tabobject(DEPTHS_DATE_VIEW, 
                                 $CFG->wwwroot.'/depths/student/projects/projects_view.php?id='.$id.'&amp;mode=date',
                                 get_string('dateview', 'depths'));
*/
    /*
    $browserow[] = new tabobject(DEPTHS_AUTHOR_VIEW, 
                                 $CFG->wwwroot.'/depths/student/projects/projects_view.php?id='.$id.'&amp;mode=author',
  
                                get_string('authorview', 'depths'));
                                */
     /*                            
    $browserow[] = new tabobject(DEPTHS_RATING_VIEW, 
                                 $CFG->wwwroot.'/depths/student/projects/projects_view.php?id='.$id.'&amp;mode=rating',
                                 get_string('ratingsview', 'depths'));
*/
    if ($tab < DEPTHS_STANDARD_VIEW || $tab > DEPTHS_AUTHOR_VIEW) {   // We are on second row
        $inactive = array('edit');
        $activated = array('edit');

        $browserow[] = new tabobject('edit', '#', get_string('edit'));
    }

/// Put all this info together

    $tabrows = array();
    $tabrows[] = $browserow;     // Always put these at the top
    if ($toolsrow) {
        $tabrows[] = $toolsrow;
    }


?>
  <div class="glossarydisplay">


<?php if ($showcommonelements) { print_tabs($tabrows, $tab, $inactive, $activated); } 
?>

  <div class="entrybox">

<?php
 
    if (!isset($category)) {
        $category = "";
    }

    
    switch ($tab) {
        case DEPTHS_CATEGORY_VIEW:
            depths_print_categories_menu($cm, $glossary, $hook, $category);
         break;
       
        case DEPTHS_AUTHOR_VIEW:
            $search = "";
            depths_print_author_menu($cm, $glossary, "author", $hook, $sortkey, $sortorder, 'print');
        break;
       
        case DEPTHS_DATE_VIEW:
            if (!$sortkey) {
                $sortkey = 'UPDATE';
            }
            if (!$sortorder) {
                $sortorder = 'desc';
            }
            glossary_print_alphabet_menu($cm, $glossary, "date", $hook, $sortkey, $sortorder);
        break;
        case DEPTHS_STANDARD_VIEW:
        default:
            depths_print_alphabet_menu($cm, $glossary, "letter", $hook, $sortkey, $sortorder);
             if ($mode == 'search' and $hook) {
                echo "<h3>$strsearch: $hook</h3>";
            } 
        break;
    } 
    echo '<hr />';
  
?> 
 
