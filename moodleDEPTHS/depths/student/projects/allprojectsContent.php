
<?php

echo '
  <style type="text/css">
<!--
.style1 {color: #000099}
.style2 {
	color: #000099;
	font-weight: bold;
	}
	.style3 {color: #FF0000}
-->
</style>';

$startindex=$page*$entriesbypage;
$endindex=$startindex+$entriesbypage;
if ($endindex>$count){
	$endindex=$count;
}

for ($i=$startindex;$i<$endindex;$i++){
	$projArr=$allProjectsUnsorted[$i];

	$projSolObj=$projArr[0];

	$proj=$projSolObj["project"];
	$solution=$projSolObj["solution"];
	$diagrams=$projSolObj["diagrams"];



	$projectName=$proj["title"];
	$projectUri=$projSolObj["projecturi"];

	$projectFileURL=$proj["fileurl"];
	$dateC=$proj["datetimecreatedE"];
	$projectCreatedDate=modelling_dateformat ($dateC/1000);
	$projectCreatedUserUri=$proj["createdBy"];
	$projectCreatedByName=$proj["creatorname"];
	$projectModifiedUserUri=$proj["modifiedBy"];
	$projectModifiedByName=$proj["modifiedByName"];

	$projectDescription=$solution["description"];
	if ($projectDescription==""){
		$projectDescription=get_string("notextprovided","depths");
	}


	$designRules=$solution["hasDesignRules"];
	if($designRules==""){
		$designRules=get_string("notextprovided","depths");
	}

	$designConstraints=$solution["hasDesignConstraints"];
	if($designConstraints==""){
		$designConstraints=get_string("notextprovided","depths");
	}

	$additional_requirements=$solution["hasAdditionalRequirements"];
	if($additional_requirements==""){
		$additional_requirements=get_string("notextprovided","depths");
	}
		
	$consequences=$solution["hasConsequences"];
	if($consequences==""){
		$consequences=get_string("notextprovided","depths");
	}
		
	$pros=$solution["hasPros"];
	if($pros==""){
		$pros=get_string("notextprovided","depths");
	}

	$cons=$solution["hasCons"];
	if($cons==""){
		$cons=get_string("notextprovided","depths");
	}
		

	echo '<div>';
	echo '<table width="80%" border="1">
  <tr>
    <td colspan="3"><div align="center" class="style2">';
	echo $projectName;
	echo '</div>';
	 
	if ($calledFrom=="selectModel"){
		 
		echo '<div align="center" > <a href="javascript:populate(\''.$projectUri.'\')">Add model</a></div>';
	}
	echo '</td>
  </tr>
  <tr>
     <td colspan="2">';
	echo "<div class='yoxview'>";
	$dgi=0;
	foreach($diagrams as $k=>$diagram){
	
		$cDiagramFileUrl=$diagram["fileUrl"];
		$diagramTitle=$diagram['title'];
		$imageLink=$CFG->imagerepository."/".$cDiagramFileUrl;
		list($width, $height, $type, $attr) = getimagesize($imageLink);
		if($width>100){
		if($dgi==0){
			echo "<a  href='".$imageLink."'><img src='".$imageLink."' title='".$diagramTitle."' width='250'/></a>";
		}else{
			echo "<a href='".$imageLink."' title='".$diagramTitle."' ><img src='".$imageLink."' title='".$diagramTitle."' width='0'/></a>";
				
		}
		$dgi++;
		}
	}
	echo "</div>";
	 
	 
	 
	echo ' </td>
    <td align="left"><span class="style1">Description: </span>';
	echo $projectDescription;
	echo '<br/>
	        <span class="style1">Design rules : </span>';
	echo $designRules;
	echo '<br/>
    <span class="style1">Design constraints : </span>';
	echo $designConstraints;
	echo ' <br/>
    <span class="style1">Additional requirements : </span>';
	echo $additional_requirements;
	echo ' <br/>
    <span class="style1">Consequences : </span>';
	echo $consequences;
	echo ' <br/>
    <span class="style1">Pros : </span>';
	echo $pros;
	echo ' <br/>
    <span class="style1">Cons : </span>';
	echo $cons;
	echo ' <br/><br/>
    
    
    
  <span class="style1">Created by: </span>';
	echo $projectCreatedByName;
	echo '<br/>
     <span class="style1">Modified by: </span>';
	echo $projectModifiedByName;
	echo '<br/>
   <span class="style1">Date : </span>';
	echo $projectCreatedDate;
	echo ' <br/>
     
   
     </td>
  </tr>
  <tr>
    <td width="16%"><div align="center">&lt;&lt;</div></td>
    <td width="16%"><div align="center">&gt;&gt;</div></td>
    <td align="left"><span class="style3"></span>';
	 

	echo '</td>
     
  </tr>
</table>';
	echo '</div>';
	echo '<br/>';
	echo ' ';

}

?>