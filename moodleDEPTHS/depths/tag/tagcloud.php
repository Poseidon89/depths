<?php
require_once(dirname(__FILE__).'/../../config.php');
global $CFG;

require_once($CFG->dirroot.'/depths/lib/depths_tagslib.php');
$tagstype=$_GET["tagstype"];

if($tagstype=='recommended'){
	$json = "({ tags:[
	{tag:'tag1',freq:'11'},
	{tag:'tag2',freq:'15'},
	{tag:'tag3',freq:'19'},
	{tag:'tag4',freq:'21'},
	{tag:'tag5',freq:'24'},
	{tag:'tag6',freq:'27'},
	{tag:'tag7',freq:'36'},
	{tag:'tag8',freq:'34'},
	{tag:'tag9',freq:'31'},
	]})";
}else if($tagstype=='mytags'){
$json=getAllMyTags();

}else if($tagstype=='peerstags'){
$json = getMyPeersTags();

}

  //return JSON with GET for JSONP callback  
  $response = $_GET["callback"].$json;  
  echo $response;  
?>