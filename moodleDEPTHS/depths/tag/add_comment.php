<?php
require_once('../../config.php');
require_once('lib.php');

$ideaVal=optional_param('ideaUri',PARAM_URL);
echo '
  <script type="text/javascript">
	$(document).ready(function(){
	var callbackUrl="'.$CFG->wwwroot.'/depths/tag/process_comment.php";
 	console.log("callback url in add comment:"+callbackUrl);
 	
 	jQuery.validator.addMethod( 
 	  "selectNone", 
 	  function(value, element) { 
 	    if (element.value == "none") 
 	    { 
 	      return false; 
 	    } 
 	    else return true; 
 	  }, 
 	  "Please select a comment type." 
 	); 
 	jQuery.validator.addMethod( 
 	  "noComment", 
 	  function(value, element) { 
 	    if (element.value == "") 
 	    { 
 	   
 	      return false; 
 	    } 
 	    else{ 
 	    
 	    return true;
 	    } 
 	  }, 
 	  "You can\'t submit empty comment." 
 	); 
 	
		$("#commentform").validate({
			debug: false,
			rules: {
			commenttype: {
			selectNone:true
			},
			comment:{
			noComment:true
			}
			},
			messages: {
				
			},
			submitHandler: function(form) {
				// do other stuff for a valid form
				$.post(callbackUrl, $("#commentform").serialize(), function(data) {
					 
				});
				closeModalDialog();
			}
		});
	});
	</script>
 <script>
function closeModalDialog(){
	$(\'#depths_dialog_dummy\').dialog(\'close\');
}
</script>
 ';

echo '<form id="commentform" name="commentform" method="post" action="">
 
<input type="hidden" name="ideaUri" value="'.$ideaVal.'" />

<table width="100%">
<tr>
  		 
  		<td  width="40%">&nbsp;</td>
  		<td  width="30%"><div align="right"><input type="checkbox" name="visibility" id="visibility"/>Mark as Private<br /></div></td>
  	</tr>  
<tr>
 <td colspan="2"><textarea cols="40" rows="5" name="comment" id="comment"></textarea></td>
</tr>
<tr>
 <td colspan="2"><select name="commenttype" id="commenttype">
 <option value="none">Comment type</option>
 <option value="triggering">Triggering</option>
 <option value="exploration">Exploration</option>
 <option value="integration">Integration</option>
 <option value="resolution">Resolution</option>
 </select></td>
</tr>
<tr>
<td>&nbsp;</td>
 <td><div align="center"><input type="submit" name="Submit" id="Submit" value="Submit"/><input type="submit" name="Cancel" id="cancel" value="Cancel" onclick="closeModalDialog();"    /></div></td>
</tr>
</table>
';

?>