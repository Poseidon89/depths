

<?php // $Id: history.php,v 1.1 2008/02/20 10:25:49 cvsadmin Exp $
      // For listing message histories between any two users
      
    require('../../../config.php');
 	 require_once('../../lib/depths_settingslib.php');
	
	 $conceptURI = required_param('conceptUri');
     require_login();
     if (isguest()) {
         redirect($CFG->wwwroot);
    }
// echo '<div id="waitloading" align="center"><br><br>
//  <p><img src="../../depths/lib/images/waitloader.gif" /><br> Please Wait</p>
//</div>
// ';
$bluestar='../../images/StarOff4.bmp';
$whitestar='../../images/StarOff2.bmp"';
 
$json_response=getRecommendedResourcesForConcept($conceptURI);
 //echo '<form action="process_suggestions.php" method="POST">';
 echo '<input type="hidden" name="designPatternURI" value="'.$fullPatternURI.'"';
 
  
   
    
  if($json_response){
  	$returnInfoArray=$json_response[0];
  	$conceptTitle=$returnInfoArray["concepttitle"];
  	$returnedOnlArray=$json_response[1];
  	$returnedContArray=$json_response[2];
  	$onlineresources=$returnedOnlArray["onlineresources"];
  	$contentresources=$returnedContArray["contentresources"];
  echo '<table align="center" cellpadding="10" width="800" border="1">';
  	if(!empty($onlineresources)){
  		  print_box_start();
  		echo '<tr  bgcolor="#E6EDFF">
   		    	<td width="60%">
  		    		<table  width="100%" >
  		    			<tr>
  		    				<td width="50%"><div align="center">Suggested web pages</div></td>
  		    				<td width="20%"><div align="center">Relevance</div></td>
  		    				<td width="30%"><div align="center">Was this useful?</div></td>
  		  				</tr>
  		     		</table>
  		     		</td>
  		     </tr>';
  		foreach($onlineresources as $key=>$webpage){
  			$pageUrl=$webpage["href"];
  			$pageTitle=$webpage["title"];
  			if($pageTitle==""){
  				$pageTitle=$pageUrl;
  			}
  			$pageOverallRelevance=$webpage["relevance"];
  			$pageUri=$webpage["uri"];
  			$relevance=round($pageOverallRelevance,2)*100;
  			if($relevance<0){
  				$relevance=0;
  			}
  			
  			echo '<tr>
  			   		    	<td width="60%">
  			  		    		<table  width="100%" >
  			  		    			<tr>
  			  		    				 <td width="50%"><div align="left"><a title="View suggested online resource"
     href="'.$pageUrl.'" target="_blank">'.$pageTitle.'</a></div> </td>
  			  		    				<td width="20%"><div align="center">'.$relevance.'%</div></td>
  			  		    				<td width="30%"><div id="'.$pageUri.'" align="center"><a id="yes"  href="#" value="1" onClick="setRating(\''.$pageUri.'\',\'1\',\''.$conceptURI.'\',\''.$CFG->wwwroot.'\'); return false;">Yes</a>
<a id="no"  href="#" value="-1" onClick="setRating(\''.$pageUri.'\',\'-1\',\''.$conceptURI.'\',\''.$CFG->wwwroot.'\'); return false;">No</a></div></td>
  			  		  				</tr>
  			  		     		</table>
  			  		     </tr>';
  			
  		}
  		
  		
  		
  	
  		  print_box_end();
  	}
 	echo '<br/>';

  	if(!empty($contentresources)){
  		
  		 print_box_start();
  		
  	 
  		echo '<tr  bgcolor="#E6EDFF">
  		   		    	<td width="60%">
  		  		    		<table  width="100%" >
  		  		    			<tr>
  		  		    				<td width="50%"><div align="center">Suggested content</div></td>
  		  		    				<td width="20%"><div align="center">Relevance</div></td>
  		  		    				<td width="30%"><div align="center">Was this useful?</div></td>
  		  		  				</tr>
  		  		     		</table>
  		  		     		</td>
  		  		     </tr>';
  			 
  			foreach($contentresources as $ckey=>$content){
  				
  				$contentType=$content["resourcetype"];
  				$contentSentBy=$content["sentby"];
  				$contentUri=$content["uri"];
  				$contentInResponseTo=$content["inresponseto"];
  				$contentContent=$content["content"];
  				$contentRelevance=$content["relevance"];
  				 
  				
  				$cRelevance=round($contentRelevance,2)*100;
  				
  				if($contentType=="brainstorming"){
  			 		$contentObject=get_record("modelling_ideas","ideauri",urldecode($contentUri));
  			 		$taskObject=get_record("modelling_tasks","id",$contentObject->instance);
  			 		$cm=get_record("course_modules","id",$taskObject->instance);
  					$contentLink=$CFG->wwwroot.'/mod/modelling/brainstorm.php?id='.$cm->id.'&t='.$taskObject->id.'&selIdea='.$contentObject->id;
  				 
  				}
  				if($cRelevance<=0){
  					$cRelevance="0%";
  				}else{
  					$cRelevance=$cRelevance."%";
  				}
  				echo '<tr>
  				  			<td width="60%">
  				  			  		 <table  width="100%" >
  				  			  		    	<tr>
  				  			  		    			<td width="50%"><div align="left"><a title="View suggested online resource"
  				     href="'.$contentLink.'" target="_blank">'.$contentContent.'</a></div> </td>
  				  			  		    			<td width="20%"><div align="center">'.$cRelevance.'</div></td>
  				  			  		    			<td width="30%"><div id="'.$contentUri.'" align="center"><a id="yes"  href="#" value="1" onClick="setRating(\''.$contentUri.'\',\'1\',\''.$conceptURI.'\',\''.$CFG->wwwroot.'\'); return false;">Yes</a>
  				<a id="no"  href="#" value="-1" onClick="setRating(\''.$contentUri.'\',\'-1\',\''.$conceptURI.'\',\''.$CFG->wwwroot.'\'); return false;">No</a></div></td>
  				  			  		  		</tr>
  				  			  		 </table>
  				  			</td>
  				     </tr>';
  			}	
  			
  		 	print_box_end();
  			
  		
  	}
 	echo '</table>';
  }
 
 
 
 //  echo "<br>";
// echo '<center><INPUT TYPE=SUBMIT VALUE="Save&Close"></center>';
 //echo '<center><INPUT TYPE=SUBMIT VALUE="Save&Close" onclick="self.close()"></center>';
//echo '</form>';
    print_footer('none');

?>


