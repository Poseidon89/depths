<?php
function getAllProjectsUnsorted(){
	global $CFG;
	global $USER;
	 
	require_once($CFG->dirroot."/depths/rest/curl_client.php");
 
	$url=$CFG->resturl."projects/listallprojects?username=".$USER->username;
	$response=curl_call($url,'GET');
 
	return json_decode($response,true);
}
function getAvailableProjectsUnsorted(){
	global $CFG;
	global $USER;

	require_once($CFG->dirroot."/depths/rest/curl_client.php");

	$url=$CFG->resturl."projects/listavailableprojects?username=".$USER->username;
	$response=curl_call($url,'GET');

	return json_decode($response,true);
}
function getMyProjectsUnsorted(){
	global $CFG;
	global $USER;

	require_once($CFG->dirroot."/depths/rest/curl_client.php");

	$url=$CFG->resturl."projects/listbyusername?username=".$USER->username;
	$response=curl_call($url,'GET');

	return json_decode($response,true);
}
function sendFileUploadedMessage(){
	global $CFG;
	global $USER;

	require_once($CFG->dirroot."/depths/rest/curl_client.php");

	$url=$CFG->resturl."projects/listbyusername?username=".$USER->username;
	$response=curl_call($url,'GET');

	return json_decode($response,true);
}
function getProjectsForThisProblemUnsorted($problemuri){
	global $CFG;
	global $USER;
	
	require_once($CFG->dirroot."/depths/rest/curl_client.php");
	
	$url=$CFG->resturl."projects/listprojectsforproblembyusername?username=".$USER->username."&designproblemuri=".urlencode($problemuri);
	$response=curl_call($url,'GET');
	
	return json_decode($response,true);
}
function setRatingForRecommendedResource($pageuri,$ratingvalue,$concepturi){
	global $CFG;
	global $USER;
	
	require_once($CFG->dirroot."/depths/rest/curl_client.php");
 
	$url=$CFG->resturl."recommendation/processrating?username=".$USER->username."&pageuri=".urlencode($pageuri)."&ratingvalue=".$ratingvalue."&concepturi=".urlencode($concepturi)."&sessionid=".session_id();
	$response=curl_call($url,'GET');
}
function getRepositoryFactory(){
	global $CFG;
	require_once($CFG->dirroot.'/depths/setup/depths_config.php');
 
		 
		return $rep;
}
function connectFacebookAccount(){
	global $CFG;
	global $USER;
	require_once($CFG->dirroot.'/depths/mapper/utility.php');
	print_message("connectFacebookAccount","facebook");
}
function getRecommendedResourcesForConcept($concepturi){
	global $CFG;
	global $USER;
	
	require_once($CFG->dirroot."/depths/rest/curl_client.php");
	require_once($CFG->dirroot.'/depths/mapper/utility.php');
	print_message("getSessionId:".session_id(),"Session");
	$url=$CFG->resturl."recommendation/relevantresources?username=".$USER->username."&concepturi=".urlencode($concepturi)."&sessionid=".session_id();
	$response=curl_call($url,'GET');
 
	$returnjson=json_decode($response,true);
 
	 
	return $returnjson;
}
 
function getRelevantPeersForDesignProblem($courseuri,$problemuri){
	global $CFG;
	global $USER;
	
	require_once($CFG->dirroot."/depths/rest/curl_client.php");
 
	$url=$CFG->resturl."peers/problembased?username=".$USER->username."&courseuri=".urlencode($courseuri)."&problemuri=".urlencode($problemuri);
	$response=curl_call($url,'GET');
	$returnjson=json_decode($response,true);
	print_message("getRelevantPeersForDesignProblem:".$problemuri,"peersRelevance");
	return $returnjson;
}
function getRelatedConceptsForDesignProblem($problemuri){
	global $CFG;
	global $USER;

	require_once($CFG->dirroot."/depths/rest/curl_client.php");
	require_once($CFG->dirroot.'/depths/mapper/utility.php');
	print_message("getRelatedConceptsForDesignProblem:".$problemuri,"relatedConcepts");
	$url=$CFG->resturl."content/relatedconcepts?problemuri=".urlencode($problemuri);
	$response=curl_call($url,'GET');
	$returnjson=json_decode($response,true);
	print_message("getRelatedConceptsForDesignProblem returns:".$returnjson,"relatedConcepts");

	return $returnjson;
}

function getRecommendedConceptsForDesignProblem($problemuri){
	global $CFG;
	global $USER;

	require_once($CFG->dirroot."/depths/rest/curl_client.php");
	require_once($CFG->dirroot.'/depths/mapper/utility.php');
	print_message("getRelatedConceptsForDesignProblem:".$problemuri,"relatedConcepts");
	$url=$CFG->resturl."content/recommendedconcepts?problemuri=".urlencode($problemuri);
	$response=curl_call($url,'GET');
	$returnjson=json_decode($response,true);
	//foreach($returnjson as $x=>$v){
		$concepts=$returnjson[0];
		foreach($concepts as $x1=>$v1){
		print_message("returned:".$x1." val:".$v1);
// 		foreach($v1 as $t1=>$vt1){
// 			print_message("returned t1:".$t1." val:".$vt1['concepturi']);
// 		}
		}
	//}
	print_message("getRecommendedConceptsForDesignProblem returns:".$concepts,"relatedConcepts");

	return $concepts;
}
 
function createProjectFactory(){
	require_once(dirname(__FILE__).'/../../config.php'); 
	global $CFG;
if (!file_exists($CFG->dirroot.'/depths/setup/depths_config.php')) {
 	redirect("setDatabaseFormData.php?message=no_config_file");
 }
 	require_once($CFG->dirroot.'/depths/setup/depths_config.php'); 
 				 $nextUrl="../../setup/setDatabaseFormData.php?message=" .$eMessage;
				 redirect($nextUrl);
	  
}
function getListOfProjectsByUri($projectsToAssess){
	global $CFG;
	require_once($CFG->dirroot."/depths/rest/curl_client.php");
	require_once($CFG->dirroot.'/depths/mapper/utility.php');
	//print_message("$projectsToAssess:".$projectsToAssess,"getprojectsjson");
	$data=json_encode($projectsToAssess);
	//print_message("getListOfProjectsByUri data:".$data,"getprojectsjson");
	$url=$CFG->resturl."projects/listprojectsbyuris";
	$response=curl_call($url,'POST',$data);
	//print_message("getListOfProjectsByUri:".$response,"getprojectsjson");
    return json_decode($response,true);
}
function getProjectByUri($projectUri){
	global $CFG;
	 global $USER;
 	require_once($CFG->dirroot."/depths/rest/curl_client.php");
	require_once($CFG->dirroot.'/depths/mapper/utility.php');
	$url=$CFG->resturl."solution/getproject?projecturi=".urlencode($projectUri);
	$response=curl_call($url,'GET');
    print_message("getProjectByUri:".$response,"getprojectjson"); 
 	return json_decode($response,true);
	
}
function setProjectAsSubmitted($projectUri,$taskUri){
	global $CFG;
	global $USER;
	require_once($CFG->dirroot."/depths/rest/curl_client.php");
	$url=$CFG->resturl."projects/setprojectassubmitted?username=".$USER->username."&projecturi=".urlencode($projectUri)."&submissionuri=".urlencode($taskUri);
	$response=curl_call($url,'GET');
	print_message("setprojectassubmitted:".$response,"setprojectjson");
	return json_decode($response,true);
}
function getProjectsNotSubmitted($designProblemUri){
	global $CFG;
	global $USER;
	require_once($CFG->dirroot."/depths/rest/curl_client.php");
	require_once($CFG->dirroot.'/depths/mapper/utility.php');
	$url=$CFG->resturl."projects/getprojectnotsubmitted?username=".$USER->username."&designproblemuri=".urlencode($designProblemUri);
	$response=curl_call($url,'GET');
	print_message("getProjectNotSubmitted:".$response,"getprojectjson");
	return json_decode($response,true);
}

 
 
 
function getMyCurrentTime(){
	$time = microtime();
$time = explode(' ', $time);
$time = $time[1] + $time[0];
return $time;
}
function getPassTime($begintime,$endtime ){
	$totaltime = ($endtime - $begintime);
	return $totaltime;
}
function print_message_time($message,$fileType='time'){
	echo 't1-';
	 $myFile='C:\_test\_'.$fileType.'.txt';  
	 	echo 't2-';
     $s= "\r\n";
 
     $x=current_time('mysql');
     $s.="[".$x."] ";
    $s.=$message;
    $s.="\r\n";    
   $fh=@fopen($myFile,'a');
    fwrite($fh,$s);
    fclose($fh);
}
function microtime_float()
{
    list($usec, $sec) = explode(" ", microtime());
    return ((float)$usec + (float)$sec);
}
function depths_diagram_popup_window($url, $name='popup', $linkname='click here',
                               $height=600, $width=800, $title='Click here to see in full size.',
                               $options='none', $return=true) {

    global $CFG;
 
    if ($options == 'none') {
        $options = 'menubar=0,location=0,scrollbars,resizable,width='. $width .',height='. $height;
    }
    $fullscreen = 0;
 
      
 $link =  '<a title="'. s(strip_tags($title)) .'" href="'. $url .'" '.
 "onclick=\"this.target='$name'; return depthsopenpopup('$url', '$name', '$options', $fullscreen);\"><img src='".$url."' width='300'/></a>";
     
    if ($return) {
        return $link;
    } else {
        echo $link;
    }
}
function depths_diagram_popup_window_link($url, $name='popup', $linkname='click here',
                               $height=600, $width=800, $title='Click here to see in full size.',
                               $options='none', $return=true) {

    global $CFG;

    if ($options == 'none') {
        $options = 'menubar=0,location=0,scrollbars,resizable,width='. $width .',height='. $height;
    }
    $fullscreen = 0;
 
        
$link="<a href=\"#\" onclick=\"javascript: window.open('".$url."','".$name."','width=".$width.", height=".$height."'); return false;\">".$linkname."</a>";
    if ($return) {
        return $link;
    } else {
        echo $link;
    }
}
 


?>



<script type="text/javascript" src="<?php echo $CFG->httpswwwroot ?>/lib/javascript-static.js"></script>
<script type="text/javascript" src="<?php echo $CFG->httpswwwroot ?>/lib/javascript-mod.php"></script>
<script type="text/javascript" src="<?php echo $CFG->httpswwwroot ?>/lib/overlib/overlib.js"></script>
<script type="text/javascript" src="<?php echo $CFG->httpswwwroot ?>/lib/overlib/overlib_cssstyle.js"></script>
<script type="text/javascript" src="<?php echo $CFG->httpswwwroot ?>/lib/cookies.js"></script>
<script type="text/javascript" src="<?php echo $CFG->httpswwwroot ?>/lib/ufo.js"></script>

<script type="text/javascript" defer="defer">




setTimeout('fix_column_widths()', 20);

function depthsopenpopup(url,name,options,fullscreen) {
  fullurl = url;
 var windowobj = window.open(fullurl,name,options);
 bodyLoaded();
  function bodyLoaded(){  
	    if(windowobj && windowobj.document && windowobj.document.body){  
  			if (fullscreen) {
  				 windowobj.moveTo(30,30);
    			 windowobj.resizeTo(screen.availWidth,screen.availHeight);
  			}
	    }else{  
	        window.setTimeout( function(){ bodyLoaded(); }, 100);  
	    }  

    }
  windowobj.focus();
  return false;
}



function populate(projectId){
 if (!window.opener.closed && window.opener){
   		var target=window.opener.document.getElementById('id_selectedproject');
   		window.opener.focus();
  		target.value=projectId;
 		window.close();
 	}
}

function sendSelectedConcepts(){
	 
	arrayList=new Array();
	k=0;
	for(var i=0;i<document.forms['addconcepts'].elements['checkbox'].length;i++){
		concept=document.forms['addconcepts'].elements['checkbox'][i].value;
		//console.log('found checkbox:'+concept);
		var checked=document.forms['addconcepts'].elements['checkbox'][i].checked;
		 //console.log('checked checkbox:'+checked);
		if(checked == 1){
			console.log('found checkbox:'+concept);
			 concArray=new Array();
			 
			concArray['concept']=concept;
			arrayList[k]=concArray;
			k=k+1;
		}
		 
	}
	//concepts2json(arrayList);
	var json_string=concepts2json(arrayList);
	console.log(json_string);
	 if (!window.opener.closed && window.opener){
	   		var target=window.opener.document.getElementById('id_selectedconcepts');
	   		window.opener.focus();
	  		target.value=json_string;
	 		  window.close();
	 	}
	}
  
  function depthsopenpopupHTML(url,name,options,fullscreen) {
   
  windowobj = window.open(url,name,options);
  if (fullscreen) {
     windowobj.moveTo(30,30);
     windowobj.resizeTo(screen.availWidth,screen.availHeight);
  }
  windowobj.focus();
  return false;
}
  function concepts2json(arr){
	  var parts = [];
	  for(var x in arr){
		  var valArr=arr[x];
	  for(var key in valArr){
		  var value=valArr[key];
		  console.log("key:"+key);
		  console.log("value:"+value);
		  str = '{"' + key + '":"' + value + '"}';
		  parts.push(str);
	  }
	  }
	  var json = parts.join(",");
	  console.log("json:"+json);
	  return '{"concepts":['+json+']}' ;
  }
  function array2json(arr) {
	    var parts = [];
	    var is_list = (Object.prototype.toString.apply(arr) === '[object Array]');

	    for(var key in arr) {
	    	var value = arr[key];
	        if(typeof value == "object") { //Custom handling for arrays
	            if(is_list) parts.push(array2json(value)); /* :RECURSION: */
	            else parts[key] = array2json(value); /* :RECURSION: */
	        } else {
	            var str = "";
	            if(!is_list) str = '"' + key + '":';

	            //Custom handling for multiple data types
	            if(typeof value == "number") str += value; //Numbers
	            else if(value === false) str += 'false'; //The booleans
	            else if(value === true) str += 'true';
	            else str += '"' + value + '"'; //All other things
	            // :TODO: Is there any more datatype we should be in the lookout for? (Functions?)

	            parts.push(str);
	        }
	    }
	    var json = parts.join(",");
	    
	    if(is_list) return '[' + json + ']';//Return numerical JSON
	    return '{' + json + '}';//Return associative JSON
	}
	  
  
</script>
