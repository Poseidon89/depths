<?php
 
function getAllMyTags(){
	global $CFG;
	global $USER;
	 
	require_once($CFG->dirroot."/depths/rest/curl_client.php");
 
	$url=$CFG->resturl."tags/getmytags?username=".$USER->username;
	$response=curl_call($url,'GET');
 	 return $response;
	
}
function getMyAnnotationsForIdea($ideauri){
	global $CFG;
	global $USER;

	require_once($CFG->dirroot."/depths/rest/curl_client.php");
	require_once($CFG->dirroot.'/depths/mapper/utility.php');
	
	print_message("getMyAnnotationsForIdea idea:".$ideauri,"ideas");
	$url=$CFG->resturl."tags/getmyannotationforidea?username=".$USER->username."&ideauri=".urlencode($ideauri);
	print_message("getMyAnnotationsForIdea URI:".$url,"ideas");
	$response=curl_call($url,'GET');
	$return=json_decode($response,true);
	return $return;

}
function getAllDomainConcepts(){
	global $CFG;
	global $USER;
	require_once($CFG->dirroot."/depths/rest/curl_client.php");

	$url=$CFG->resturl."tags/getalldomainconcepts?username=".$USER->username;
	$response=curl_call($url,'GET');
	return $response;

}


function getMyPeersTags(){
	global $CFG;
	global $USER;

	require_once($CFG->dirroot."/depths/rest/curl_client.php");

	$url=$CFG->resturl."tags/getmypeerstags?username=".$USER->username;
	$response=curl_call($url,'GET');
	return $response;

}

function getPeersNotes($ideauri){
	global $CFG;
	global $USER;
	
	require_once($CFG->dirroot."/depths/rest/curl_client.php");
	
	$url=$CFG->resturl."tags/getmypeersnotes?username=".$USER->username."&ideauri=".urlencode($ideauri);
	$response=curl_call($url,'GET');
	return $response;
}
//function storeNewTagsForIdea($ideauri,$tags,$notes,$visibility){
	function storeNewTagsForIdea($ideauri,$tags, $visibility){
	global $CFG;
	global $USER;
	
	require_once($CFG->dirroot."/depths/rest/curl_client.php");
	require_once($CFG->dirroot.'/depths/mapper/utility.php');
	
	$dataArray=array();
	$dataArray['username']=$USER->username;
	$dataArray['ideauri']=$ideauri;
	//$dataArray['notes']=$notes;
	$dataArray['visibility']=$visibility;
	$dataArray['tags']=json_encode($tags);
	$data=json_encode($dataArray);
	$url=$CFG->resturl."tags/addtagsforidea";
	$response=curl_call($url,'POST',$data);
}
function storeNewCommentForIdea($ideauri,$comment,$visibility,$commenttype){
	global $CFG;
	global $USER;

	require_once($CFG->dirroot."/depths/rest/curl_client.php");

	$dataArray=array();
	$dataArray['username']=$USER->username;
	$dataArray['ideauri']=$ideauri;
	$dataArray['comment']=$comment;
	 $dataArray['visibility']=$visibility;
	 $dataArray['commenttype']=$commenttype;
	//$dataArray['tags']=json_encode($tags);
	$data=json_encode($dataArray);
	$url=$CFG->resturl."tags/addcommentforidea";
	print_message("commenttype:".$commenttype,"comment");
	$response=curl_call($url,'POST',$data);
}

?>