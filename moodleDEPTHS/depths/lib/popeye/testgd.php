<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="css/site/site.css" media="screen" />
<link rel="stylesheet" type="text/css" href="css/popeye/jquery.popeye.css" media="screen" />
<link rel="stylesheet" type="text/css" href="css/popeye/jquery.popeye.style.css" media="screen" />

<script type="text/javascript" src="lib/scripts/jquery-1.6.2.min.js"></script>
<!--<script type="text/javascript" src="lib/popeye/jquery.popeye-2.0.4.min.js"></script>
-->
<script type="text/javascript" src="lib/popeye/popeye.js"></script>
<title>Test </title>
</head>

<body>
<?php
function ImageTrueColorToPalette2($image, $dither, $ncolors) {
    $width = imagesx( $image );
    $height = imagesy( $image );
    $colors_handle = ImageCreateTrueColor( $width, $height );
    ImageCopyMerge( $colors_handle, $image, 0, 0, 0, 0, $width, $height, 100 );
    ImageTrueColorToPalette( $image, $dither, $ncolors );
    ImageColorMatch( $colors_handle, $image );
    ImageDestroy($colors_handle);
    return $image;
}
function resize_png($src,$dst,$dstw,$dsth) {
    list($width, $height, $type, $attr) = getimagesize($src);
    $im = imagecreatefrompng($src);
	imagecolortransparent($im, imagecolorallocate($im, 0, 0, 0));
    $tim = imagecreatetruecolor($dstw,$dsth);
    imagecopyresampled($tim,$im,0,0,0,0,$dstw,$dsth,$width,$height);
    $tim = ImageTrueColorToPalette2($tim,false,255);
    imagepng($tim,$dst);
}
$i=0;
if ($handle = opendir('images/')) {
    /* This is the correct way to loop over the directory. */
    while (false !== ($file = readdir($handle))) {
if ($file != "." && $file != ".." && !is_dir('images/'.$file)) {
 $diagrams[$i]=$file;
 $i++;
 }
    }


    closedir($handle);
}

echo '<div class="ppy" id="ppy1">
            <ul class="ppy-imglist">';

 foreach($diagrams as $k=>$v){
		echo "<br/>K:".$k." v:".$v;
		$diagramFileUrl="ClassDiagram_1_user1__2011_07_10_12_28_36.png";
$filename="images/".$diagramFileUrl;
$newwidth=240;
$newheight=173;
if (!is_file("images/tmb/".$diagramFileUrl))
	resize_png($filename,"images/tmb/".  substr($diagramFileUrl,0,strlen($diagramFileUrl)-4).".png",$newwidth,$newheight);
	      echo '<li>
                    <a href="images/'.$diagramFileUrl.'">
                        <img src="images/'.$diagramFileUrl.'" alt=""/>
					</a>
                    <span class="ppy-extcaption">
                        Image description</span> 
				</li>';
	}
echo '</ul>
<div class="ppy-outer">
                <div class="ppy-stage">
                    <div class="ppy-nav">
                        <a class="ppy-prev" title="Previous image">Previous image</a>
                        <a class="ppy-switch-enlarge" title="Enlarge">Enlarge</a>
                        <a class="ppy-switch-compact" title="Close">Close</a>
                        <a class="ppy-next" title="Next image">Next image</a>
                    </div>
                </div>
            </div>
<div class="ppy-caption">
                <div class="ppy-counter">
                    Image <strong class="ppy-current"></strong> of <strong class="ppy-total"></strong> 
                </div>
                <span class="ppy-text"></span>
            </div>			
</div>';

// I na dnu strane treba dodati ovaj js

/*

*/
?> 
   <!-- [example js] begin -->
<script type="text/javascript">
    <!--//<![CDATA[
    
    $(document).ready(function () {
        var options2 = {
            caption:    false,
            navigation: 'permanent',
            direction:  'left'
        }
        var options3 = {
            caption:    'permanent',
            opacity:    1
        }
    
        $('#ppy1').popeye();
    });
    
    //]]>-->
</script>
<!-- [example js] end -->

</body>
</html>