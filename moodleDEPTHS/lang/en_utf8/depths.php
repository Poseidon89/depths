<?php
///depths file
$string['adminstartpage'] = 'Configure your accounts on social networks';

$string['blockdepthsstudenttitle'] = 'DEPTHS student';
$string['blockdepthstitle'] = 'DEPTHS admin';
$string['dbname'] = 'Database name (not the moodle one)';
$string['depthsblock'] = 'DEPTHS admin';
$string['depthsini'] = 'OPOS configuration';
$string['webrepositoryadminstartpage'] = 'Start page for Web repository initialization';
$string['webrepositoryini'] = 'Web Repository initialisation';
$string['webrepositoriesstartpage'] = 'Online repositories administration';
$string['webrepositoriesadmin'] = 'Repositories administration';
$string['webrepositoryname'] = 'Web repository name';
$string['webrepositoryurl'] = 'Web repository URL';
$string['webrepositorymaxlinks'] = 'Max number of links to annotate';
$string['webrepositoryaddnewintro'] = 'Set parameters for online repository to annotate. <p> As this process could take a while, it will be performed as the background activity. <p> In the meantime you can perform other actions.';
$string['webRepositoryWaitForAWhileMessage'] = 'Process of annotating Web repository is in progress.<p> This process could take same time depending on the number of the pages you have requested to annotate and the complexity of pages.<p>As this operation is performed as the background thread in the meantime you can do other staffs, but it will not be available until the process is over.';
$string['depthsstudentblock'] = 'DEPTHS student';
$string['ftphostname'] = 'FTP Server hostname';  
$string['ftppassword'] = 'FTP Server password';   
$string['ftpsetup'] = 'Please, provide a valid data to access FTP Server used to store UML models.';  
$string['ftpusername'] = 'FTP Server username';  
$string['hostname'] = 'Hostname (i.e. localhost)';
$string['sesameurl'] = 'Sesame URL';
$string['javabridge']='http://localhost:8080/JavaBridge/java/Java.inc';
$string['locodatabaseini'] = 'Set parameters for remote LOCO-Cite repository.<p> These data will be used by Sesame to store data.';
$string['locoini'] = 'LOCO-Cite repository initialisation';
$string['locoinitializationpage'] = 'Initialization of LOCO-Cite repository for DEPTHS';
$string['mappOntologies']='Click here to perform initial mapping of necessary ontologies to the Sesame repository.';
$string['mappScales']='Click here to perform initial mapping of necessary scales to the Sesame repository.';
$string['moodle_jar_file'] = 'moodleLOCMapper_v0.304.jar';
$string['no_config_file']='You have not configured database to store Sesame repository yet. Please do it before perform mapping data.';
$string['notextprovided']='- -';
$string['parseAllMoodleDataIntro']= 'Press submit button to parse data from moodle database to LOCO-Cite ontology.<p>This should be done ONLY ONCE before initializing cite.<p>This process could take some time, so please be patient';
$string['password'] = 'Password';
$string['port'] = 'Port (default 3306)';
$string['repositoryname'] = 'Sesame repository name';
$string['projectpreview'] ='Preview projects';
$string['projectpreviewpage'] ='My projects preview page';
$string['set_dbinfo_error']='PLEASE CHECK INFORMATION FOR ACCESSING DATABASE';
$string['setDatabase'] = 'Set database';
$string['setSesame'] = 'Set sesame';
$string['setRepository'] = 'Set repository';   

$string['repositorysetup'] = 'Please, provide a valid path to access UML diagrams'; 
$string['imageurl'] = 'URL of UML diagrams\' repository'; 

$string['setDatabaseIntro'] = 'If you haven\'t configured MySQL database to store LOCO-Cite repository please do it now. <p>Before this step you should create an empty database.';
$string['setSesameRepositoryIntro'] = 'If you haven\'t configured remote Sesame repository please do it now. <p>Before this step you should setup your Sesame repository if it does not exists.';
$string['setFacebookAccountIntro']='Please select \'Set Facebook account\' button in order to connect your facebook account with Moodle. 
<p>If you are not the only user of this PC please check if somebody else has not already logged to his facebook account.</p>
<p>You will be redirected to your facebook account where you will be asked to accept using OPOS application.<p/>
<p>This information will be used by our application to make you reachable by other collegues even if you are not available on Moodle.</p>';
$string['setFacebook'] = 'Set Facebook account';
$string['closeFacebook'] = 'Close';
$string['setFacebookAccountSuccess']='You have successfully configured your Facebook account. Your friends can send you a message through the Moodle now.';

$string['facebookuser']= 'Facebook username';
$string['facebookpass']='Facebook password';

$string['setRepositoryIntro'] = 'Please, configure your repository of UML diagrams.';
$string['setFTP'] = 'Set FTP Server';  
$string['setFtpRepository']='Click here to setup your FTP Server repository';

$string['setKim'] = 'Set KIM Server';  
$string['setKimServer']='Click here to setup your KIM Server';

$string['kimhostname'] = 'KIM Server hostname';  
$string['kimport'] = 'KIM Server port';   

$string['kimsetup'] = 'Please, provide a valid data to access KIM Annotation Server used to annotate content and web resources.';  

$string['startMappOntologies'] = 'Start parsing'; 
$string['startMappScales'] = 'Start parsing';
$string['startparsing'] = 'Start parsing';
$string['submit'] = 'Submit';
$string['username'] = 'Username';
$string['waitForAWhileMessage'] = 'Process of parsing data from Moodle database to LOCO-Cite ontology is in progress.<p> This process could take same time depending on the amount of data in your database.<p>Please be patient.';
$string['wrong_dbname']='Wrong database name. Please check if it exists and try again.';
$string['wrong_hostname_user_or_pass']='Wrong hostname, username or password. Please check it and try again.';

$string['standardview'] = 'Browse available projects';
$string['myprojectsview'] = 'Browse my projects';
$string['thisproblemview'] = 'Browse this problem';
$string['dateview'] = 'Browse by date';
$string['authorview'] = 'Browse by authors';
$string['ratingsview'] = 'Browse my projects';

$string['blocktagstitle'] =  'DEPTHS Tags';
$string['numberoftags'] = 'Number of tags to display';
$string['blocktitle'] = 'DEPTHS tags block title';
$string['thingstaggedwith'] = '$a->count things tagged with \"$a->name\"';
$string['relatedideas'] = 'Recent ideas with this tag';
$string['seeallideas'] = 'See all ideas with this tag';
$string['seealltags'] = 'See all tags';
?>