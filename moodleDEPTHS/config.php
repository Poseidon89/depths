<?php  /// Moodle Configuration File 

unset($CFG);

$CFG = new stdClass();
$CFG->dbtype    = 'mysql';
$CFG->dbhost    = 'localhost';
$CFG->dbname    = 'moodledepths';
$CFG->dbuser    = 'zoran';
$CFG->dbpass    = 'zoran';
$CFG->dbpersist =  false;
$CFG->prefix    = 'mdl_';

$CFG->wwwroot   = 'http://localhost/moodleDEPTHS';
$CFG->dirroot   = '/var/www/moodleDEPTHS';
$CFG->dataroot  = '/home/zoran/moodledata2';
$CFG->admin     = 'admin';

$CFG->directorypermissions = 00777;  // try 02777 on a server in Safe Mode

// $CFG->passwordsaltmain = '_bIGtWTbUktk)=UHW2lCDGP:';




 $CFG->imagerepository='http://localhost/moodleDEPTHS/depths_repository/images';
 $CFG->resturl='http://localhost:8080/depths/rest/';
 $CFG->oposurl='http://147.91.128.71:9090/opos/';
 $CFG->moodlelogsdir='/home/zoran/moodlelogs/';

require_once("$CFG->dirroot/lib/setup.php");

 $CFG->fb_dbhost = "localhost";
 $CFG->fb_dbname = "fbtest";
 $CFG->fb_dbuser = "fbtest";
 $CFG->fb_dbpass = "fbtest";

$app_id = '243094575732563';
$app_secret = 'c4284042920b023e328423fe5fe83a92';
$fb_app_url = 'http://learningdesignpatterns.org/opfb/';
$fbcanvas='http://apps.facebook.com/opos-test/';

// MAKE SURE WHEN YOU EDIT THIS FILE THAT THERE ARE NO SPACES, BLANK LINES,
// RETURNS, OR ANYTHING ELSE AFTER THE TWO CHARACTERS ON THE NEXT LINE.
?>