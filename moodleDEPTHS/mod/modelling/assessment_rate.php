 <?php // $Id: v 2.0 2007/12/01 12:37:00 serafim panov

require_once("../../config.php");
require_once("lib.php");
require_once ($CFG->dirroot.'/course/moodleform_mod.php');
require_once($CFG->dirroot.'/depths/lib/depths_settingslib.php');
require_once($CFG->dirroot.'/depths/setup/depths_config.php');
require_once($CFG->dirroot."/depths/rest/curl_client.php");
require_once($CFG->dirroot.'/depths/mapper/utility.php');
require_once($CFG->dirroot.'/depths/lib/depths_lib.php');

$id = required_param('id');
$t  = required_param('t');
$submissionid=required_param('submissionid');

$criteria  = optional_param('criteria');
$comment  = optional_param('comment');
$overcomm = optional_param('overcomm');
$editmy = optional_param('editmy');

if ($id) {
	if (! $cm = get_record("course_modules", "id", $id)) {
		error("Course Module ID was incorrect");
	}
	if (! $course = get_record("course", "id", $cm->course)) {
		error("Course is misconfigured");
	}
	if (! $project = get_record("modelling", "id", $cm->instance)) {
		error("Course module is incorrect");
	}
} else {
	if (! $project = get_record("modelling", "id", $submissionid)) {
		error("Course module is incorrect");
	}
	if (! $course = get_record("course", "id", $project->course)) {
		error("Course is misconfigured");
	}
	if (! $cm = get_coursemodule_from_instance("modelling", $project->id, $course->id)) {
		error("Course Module ID was incorrect");
	}
}
//$projectdata = get_record ("modelling_projects", "id", $submissionid);
require_login($course->id);
add_to_log($course->id, "modelling", "Assessment rate", "view.php?id=$id", "$cm->instance");
/// Print the page header

$navigation = "<a href=\"../../course/view.php?id=$course->id\">$course->shortname</a> ->";
print_header("$course->shortname: $project->name", "$course->fullname",
                 "$navigation <a href=\"index.php?id=$course->id\">Project</a> -> $project->name", 
                  "", "", true, update_module_button($id, $course->id, $strproject), 
navmenu($course));


$taskdata = get_record ("modelling_tasks", "id", $t);

//--------Add Criteria-------------//

echo '<form  method="post" action="assessment_rate.php?id='.$id.'&t='.$t.'"> ';

echo '<input type="hidden" name="submissionid" value="'.$submissionid.'" />';
if ($criteria) {
	if ($editmy) {
		 print_message("overall comment:".$overcomm." submissionid:".$submissionid." userid:".$USER->id." assessmentid=".$t,"assessing_file");
		set_field ("modelling_as_rating", "overallcomments", $overcomm, "submissionid", $submissionid, "userid", $USER->id, "assessmentid", $t);
		$oldratingsids = get_records_sql ("SELECT * FROM ".$CFG->prefix."modelling_as_crit_rating WHERE  submissionid='".$submissionid."' and userid='".$USER->id."' and assessmentid='".$t."'");
		foreach ($criteria as $criteriakey => $criteriavalue) {
			foreach ($oldratingsids as $oldratingsid) {
				if ($oldratingsid->criteriaid == $criteriakey) {
					set_field ("modelling_as_crit_rating", "rate", $criteriavalue, "id", $oldratingsid->id);
					set_field ("modelling_as_crit_rating", "comment", $comment[$criteriakey], "id", $oldratingsid->id);
				}
			}
		}
	}
	else
	{
		$dataArray=array();
			$dataArray['action']="insert";
		$rating = new object;
		//$rating->selectedproject = $selectedprojecturi;
		$rating->assessmentid = $t;
		$task=get_record("modelling_tasks","id",$rating->assessmentid);
		$rating->assessmenturi=$task->taskuri;
	 
		$rating->userid = $USER->id;
		$useruri=get_condition_value("modelling_urimapping",'uri','instanceid',$USER->id,'domainconcept','User');
		$rating->useruri=$useruri;
		$rating->overallcomments = $overcomm;
		$rating->time = time();
		$rating->submissionid=$submissionid;
		$submissionuri=get_condition_value("modelling_projects",'submittinguri','id', $submissionid);
 		$rating->submissionuri=$submissionuri;
		$ratingid=insert_record ("modelling_as_rating", $rating);
		$rating->id=$ratingid;
		$dataArray['rating']=json_encode($rating);
		$dataArray['tablename']="modelling_as_rating";
		foreach ($criteria as $criteriakey => $criteriavalue) {
			$ordNumb=$ordNumb+1;
			$criteria = new object;
//			$criteria->selectedproject = $selectedprojecturi;
			$criteria->assessmentid = $t;
			$task=get_record("modelling_tasks","id",$criteria->assessmentid);
			$assessmenturi=$task->taskuri;
			$criteria->assessmenturi=$assessmenturi;
			$criteria->userid = $USER->id;
			//	$citeria->useruri=urlencode($useruri);
			$criteria->criteriaid = $criteriakey;
			$criteriauri=get_condition_value("modelling_crit_name",'scaleuri','id',$criteria->criteriaid);
			$criteria->criteriauri=$criteriauri;
			$criteria->rate = $criteriavalue;
			$ratinguri=get_condition_value("modelling_crit_rating",'scaleitemuri','id',$criteria->rate,'criteriaid',$criteria->criteriaid);
			 $criteria->ratinguri= $ratinguri;
			//$criteria->ratinguri=urlencode($ratinguri);
			$criteria->comment = $comment[$criteriakey];
			$criteria->time = time();
			$criteria->submissionid=$submissionid;
				$criteria->submissionuri= $submissionuri;
			$criteriaid=insert_record ("modelling_as_crit_rating", $criteria);
			$criteria->id=$criteriaid;
				
			$criterias[$ordNumb-1]=json_encode($criteria);
	 
		}
	 
		$dataArray['criterias']=json_encode($criterias);
		$url=$CFG->resturl."insert/table";

		$data=json_encode($dataArray);
		$response=curl_call($url,'POST', $data);
 
		$datatochange=json_decode($response, true);
		foreach($datatochange as $key=>$value){
			$operation=$value["operation"];
			if($operation=="urimapping"){
			$urimapping=new object;
			$urimapping->domainconcept=$value['domainconcept'];
			$urimapping->instanceid=$value['instanceid'];
			$urimapping->uri=$value['uri'];
			print_message("***insert_table_to_rdf table:".$urimapping->domainconcept." instance:".$urimapping->instanceid.
					" uri:".$urimapping->uri,"assessment");
 
			insert_record("modelling_urimapping", $urimapping);
 
			}else if($operation=="setfield"){
			 
			foreach($value as $k=>$v){
			 
					if($k!="operation"){
						print_message("set field: modelling_as_rating "." k:".$k." v:".$v." for id=".$rating->id,"fileassessment");
						set_field("modelling_as_rating", $k, $v, "id", $rating->id);
					}
				}
	 
			}else if ($operation=="setotherfield"){
		 
			foreach($value as $k=>$v){
				 
					if($k!="operation"){
						$crit_ratingid=$value["criteriaratingid"];
						print_message("set other field: modelling_as_crit_rating "." k:".$k." v:".$v." for id=".$crit_ratingid,"fileassessment");
						set_field("modelling_as_crit_rating", $k, $v, "id", $crit_ratingid);
					}
				}
 
			}
		}
	}
 	redirect ("assessment.php?id=".$id."&t=".$t);
}

echo '<div style="text-align: center"><h1>'.$taskdata->name.'</h1></div><hr /><br />';

echo '<table >';

echo '<tr>
    <td>';
echo "
<script type=\"text/javascript\">
function getBodyScrollTop()
{
  return self.pageYOffset || (document.documentElement && document.documentElement.scrollTop) || (document.body && document.body.scrollTop);
}
</script>

<style type=\"text/css\">
    #videolaer
    {
        left: 20px;
        top: 200px;
        height: 360px;
        width: 400px;
        position: fixed;
        //position: absolute;
        overflow-y: auto;
        top: expression(
                    getBodyScrollTop() + 200 +\"px\"
                );
    }
</style>
        ";

$file = get_record ("modelling_files", "user_groupid",$USER->id, "taskid", $taskdata->tasktoassess);

$fullpath = $CFG->wwwroot . "/file.php/" . modelling_get_file_path_www ($cm->course) . "/" . modelling_return_file_name ($file->file);
$filename = modelling_return_file_name ($file->file);
$fullpath=depthsConvertUrlForFile($fullpath);
if ($taskdata->fixedlayer == "on") {

	echo '<div id="videolaer"><center>';

	$videotypes_media = Array ( "ASF", "ASX", "WAX", "WM", "WMA", "WMD", "WMP", "WMV", "WMX", "WPL", "WVX", "AVI", "WAV", "MPEG", "MPG" );
	$videotypes_quick = Array ( "MOV", "QT", "3GP" );
	$videotypes_swf   = Array ( "SWF" );
	$videotypes_real  = Array ( "MP4", "RT", "RA", "RM", "RP", "RV", "M4A", "MPGA", "SMI", "SSM", "AMR", "AWB", "3G2", "DIVX" );
	$imagetypes  = Array ( "JPG", "JPEG", "PNG", "GIF");
	$documentstype  = Array ( "TXT", "PPT", "DOC");

	$filetype = explode (".", $fullpath);
	$filetype = strtoupper (end($filetype));

	echo '<small><a href="'.$fullpath.'" target="_blank">'.$filename.'</a></small><br />';

	if (in_array($filetype,$videotypes_quick))
	{
		echo "<object classid=\"clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B\" align=\"left\" height=\"400\" width=\"500\" codebase=\"http://www.apple.com/qtactivex/qtplugin.cab\"> <param name=\"src\"
value=\"".$fullpath."\"> <param name=\"autoplay\" value=\"false\"> <param name=\"controller\" value=\"true\"><param name=\"bgcolor\" value=\"#ffffff\">
<embed 
src=\"".$fullpath."\" align =\"left\" height=\"400\" width=\"500\" autoplay=\"false\" controller=\"true\" bgcolor=\"#ffffff\" pluginspage=\"http://www.apple.com/quicktime/download/\" ></object>";
		 
	}
	else if (in_array($filetype,$videotypes_media))
	{
		echo "<object id=\"movie\" classid=\"CLSID:22d6f312-b0f6-11d0-94ab-0080c74c7e95\" codebase=\"http://activex.microsoft.com/activex/controls/mplayer/en/nsmp2inf.cab#Version=5,1,52,701\"
standby=\"Loading Microsoft Windows Media Player components...\" type=\"application/x-oleobject\" name=\"movie\" width=\"500\" height=\"400\">
<param name=\"FileName\" value=\"".$fullpath."\"><param name=\"AutoStart\" value=\"false\"><param name=\"ShowControls\" value=\"true\"><param name=\"autoSize\" value=\"true\"><param name=\"displaySize\" value=\"true\"><param name='loop' value=\"true\"><param name='ShowStatusBar' value=\"true\">
<embed width=\"500\" height=\"400\" type=\"application/x-mplayer2\" pluginspage=\"http://www.microsoft.com/Windows/MediaPlayer/\" filename=\"".$fullpath."\" autostart=\"0\" showcontrols=\"1\" autosize=\"1\" displaysize=\"1\" loop=\"1\" ShowStatusBar=\"1\" src=\"".$fullpath."\" name=\"movie\"></embed>
</object> ";

	}
	else if (in_array($filetype,$videotypes_swf))
	{
		echo "<object classid=\"clsid:d27cdb6e-ae6d-11cf-96b8-444553540000\" codebase=\"http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,0,0\" width=\"500\" height=\"400\" id=\"flash_intro\" align=\"middle\">
<param name=\"allowScriptAccess\" value=\"sameDomain\" />
<param name=\"movie\" value=\"".$fullpath."\" />
<param name=\"quality\" value=\"high\" />
<param name=\"bgcolor\" value=\"#ffffff\" />
<embed src=\"".$fullpath."\" quality=\"high\" bgcolor=\"#ffffff\" width=\"500\" height=\"400\" name=\"flash_intro\" align=\"middle\" allowScriptAccess=\"sameDomain\" type=\"application/x-shockwave-flash\" pluginspage=\"http://www.macromedia.com/go/getflashplayer\" />
</object>";
	}
	else if (in_array($filetype,$videotypes_real))
	{
		echo "<object id=\"rvocx\" classid=\"clsid:cfcdaa03-8be4-11cf-b84b-0020afbbccfa\" width=\"500\" height=\"400\">
<param name=\"src\" value=\"".$fullpath."\">
<param name=\"controls\" value=\"imagewindow,controlpanel,statusbar\">
<param name=\"console\" value=\"one\">
<param name=\"autostart\" value=\"false\">
<embed src=\"".$fullpath."\" width=\"500\" height=\"400\" controls=\"imagewindow,controlpanel,statusbar\" console=\"one\" autostart=\"false\" type=\"audio/x-pn-realaudio-plugin\">
</object>";
	}
	else if (in_array($filetype,$imagetypes))
	{
		list($width, $height, $type, $attr) = getimagesize("$CFG->dataroot/$course->id/moddata/project/tasks/submit/$project->id/$task->submitid/$groupid/$filename");
		$needimagelink = false;
		if ($width > $SESSION->projectod['widthdisplay'] || $height > 500 ) {
			$needimagelink = true;
			if ($width > $height) {
				$newsize = ' width="'.($SESSION->projectod['widthdisplay'] - 10).'" height="'.round(($SESSION->projectod['widthdisplay']/$width)*$height).'" ';
			}
			else
			{
				$newsize = ' width="'.round((500/$height)*$width).'" height="500" ';
			}
		}
		else
		{
			$newsize = ' width="'.$width.'" height="'.$height.'" ';
		}

		if ($needimagelink) {
			echo '<a href="'.$fullpath.'" target="_blank"><img src="'.$fullpath.'" '.$newsize.' /></a>';
		}
		else
		{
			echo '<img src="'.$fullpath.'" '.$newsize.' />';
		}
	}
	echo '</center></div>';
}
else
{
	$videotypes_media = Array ( "ASF", "ASX", "WAX", "WM", "WMA", "WMD", "WMP", "WMV", "WMX", "WPL", "WVX", "AVI", "WAV", "MPEG", "MPG" );
	$videotypes_quick = Array ( "MOV", "QT", "3GP" );
	$videotypes_swf   = Array ( "SWF" );
	$videotypes_real  = Array ( "MP4", "RT", "RA", "RM", "RP", "RV", "M4A", "MPGA", "SMI", "SSM", "AMR", "AWB", "3G2", "DIVX" );
	$imagetypes  = Array ( "JPG", "JPEG", "PNG", "GIF");
	$documentstype  = Array ( "TXT", "PPT", "DOC");
	$filetype = explode (".", $fullpath);
	$filetype = strtoupper (end($filetype));
	echo '<small><a href="'.$fullpath.'" target="_blank">'.$filename.'</a></small><br />';

	if (in_array($filetype,$videotypes_quick))
	{
		echo "<object classid=\"clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B\" align=\"left\" height=\"400\" width=\"500\" codebase=\"http://www.apple.com/qtactivex/qtplugin.cab\"> <param name=\"src\"
value=\"".$fullpath."\"> <param name=\"autoplay\" value=\"false\"> <param name=\"controller\" value=\"true\"><param name=\"bgcolor\" value=\"#ffffff\">
<embed 
src=\"".$fullpath."\" align =\"left\" height=\"400\" width=\"500\" autoplay=\"false\" controller=\"true\" bgcolor=\"#ffffff\" pluginspage=\"http://www.apple.com/quicktime/download/\" ></object>";
		 
	}
	else if (in_array($filetype,$videotypes_media))
	{
		echo "<object id=\"movie\" classid=\"CLSID:22d6f312-b0f6-11d0-94ab-0080c74c7e95\" codebase=\"http://activex.microsoft.com/activex/controls/mplayer/en/nsmp2inf.cab#Version=5,1,52,701\"
standby=\"Loading Microsoft Windows Media Player components...\" type=\"application/x-oleobject\" name=\"movie\" width=\"500\" height=\"400\">
<param name=\"FileName\" value=\"".$fullpath."\"><param name=\"AutoStart\" value=\"false\"><param name=\"ShowControls\" value=\"true\"><param name=\"autoSize\" value=\"true\"><param name=\"displaySize\" value=\"true\"><param name='loop' value=\"true\"><param name='ShowStatusBar' value=\"true\">
<embed width=\"500\" height=\"400\" type=\"application/x-mplayer2\" pluginspage=\"http://www.microsoft.com/Windows/MediaPlayer/\" filename=\"".$fullpath."\" autostart=\"0\" showcontrols=\"1\" autosize=\"1\" displaysize=\"1\" loop=\"1\" ShowStatusBar=\"1\" src=\"".$fullpath."\" name=\"movie\"></embed>
</object> ";

	}
	else if (in_array($filetype,$videotypes_swf))
	{
		echo "<object classid=\"clsid:d27cdb6e-ae6d-11cf-96b8-444553540000\" codebase=\"http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,0,0\" width=\"500\" height=\"400\" id=\"flash_intro\" align=\"middle\">
<param name=\"allowScriptAccess\" value=\"sameDomain\" />
<param name=\"movie\" value=\"".$fullpath."\" />
<param name=\"quality\" value=\"high\" />
<param name=\"bgcolor\" value=\"#ffffff\" />
<embed src=\"".$fullpath."\" quality=\"high\" bgcolor=\"#ffffff\" width=\"500\" height=\"400\" name=\"flash_intro\" align=\"middle\" allowScriptAccess=\"sameDomain\" type=\"application/x-shockwave-flash\" pluginspage=\"http://www.macromedia.com/go/getflashplayer\" />
</object>";
	}
	else if (in_array($filetype,$videotypes_real))
	{
		echo "<object id=\"rvocx\" classid=\"clsid:cfcdaa03-8be4-11cf-b84b-0020afbbccfa\" width=\"500\" height=\"400\">
<param name=\"src\" value=\"".$fullpath."\">
<param name=\"controls\" value=\"imagewindow,controlpanel,statusbar\">
<param name=\"console\" value=\"one\">
<param name=\"autostart\" value=\"false\">
<embed src=\"".$fullpath."\" width=\"500\" height=\"400\" controls=\"imagewindow,controlpanel,statusbar\" console=\"one\" autostart=\"false\" type=\"audio/x-pn-realaudio-plugin\">
</object>";
	}
	else
	{
		echo '<iframe width="100%" height="100%" src="'.$fullpath.'"></iframe>';
	}
}

echo'</td>
    <td rowspan="2" valign="top" >';

echo '<b>'.get_string("rating", "modelling").'</b><br />';

if ($criterias = get_records("modelling_crit_name", "assessmentid", $t, 'id')) {

	if ($editmy) {
			$oldratings = get_records_sql ("SELECT * FROM ".$CFG->prefix."modelling_as_crit_rating WHERE  submissionid='".$submissionid."' and userid='".$USER->id."' and assessmentid='".$t."'");
		foreach ($oldratings as $oldrating) {
			$oldratings2[$oldrating->criteriaid] = $oldrating;
		}
		echo '<input type="hidden" name="editmy" value="1" />';
	}
	foreach ($criterias as $criteria) {
		echo '<table cellpadding="0" cellspacing="0" width="220px" ><tr><td><br /><b>'.$criteria->name.'</b></td></tr>';
		if ($ratings = get_records("modelling_crit_rating", "criteriaid", $criteria->id, 'id')) {
			foreach ($ratings as $rating) {
				print_message("criteria KT-2:","assessing_file");
				if ($bgcolor == "#ffffcc" || empty($bgcolor)) {
					$bgcolor = "#ffffff";
				}
				else
				{
					$bgcolor = "#ffffcc";
				}
				echo '<tr><td bgcolor="'.$bgcolor.'"><input type="radio" name="criteria['.$criteria->id.']" value="'.$rating->id.'" ';
				if ($editmy && ($rating->id == $oldratings2[$criteria->id]->rate)) {
					echo 'checked';
				}
				echo ' /> <small>'.$rating->name.'</small></td></tr>';
			}
		}
		if ($taskdata->peercommenting == "yes") {
			echo '<tr><td> <small>'.get_string("criterianamecomments", "modelling").'</small><br /><textarea name="comment['.$criteria->id.']" rows="2" cols="40">';
			if ($editmy) {
				echo $oldratings2[$criteria->id]->comment;
			}
			echo '</textarea><br /> </td></tr>';
		}
		else
		{
			echo '<tr><td> </td></tr>';
		}

		echo '</table>';

	}
	 
	if ($taskdata->peercommenting == "yes") {
		echo '<tr> <td> <br /><small><b>'.get_string("overallcomments", "modelling").'</b></small><br /><textarea name="overcomm" rows="5" cols="80">';
		if ($editmy) {
			$oldratings = get_record_sql ("SELECT * FROM ".$CFG->prefix."modelling_as_rating WHERE  submissionid='".$submissionid."' and userid='".$USER->id."' and assessmentid='".$t."'");
			echo $oldratings->overallcomments;
		}
		echo '</textarea> </td></tr>';
	}
 
	echo '<tr><td align="center"><br/><input type="submit" name="submit" value="Save" /></td></tr>';


}

echo '</form>';

echo '</td></tr></table>';

echo '</td>
  </tr>
  <tr>
   
    <td>';
echo '</td><td valign="top" width="220">';


echo '</td>
  </tr>';
echo '</table>';
print_footer($course);
?>