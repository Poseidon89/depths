<?php //$Id: backuplib.php,v 1.0 2007/06/17 03:45:29 SerafimPanov Exp $
    //This php script contains all the stuff to backup/restore
    //project mods

    //
    // Meaning: pk->primary key field of the table
    //          fk->foreign key to link with parent
    //          nt->nested field (recursive data)
    //          CL->course level info
    //          UL->user level info
    //          files->table may have files)
    //
    //-----------------------------------------------------------

    function modelling_check_backup_mods($course,$user_data=false,$backup_unique_code,$instances=null) {

        if (!empty($instances) && is_array($instances) && count($instances)) {
            $info = array();
            foreach ($instances as $id => $instance) {
                $info += modelling_check_backup_mods_instances($instance,$backup_unique_code);
            }
            return $info;
        }
        return $info;
    }
    
    function modelling_check_backup_mods_instances($instance,$backup_unique_code) {
        //First the course data
        $info[$instance->id.'0'][0] = '<b>'.$instance->name.'</b>';
        $info[$instance->id.'0'][1] = '';

        //Now, if requested, the user_data
        if (!empty($instance->userdata)) {
            $info[$instance->id.'1'][0] = get_string("messages","modelling");
            if ($ids = chat_message_ids_by_instance ($instance->id)) { 
                $info[$instance->id.'1'][1] = count($ids);
            } else {
                $info[$instance->id.'1'][1] = 0;
            }
        }
        return $info;
    }

    function modelling_backup_mods($bf,$preferences) {

        global $CFG;

        $status = true;

        //Iterate over project table
        $projects = get_records ("modelling","course",$preferences->backup_course,"id");
        if ($projects) {
            foreach ($projects as $project) {
                if (backup_mod_selected($preferences,'modelling',$project->id)) {
                    $status = modelling_backup_one_mod($bf,$preferences,$project);
                }
            }
        }
 
        return $status;  
    }
    
    function modelling_backup_one_mod($bf,$preferences,$project) {
     

        global $CFG;
    
        if (is_numeric($project)) {
            $project = get_record('modelling','id',$project);
        }
    
        $status = true;

        //Start mod
        fwrite ($bf,start_tag("MOD",3,true));
        //Print project data
        fwrite ($bf,full_tag("ID",4,false,$project->id));
        fwrite ($bf,full_tag("MODTYPE",4,false,"modelling"));
        fwrite ($bf,full_tag("COURSE",4,false,$project->course));
        fwrite ($bf,full_tag("NAME",4,false,$project->name));
        fwrite ($bf,full_tag("DESCRIPTION",4,false,$project->description));
        fwrite ($bf,full_tag("USEPROJECTDATES",4,false,$project->useprojectdates));
        fwrite ($bf,full_tag("TIMEOPEN",4,false,$project->timeopen));
        fwrite ($bf,full_tag("TIMECLOSE",4,false,$project->timeclose));
        fwrite ($bf,full_tag("TOTALPOINTS",4,false,$project->totalpoints));
        fwrite ($bf,full_tag("TIMELATE",4,false,$project->timelate));
        fwrite ($bf,full_tag("TIMEMODIFIED",4,false,$project->timemodified));
        fwrite ($bf,full_tag("TYPE",4,false,$project->type));
        fwrite ($bf,full_tag("FILENAME",4,false,$project->filename));
        fwrite ($bf,full_tag("PROJECTTYPE",4,false,$project->projecttype));
        fwrite ($bf,full_tag("PROJECTTIME",4,false,time()));
        //if we've selected to backup users info, then execute backup_modelling_messages
        modelling_backup_modelling_tasks ($bf,$preferences,$project);
        modelling_backup_modelling_groups ($bf,$preferences,$project);
        modelling_backup_modelling_gr_students ($bf,$preferences,$project);
        modelling_backup_modelling_topics ($bf,$preferences,$project);
        modelling_backup_modelling_top_template ($bf,$preferences,$project);
        modelling_backup_modelling_crit_name ($bf,$preferences,$project);
        modelling_backup_modelling_crit_rating ($bf,$preferences,$project);
        modelling_backup_modelling_crit_template ($bf,$preferences,$project);
        modelling_backup_modelling_files ($bf,$preferences,$project);
        modelling_backup_modelling_schedule ($bf,$preferences,$project);
        modelling_backup_modelling_as_crit_rating ($bf,$preferences,$project);
        modelling_backup_modelling_as_rating ($bf,$preferences,$project);
        //End mod
        $status =fwrite ($bf,end_tag("MOD",3,true));

        return $status;
    }
    
    
    function modelling_backup_modelling_tasks ($bf,$preferences,$project) {

        global $CFG;

        $status = true;
        
        $cm = get_coursemodule_from_instance("modelling", $project->id, $preferences->backup_course);

        $datas = get_records("modelling_tasks", 'instance', $cm->id);
        //If there is levels
        if ($datas) {
            //Write start tag
            $status =fwrite ($bf,start_tag("PROJECT_TASKS",4,true));
            //Iterate over each message
            foreach ($datas as $cha_mes) {
                //Start message
                $status =fwrite ($bf,start_tag("ROWS",5,true));
                //Print message contents
                fwrite ($bf,full_tag("ID",6,false,$cha_mes->id));       
                fwrite ($bf,full_tag("INSTANCE",6,false,$cha_mes->instance));       
                fwrite ($bf,full_tag("TYPE",6,false,$cha_mes->type));       
                fwrite ($bf,full_tag("NAME",6,false,$cha_mes->name));       
                fwrite ($bf,full_tag("DESCRIPTION",6,false,$cha_mes->description));       
                fwrite ($bf,full_tag("STARTDATE",6,false,$cha_mes->startdate));       
                fwrite ($bf,full_tag("ENDDATE",6,false,$cha_mes->enddate));       
                fwrite ($bf,full_tag("LATERENDDATE",6,false,$cha_mes->laterenddate));       
                fwrite ($bf,full_tag("MAXSIZE",6,false,$cha_mes->maxsize));       
                fwrite ($bf,full_tag("MINSIZE",6,false,$cha_mes->minsize));       
                fwrite ($bf,full_tag("ALLOWSTUDENTSTOADDTOPICS",6,false,$cha_mes->allowstudentstoaddtopics));       
                fwrite ($bf,full_tag("UPLOADFILESTYPE",6,false,$cha_mes->uploadfilestype));       
                fwrite ($bf,full_tag("FASTTRACKTOSUBMIT",6,false,$cha_mes->fasttracktosubmit));       
                fwrite ($bf,full_tag("STANDARDFILENAME",6,false,$cha_mes->standardfilename));       
                fwrite ($bf,full_tag("DURATION",6,false,$cha_mes->duration));       
                fwrite ($bf,full_tag("WEIGHTING",6,false,$cha_mes->weighting));       
                fwrite ($bf,full_tag("WEIGHTINGTEACHER",6,false,$cha_mes->weightingteacher));       
                fwrite ($bf,full_tag("WEIGHTINGPEER",6,false,$cha_mes->weightingpeer));       
                fwrite ($bf,full_tag("WEIGHTINGSELF",6,false,$cha_mes->weightingself));       
                fwrite ($bf,full_tag("PEERRATING",6,false,$cha_mes->peerrating));       
                fwrite ($bf,full_tag("PEERVIEWING",6,false,$cha_mes->peerviewing));       
                fwrite ($bf,full_tag("PEERCOMMENTING",6,false,$cha_mes->peercommenting));       
                fwrite ($bf,full_tag("NUMBEROFITEMS",6,false,$cha_mes->numberofitems));       
                fwrite ($bf,full_tag("TASKTOASSESS",6,false,$cha_mes->tasktoassess));       
                fwrite ($bf,full_tag("FIXEDLAYER",6,false,$cha_mes->fixedlayer));       
                fwrite ($bf,full_tag("POSITION",6,false,$cha_mes->position));       
                //End submission
                $status =fwrite ($bf,end_tag("ROWS",5,true));
            }
            //Write end tag
            $status =fwrite ($bf,end_tag("PROJECT_TASKS",4,true));
        }
        return $status;
    }
    
    
    function modelling_backup_modelling_groups ($bf,$preferences,$project) {

        global $CFG;

        $status = true;
        
        $cm = get_coursemodule_from_instance("modelling", $project->id, $preferences->backup_course);

        $datas = get_records("modelling_groups", 'instance', $cm->id);
        //If there is levels
        if ($datas) {
            //Write start tag
            $status =fwrite ($bf,start_tag("PROJECT_GROUPS",4,true));
            //Iterate over each message
            foreach ($datas as $cha_mes) {
                //Start message
                $status =fwrite ($bf,start_tag("ROWS",5,true));
                //Print message contents
                fwrite ($bf,full_tag("ID",6,false,$cha_mes->id));       
                fwrite ($bf,full_tag("INSTANCE",6,false,$cha_mes->instance));       
                fwrite ($bf,full_tag("NAME",6,false,$cha_mes->name));       
                //End submission
                $status =fwrite ($bf,end_tag("ROWS",5,true));
            }
            //Write end tag
            $status =fwrite ($bf,end_tag("PROJECT_GROUPS",4,true));
        }
        return $status;
    }
    
    
    function modelling_backup_modelling_gr_students ($bf,$preferences,$project) {

        global $CFG;

        $status = true;
        
        $cm = get_coursemodule_from_instance("modelling", $project->id, $preferences->backup_course);

        $datas = get_records("modelling_gr_students", 'instance', $cm->id);
        //If there is levels
        if ($datas) {
            //Write start tag
            $status =fwrite ($bf,start_tag("PROJECT_GROUPS_STUDENTS",4,true));
            //Iterate over each message
            foreach ($datas as $cha_mes) {
                //Start message
                $status =fwrite ($bf,start_tag("ROWS",5,true));
                //Print message contents
                fwrite ($bf,full_tag("ID",6,false,$cha_mes->id));       
                fwrite ($bf,full_tag("INSTANCE",6,false,$cha_mes->instance));       
                fwrite ($bf,full_tag("GROUPID",6,false,$cha_mes->groupid));       
                fwrite ($bf,full_tag("USERID",6,false,$cha_mes->userid));       
                //End submission
                $status =fwrite ($bf,end_tag("ROWS",5,true));
            }
            //Write end tag
            $status =fwrite ($bf,end_tag("PROJECT_GROUPS_STUDENTS",4,true));
        }
        return $status;
    }
    
    
    function modelling_backup_modelling_topics ($bf,$preferences,$project) {

        global $CFG;

        $status = true;
        
        $cm = get_coursemodule_from_instance("modelling", $project->id, $preferences->backup_course);

        $datas = get_records("modelling_topics", 'instance', $cm->id);
        //If there is levels
        if ($datas) {
            //Write start tag
            $status =fwrite ($bf,start_tag("PROJECT_TOPICS",4,true));
            //Iterate over each message
            foreach ($datas as $cha_mes) {
                //Start message
                $status =fwrite ($bf,start_tag("ROWS",5,true));
                //Print message contents
                fwrite ($bf,full_tag("ID",6,false,$cha_mes->id));       
                fwrite ($bf,full_tag("INSTANCE",6,false,$cha_mes->instance));       
                fwrite ($bf,full_tag("NAME",6,false,$cha_mes->name));       
                fwrite ($bf,full_tag("DESCRIPTION",6,false,$cha_mes->description));       
                fwrite ($bf,full_tag("USER_GROUPID",6,false,$cha_mes->user_groupid));       
                fwrite ($bf,full_tag("TIME",6,false,$cha_mes->time));       
                //End submission
                $status =fwrite ($bf,end_tag("ROWS",5,true));
            }
            //Write end tag
            $status =fwrite ($bf,end_tag("PROJECT_TOPICS",4,true));
        }
        return $status;
    }
    

    function modelling_backup_modelling_top_template ($bf,$preferences,$project) {

        global $CFG;

        $status = true;

        $datas = get_records("modelling_top_template");
        //If there is levels
        if ($datas) {
            //Write start tag
            $status =fwrite ($bf,start_tag("PROJECT_TOPICS_TEMPLATE",4,true));
            //Iterate over each message
            foreach ($datas as $cha_mes) {
                //Start message
                $status =fwrite ($bf,start_tag("ROWS",5,true));
                //Print message contents
                fwrite ($bf,full_tag("ID",6,false,$cha_mes->id));       
                fwrite ($bf,full_tag("TITLE",6,false,$cha_mes->title));       
                fwrite ($bf,full_tag("TOPICS",6,false,$cha_mes->topics));       
                fwrite ($bf,full_tag("DESCRIPTION",6,false,$cha_mes->description));       
                //End submission
                $status =fwrite ($bf,end_tag("ROWS",5,true));
            }
            //Write end tag
            $status =fwrite ($bf,end_tag("PROJECT_TOPICS_TEMPLATE",4,true));
        }
        return $status;
    }
    

    function modelling_backup_modelling_crit_name ($bf,$preferences,$project) {

        global $CFG;

        $status = true;
        
        $cm = get_coursemodule_from_instance("modelling", $project->id, $preferences->backup_course);

        $datas = get_records("modelling_crit_name", 'instance', $cm->id);
        //If there is levels
        if ($datas) {
            //Write start tag
            $status =fwrite ($bf,start_tag("PROJECT_CRITERIA_NAME",4,true));
            //Iterate over each message
            foreach ($datas as $cha_mes) {
                //Start message
                $status =fwrite ($bf,start_tag("ROWS",5,true));
                //Print message contents
                fwrite ($bf,full_tag("ID",6,false,$cha_mes->id));       
                fwrite ($bf,full_tag("INSTANCE",6,false,$cha_mes->instance));       
                fwrite ($bf,full_tag("ASSESSMENTID",6,false,$cha_mes->assessmentid));       
                fwrite ($bf,full_tag("NAME",6,false,$cha_mes->name));       
                fwrite ($bf,full_tag("TIME",6,false,$cha_mes->time));       
                //End submission
                $status =fwrite ($bf,end_tag("ROWS",5,true));
            }
            //Write end tag
            $status =fwrite ($bf,end_tag("PROJECT_CRITERIA_NAME",4,true));
        }
        return $status;
    }
    

    function modelling_backup_modelling_crit_rating ($bf,$preferences,$project) {

        global $CFG;

        $status = true;

        $datas = get_records("modelling_crit_rating");
        //If there is levels
        if ($datas) {
            //Write start tag
            $status =fwrite ($bf,start_tag("PROJECT_CRITERIA_RATING",4,true));
            //Iterate over each message
            foreach ($datas as $cha_mes) {
                //Start message
                $status =fwrite ($bf,start_tag("ROWS",5,true));
                //Print message contents
                fwrite ($bf,full_tag("ID",6,false,$cha_mes->id));       
                fwrite ($bf,full_tag("CRITERIAID",6,false,$cha_mes->criteriaid));       
                fwrite ($bf,full_tag("NAME",6,false,$cha_mes->name));       
                fwrite ($bf,full_tag("TIME",6,false,$cha_mes->time));       
                //End submission
                $status =fwrite ($bf,end_tag("ROWS",5,true));
            }
            //Write end tag
            $status =fwrite ($bf,end_tag("PROJECT_CRITERIA_RATING",4,true));
        }
        return $status;
    }
    

    function modelling_backup_modelling_crit_template ($bf,$preferences,$project) {

        global $CFG;

        $status = true;

        $datas = get_records("modelling_crit_template");
        //If there is levels
        if ($datas) {
            //Write start tag
            $status =fwrite ($bf,start_tag("PROJECT_CRITERIA_TEMPLATE",4,true));
            //Iterate over each message
            foreach ($datas as $cha_mes) {
                //Start message
                $status =fwrite ($bf,start_tag("ROWS",5,true));
                //Print message contents
                fwrite ($bf,full_tag("ID",6,false,$cha_mes->id));       
                fwrite ($bf,full_tag("TITLE",6,false,$cha_mes->title));       
                fwrite ($bf,full_tag("CRITERIA",6,false,$cha_mes->criteria));       
                fwrite ($bf,full_tag("RATINGS",6,false,$cha_mes->ratings));       
                //End submission
                $status =fwrite ($bf,end_tag("ROWS",5,true));
            }
            //Write end tag
            $status =fwrite ($bf,end_tag("PROJECT_CRITERIA_TEMPLATE",4,true));
        }
        return $status;
    }
    
    

    function modelling_backup_modelling_files ($bf,$preferences,$project) {

        global $CFG;

        $status = true;
        
        $cm = get_coursemodule_from_instance("modelling", $project->id, $preferences->backup_course);

        $datas = get_records("modelling_files", 'instance', $cm->id);
        //If there is levels
        if ($datas) {
            //Write start tag
            $status =fwrite ($bf,start_tag("PROJECT_FILES",4,true));
            //Iterate over each message
            foreach ($datas as $cha_mes) {
                //Start message
                $status =fwrite ($bf,start_tag("ROWS",5,true));
                //Print message contents
                fwrite ($bf,full_tag("ID",6,false,$cha_mes->id));       
                fwrite ($bf,full_tag("INSTANCE",6,false,$cha_mes->instance));       
                fwrite ($bf,full_tag("TASKID",6,false,$cha_mes->taskid));       
                fwrite ($bf,full_tag("USER_GROUPID",6,false,$cha_mes->user_groupid));       
                fwrite ($bf,full_tag("FILE",6,false,$cha_mes->file));       
                fwrite ($bf,full_tag("TIME",6,false,$cha_mes->time));       
                //End submission
                $status =fwrite ($bf,end_tag("ROWS",5,true));
            }
            //Write end tag
            $status =fwrite ($bf,end_tag("PROJECT_FILES",4,true));
        }
        return $status;
    }
    
    
    

    function modelling_backup_modelling_schedule ($bf,$preferences,$project) {

        global $CFG;

        $status = true;
        
        $cm = get_coursemodule_from_instance("modelling", $project->id, $preferences->backup_course);

        $datas = get_records("modelling_schedule", 'instance', $cm->id);
        //If there is levels
        if ($datas) {
            //Write start tag
            $status =fwrite ($bf,start_tag("PROJECT_SCHEDULE",4,true));
            //Iterate over each message
            foreach ($datas as $cha_mes) {
                //Start message
                $status =fwrite ($bf,start_tag("ROWS",5,true));
                //Print message contents
                fwrite ($bf,full_tag("ID",6,false,$cha_mes->id));       
                fwrite ($bf,full_tag("INSTANCE",6,false,$cha_mes->instance));       
                fwrite ($bf,full_tag("DAY",6,false,$cha_mes->day));       
                fwrite ($bf,full_tag("BEGIN",6,false,$cha_mes->begin));       
                fwrite ($bf,full_tag("END",6,false,$cha_mes->end));       
                fwrite ($bf,full_tag("TIMEPERIOD",6,false,$cha_mes->timeperiod));       
                fwrite ($bf,full_tag("STUDENT_GROUPID",6,false,$cha_mes->student_groupid));       
                fwrite ($bf,full_tag("TOPIC",6,false,$cha_mes->topic));       
                fwrite ($bf,full_tag("TIME",6,false,$cha_mes->time));       
                //End submission
                $status =fwrite ($bf,end_tag("ROWS",5,true));
            }
            //Write end tag
            $status =fwrite ($bf,end_tag("PROJECT_SCHEDULE",4,true));
        }
        return $status;
    }
    
    
    

    function modelling_backup_modelling_as_crit_rating ($bf,$preferences,$project) {

        global $CFG;

        $status = true;

        $datas = get_records("modelling_as_crit_rating");
        //If there is levels
        if ($datas) {
            //Write start tag
            $status =fwrite ($bf,start_tag("PROJECT_AS_CRITERIA_RATING",4,true));
            //Iterate over each message
            foreach ($datas as $cha_mes) {
                //Start message
                $status =fwrite ($bf,start_tag("ROWS",5,true));
                //Print message contents
                fwrite ($bf,full_tag("ID",6,false,$cha_mes->id));       
                fwrite ($bf,full_tag("TOPICID",6,false,$cha_mes->topicid));       
                fwrite ($bf,full_tag("ASSESSMENTID",6,false,$cha_mes->assessmentid));       
                fwrite ($bf,full_tag("USERID",6,false,$cha_mes->userid));       
                fwrite ($bf,full_tag("CRITERIAID",6,false,$cha_mes->criteriaid));       
                fwrite ($bf,full_tag("RATE",6,false,$cha_mes->rate));       
                fwrite ($bf,full_tag("COMMENT",6,false,$cha_mes->comment));       
                fwrite ($bf,full_tag("TIME",6,false,$cha_mes->time));       
                //End submission
                $status =fwrite ($bf,end_tag("ROWS",5,true));
            }
            //Write end tag
            $status =fwrite ($bf,end_tag("PROJECT_AS_CRITERIA_RATING",4,true));
        }
        return $status;
    }
    
    

    function modelling_backup_modelling_as_rating ($bf,$preferences,$project) {

        global $CFG;

        $status = true;

        $datas = get_records("modelling_as_rating");
        //If there is levels
        if ($datas) {
            //Write start tag
            $status =fwrite ($bf,start_tag("PROJECT_AS_RATING",4,true));
            //Iterate over each message
            foreach ($datas as $cha_mes) {
                //Start message
                $status =fwrite ($bf,start_tag("ROWS",5,true));
                //Print message contents
                fwrite ($bf,full_tag("ID",6,false,$cha_mes->id));       
                fwrite ($bf,full_tag("TOPICID",6,false,$cha_mes->topicid));       
                fwrite ($bf,full_tag("ASSESSMENTID",6,false,$cha_mes->assessmentid));       
                fwrite ($bf,full_tag("USERID",6,false,$cha_mes->userid));       
                fwrite ($bf,full_tag("OVERALLCOMMENTS",6,false,$cha_mes->overallcomments));       
                fwrite ($bf,full_tag("TIME",6,false,$cha_mes->time));       
                //End submission
                $status =fwrite ($bf,end_tag("ROWS",5,true));
            }
            //Write end tag
            $status =fwrite ($bf,end_tag("PROJECT_AS_RATING",4,true));
        }
        return $status;
    }
    
?>