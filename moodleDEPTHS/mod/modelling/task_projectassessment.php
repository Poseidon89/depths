<?php // $Id: v 2.0 2007/12/01 12:37:00 serafim panov

    require_once("../../config.php");
    require_once("lib.php");
    require_once ($CFG->dirroot.'/course/moodleform_mod.php');

    $id = required_param('id'); 
    $name = optional_param('name'); 
    $description = optional_param('description'); 
    $timeopen = optional_param('timeopen'); 
    $timeclose = optional_param('timeclose'); 
    $laterenddate = optional_param('laterenddate'); 
    //$weighting = optional_param('weighting', 100, PARAM_INT); 
    $weightingteacher = optional_param('weightingteacher', 100, PARAM_INT); 
    $weightingpeer = optional_param('weightingpeer', 0, PARAM_INT); 
    $weightingself = optional_param('weightingself', 0, PARAM_INT); 
    $peerrating = optional_param('peerrating'); 
    $peerviewing = optional_param('peerviewing'); 
    $peercommenting = optional_param('peercommenting'); 
    $numberofitems = optional_param('numberofitems', 2, PARAM_INT); 
    $tasktoassess = optional_param('tasktoassess'); 
    
    
    $update = optional_param('update'); 

    if ($id) {
        if (! $cm = get_record("course_modules", "id", $id)) {
            error("Course Module ID was incorrect");
        }
        if (! $course = get_record("course", "id", $cm->course)) {
            error("Course is misconfigured");
        }
        if (! $project = get_record("modelling", "id", $cm->instance)) {
            error("Course module is incorrect");
        }
    } else {
        if (! $project = get_record("modelling", "id", $a)) {
            error("Course module is incorrect");
        }
        if (! $course = get_record("course", "id", $project->course)) {
            error("Course is misconfigured");
        }
        if (! $cm = get_coursemodule_from_instance("modelling", $project->id, $course->id)) {
            error("Course Module ID was incorrect");
        }
    }

    require_login($course->id);

    add_to_log($course->id, "modelling", "assessment task", "view.php?id=$id", "$cm->instance");
    
/// Print the page header

    $navigation = "<a href=\"../../course/view.php?id=$course->id\">$course->shortname</a> ->";
    
     
    

    print_header("$course->shortname: $project->name", "$course->fullname",
                 "$navigation <a href=\"index.php?id=$course->id\">Project</a> -> $project->name", 
                  "", "", true, update_module_button($id, $course->id, $strproject), 
                  navmenu($course));
                  
    //For teacher, view select button
    
    if (!isteacher($cm->course)) {
        error("Only for teachers");
    }
    
    //---------------------------
    
    if ($name) {
    
        if ($weightingteacher + $weightingpeer + $weightingself != 100) {
            $error = true;
            $weightingteacher = 100;
            $weightingpeer = 0;
            $weightingself = 0;
        }
        else
        {
            $error = false;
        }
    
        $task = new object;
        $task->instance = $id;
        $task->name = $name;
        $task->description = $description;
        if ($timeopen && $timeclose) {
            $task->startdate = mktime (0,0,0,$timeopen['month'],$timeopen['day'],$timeopen['year']);
            $task->enddate = mktime (0,0,0,$timeclose['month'],$timeclose['day'],$timeclose['year']);
            $task->laterenddate = mktime (0,0,0,$laterenddate['month'],$laterenddate['day'],$laterenddate['year']);
        }
        /* GET weighting */
        if ($otherassessmentdata = get_records_sql ("SELECT * FROM ".$CFG->prefix."modelling_tasks WHERE instance='".$id."' and type='projectassessment'") && !$update) {
            $totalweighting = 0;
            foreach ($otherassessmentdata as $otherassessmentdata_) {
                $totalweighting += $otherassessmentdata_->weighting;
            }
            $task->weighting = 100 - $totalweighting;
        }
        //$task->weighting = $weighting;
        $task->weightingteacher = $weightingteacher;
        $task->weightingpeer = $weightingpeer;
        $task->weightingself = $weightingself;
        $task->peerrating = $peerrating;
        $task->peerviewing = $peerviewing;
        $task->peercommenting = $peercommenting;
        $task->numberofitems = $numberofitems;
        $task->tasktoassess = $tasktoassess;
        
        $task->type = "projectassessment";
        
        if ($update) {
            $task->id = $update;
            
            if (update_record("modelling_tasks", $task)) {
                if (!$error) {
                    redirect ("view.php?id=".$id, "Project Assessment Task updated");
                }
            }
        }
        else
        {
            $positiontasks = get_records ("modelling_tasks", "instance", $id, "position desc");
            
            if ($positiontasks) {
                $positiontasks = current($positiontasks);
                $task->position = $positiontasks->position + 100;
            }
            else
            {
                $task->position = 100;
            }
            
            if ($update = insert_record("modelling_tasks", $task)) {
                if (!$error) {
                    redirect ("view.php?id=".$id, "Assessment Task added");
                }
            }
        }
        
        if ($error) {
            error ("Warning:  Weighting must total 100.  Please change the weightings so the Teacher, Peer, and Self Assessment total is equal to 100.", "task_projectassessment.php?id=".$id."&update=".$update);
        }
    }
    
    //---------------------------
    
    class mod_modelling_projectassessment_form extends moodleform {

        function definition() {

            global $CFG, $cm, $project, $USER, $update, $id;

            if ($update) {
                $data = get_record ("modelling_tasks", "id", $update);
            }

            $mform    =& $this->_form;
            
            $mform->addElement('header', 'general', get_string('assessment', 'modelling'));
              
            $mform->addElement('text', 'name', get_string('assessmenttaskname', 'modelling'), array('size'=>'64'));
            $mform->setType('name', PARAM_TEXT);
            $mform->addRule('name', null, 'required', null, 'client');

            $mform->addElement('htmleditor', 'description', get_string('assessmenttaskdescription', 'modelling'));
            $mform->setType('description', PARAM_RAW);
            $mform->setHelpButton('description', array('writing', 'questions', 'richtext'), false, 'editorhelpbutton');
            $mform->addRule('description', get_string('required'), 'required', null, 'client');
            
            if ($project->useprojectdates == "true") {
                $mform->addElement('date_selector', 'timeopen', get_string('brainstormtaskstartdate', 'modelling'));
                $mform->addElement('date_selector', 'timeclose', get_string('brainstormtaskenddate', 'modelling'));
                $mform->setDefault('timeclose', mktime(0,0,0,date("m") + 1,date("d"),date("Y"))); 
                $mform->addElement('date_selector', 'laterenddate', get_string('brainstormtasklateenddate', 'modelling'));
                $mform->setDefault('laterenddate', mktime(0,0,0,date("m") + 1,date("d"),date("Y"))); 
            }
            
            
            
            $standardfilename = array();
            $standardfilename[] = &MoodleQuickForm::createElement('text', 'weightingteacher', get_string('assessmentweightingofteacherassessments', 'modelling'), array('size'=>'4'));
            $standardfilename[] = &MoodleQuickForm::createElement('static', 'subweightingpeer', '', '% ');
            $mform->addGroup($standardfilename, 'weightingteacher1', get_string('assessmentweightingofteacherassessments', 'modelling'), ' ', false);
            $mform->setDefault('weightingteacher', 100); 
            
            //---
            
            $standardfilename = array();
            $standardfilename[] = &MoodleQuickForm::createElement('text', 'weightingpeer', get_string('assessmentweightingofpeerassessments', 'modelling'), array('size'=>'4'));
            $standardfilename[] = &MoodleQuickForm::createElement('static', 'subweightingpeer', '', '% ');
            $mform->addGroup($standardfilename, 'weightingpeer1', get_string('assessmentweightingofpeerassessments', 'modelling'), ' ', false);
            $mform->setDefault('weightingpeer', 0); 
            
            //---
            
            $standardfilename = array();
            $standardfilename[] = &MoodleQuickForm::createElement('text', 'weightingself', get_string('assessmentweightingofselfassessments', 'modelling'), array('size'=>'4'));
            $standardfilename[] = &MoodleQuickForm::createElement('static', 'subweightingpeer', '', '% ');
            $mform->addGroup($standardfilename, 'weightingself1', get_string('assessmentweightingofselfassessments', 'modelling'), ' ', false);
            $mform->setDefault('weightingself', 0); 
            
            //---
            
            $mform->addElement('select', 'peerrating', get_string('assessmentpeerrating', 'modelling'), Array("yes"=>"Yes", "no"=>"No"));
            $mform->addElement('select', 'peerviewing', get_string('assessmentpeerviewing', 'modelling'), Array("yes"=>"Yes", "no"=>"No"));
            $mform->addElement('select', 'peercommenting', get_string('assessmentpeercommenting', 'modelling'), Array("yes"=>"Yes", "no"=>"No"));
            
            $options=array();
            for ($i=2; $i <=10; $i++) {
                $options[$i] = $i;
            }
            $mform->addElement('select', 'numberofitems', get_string('assessmentnumberofitems', 'modelling'), $options);
            $mform->setHelpButton('numberofitems', array('ratingscale', '', 'modelling', true, false, '', true));
            
            $gettasks = get_records_sql ("SELECT * FROM ".$CFG->prefix."modelling_tasks WHERE instance='".$id."' and type='submitproject'");
            foreach ($gettasks as $gettasks_) {
                $tasktoassess[$gettasks_->id] = $gettasks_->name;
            }
            
            $mform->addElement('select', 'tasktoassess', get_string('assessmenttasktoassess', 'modelling'), $tasktoassess);
            
           // $mform->addElement('select', 'fixedlayer', get_string('assessmentfixedlayer', 'modelling'), Array("off"=>"Off", "on"=>"On"));
            
            if ($update) {
                $mform->setDefault('name', $data->name);
                $mform->setDefault('description', $data->description);
                $mform->setDefault('timeopen', $data->startdate);
                $mform->setDefault('timeclose', $data->enddate);
                $mform->setDefault('laterenddate', $data->laterenddate);
                //$mform->setDefault('weighting', $data->weighting);
                $mform->setDefault('weightingteacher', $data->weightingteacher);
                $mform->setDefault('weightingpeer', $data->weightingpeer);
                $mform->setDefault('weightingself', $data->weightingself);
                $mform->setDefault('peerrating', $data->peerrating);
                $mform->setDefault('peerviewing', $data->peerviewing);
                $mform->setDefault('peercommenting', $data->peercommenting);
                $mform->setDefault('numberofitems', $data->numberofitems);
                $mform->setDefault('tasktoassess', $data->tasktoassess);
                //$mform->setDefault('fixedlayer', $data->fixedlayer);
            }
            
            $this->add_action_buttons(false); 
        }
    }
    
    if (!$update) {
        $mform = new mod_modelling_projectassessment_form('task_projectassessment.php?id=' . $id);
    }
    else
    {
        $mform = new mod_modelling_projectassessment_form('task_projectassessment.php?id=' . $id . '&update=' . $update);
    }
    $mform->display();

    print_footer($course);

?>