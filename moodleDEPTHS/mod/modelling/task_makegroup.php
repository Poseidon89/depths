<?php // $Id: v 2.0 2007/12/01 12:37:00 serafim panov

    require_once("../../config.php");
    require_once("lib.php");
    require_once ($CFG->dirroot.'/course/moodleform_mod.php');

    $id = required_param('id'); 
    $name = optional_param('name'); 
    $description = optional_param('description'); 
    $timeopen = optional_param('timeopen'); 
    $timeclose = optional_param('timeclose'); 
    $mingroupsize = optional_param('mingroupsize', 2, PARAM_INT); 
    $maxgroupsize = optional_param('maxgroupsize', 4, PARAM_INT); 
    
    $update = optional_param('update'); 

    if ($id) {
        if (! $cm = get_record("course_modules", "id", $id)) {
            error("Course Module ID was incorrect");
        }
        if (! $course = get_record("course", "id", $cm->course)) {
            error("Course is misconfigured");
        }
        if (! $project = get_record("modelling", "id", $cm->instance)) {
            error("Course module is incorrect");
        }
    } else {
        if (! $project = get_record("modelling", "id", $a)) {
            error("Course module is incorrect");
        }
        if (! $course = get_record("course", "id", $project->course)) {
            error("Course is misconfigured");
        }
        if (! $cm = get_coursemodule_from_instance("modelling", $project->id, $course->id)) {
            error("Course Module ID was incorrect");
        }
    }

    require_login($course->id);

    add_to_log($course->id, "modelling", "make group", "view.php?id=$id", "$cm->instance");
    
/// Print the page header

    $navigation = "<a href=\"../../course/view.php?id=$course->id\">$course->shortname</a> ->";
    
    
    

    print_header("$course->shortname: $project->name", "$course->fullname",
                 "$navigation <a href=\"index.php?id=$course->id\">Project</a> -> $project->name", 
                  "", "", true, update_module_button($id, $course->id, $strproject), 
                  navmenu($course));
                  
    //For teacher, view select button
    
    if (!isteacher($cm->course)) {
        error("Only for teachers");
    }
    
    //---------------------------
    
    if ($name) {
        $task = new object;
        $task->instance = $id;
        $task->name = $name;
        $task->description = $description;
        if ($timeopen && $timeclose) {
            $task->startdate = mktime (0,0,0,$timeopen['month'],$timeopen['day'],$timeopen['year']);
            $task->enddate = mktime (0,0,0,$timeclose['month'],$timeclose['day'],$timeclose['year']);
        }
        $task->minsize = $mingroupsize;
        $task->maxsize = $maxgroupsize;
        $task->type = "makegroup";
        
        if ($update) {
            $task->id = $update;
            if (update_record("modelling_tasks", $task)) {
                redirect ("view.php?id=".$id, "Group Task updated");
            }
        }
        else
        {
            $positiontasks = get_records ("modelling_tasks", "instance", $id, "position desc");
            
            if ($positiontasks) {
                $positiontasks = current($positiontasks);
                $task->position = $positiontasks->position + 100;
            }
            else
            {
                $task->position = 100;
            }
            
            if (insert_record("modelling_tasks", $task)) {
                redirect ("view.php?id=".$id, "Group Task added");
            }
        }
    }
    
    //---------------------------
    
    class mod_modelling_makegroup_form extends moodleform {

        function definition() {

            global $CFG, $cm, $project, $USER, $update;

            if ($update) {
                $data = get_record ("modelling_tasks", "id", $update);
            }
            
            $mform    =& $this->_form;
            
            $mform->addElement('header', 'general', get_string('grouptask', 'modelling'));
              
            $mform->addElement('text', 'name', get_string('grouptaskname', 'modelling'), array('size'=>'64'));
            $mform->setType('name', PARAM_TEXT);
            $mform->addRule('name', null, 'required', null, 'client');

            $mform->addElement('htmleditor', 'description', get_string('grouptaskdescription', 'modelling'));
            $mform->setType('description', PARAM_RAW);
            $mform->setHelpButton('description', array('writing', 'questions', 'richtext'), false, 'editorhelpbutton');
            $mform->addRule('description', get_string('required'), 'required', null, 'client');
            
            if ($project->useprojectdates == "true") {
                $mform->addElement('date_selector', 'timeopen', get_string('grouptaskstartdate', 'modelling'));
                $mform->addElement('date_selector', 'timeclose', get_string('grouptaskenddate', 'modelling'));
                $mform->setDefault('timeclose', mktime(0,0,0,date("m") + 1,date("d"),date("Y"))); 
            }
            
            $mform->addElement('text', 'mingroupsize', get_string('minimumgroupsize', 'modelling'), array('size'=>'4'));
            $mform->setDefault('mingroupsize', 2); 
            $mform->addElement('text', 'maxgroupsize', get_string('maximumgroupsize', 'modelling'), array('size'=>'4'));
            $mform->setDefault('maxgroupsize', 4); 
            
            if ($update) {
                $mform->setDefault('name', $data->name);
                $mform->setDefault('description', $data->description);
                $mform->setDefault('timeopen', $data->startdate);
                $mform->setDefault('timeclose', $data->enddate);
                $mform->setDefault('mingroupsize', $data->minsize);
                $mform->setDefault('maxgroupsize', $data->maxsize);
            }
            
            $this->add_action_buttons(false); 
        }
    }
    
    if (!$update) {
        $mform = new mod_modelling_makegroup_form('task_makegroup.php?id=' . $id);
    }
    else
    {
        $mform = new mod_modelling_makegroup_form('task_makegroup.php?id=' . $id . '&update=' . $update);
    }
    $mform->display();

    print_footer($course);

?>