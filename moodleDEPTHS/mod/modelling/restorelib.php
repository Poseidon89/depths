<?php //$Id: backuplib.php,v 1.0 2007/06/17 03:45:29 SerafimPanov Exp $
    //This php script contains all the stuff to backup/restore
    //project mods

    //
    // Meaning: pk->primary key field of the table
    //          fk->foreign key to link with parent
    //          nt->nested field (recursive data)
    //          CL->course level info
    //          UL->user level info
    //          files->table may have files)
    //
    //-----------------------------------------------------------
    //This function executes all the restore procedure about this mod
    function modelling_restore_mods($mod,$restore) {
 
        global $CFG, $oldidarray;

        $status = true;

        //Get record from backup_ids
        $data = backup_getid($restore->backup_unique_code,$mod->modtype,$mod->id);

        if ($data) {

            //Now get completed xmlized object   
            $info = $data->info;

            //traverse_xmlize($info);                                                                     //Debug
            //print_object ($GLOBALS['traverse_array']);                                                  //Debug
            //$GLOBALS['traverse_array']="";                                                              //Debug
            // if necessary, write to restorelog and adjust date/time fields
            if ($restore->course_startdateoffset) {
                restore_log_date_changes('modelling', $restore, $info['MOD']['#'], array('PROJECTTIME'));
            }
            //Now, build the project record structure
            $project->course = $restore->course_id;
            $project->name = backup_todb($info['MOD']['#']['NAME']['0']['#']);
            $project->description = backup_todb($info['MOD']['#']['DESCRIPTION']['0']['#']);
            $project->useprojectdates = backup_todb($info['MOD']['#']['USEPROJECTDATES']['0']['#']);
            $project->timeopen = backup_todb($info['MOD']['#']['TIMEOPEN']['0']['#']);
            $project->timeclose = backup_todb($info['MOD']['#']['TIMECLOSE']['0']['#']);
            $project->totalpoints = backup_todb($info['MOD']['#']['TOTALPOINTS']['0']['#']);
            $project->timelate = backup_todb($info['MOD']['#']['TIMELATE']['0']['#']);
            $project->timemodified = backup_todb($info['MOD']['#']['TIMEMODIFIED']['0']['#']);
            $project->type = backup_todb($info['MOD']['#']['TYPE']['0']['#']);
            $project->filename = backup_todb($info['MOD']['#']['FILENAME']['0']['#']);
            $project->projecttype = backup_todb($info['MOD']['#']['PROJECTTYPE']['0']['#']);
            $project->time = backup_todb($info['MOD']['#']['TIME']['0']['#']);

            //The structure is equal to the db, so insert the project
            $newid = insert_record ("modelling",$project);

            

            if ($newid) {
                //We have the newid, update backup_ids
                backup_putid($restore->backup_unique_code,$mod->modtype, $mod->id, $newid);
                //Now check if want to restore user data and do it.
                if (restore_userdata_selected($restore,'modelling',$mod->id)) {
                
                    $moduleid = get_record ("modules", "name", "modelling");
                    
                    $cm = get_record ("course_modules", "course", $restore->course_id, "module", $moduleid->id, "instance", "0");
                
                    //Restore modelling_messages
                    modelling_restore_modelling_tasks ($mod->id, $cm->id, $info, $restore);
                    modelling_restore_modelling_groups ($mod->id, $cm->id, $info, $restore);
                    modelling_restore_modelling_gr_students ($mod->id, $cm->id, $info, $restore);
                    modelling_restore_modelling_topics ($mod->id, $cm->id, $info, $restore, $project);
                    modelling_restore_modelling_top_template ($mod->id, $cm->id, $info, $restore);
                    modelling_restore_modelling_crit_name ($mod->id, $cm->id, $info, $restore);
                    modelling_restore_modelling_crit_rating ($mod->id, $cm->id, $info, $restore);
                    modelling_restore_modelling_crit_template ($mod->id, $cm->id, $info, $restore);
                    modelling_restore_modelling_files ($mod->id, $cm->id, $info, $restore, $project);
                    modelling_restore_modelling_schedule ($mod->id, $cm->id, $info, $restore, $project);
                    modelling_restore_modelling_as_crit_rating ($mod->id, $cm->id, $info, $restore);
                    modelling_restore_modelling_as_rating ($mod->id, $cm->id, $info, $restore);
                }
            } else {
                $status = false;
            }
        } else {
            $status = false;
        }

        return $status;
    }


    
    function modelling_restore_modelling_tasks ($old_modelling_id, $new_modelling_id,$info,$restore) {

        global $CFG, $oldidarray;

        $status = true;
        
        //Get the messages array 
        $messages = $info['MOD']['#']['PROJECT_TASKS']['0']['#']['ROWS'];

        //Iterate over messages
        for($i = 0; $i < sizeof($messages); $i++) {
            $mes_info = $messages[$i];
            //Now, build the modelling_MESSAGES record structure
            
            $message->instance = $new_modelling_id;
            $message->name = backup_todb($mes_info['#']['NAME']['0']['#']);
            $message->type = backup_todb($mes_info['#']['TYPE']['0']['#']);
            $message->description = backup_todb($mes_info['#']['DESCRIPTION']['0']['#']);
            $message->startdate = backup_todb($mes_info['#']['STARTDATE']['0']['#']);
            $message->enddate = backup_todb($mes_info['#']['ENDDATE']['0']['#']);
            $message->laterenddate = backup_todb($mes_info['#']['LATERENDDATE']['0']['#']);
            $message->maxsize = backup_todb($mes_info['#']['MAXSIZE']['0']['#']);
            $message->minsize = backup_todb($mes_info['#']['MINSIZE']['0']['#']);
            $message->allowstudentstoaddtopics = backup_todb($mes_info['#']['ALLOWSTUDENTSTOADDTOPICS']['0']['#']);
            $message->uploadfilestype = backup_todb($mes_info['#']['UPLOADFILESTYPE']['0']['#']);
            $message->fasttracktosubmit = backup_todb($mes_info['#']['FASTTRACKTOSUBMIT']['0']['#']);
            $message->standardfilename = backup_todb($mes_info['#']['STANDARDFILENAME']['0']['#']);
            $message->duration = backup_todb($mes_info['#']['DURATION']['0']['#']);
            $message->weighting = backup_todb($mes_info['#']['WEIGHTING']['0']['#']);
            $message->weightingteacher = backup_todb($mes_info['#']['WEIGHTINGTEACHER']['0']['#']);
            $message->weightingpeer = backup_todb($mes_info['#']['WEIGHTINGPEER']['0']['#']);
            $message->weightingself = backup_todb($mes_info['#']['WEIGHTINGSELF']['0']['#']);
            $message->peerrating = backup_todb($mes_info['#']['PEERRATING']['0']['#']);
            $message->peerviewing = backup_todb($mes_info['#']['PEERVIEWING']['0']['#']);
            $message->peercommenting = backup_todb($mes_info['#']['PEERCOMMENTING']['0']['#']);
            $message->numberofitems = backup_todb($mes_info['#']['NUMBEROFITEMS']['0']['#']);
            $message->tasktoassess = backup_todb($mes_info['#']['TASKTOASSESS']['0']['#']);
            $message->fixedlayer = backup_todb($mes_info['#']['FIXEDLAYER']['0']['#']);
            $message->position = backup_todb($mes_info['#']['POSITION']['0']['#']);
            
            if ($message->tasktoassess > 0) {
                $message->tasktoassess = $oldidarray[$old_modelling_id]['modelling_tasks'][$mes_info['#']['TASKTOASSESS']['0']['#']];
            }
            
            $newidm = insert_record ("modelling_tasks",$message);
            $oldidarray[$old_modelling_id]['modelling_tasks'][backup_todb($mes_info['#']['ID']['0']['#'])] = $newidm;

            //Do some output
            if (($i+1) % 50 == 0) {
                if (!defined('RESTORE_SILENTLY')) {
                    echo ".";
                    if (($i+1) % 1000 == 0) {
                        echo "<br />";
                    }
                }
                backup_flush(300);
            }
        }
        
        return $status;
    }
    
    
    
    
    function modelling_restore_modelling_groups ($old_modelling_id, $new_modelling_id,$info,$restore) {

        global $CFG, $oldidarray;

        $status = true;

        //Get the messages array 
        $messages = $info['MOD']['#']['PROJECT_GROUPS']['0']['#']['ROWS'];

        //Iterate over messages
        for($i = 0; $i < sizeof($messages); $i++) {
            $mes_info = $messages[$i];
            //Now, build the modelling_MESSAGES record structure
            
            $message->instance = $new_modelling_id;
            $message->name = backup_todb($mes_info['#']['NAME']['0']['#']);
            
            $newidm = insert_record ("modelling_groups",$message);
            $oldidarray[$old_modelling_id]['modelling_groups'][backup_todb($mes_info['#']['ID']['0']['#'])] = $newidm;

            //Do some output
            if (($i+1) % 50 == 0) {
                if (!defined('RESTORE_SILENTLY')) {
                    echo ".";
                    if (($i+1) % 1000 == 0) {
                        echo "<br />";
                    }
                }
                backup_flush(300);
            }
        }
        
        return $status;
    }
    
    
    
    function modelling_restore_modelling_gr_students ($old_modelling_id, $new_modelling_id,$info,$restore) {

        global $CFG, $oldidarray;

        $status = true;

        //Get the messages array 
        $messages = $info['MOD']['#']['PROJECT_GROUPS_STUDENTS']['0']['#']['ROWS'];

        //Iterate over messages
        for($i = 0; $i < sizeof($messages); $i++) {
            $mes_info = $messages[$i];
            //Now, build the modelling_MESSAGES record structure
            
            $message->instance = $new_modelling_id;
            $message->groupid = $oldidarray[$old_modelling_id]['modelling_groups'][$mes_info['#']['GROUPID']['0']['#']]; 
            $message->userid = backup_todb($mes_info['#']['USERID']['0']['#']);
            
            $user = backup_getid($restore->backup_unique_code,"user",$message->userid);
            if ($user) {
                $message->userid = $user->new_id;
            }

            $newidm = insert_record ("modelling_gr_students",$message);
            $oldidarray[$old_modelling_id]['modelling_gr_students'][backup_todb($mes_info['#']['ID']['0']['#'])] = $newidm;

            //Do some output
            if (($i+1) % 50 == 0) {
                if (!defined('RESTORE_SILENTLY')) {
                    echo ".";
                    if (($i+1) % 1000 == 0) {
                        echo "<br />";
                    }
                }
                backup_flush(300);
            }
        }
        
        return $status;
    }
    
    
    function modelling_restore_modelling_topics ($old_modelling_id, $new_modelling_id,$info,$restore, $project) {

        global $CFG, $oldidarray;

        $status = true;

        //Get the messages array 
        $messages = $info['MOD']['#']['PROJECT_TOPICS']['0']['#']['ROWS'];

        //Iterate over messages
        for($i = 0; $i < sizeof($messages); $i++) {
            $mes_info = $messages[$i];
            //Now, build the modelling_MESSAGES record structure
            
            $message->instance = $new_modelling_id;
            $message->name = backup_todb($mes_info['#']['NAME']['0']['#']);
            $message->description = backup_todb($mes_info['#']['DESCRIPTION']['0']['#']);
            $message->time = backup_todb($mes_info['#']['TIME']['0']['#']);
            $message->user_groupid = backup_todb($mes_info['#']['USER_GROUPID']['0']['#']);
            
            if ($project->projecttype == "individual") {
                $user = backup_getid($restore->backup_unique_code,"user",$message->user_groupid);
                if ($user) {
                    $message->user_groupid = $user->new_id;
                }
            } else if ($project->projecttype == "student groups" || $project->projecttype == "teacher groups project") {
                $message->user_groupid = $oldidarray[$old_modelling_id]['modelling_groups'][$mes_info['#']['USER_GROUPID']['0']['#']]; 
            }
            
            $newidm = insert_record ("modelling_topics",$message);
            $oldidarray[$old_modelling_id]['modelling_topics'][backup_todb($mes_info['#']['ID']['0']['#'])] = $newidm;

            //Do some output
            if (($i+1) % 50 == 0) {
                if (!defined('RESTORE_SILENTLY')) {
                    echo ".";
                    if (($i+1) % 1000 == 0) {
                        echo "<br />";
                    }
                }
                backup_flush(300);
            }
        }
        
        return $status;
    }
    
    
    
    function modelling_restore_modelling_top_template ($old_modelling_id, $new_modelling_id,$info,$restore) {

        global $CFG, $oldidarray;

        $status = true;

        //Get the messages array 
        $messages = $info['MOD']['#']['PROJECT_TOPICS_TEMPLATE']['0']['#']['ROWS'];

        //Iterate over messages
        for($i = 0; $i < sizeof($messages); $i++) {
            $mes_info = $messages[$i];
            //Now, build the modelling_MESSAGES record structure
            
            $message->title = backup_todb($mes_info['#']['TITLE']['0']['#']);
            $message->topics = backup_todb($mes_info['#']['TOPICS']['0']['#']);
            $message->description = backup_todb($mes_info['#']['DESCRIPTION']['0']['#']);
            
            //$newidm = insert_record ("modelling_top_template",$message);
            $oldidarray[$old_modelling_id]['modelling_top_template'][backup_todb($mes_info['#']['ID']['0']['#'])] = $newidm;

            //Do some output
            if (($i+1) % 50 == 0) {
                if (!defined('RESTORE_SILENTLY')) {
                    echo ".";
                    if (($i+1) % 1000 == 0) {
                        echo "<br />";
                    }
                }
                backup_flush(300);
            }
        }
        
        return $status;
    }
    
    function modelling_restore_modelling_crit_name ($old_modelling_id, $new_modelling_id,$info,$restore) {

        global $CFG, $oldidarray;

        $status = true;

        //Get the messages array 
        $messages = $info['MOD']['#']['PROJECT_CRITERIA_NAME']['0']['#']['ROWS'];

        //Iterate over messages
        for($i = 0; $i < sizeof($messages); $i++) {
            $mes_info = $messages[$i];
            //Now, build the modelling_MESSAGES record structure
            
            $message->instance = $new_modelling_id;
            $message->assessmentid = $oldidarray[$old_modelling_id]['modelling_tasks'][$mes_info['#']['ASSESSMENTID']['0']['#']];
            $message->name = backup_todb($mes_info['#']['NAME']['0']['#']);
            $message->time = backup_todb($mes_info['#']['TIME']['0']['#']);
            
            $newidm = insert_record ("modelling_crit_name",$message);
            $oldidarray[$old_modelling_id]['modelling_crit_name'][backup_todb($mes_info['#']['ID']['0']['#'])] = $newidm;

            //Do some output
            if (($i+1) % 50 == 0) {
                if (!defined('RESTORE_SILENTLY')) {
                    echo ".";
                    if (($i+1) % 1000 == 0) {
                        echo "<br />";
                    }
                }
                backup_flush(300);
            }
        }
        
        return $status;
    }
    
    
    
    function modelling_restore_modelling_crit_rating ($old_modelling_id, $new_modelling_id,$info,$restore) {

        global $CFG, $oldidarray;

        $status = true;

        //Get the messages array 
        $messages = $info['MOD']['#']['PROJECT_CRITERIA_RATING']['0']['#']['ROWS'];

        //Iterate over messages
        for($i = 0; $i < sizeof($messages); $i++) {
            $mes_info = $messages[$i];
            //Now, build the modelling_MESSAGES record structure
            
            $message->criteriaid = $oldidarray[$old_modelling_id]['modelling_crit_name'][$mes_info['#']['CRITERIAID']['0']['#']];
            $message->name = backup_todb($mes_info['#']['NAME']['0']['#']);
            $message->time = backup_todb($mes_info['#']['TIME']['0']['#']);
            
            $newidm = insert_record ("modelling_crit_rating",$message);
            $oldidarray[$old_modelling_id]['modelling_crit_rating'][backup_todb($mes_info['#']['ID']['0']['#'])] = $newidm;

            //Do some output
            if (($i+1) % 50 == 0) {
                if (!defined('RESTORE_SILENTLY')) {
                    echo ".";
                    if (($i+1) % 1000 == 0) {
                        echo "<br />";
                    }
                }
                backup_flush(300);
            }
        }
        
        return $status;
    }
    
    
    
    
    function modelling_restore_modelling_crit_template ($old_modelling_id, $new_modelling_id,$info,$restore) {

        global $CFG, $oldidarray;

        $status = true;

        //Get the messages array 
        $messages = $info['MOD']['#']['PROJECT_CRITERIA_TEMPLATE']['0']['#']['ROWS'];

        //Iterate over messages
        for($i = 0; $i < sizeof($messages); $i++) {
            $mes_info = $messages[$i];
            //Now, build the modelling_MESSAGES record structure
            
            $message->title = backup_todb($mes_info['#']['TITLE']['0']['#']);
            $message->criteria = backup_todb($mes_info['#']['CRITERIA']['0']['#']);
            $message->ratings = backup_todb($mes_info['#']['RATINGS']['0']['#']);
            
            $newidm = insert_record ("modelling_crit_template",$message);
            $oldidarray[$old_modelling_id]['modelling_crit_template'][backup_todb($mes_info['#']['ID']['0']['#'])] = $newidm;

            //Do some output
            if (($i+1) % 50 == 0) {
                if (!defined('RESTORE_SILENTLY')) {
                    echo ".";
                    if (($i+1) % 1000 == 0) {
                        echo "<br />";
                    }
                }
                backup_flush(300);
            }
        }
        
        return $status;
    }
    
    
    function modelling_restore_modelling_files ($old_modelling_id, $new_modelling_id,$info,$restore, $project) {

        global $CFG, $oldidarray;

        $status = true;

        //Get the messages array 
        $messages = $info['MOD']['#']['PROJECT_FILES']['0']['#']['ROWS'];

        //Iterate over messages
        for($i = 0; $i < sizeof($messages); $i++) {
            $mes_info = $messages[$i];
            //Now, build the modelling_MESSAGES record structure
            
            $message->instance = $new_modelling_id;
            $message->taskid = $oldidarray[$old_modelling_id]['modelling_tasks'][$mes_info['#']['TASKID']['0']['#']];
            $message->file = backup_todb($mes_info['#']['FILE']['0']['#']);
            $message->time = backup_todb($mes_info['#']['TIME']['0']['#']);
            $message->user_groupid = backup_todb($mes_info['#']['USER_GROUPID']['0']['#']);
            
            if ($project->projecttype == "individual") {
                $user = backup_getid($restore->backup_unique_code,"user",$message->user_groupid);
                if ($user) {
                    $message->user_groupid = $user->new_id;
                }
            } else if ($project->projecttype == "student groups" || $project->projecttype == "teacher groups project") {
                $message->user_groupid = $oldidarray[$old_modelling_id]['modelling_groups'][$mes_info['#']['USER_GROUPID']['0']['#']]; 
            }
            
            $newidm = insert_record ("modelling_files",$message);
            $oldidarray[$old_modelling_id]['modelling_files'][backup_todb($mes_info['#']['ID']['0']['#'])] = $newidm;

            //Do some output
            if (($i+1) % 50 == 0) {
                if (!defined('RESTORE_SILENTLY')) {
                    echo ".";
                    if (($i+1) % 1000 == 0) {
                        echo "<br />";
                    }
                }
                backup_flush(300);
            }
        }
        
        return $status;
    }
    
    
    
    function modelling_restore_modelling_schedule ($old_modelling_id, $new_modelling_id,$info,$restore, $project) {

        global $CFG, $oldidarray;

        $status = true;

        //Get the messages array 
        $messages = $info['MOD']['#']['PROJECT_SCHEDULE']['0']['#']['ROWS'];
        //Iterate over messages
        for($i = 0; $i < sizeof($messages); $i++) {
            $mes_info = $messages[$i];
            //Now, build the modelling_MESSAGES record structure
            
            $message->instance = $new_modelling_id;
            $message->day = backup_todb($mes_info['#']['DAY']['0']['#']);
            $message->begin = backup_todb($mes_info['#']['BEGIN']['0']['#']);
            $message->end = backup_todb($mes_info['#']['END']['0']['#']);
            $message->timeperiod = backup_todb($mes_info['#']['TIMEPERIOD']['0']['#']);
            $message->topic = $oldidarray[$old_modelling_id]['modelling_topics'][$mes_info['#']['TOPIC']['0']['#']];
            $message->time = backup_todb($mes_info['#']['TIME']['0']['#']);
            $message->student_groupid = backup_todb($mes_info['#']['STUDENT_GROUPID']['0']['#']);
            
            if ($project->projecttype == "individual") {
                $user = backup_getid($restore->backup_unique_code,"user",$message->student_groupid);
                if ($user) {
                    $message->student_groupid = $user->new_id;
                }
            } else if ($project->projecttype == "student groups" || $project->projecttype == "teacher groups project") {
                $message->student_groupid = $oldidarray[$old_modelling_id]['modelling_groups'][$mes_info['#']['STUDENT_GROUPID']['0']['#']]; 
            }
            
            if (!$message->topic) {
                $message->topic = 0;
            }
            
            $newidm = insert_record ("modelling_schedule", $message);

            $oldidarray[$old_modelling_id]['modelling_schedule'][backup_todb($mes_info['#']['ID']['0']['#'])] = $newidm;

            //Do some output
            if (($i+1) % 50 == 0) {
                if (!defined('RESTORE_SILENTLY')) {
                    echo ".";
                    if (($i+1) % 1000 == 0) {
                        echo "<br />";
                    }
                }
                backup_flush(300);
            }
        }
        
        return $status;
    }
    
    
    function modelling_restore_modelling_as_crit_rating ($old_modelling_id, $new_modelling_id,$info,$restore) {

        global $CFG, $oldidarray;

        $status = true;

        //Get the messages array 
        $messages = $info['MOD']['#']['PROJECT_AS_CRITERIA_RATING']['0']['#']['ROWS'];

        //Iterate over messages
        for($i = 0; $i < sizeof($messages); $i++) {
            $mes_info = $messages[$i];
            //Now, build the modelling_MESSAGES record structure
            
            $message->topicid = $oldidarray[$old_modelling_id]['modelling_topics'][$mes_info['#']['TOPICID']['0']['#']];
            $message->assessmentid = $oldidarray[$old_modelling_id]['modelling_tasks'][$mes_info['#']['ASSESSMENTID']['0']['#']];
            $message->userid = backup_todb($mes_info['#']['USERID']['0']['#']);
            $message->criteriaid = $oldidarray[$old_modelling_id]['modelling_crit_name'][$mes_info['#']['CRITERIAID']['0']['#']];
            $message->rate = $oldidarray[$old_modelling_id]['modelling_crit_rating'][$mes_info['#']['RATE']['0']['#']];
            $message->comment = backup_todb($mes_info['#']['COMMENT']['0']['#']);
            $message->time = backup_todb($mes_info['#']['TIME']['0']['#']);
            
            $user = backup_getid($restore->backup_unique_code,"user",$message->userid);
            if ($user) {
                $message->userid = $user->new_id;
            }
            
            $newidm = insert_record ("modelling_as_crit_rating",$message);
            $oldidarray[$old_modelling_id]['modelling_as_crit_rating'][backup_todb($mes_info['#']['ID']['0']['#'])] = $newidm;

            //Do some output
            if (($i+1) % 50 == 0) {
                if (!defined('RESTORE_SILENTLY')) {
                    echo ".";
                    if (($i+1) % 1000 == 0) {
                        echo "<br />";
                    }
                }
                backup_flush(300);
            }
        }
        
        return $status;
    }
    
    
    function modelling_restore_modelling_as_rating ($old_modelling_id, $new_modelling_id,$info,$restore) {

        global $CFG, $oldidarray;

        $status = true;

        //Get the messages array 
        $messages = $info['MOD']['#']['PROJECT_AS_RATING']['0']['#']['ROWS'];

        //Iterate over messages
        for($i = 0; $i < sizeof($messages); $i++) {
            $mes_info = $messages[$i];
            //Now, build the modelling_MESSAGES record structure
            
            $message->topicid = $oldidarray[$old_modelling_id]['modelling_topics'][$mes_info['#']['TOPICID']['0']['#']];
            $message->assessmentid = $oldidarray[$old_modelling_id]['modelling_tasks'][$mes_info['#']['ASSESSMENTID']['0']['#']];
            $message->userid = backup_todb($mes_info['#']['USERID']['0']['#']);
            $message->overallcomments = backup_todb($mes_info['#']['OVERALLCOMMENTS']['0']['#']);
            $message->time = backup_todb($mes_info['#']['TIME']['0']['#']);
            
            $user = backup_getid($restore->backup_unique_code,"user",$message->userid);
            if ($user) {
                $message->userid = $user->new_id;
            }
            
            $newidm = insert_record ("modelling_as_rating",$message);
            $oldidarray[$old_modelling_id]['modelling_as_rating'][backup_todb($mes_info['#']['ID']['0']['#'])] = $newidm;

            //Do some output
            if (($i+1) % 50 == 0) {
                if (!defined('RESTORE_SILENTLY')) {
                    echo ".";
                    if (($i+1) % 1000 == 0) {
                        echo "<br />";
                    }
                }
                backup_flush(300);
            }
        }
        
        return $status;
    }
    

    function modelling_restore_logs($restore,$log) {

        $status = true;

        return $status;
    }
    
?>
