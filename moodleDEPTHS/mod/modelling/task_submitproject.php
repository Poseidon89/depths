<?php // $Id: v 2.0 2007/12/01 12:37:00 serafim panov

    require_once("../../config.php");
    require_once("lib.php");
    require_once ($CFG->dirroot.'/course/moodleform_mod.php');

    $id = required_param('id'); 
    $name = optional_param('name'); 
    $description = optional_param('description'); 
    $timeopen = optional_param('timeopen'); 
    $timeclose = optional_param('timeclose'); 
    $laterenddate = optional_param('laterenddate'); 
    $uploadfilestype = optional_param('uploadfilestype'); 
    $fasttracktosubmit = optional_param('fasttracktosubmit'); 
    $standardfilename = optional_param('standardfilename'); 
    $standardfilenamedisabled = optional_param('standardfilenamedisabled'); 
    
    $update = optional_param('update'); 
    
    if ($id) {
        if (! $cm = get_record("course_modules", "id", $id)) {
            error("Course Module ID was incorrect");
        }
        if (! $course = get_record("course", "id", $cm->course)) {
            error("Course is misconfigured");
        }
        if (! $project = get_record("modelling", "id", $cm->instance)) {
            error("Course module is incorrect");
        }
    } else {
        if (! $project = get_record("modelling", "id", $a)) {
            error("Course module is incorrect");
        }
        if (! $course = get_record("course", "id", $project->course)) {
            error("Course is misconfigured");
        }
        if (! $cm = get_coursemodule_from_instance("modelling", $project->id, $course->id)) {
            error("Course Module ID was incorrect");
        }
    }

    require_login($course->id);
 
    add_to_log($course->id, "modelling", "submit task", "view.php?id=$id", "$cm->instance");
  
/// Print the page header

    $navigation = "<a href=\"../../course/view.php?id=$course->id\">$course->shortname</a> ->";
    
     
    

    print_header("$course->shortname: $project->name", "$course->fullname",
                 "$navigation <a href=\"index.php?id=$course->id\">Project</a> -> $project->name", 
                  "", "", true, update_module_button($id, $course->id, $strproject), 
                  navmenu($course));
                  
    //For teacher, view select button
     
    if (!isteacher($cm->course)) {
    	 
        error("Only for teachers");
    }
    
    //---------------------------
    
    if ($name) {
    	 
        $task = new object;
        $task->instance = $id;
        $task->name = $name;
        $task->description = $description;
        if ($timeopen && $timeclose) {
            $task->startdate = mktime (0,0,0,$timeopen['month'],$timeopen['day'],$timeopen['year']);
            $task->enddate = mktime (0,0,0,$timeclose['month'],$timeclose['day'],$timeclose['year']);
            $task->laterenddate = mktime (0,0,0,$laterenddate['month'],$laterenddate['day'],$laterenddate['year']);
        }
       
        $task->uploadfilestype = strtolower(str_replace (".", "", $uploadfilestype));
        if ($fasttracktosubmit) {
            $task->fasttracktosubmit = $fasttracktosubmit;
        }
        else
        {
            $task->fasttracktosubmit = 0;
        }
        
       //if ($task->standardfilenamedisabled == 1) {
            $task->standardfilename = "";
       // }
       // else
       // {
        //    $task->standardfilename = $standardfilename;
       // }
        
        $task->type = "submitproject";
        
        if ($update) {
        	 
            $task->id = $update;
            if (update_record("modelling_tasks", $task)) {
             
                redirect ("view.php?id=".$id, "Submit UML model Task updated");
             
            }
        }
        else
        {
        	 
            $positiontasks = get_records ("modelling_tasks", "instance", $id, "position desc");
            
            if ($positiontasks) {
            	  
                $positiontasks = current($positiontasks);
                $task->position = $positiontasks->position + 100;
               
            }
            else
            {
            	 
                $task->position = 100;
            }
            
            if (insert_record("modelling_tasks", $task)) {
            	 
                redirect ("view.php?id=".$id, "Submit UML model Task added");
            }
            
        }
    }
    
    //---------------------------
    
    class mod_modelling_submitproject_form extends moodleform {

        function definition() {
 
            global $CFG, $cm, $project, $USER, $update, $id;
        

            if ($update) {
            
                $data = get_record ("modelling_tasks", "id", $update);
              
            }

            $mform    =& $this->_form;
            
            $mform->addElement('header', 'general', get_string('submitprojecttask', 'modelling'));
              
            $mform->addElement('text', 'name', get_string('submittasktaskname', 'modelling'), array('size'=>'64'));
            $mform->setType('name', PARAM_TEXT);
            $mform->addRule('name', null, 'required', null, 'client');

            $mform->addElement('htmleditor', 'description', get_string('submittasktaskdescription', 'modelling'));
            $mform->setType('description', PARAM_RAW);
            $mform->setHelpButton('description', array('writing', 'questions', 'richtext'), false, 'editorhelpbutton');
            $mform->addRule('description', get_string('required'), 'required', null, 'client');
            
            if ($project->useprojectdates == "true") {
                $mform->addElement('date_selector', 'timeopen', get_string('submittasktaskstartdate', 'modelling'));
                $mform->addElement('date_selector', 'timeclose', get_string('submittasktaskenddate', 'modelling'));
                $mform->setDefault('timeclose', mktime(0,0,0,date("m") + 1,date("d"),date("Y"))); 
                $mform->addElement('date_selector', 'laterenddate', get_string('submittasktasklateenddate', 'modelling'));
                $mform->setDefault('laterenddate', mktime(0,0,0,date("m") + 1,date("d"),date("Y"))); 
            }
            
           // $mform->addElement('text', 'uploadfilestype', get_string('submittaskuploadfilestype', 'modelling'), array('size'=>'64'));
            
            if ($project->projecttype == "individual" && count_records ("modelling_tasks", "instance", $id, "type", "brainstorm") == 0) {
                $mform->addElement('checkbox', 'fasttracktosubmit', get_string('submittaskfasttracktosubmit', 'modelling'));
            }
            
            /*$standardfilename = array();
            $standardfilename[] = &MoodleQuickForm::createElement('text', 'standardfilename', get_string('submittaskstandardfilename', 'modelling'), array('size'=>'64'));
            $standardfilename[] = &MoodleQuickForm::createElement('checkbox', 'standardfilenamedisabled', null, get_string('disable'), array('onclick' => "if (form.standardfilename.disabled == true) {form.standardfilename.disabled = false; } else {form.standardfilename.disabled = true;}"));
            $mform->addGroup($standardfilename, 'standardfilename', get_string('submittaskstandardfilename', 'modelling'), ' ', false);
            $mform->setHelpButton('standardfilename', array('standartfilenames', '', 'modelling', true, false, '', true));
            $mform->disabledIf('standardfilename', 'standardfilenamedisabled', 'checked');
            */
            if ($update) {
                $mform->setDefault('name', $data->name);
                $mform->setDefault('description', $data->description);
                $mform->setDefault('timeopen', $data->startdate);
                $mform->setDefault('timeclose', $data->enddate);
                $mform->setDefault('laterenddate', $data->laterenddate);
                //$mform->setDefault('uploadfilestype', $data->uploadfilestype);
                $mform->setDefault('fasttracktosubmit', $data->fasttracktosubmit);
                //$mform->setDefault('standardfilename', $data->standardfilename);
                /*if (!empty($data->standardfilename)) {
                    $mform->setDefault('standardfilenamedisabled', 0); 
                }
                else
                {
                    $mform->setDefault('standardfilenamedisabled', 1); 
                }*/
            }
            
            $this->add_action_buttons(false); 
        }
    }
    
    if (!$update) {
        $mform = new mod_modelling_submitproject_form('task_submitproject.php?id=' . $id);
    }
    else
    {
        $mform = new mod_modelling_submitproject_form('task_submitproject.php?id=' . $id . '&update=' . $update);
    }
    $mform->display();

    print_footer($course);

?>