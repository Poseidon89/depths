<?php // $Id: v 2.0 2007/12/01 12:37:00 serafim panov

    require_once("../../config.php");
    require_once("lib.php");

    $id = required_param('id'); 
    $a  = optional_param('a');  
    $t  = optional_param('t');  
    $sort  = optional_param('sort');
    $dir   = optional_param('dir');
    
    if (!$sort) {
        $sort = 'criteria';
    }
    
    if (!$dir) {
        $dir  = 'ASC';
    }

    if ($id) {
        if (! $cm = get_record("course_modules", "id", $id)) {
            error("Course Module ID was incorrect");
        }
        if (! $course = get_record("course", "id", $cm->course)) {
            error("Course is misconfigured");
        }
        if (! $project = get_record("modelling", "id", $cm->instance)) {
            error("Course module is incorrect");
        }
    } else {
        if (! $project = get_record("modelling", "id", $a)) {
            error("Course module is incorrect");
        }
        if (! $course = get_record("course", "id", $project->course)) {
            error("Course is misconfigured");
        }
        if (! $cm = get_coursemodule_from_instance("modelling", $project->id, $course->id)) {
            error("Course Module ID was incorrect");
        }
    }

    require_login($course->id);

    add_to_log($course->id, "modelling", "Preview template", "templatepreview.php?id=$id", "$cm->instance");
    
/// Print the page header

    $navigation = "<a href=\"../../course/view.php?id=$course->id\">$course->shortname</a> ->";
  

    print_header("$course->shortname: $project->name", "$course->fullname",
                 "$navigation <a href=\"index.php?id=$course->id\">Project</a> -> $project->name", 
                  "", "", true, update_module_button($id, $course->id, $strproject), 
                  navmenu($course));
                  
                  
    if ($a == "assessment") {
        $template = get_record ("modelling_crit_template", "id", $t);
        $criteria            = explode ('{}', $template->criteria);
        $users1              = explode ('{}', $template->users);
        $ratings             = explode ('||', $template->ratings);
        $value               = explode ('||', $template->value);
        
        $count = 0;
        
        foreach ($criteria as $criteria_) {
            if ($sort == 'criteria') {
                $allocatedkey = $criteria_.rand(999,9999); 
            }

            $templatedata[$allocatedkey]['criteria'] = $criteria_;
            $templatedata[$allocatedkey]['users']    = $users1[$count];
            $templatedata[$allocatedkey]['ratings']  = $ratings[$count];
            $templatedata[$allocatedkey]['value']    = $value[$count];
            $count++;
        }
    
        ksort($templatedata);
        if ($dir == 'DESC') {
            $templatedata = array_reverse ($templatedata);
        }
    
        print_simple_box_start('center', '500', '#ffffff', 10);
    
        $table = new object;
        $titlesarray = Array ("Criteria"=>'criteria', "Rating"=>'');
        //$titlesarray = Array ("Criteria"=>'', "Rating"=>'');
        
        
        
        foreach ($titlesarray as $titlesarraykey => $titlesarrayvalue) {
            if ($sort != $titlesarrayvalue) {
                $columnicon = "";
            }
            else
            { 
                if ($dir == "ASC") {
                    $columndir    = "DESC";
                    $columndirimg = "down";
                }
                else
                {
                    $columndir    = "ASC";
                    $columndirimg = "up";
                }
                $columnicon = " <img src=\"$CFG->pixpath/t/$columndirimg.gif\" alt=\"\" />";
            }
            if (!empty($titlesarrayvalue)) {
                $table->head[] = "<a href=\"templatepreview.php?id=$id&sort=$titlesarrayvalue&dir=$columndir&t=$t&a=assessment\">$titlesarraykey</a>$columnicon";
            }
            else
            {
                $table->head[] = "$titlesarraykey";
            } 
        }
     
        $table->width = "400";
        $table->align = array ("left", "left");
        
        foreach ($templatedata as $templatedata_) {
            $j = 0;
            unset($newoftemplatedata);
            
            $addlabel = explode("{}", $templatedata_['ratings']);
            foreach ($addlabel as $addlabel_) {
                $j++;
                $newoftemplatedata .= $addlabel_ ." = " . $j ."<br />";
            }
            $table->data[] = Array($templatedata_['criteria'],$newoftemplatedata);
        }
    
        if (!empty($table)) {
            print_table($table);
        }
    
        print_simple_box_end();
    
        
    }
    else
    {
        $template = get_record ("modelling_top_template", "id", $t);
        
        $topics            = explode ('{}', $template->topics);
        $topicdescriptions = explode ('{}', $template->topicdescriptions);
        
        $count = 0;
        
        foreach ($topics as $topics_) {
            if ($sort == 'topic') {
                $allocatedkey = $topics_; 
            }
            else
            {
                $allocatedkey = $topicdescriptions[$count]; 
            }
            $templatedata[$allocatedkey]['topics']            = $topics_;
            $templatedata[$allocatedkey]['topicdescriptions'] = $topicdescriptions[$count];
            $count++;
        }
    
        ksort($templatedata);
        if ($dir == 'DESC') {
            $templatedata = array_reverse ($templatedata);
        }
    
        print_simple_box_start('center', '500', '#ffffff', 10);
    
        $table = new object;
        $titlesarray = Array ("Topic"=>'topic', "Description"=>'description');
        
        foreach ($titlesarray as $titlesarraykey => $titlesarrayvalue) {
            if ($sort != $titlesarrayvalue) {
                $columnicon = "";
            }
            else
            {
                if ($dir == "ASC") {
                    $columndir    = "DESC";
                    $columndirimg = "down";
                }
                else
                {
                    $columndir    = "ASC";
                    $columndirimg = "up";
                }
                $columnicon = " <img src=\"$CFG->pixpath/t/$columndirimg.gif\" alt=\"\" />";
            }
            if (!empty($titlesarrayvalue)) {
                $table->head[] = "<a href=\"templatepreview.php?id=$id&sort=$titlesarrayvalue&dir=$columndir\">$titlesarraykey</a>$columnicon";
            }
            else
            {
                $table->head[] = "$titlesarraykey";
            } 
        }
    
        $table->width = "400";
        $table->align = array ("left", "left");
   
        foreach ($templatedata as $templatedata_) {
            $table->data[] = Array($templatedata_['topics'],$templatedata_['topicdescriptions']);
        }
    
        if (!empty($table)) {
            print_table($table);
        }
    
        print_simple_box_end();
    }
   

    

    
    print_footer($course);
?>