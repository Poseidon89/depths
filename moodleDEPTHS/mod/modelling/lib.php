<?php // $Id: v 2.0 2007/12/01 12:37:00 serafim panov

function modelling_add_instance($project) {
 
global $CFG, $USER, $SESSION;
   
    if (isset($_FILES["attachment"]) && !empty($_FILES['attachment']['name'])) {
        require_once($CFG->dirroot.'/lib/uploadlib.php');
        $userfile = optional_param('attachment','',PARAM_FILE);
        $um = new upload_manager('attachment',false,false,'1',false,0);
        $dir = $CFG->dataroot."/1";
        $um->handle_filename_collision($dir, $_FILES['attachment']);
        $um->process_file_uploads($dir);
        
        $project->filename     = $_FILES['attachment']['name'];
    }
    else
    {
        $project->filename     = "none";
    }
    
    if ($project->useprojectdates == 1) {
        $project->useprojectdates = "true";
    }
    else
    {
        $project->useprojectdates = "false";
    }
     
if((!$project->ratingtime)||(!$project->userating)){
     
    	$project->ratingtime=0;
    	$project->assesstimestart=0;
    	$project->assesstimefinish=0;
    }
   
    if(!$project->userating){
    
    	$project->assessed=0;
    }
  
    $id = $project->courseid;
    $project->timemodified = time();
   
    return insert_record("modelling", $project);
}




function modelling_update_instance($project, $id) {
    global $CFG;
  
   ////
    if (isset($_FILES["attachment"]) && !empty($_FILES['attachment']['name'])) {
        require_once($CFG->dirroot.'/lib/uploadlib.php');
        $userfile = optional_param('attachment','',PARAM_FILE);
        $um = new upload_manager('attachment',false,false,'1',false,0);
        $dir = $CFG->dataroot."/1";
        $um->handle_filename_collision($dir, $_FILES['attachment']);
        $um->process_file_uploads($dir);
        
        $project->filename     = $_FILES['attachment']['name'];
    }
    else
    {
        $project->filename     = "none";
    }
    
    if ($project->useprojectdates == 1) {
        $project->useprojectdates = "true";
    }
    else
    {
        $project->useprojectdates = "false";
    }
    if((!$project->ratingtime)||(!$project->userating)){
     
    	$project->ratingtime=0;
    	$project->assesstimestart=0;
    	$project->assesstimefinish=0;
    }
    if(!$project->userating){
    
    	$project->assessed=0;
    }
    
    $project->timemodified = time();
    
    $project->id = $project->instance;

    # May have to add extra stuff in here #

    return update_record("modelling", $project);
    
}



function modelling_submit_instance($project, $id) {

    global $CFG;
    

}


function modelling_delete_task($tablename,$field,$value){
	 
	$task=get_record($tablename,$field,$value);	
	 
	if ($task->type=="projectassessment"){	
		 
	$crit_names=get_records("modelling_crit_name","instance",$task->instance);
	 
	foreach($crit_names as $crit_name){
	 
		if (!delete_records("modelling_crit_rating","criteriaid",$crit_name->id)){
			 
		}
	 
		
		 
		if(!delete_records("modelling_crit_name","id",$crit_name->id)){
			print_error_message("Not deleted records in modelling_crit_name","ERRORS");
		}
	}
		 
	if(!delete_records("modelling_as_crit_rating","assessmentid",$task->id)){
		print_error_message("Not deleted records in modelling_as_crit_rating","ERRORS");
	}
	 
	if(!delete_records("modelling_as_rating","assessmentid",$task->id)){
		print_error_message("Not deleted records in modelling_as_rating","ERRORS");
	}
	 
	
	}else if ($task->type=="submitproject"){
		 	
		delete_records("modelling_projects","instance",$task->instance);
		 
	}else if ($task->type=="brainstorm"){
		 
		if($ideas=get_records("modelling_ideas","instance",$task->id)){
			 
		foreach($ideas as $idea){
			 
			if (!delete_records("modelling_idea_ratings","entryid",$idea->id)){
				print_error_message("Not deleted records in modelling_idea_ratings","ERRORS");
			}
		 
		}
		delete_records("modelling_ideas","instance",$task->id);
		}else{
			 
		}
	}
	delete_records("modelling_tasks", "id", $value);
}
function modelling_delete_instance($id) {
 
    global $CFG;
 
    if (! $problem = get_record("modelling", "id", $id)) {
        return false;
    }
 
    $result = true;
 
    # Delete any dependent records here #
    $module = get_record("modules","name","modelling");
     
    $cm=get_record("course_modules","module",$module->id,"instance", $problem->id );
    $dp_tasks=get_records("modelling_tasks","instance",$cm->id);

   
   
 if(!delete_records("modelling_projects","instance",$cm->id)){
    print_error_message("could not delete_records modelling_projects","ERRORS");
    
    }
 
   //?
	if(!delete_records("modelling_files","instance",$cm->id)){
    print_error_message("could not delete_records modelling_files","ERRORS");
    
    }
 
    if (! delete_records("modelling", "id", $problem->id)) {
    	print_error_message("could not delete_records modelling","ERRORS");
        $result = false;
    }
 
foreach($dp_tasks as $dp_task){
	
	modelling_delete_task("modelling_tasks","id",$dp_task->id);
 
}
    
    return $result;
}





function modelling_user_outline($course, $user, $mod, $project) {
    return $return;
}




function modelling_user_complete($course, $user, $mod, $project) {
    return true;
}




function modelling_print_recent_activity($course, $isteacher, $timestart) {
    global $CFG;

    return false;  //  True if anything was printed, otherwise false 
}




function modelling_cron () {
    global $CFG;

    return true;
}




function modelling_grades($projectid) {
   return NULL;
}




function modelling_get_participants($projectid) {
    return false;
}




function modelling_scale_used ($projectid,$scaleid) {
    $return = false;
   
    return $return;
}


function modelling_dateformat ($time) {
    if (!empty($time)) { 
        return date("d M y", $time); 
    }
    else
    {
        return "N/A";
    }
}

function modelling_unassignedusers ($course, $instance) {

global $USER;

    $students = get_course_students ($course);
    
    $groups = get_records ("modelling_groups", "instance", $instance);
    
    $exist = array();
    
    foreach ($groups as $group) {
        $existstudens = get_records ("modelling_gr_students", "groupid", $group->id);
        foreach ($existstudens as $existstuden) {
            $exist[] = $existstuden->userid;
        }
    }
    
    $exist[] = $USER->id;
    
    foreach ($students as $student) {
        if (!in_array($student->id,$exist)) {
            $result[$student->id] = fullname($student);
        }
    }
    
    return $result;
}

function modelling_checkuseringroup ($instance) {

global $USER;

    $groups = get_records ("modelling_groups", "instance", $instance);
    
    $exist = array();
    
    foreach ($groups as $group) {
        $existstudens = get_records ("modelling_gr_students", "groupid", $group->id);
        foreach ($existstudens as $existstuden) {
            $exist[] = $existstuden->userid;
        }
    }

    if (!in_array($USER->id,$exist)) {
        return false;
    }
    else
    {
        return true;
    }
}


function modelling_gettabbar ($id, $t) {

global $USER;

    $tasks = get_records ("modelling_tasks", "instance", $id, "position");
    
    foreach ($tasks as $task) {
        $tasksnew[] = $task;
    }
    
    foreach ($tasksnew as $taskkey => $task) {
        if ($task->id == $t) {
            if (($taskkey - 1) >= 0) {
                $prev = $tasksnew[($taskkey - 1)];
            }
            if (($taskkey + 1) <= (count($tasksnew) - 1)) {
                $next = $tasksnew[($taskkey + 1)];
            }
        }
    }
    
    if ($prev) {
        $return['prev'] = $prev->type . ".php?id=".$id."&t=".$prev->id;
    }
    
    if ($next) {
        $return['next'] = $next->type . ".php?id=".$id."&t=".$next->id;
    }
    
    return $return;
}

function modelling_freetopics ($id) {

global $USER;

    $alltopics = get_records ("modelling_topics", "instance", $id);
    
    foreach ($alltopics as $alltopic) {
        if (empty($alltopic->user_groupid) || $alltopic->user_groupid == 0) {
            $return[$alltopic->id] = $alltopic;
        }
    }
    
    return $return;
}


function modelling_student_groupid ($id) {

global $USER;

    $groups = get_records ("modelling_groups", "instance", $id);
    foreach ($groups as $group) {
        if (get_record ("modelling_gr_students", "groupid", $group->id, "userid", $USER->id)) {
            $groupid = $group->id;
        }
    }
    
    return $groupid;
}


function modelling_make_table_headers ($titlesarray, $orderby, $sort, $link) {

global $USER, $CFG;

    foreach ($titlesarray as $titlesarraykey => $titlesarrayvalue) {
        if ($sort != $titlesarrayvalue) {
            $columnicon = "";
        }
        else
        {
            if ($orderby == "ASC") {
                $columndir    = "DESC";
                $columndirimg = "down";
            }
            else
            {
                $columndir    = "ASC";
                $columndirimg = "up";
            }
            $columnicon = " <img src=\"$CFG->pixpath/t/$columndirimg.gif\" alt=\"\" />";
        }
        if (!empty($titlesarrayvalue)) {
            $table->head[] = "<a href=\"".$link."&sort=$titlesarrayvalue&orderby=$columndir\">$titlesarraykey</a>$columnicon";
        }
        else
        {
            $table->head[] = "$titlesarraykey";
        } 
    }
    
    return $table->head;

}

function modelling_sort_table_data ($data, $titlesarray, $orderby, $sort) {

global $USER, $CFG;

    $j = 0;
    if ($sort) {
        foreach ($titlesarray as $titlesarray_) {
            if ($titlesarray_ == $sort) {
                $orderkey = $j;
            }
            $j++;
        }
    }
    else
    {
        $orderkey = 0;
    }
    
    $newarray = array ();

    foreach ($data as $datakey => $datavalue) {
        if (!is_array($datavalue[$orderkey])) {
            if (array_key_exists($datavalue[$orderkey], $newarray)) { 
                $key = $datavalue[$orderkey].rand(999, 9999);
            }
            else
            {
                $key = $datavalue[$orderkey];
            }
        }
        else
        {
            if (array_key_exists($datavalue[$orderkey][1], $newarray)) { 
                $key = $datavalue[$orderkey][1].rand(999, 9999);
            }
            else
            {
                $key = $datavalue[$orderkey][1];
            }
        }
        
        for ($j=0; $j < count($datavalue); $j++) {
            if (!is_array($datavalue[$j])) {
                $newarray[$key][$j] = $datavalue[$j];
            }
            else
            {
                $newarray[$key][$j] = $datavalue[$j][0];
            }
        }
    }
    
    if (empty($orderby) || $orderby == "ASC") {
        ksort ($newarray); 
    }
    else
    {
        krsort ($newarray);
    }
    
    reset($newarray);
    
    foreach ($newarray as $newarray_) {
        $finaldata[] = $newarray_;
    }
    
    return $finaldata;
}


function modelling_shedule_colculate ($schedule_, $taskdata, $project, $id, $t) {

global $USER, $CFG;

    $begintime = explode (":", $schedule_->begin);
                
    $endtime1['hour'] = $begintime[0];
    $endtime1['minute'] = $begintime[1] + $schedule_->timeperiod;
    while ($endtime1['minute'] >= 60) {
        $endtime1['hour'] ++;
        $endtime1['minute'] -= 60;
    }
    
    if (($endtime1['minute'] - $taskdata->duration) < 0) {
        $begintime2 ['hour'] = $endtime1['hour'] - 1;
        $begintime2 ['minute'] = $endtime1['minute'] + 60 - $taskdata->duration;
    }
    else
    {
        $begintime2 ['hour'] = $endtime1['hour'];
        $begintime2 ['minute'] = $endtime1['minute'] - $taskdata->duration;
    }
    
    if ($endtime1['minute'] == "0") {
        $endtime1['minute'] = "00";
    }
    
    if ($begintime2['minute'] == "0") {
        $begintime2['minute'] = "00";
    }
    
    if (!empty($schedule_->student_groupid)) {
        $printabledata['student_groupid'] = modelling_return_user_groupid_name ($project, $schedule_->student_groupid);

        $topic = get_record ("modelling_topics", "instance", $id, "user_groupid", $schedule_->student_groupid);
        $printabledata['topic'] = $topic->name;
        
        $printabledata['dateadded'] = array(date ("m:i d M Y", $schedule_->time), $schedule_->time);
        $printabledata['removeappointment'] = '<input type="checkbox" name="removeappointment['.$schedule_->id.']" />';
        $printabledata['addedlink'] = $printabledata['student_groupid'];
        
        if (modelling_return_user_groupid ($project, $USER->id) == $schedule_->student_groupid) {
            $printabledata['addedlink'] .= ' - <a href="schedule.php?id='.$id.'&t='.$t.'&shdel='.$schedule_->id.'">Cancel Booking</a>';
        }
    }
    else
    {
        $printabledata['student_groupid'] = "-";
        $printabledata['dateadded'] = "-";
        $printabledata['removeappointment'] = "-";
        $printabledata['topic'] = "-";
        
        if (count_records("modelling_schedule", "instance", $id, "student_groupid", modelling_return_user_groupid ($project, $USER->id)) == 0) {
            $printabledata['addedlink'] = '<a href="schedule.php?id='.$id.'&t='.$t.'&sh='.$schedule_->id.'">Book Appointment</a>';
        }
        else
        {
            $printabledata['addedlink'] = '';
        }
    }
    
    $printabledata['removetime'] = '<input type="checkbox" name="removetime['.$schedule_->id.']" />';
    
    $printabledata['time'] = array ($begintime2['hour'] . ":" . $begintime2['minute'] . " - " . $endtime1['hour'] . ":" . $endtime1['minute'], $schedule_->timeperiod);
    
    return $printabledata;

}

function modelling_get_file_path ($course) {

global $USER, $CFG;

    $dir = $CFG->dataroot."/".$course."/modelling_files";
    
    return $dir;

}

function modelling_get_file_path_www ($course) {

global $USER, $CFG;

    $dir = $course."/modelling_files";
    
    return $dir;

}

function modelling_return_user_groupid ($project, $userid) {

global $USER, $CFG;

    if ($project->projecttype == "individual") {
        return $userid;
    }
    else if ($project->projecttype == "no_groups") {
    	return $userid;
    }
    else if ($project->projecttype == "teacher_groups") {
    	return $userid;
    }
        
    else if ($project->projecttype == "student groups") {
        $group = get_record("modelling_gr_students", "userid", $userid);
        return $group->groupid;
    }
    else if ($project->projecttype == "teacher_groups_one_project") {
        $groups = groups_get_all_groups($project->course);
        foreach ($groups as $group) {
            if (groups_is_member($group->id, $userid)) {
                return $group->id;
            }
        }
    }
}

function modelling_return_user_groupid_name ($project, $userid) {
 
global $USER, $CFG;
 
    if ($project->projecttype == "individual") {
    	 
        $userdata = get_record ("user", "id", $userid);
       
        return fullname ($userdata);
    }
    else if ($project->projecttype == "no_groups") {
    	 $userdata = get_record ("user", "id", $userid);
        
        return fullname ($userdata);
    }
    else if ($project->projecttype == "teacher_groups") {
    	 $userdata = get_record ("user", "id", $userid);
      
        return fullname ($userdata);
    }
    else if ($project->projecttype == "student groups" || $project->projecttype == "teacher groups project") {
        $group = get_record("modelling_gr_students", "userid", $userid);
        $group = get_record("modelling_gr", "id", $group->groupid);
        
        return $group->name;
    }
    else if ($project->projecttype == "teacher groups") {
        $groups = groups_get_all_groups($project->course);
        foreach ($groups as $group) {
            if (groups_is_member($group->id, $userid)) {
                return groups_get_group_name($group->id);
            }
        }
    }

}


function modelling_return_file_name ($file) {

global $USER, $CFG;

    $file = str_replace (" ", "_", $file);
    $file = str_replace ("__", "_", $file);
    
    return $file;

}

function modelling_makelocalhelplinkmain ($name, $value, $modulename) {

    global $CFG;

    $return = '<span class="helplink"><a target="popup" title="'.$value.'" href="'.$CFG->wwwroot.'/help.php?module='.$modulename.'&amp;file='.$name.'.html&amp;forcelang=" onclick="return openpopup(\'/help.php?module='.$modulename.'&amp;file='.$name.'.html&amp;forcelang=\', \'popup\', \'menubar=0,location=0,scrollbars,resizable,width=500,height=400\', 0);"><img alt="'.$value.'" src="'.$CFG->wwwroot.'/pix/help.gif" /></a></span>';

    return $return;
}


function modelling_get_assessment_criterias ($t,  $project, $submissiondata) {

    global $CFG, $USER;
 
    $criterias = get_records ("modelling_crit_name", "assessmentid", $t);
    $task=get_record("modelling_tasks","id",$t);
    $cm=get_record("course_modules","id",$task->instance);
    foreach ($criterias as $criteria) {
  
        $ratings = get_records_sql ("SELECT * FROM ".$CFG->prefix."modelling_as_crit_rating WHERE criteriaid='".$criteria->id."' and assessmentid='".$t."'  and submissionid='".$submissiondata->id."'");
        
        unset($ratingdata);
        
        foreach ($ratings as $rating) {
            if ($submissiondata->user_groupid == modelling_return_user_groupid ($project, $rating->userid)) {
                $ratingdata['self'][] = $rating;
            } else if (isteacher($cm->course, $rating->userid)) {
                $ratingdata['averteacher'][] = $rating;
            } else {
                $ratingdata['averstudent'][] = $rating;
            }
            
        }
            
            unset($value, $allratingsrate, $tabledata, $tablkey, $grade);

            $criteriaratings = get_records ("modelling_crit_rating", "criteriaid", $criteria->id, "id");
            $i = 1;
            foreach ($criteriaratings as $criteriarating) {
                $value[$criteriarating->id] = $i;
                $i++;
            }
            
            foreach (array ("self", "averteacher", "averstudent") as $tablkey) {
                if (!empty($ratingdata[$tablkey])) {
                    unset ($allratingsrate);
                    foreach ($ratingdata[$tablkey] as $rating) {
                        $allratingsrate += $value[$rating->rate];
                    }
                    $grade = round(($allratingsrate/count($ratingdata[$tablkey])),2);
                    $tabledata[$tablkey] = array ($grade . "/" . count($value), $grade);
                }
                else
                {
                    $tabledata[$tablkey] = "N/A";
                }
            }
        //}
        
        $data[$criteria->name] = array ("averstudent" => $tabledata['averstudent'], "self" => $tabledata['self'], "averteacher" => $tabledata['averteacher']);
    }
    
    return $data;
    
}
function getmygroupmembers($modelling,$course){
	global $CFG, $USER;
	require_once($CFG->dirroot.'/depths/mapper/utility.php');
 
	$mygroupsmembers = array();
	$currentUserId=$USER->id;
 
 
	if($modelling->projecttype=="teacher_groups"){
		$groups_sql="SELECT DISTINCT(groupid) FROM {$CFG->prefix}groups_members WHERE userid=".$currentUserId;
  
		if ($groups = get_records_sql($groups_sql)) {
 
			foreach ($groups as $group) {
				$members_sql="SELECT DISTINCT(userid) FROM {$CFG->prefix}groups_members WHERE groupid=".$group->groupid;
			 
				if($members=get_records_sql($members_sql)){
					foreach($members as $member){
						if (!in_array($member->userid, $mygroupsmembers)) {
							$mygroupsmembers[]=$member->userid;
						}
					}
				}
			}
		}
	}else if(($modelling->projecttype=="no_groups")||($modelling->projecttype=="individual")){
 
		$contextQuery="SELECT (id) FROM {$CFG->prefix}context WHERE contextlevel=".CONTEXT_COURSE." and instanceid=".$course->id;
	 
		if($contextId=get_field_sql($contextQuery)){
			$query = "select u.id as userid from {$CFG->prefix}role_assignments as a,{$CFG->prefix}user as u where contextid=".$contextId. "  and a.userid=u.id ";//and u.id <>'$USER->id' // If user see itself add this
 
			$rs = get_records_sql($query);
	 
			foreach($rs as $member){
 
				if (!in_array($member->userid, $mygroupsmembers)) {
					$mygroupsmembers[]=$member->userid;
				}
			}
		}
 
	}
	 
	return $mygroupsmembers;
}

?>