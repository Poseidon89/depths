<?php // $Id: v 2.0 2007/12/01 12:37:00 serafim panov

    require_once("../../config.php");
    require_once("lib.php");
    require_once ($CFG->dirroot.'/course/moodleform_mod.php');
    require_once($CFG->dirroot.'/depths/lib/depths_settingslib.php');
    require_once($CFG->dirroot.'/depths/lib/depths_lib.php');

    $id = required_param('id'); 
    $t  = required_param('t'); 
    $v  = optional_param('v'); 
    $delfile  = optional_param('delfile'); 
    
    $sort = optional_param('sort', 'studentsgroups', PARAM_ALPHA); 
    $orderby = optional_param('orderby', 'ASC', PARAM_ALPHA); 

    if ($id) {
        if (! $cm = get_record("course_modules", "id", $id)) {
            error("Course Module ID was incorrect");
        }
        if (! $course = get_record("course", "id", $cm->course)) {
            error("Course is misconfigured");
        }
        if (! $project = get_record("modelling", "id", $cm->instance)) {
            error("Course module is incorrect");
        }
    } else {
        if (! $project = get_record("modelling", "id", $a)) {
            error("Course module is incorrect");
        }
        if (! $course = get_record("course", "id", $project->course)) {
            error("Course is misconfigured");
        }
        if (! $cm = get_coursemodule_from_instance("modelling", $project->id, $course->id)) {
            error("Course Module ID was incorrect");
        }
    }

    require_login($course->id);

    add_to_log($course->id, "modelling", "make group viewing", "view.php?id=$id", "$cm->instance");
    
/// Print the page header

    $navigation = "<a href=\"../../course/view.php?id=$course->id\">$course->shortname</a> ->";
    
    
    

    print_header("$course->shortname: $project->name", "$course->fullname",
                 "$navigation <a href=\"index.php?id=$course->id\">Project</a> -> $project->name", 
                  "", "", true, update_module_button($id, $course->id, $strproject), 
                  navmenu($course));
                  
    //For teacher, view select button
    if (isteacher($cm->course)) {
        if (empty($_SESSION['SESSION']->modelling_teacherview)) {
            $_SESSION['SESSION']->modelling_teacherview = "teacherview";
        }
        if ($v == "teacher") {
            $_SESSION['SESSION']->modelling_teacherview = "teacherview";
        }
        if ($v == "student") {
            $_SESSION['SESSION']->modelling_teacherview = "studentview";
        }
        if ($_SESSION['SESSION']->modelling_teacherview == "studentview") {
            echo '<div style="text-align: right"><form action="?id='.$id.'&t='.$t.'&v=teacher" method="post"><input type="submit" value="'.get_string('teacherview', 'modelling').'"></form></div>';
        }
        if ($_SESSION['SESSION']->modelling_teacherview == "teacherview") {
            echo '<div style="text-align: right"><form action="?id='.$id.'&t='.$t.'&v=student" method="post"><input type="submit" value="'.get_string('studentview', 'modelling').'"></form></div>';
        }
    }

    $taskdata = get_record ("modelling_tasks", "id", $t);
    
    //-------Add file-------------------//
   
    if (isset($_FILES["attachment"]) && !empty($_FILES['attachment']['name'])) {
        if (strstr($taskdata->uploadfilestype, ',')) {
            $validtypes = explode (',', $taskdata->uploadfilestype);
        }
        else
        {
            $validtypes[] = $taskdata->uploadfilestype;
        }
        
        $filetype = end (explode (".", $_FILES['attachment']['name']));
        
        if (in_array(strtolower($filetype), $validtypes) || empty($taskdata->uploadfilestype)) {
            require_once($CFG->dirroot.'/lib/uploadlib.php');
            $userfile = optional_param('attachment','',PARAM_FILE);
            $um = new upload_manager('attachment',false,false,'1',false,0);
            $dir = modelling_get_file_path ($cm->course);
            $um->handle_filename_collision($dir, $_FILES['attachment']);
            $um->process_file_uploads($dir);
            
            //----------STANDART FILE NAMES------//
            $titlesdata = array ("username"=>$USER->username, "firstname"=>$USER->firstname, "lastname"=>$USER->lastname, "fullname"=>fullname($USER), "id"=>$USER->id, "projectname"=>$project->name, "topicname"=>modelling_return_user_groupid_name ($project, $USER->id));
            
            if (!empty($taskdata->standardfilename)) {
                if (strstr($taskdata->standardfilename, "[")) {
                    $setstandardfilenames = explode ("[", $taskdata->standardfilename);
                    foreach ($setstandardfilenames as $setstandardfilename) {
                        $setstandardfilename = explode ("]", $setstandardfilename);
                        foreach ($setstandardfilename as $setstandardfilename_) {
                            if (array_key_exists ($setstandardfilename_, $titlesdata)) {
                                @$newname .= $titlesdata[$setstandardfilename_];
                            }
                            else
                            {
                                @$newname .= $setstandardfilename_;
                            }
                        }
                    }
                    $newname = str_replace (" ", "_", $newname);

                    rename($dir.'/'.$_FILES['attachment']['name'], $dir.'/'.$newname.'.'.strtolower($filetype)); 
                    $_FILES['attachment']['name'] = $newname.'.'.strtolower($filetype);
                }
                else
                {
           
                    rename($dir.'/'.$_FILES['attachment']['name'], $dir.'/'.$taskdata->standardfilename.'.'.strtolower($filetype)); 
                    $_FILES['attachment']['name'] = $taskdata->standardfilename.'.'.strtolower($filetype);
                }
            }
            //-----------------------------------//
        
            //-----------FAST TRACK--------------//
            if ($taskdata->fasttracktosubmit == 1) {
                $fasttopic = new object;
                $fasttopic->name = str_replace (".".$filetype, "", $_FILES['attachment']['name']);
                $fasttopic->description = $fasttopic->name;
                $fasttopic->instance = $id;
                $fasttopic->user_groupid = $USER->id;
                $fasttopic->time = time();
                
                insert_record ("modelling_topics", $fasttopic);
            }
            //-----------------------------------//

            $newfile = new object;

            $group = get_record("modelling_gr_students", "userid", modelling_return_user_groupid ($project, $USER->id));
            $oldfiledata = get_record ("modelling_files", "user_groupid", modelling_return_user_groupid ($project, $USER->id), "instance", $id, "taskid", $t);
            @unlink (modelling_get_file_path ($cm->course)."/".$oldfiledata->file);
            delete_records ("modelling_files", "user_groupid", modelling_return_user_groupid ($project, $USER->id), "instance", $id, "taskid", $t);
            
            $newfile->user_groupid = modelling_return_user_groupid ($project, $USER->id);

            $newfile->instance = $id;
            $newfile->taskid = $t;
            $newfile->file = $_FILES['attachment']['name'];  // ��������� ������� �� �������� ������� � ������� __
            $newfile->time = time();
            insert_record ("modelling_files", $newfile);
        }
        else
        {
            error ("Not valid file type ".$filetype." (Allowed: ".$taskdata->uploadfilestype.")", 'submit.php?id='.$id.'&t='.$t);
        }
    }
    
    //----------------------------------//
    
    if ($delfile) {
        if (isteacher($cm->course)) {
            if ($oldfiledata = get_record ("modelling_files", "id", $delfile)) {
                @unlink (modelling_get_file_path ($cm->course)."/".$oldfiledata->file);
                delete_records ("modelling_files", "id", $delfile);
            }
        }
        else
        {
            if ($oldfiledata = get_record ("modelling_files", "user_groupid", modelling_return_user_groupid ($project, $USER->id), "instance", $id, "id", $delfile)) {
                @unlink (modelling_get_file_path ($cm->course)."/".$oldfiledata->file);
                delete_records ("modelling_files", "id", $delfile);
            }
        }
    }
    
    //----------------------------------//
    
    echo '<div style="text-align: center"><h1>'.$taskdata->name.'</h1></div>';
    
    if (isteacher($cm->course)) {
        if ($_SESSION['SESSION']->modelling_teacherview == "studentview") {
            echo '<div style="text-align: center">'.get_string('studentview1', 'modelling').'</div><br />';
        }
        else
        {
            echo '<div style="text-align: center">'.get_string('teacherview1', 'modelling').'</div><br />';
        }
    }
    
    print_simple_box_start('center', '100%', '#ffffff', 10);
    
    echo '<table cellpadding="5" cellspacing="0">';

    echo '<tr><td><b>'.get_string ('grouptaskdescription', 'modelling').'</b></td>';
    echo '<td>'.$taskdata->description.'</td></tr>';
    
    if ($project->useprojectdates == "true") {
        echo '<tr><td><b>'.get_string ('grouptaskstartdate', 'modelling').'</b></td>';
        echo '<td>'.date("d M Y", $taskdata->startdate).'</td></tr>';
        echo '<tr><td><b>'.get_string ('grouptaskenddate', 'modelling').'</b></td>';
        echo '<td>'.date("d M Y", $taskdata->enddate).'</td></tr>';
        echo '<tr><td><b>'.get_string ('scheduletasklateenddate', 'modelling').'</b></td>';
        echo '<td>'.date("d M Y", $taskdata->laterenddate).'</td></tr>';
    }
    
    echo "</table>";
    
    print_simple_box_end();
   
    //------------PRINT TABS------------//
    $row  = array();

    $bar = modelling_gettabbar ($id, $t);
   
    if (!empty($bar['prev'])) {
        $row[] = new tabobject('return', $CFG->wwwroot . "/mod/modelling/" . $bar['prev'], "<< Previous Task");
    }

    $row[] = new tabobject('return', "view.php?id=$id", get_string('returntomainprojectpage', 'modelling'));

    
    if (!empty($bar['next'])) {
        $row[] = new tabobject('return', $CFG->wwwroot . "/mod/modelling/" . $bar['next'], "Next Task >>");
    }
    
    $tabs[] = $row;

    print_tabs($tabs);
    //----------------------------------//
    
    echo "<hr /><br />";
   
    if (isteacher($cm->course) && $_SESSION['SESSION']->modelling_teacherview == "teacherview") {
        $titlesarray = Array ('#'=>'', 'Students/Groups'=>'studentsgroups', 'Uploaded File'=>'uploadedfile', 'View File'.modelling_makelocalhelplinkmain ("viewfile", "Help, View File", "modelling")=>'', 'View ZIP'.modelling_makelocalhelplinkmain ("viewzip", "Help, View ZIP", "modelling")=>'', 'Date Added'=>'dateadded', 'Delete File'.modelling_makelocalhelplinkmain ("deletefile", "Help, Delete File", "modelling")=>'');

        $table->head = modelling_make_table_headers ($titlesarray, $orderby, $sort, 'submit.php?id='.$id.'&t='.$t);
        $table->align = array ("center", "left", "left", "center", "center", "center", "center");
        $table->width = "1000";
        
        $files = get_records_sql ("SELECT * FROM ".$CFG->prefix."modelling_files WHERE instance='".$id."' and taskid='".$t."'");
       
        if ($files) {
            $j = 0; 
            foreach ($files as $file) {
                $j++;
                echo "KT-xse:".$file->file;
                if ($CFG->slasharguments) {
                    $ffurl = "$CFG->wwwroot/file.php/".modelling_get_file_path_www ($cm->course)."/".$file->file;
                } else {
                    $ffurl = "$CFG->wwwroot/file.php?file=/".modelling_get_file_path_www ($cm->course)."/".$file->file;
                }
                $ffurl=depthsConvertUrlForFile($ffurl);
                $table->data[] = array ($j . ".", modelling_return_user_groupid_name ($project, $file->user_groupid), $file->file, '<a href="'.str_replace(" ","",$ffurl).'">view</a>', "", array (date("m:i d M Y", $file->time), $file->time), '<a href="submit.php?id='.$id.'&t='.$t.'&delfile='.$file->id.'">Delete</a> '.modelling_makelocalhelplinkmain ("deletefile", "Help, Delete File", "modelling"));
            }
            
            $table->data = modelling_sort_table_data ($table->data, $titlesarray, $orderby, $sort);
            
            if ($table) {
                print_table($table);
            }
        }
        else
        {
            echo '<br /><div style="text-align: center"><b>'.get_string ('nofiles', 'modelling').'</b></div>';
        }
    }
    else
    {
    
        if (get_record ("modelling_files", "instance", $id, "user_groupid", modelling_return_user_groupid ($project, $USER->id), "taskid", $t)) {
            $alreadyuploaded = 
            	array('onclick' => 
            			"if(confirm('Uploading a new file will erase the file you uploaded before. Are you sure you want to upload a new file?')) {console.log('new file is uploaded');return true; }else return false;");
        }
        else
        {
            $alreadyuploaded = array();
        }
    
        class mod_modelling_uploadfile_form extends moodleform {

            function definition() {

                global $CFG, $cm, $USER, $project, $alreadyuploaded;

                $mform    =& $this->_form;
        
//-------------------------------------------------------------------------------
                $mform->addElement('header', 'general', get_string('act_uploadfile', 'modelling'));

                $mform->addElement('file', 'attachment', get_string('act_uploadfile', 'modelling'), $alreadyuploaded);

                $mform->setHelpButton('attachment', array('uploadfile', 'Help with Upload File', 'modelling', true, false, '', true));
//-------------------------------------------------------------------------------

                $this->add_action_buttons($cancel = false, $submitlabel = get_string('upload', 'modelling'));

            }
        }
        
        if ($file = get_record ("modelling_files", "instance", $id, "user_groupid", modelling_return_user_groupid ($project, $USER->id), "taskid", $t)) {
            $table->head = array ("File", "View", "Delete");
            $table->align = array ("left", "center", "center");
            $table->width = "700";
            
            if ($CFG->slasharguments) {
                $ffurl = "$CFG->wwwroot/file.php/".modelling_get_file_path_www ($cm->course)."/".$file->file;
            } else {
                $ffurl = "$CFG->wwwroot/file.php?file=/".modelling_get_file_path_www ($cm->course)."/".$file->file;
            }
 
            $ffurl=depthsConvertUrlForFile($ffurl);
       
            $table->data[] = array ($file->file, '<a href="'.str_replace(' ','_',$ffurl).'">view</a>', '<a href="submit.php?id='.$id.'&t='.$t.'&delfile='.$file->id.'">Delete</a> '.modelling_makelocalhelplinkmain ("deletefile", "Help, Delete File", "modelling"));
            
            if ($table) {
                print_table($table);
            }
        }
        
        echo '<br /><hr /><br />';
        
        $mform = new mod_modelling_uploadfile_form('submit.php?id=' . $id . '&t=' . $t);
        $mform->display(); 
        
        echo '<center><table width="800"><tr><td>'.str_replace ('{type}', $taskdata->uploadfilestype, str_replace ("\r", "<br />", get_string ("uploadrules", "modelling"))) . '</td></tr></table></center>';
        
    }
    
    print_footer($course);

?>