<?PHP 
class block_onlinepresence extends block_base {
    function init() {
        $this->title = get_string('onlinepresencetitle','block_onlinepresence');
        $this->version = 2007101509;
    }

    function get_content() {
        global $USER, $CFG, $COURSE, $DB, $context;
        require_once($CFG->dirroot.'/depths/mapper/utility.php');
        if (!$CFG->messaging) {
            return ''; 
		}

        if ($this->content !== NULL) {
            return $this->content;
        }

        $this->content = new stdClass;
        $this->content->text = '';
        $this->content->footer = '';
 		$context = get_context_instance(CONTEXT_COURSE, $COURSE->id );
 		 print_message('block_onlinepresence courseid:'.$COURSE->id." contentid:".$context->id,"getpeersjson");
 		 print_message('block_onlinepresence lastaccessedproblemid:'.$_SESSION['lastaccessedproblem']." contentid:".$context->id,"getpeersjson");
        if (empty($this->instance) or empty($USER->id) or isguest() ) {
            return $this->content;
        }
		 $session_key = sesskey();

		 $this->content->text .='<div id="OnlinePresence" style="font-size:0.75em"></div>';
        
        if(empty($CFG->block_facebook_refresh) || $CFG->block_facebook_refresh == 0){ //never or not set!
          // $refreshtime = 180000; //60 seconds
          $refreshtime = 60000; //30 seconds
        }else{
           $refreshtime = $CFG->block_facebook_refresh;
        }
		 $this->content->text .='<script src="http://code.jquery.com/jquery-latest.js"></script>';

 $this->content->text .='
								<script type="text/javascript">   
							    <!--
								function ShowUsers(){
								    var notfication = "connecting...";
								    //document.title = notfication;
								    document.getElementById("OnlinePresence").innerHTML= notfication;
								    GetNewUsers();
           						}
           						function GetNewUsers(){
           						 	var objXMLHttp=null;
									if (window.XMLHttpRequest){
									  objXMLHttp=new XMLHttpRequest();
									}
									else if (window.ActiveXObject){
									  objXMLHttp=new ActiveXObject("Microsoft.XMLHTTP");
									}
									xmlHttp =  objXMLHttp; 
									var url="'.$CFG->wwwroot.'/blocks/onlinepresence/check_users.php";
									 
									url=url+"?updatemessage=1";
									url=url+"&sid="+Math.random();
									url=url+"&sesskey='.$session_key.'";
									url=url+"&conid='.$context->id .'";
									url=url+"&courseid='.$COURSE->id .'";
									url=url+"&problemid='.$_SESSION['lastaccessedproblem'].'";
									xmlHttp.onreadystatechange=stateChangedResult; // this is the function name to call
									xmlHttp.open("GET",url,true);
									xmlHttp.send(null);									
           						}
           						
           						function stateChangedResult(){
									if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete"){
									    document.getElementById("OnlinePresence").innerHTML= xmlHttp.responseText;
									    var titlebox = xmlHttp.responseText;
									    if(titlebox.search( /<!--/ ) != -1){
									    	titlebox = titlebox.slice( titlebox.search( /<!--/ ) );
										    titlebox = titlebox.replace(/<!--/,"",titlebox);
										    titlebox = titlebox.replace(/--><\/ul>/,"",titlebox); 
										    document.title = titlebox;
										   // I know! ugly hack :S
										}else{
										    document.title="'.$COURSE->shortname.'";
										}
									}
								}
								
							     setInterval(ShowUsers, '.$refreshtime.'); // Reload every 60 seconds :-)
							    ShowUsers(); // to load with page load!
							    //-->
							    </script>';
							    

       return $this->content;
    }

function applicable_formats(){
	return array('site' => false, 'course' => true, 'mod-modelling' => true);
}
}
?>
