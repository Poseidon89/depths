<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<script src="http://code.jquery.com/jquery-latest.js"></script>
</head>

<body>
<?PHP 
require_once('../../config.php');

//require_login();
 global $USER, $CFG, $COURSE, $DB, $context, $DPTH;
 require_once($CFG->dirroot.'/depths/lib/depths_settingslib.php');
 require_once($CFG->dirroot.'/depths/mapper/utility.php');
 
  
 	$courseid=required_param('courseid', PARAM_INT);
 	$problemid=required_param('problemid', PARAM_INT);
 	print_message("get course id:".$courseid,"getpeersjson");
  

 $courseUri=get_condition_value("modelling_urimapping","uri","instanceid",$courseid,'domainconcept','Course');

 
 $dp=get_record('modelling','id',$problemid);
 $designproblemuri=$dp->designproblemuri;
 print_message('calling getRelevantPeersForDesignProblem in courseid:'.$courseid."\n courseuri:".$courseUri."\n for design problem id:".$problemid." dp uri:".$designproblemuri,"getpeersjson");
 //$relevantPeers=getRelevantPeersForDesignProblem($courseUri,$designproblemuri);
		$query = "select u.id as id, firstname, lastname, username, picture, imagealt, email from {$CFG->prefix}role_assignments as a,{$CFG->prefix}user as u where contextid=" . $_GET['conid']. "  and a.userid=u.id ";//and u.id <>'$USER->id' // If user see itself add this
		print_message("query:".$query,"getpeersjson");
		$rs = get_records_sql($query); 
		
       // if (!empty($rs)) {
		print_message("before if","opos");
       if($relevantPeers){
		$i=1;
			$content->text .= '<ul class="list">';
			//foreach($rs as $id=>$r ) { 
			print_message("has relevant peers","opos");
			foreach($relevantPeers as $peerKey=>$peer){
				//$content->text.='<br/>'.$peer["username"];
				print_message("found:".$peer["username"],"opos");
				$content->text .= '<li class="listentry">';
				$request_url=$CFG->oposurl."rest/onlinePresences?type=simple&service=moodle&username=".$peer['username'];
				$userrecord=get_record("user","username",$peer['username']);
				print_message("userrecord:".$userrecord->id,"opos");
				print_message("request:".$request_url,"opos");
				// file_get_contents replacement for json
				$ch = curl_init();
				$timeout = 10; // set to zero for no timeout
				curl_setopt ($ch, CURLOPT_URL, $request_url);
				curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
				$file_contents = curl_exec($ch); // take out the spaces of curl statement!!
				curl_close($ch);
				
				$json = $file_contents;
				$json_a=json_decode($json,true);
				print_message("json_decoded:".$json_a,"opos"); 
				print_message("json:".$json,"opos");
				print_message("file_contents:".$file_contents,"opos");
				//Set array of apps to check
				$applications=array("http://www.facebook.com","http://twitter.com"); 
				
			//	if (!empty($json_a)) {
					foreach($applications as $apptoCheck) {
				$dom=parse_url($apptoCheck);
					$appname=str_replace(array("www.",".com"),"",$dom['host']);
	
					$found[$appname]=false;

					// Check each user for a app
					foreach ($json_a['onlinepresences'] as $apps) {
					$imgstatus="";
					if ($apptoCheck==$apps['application']) {
						if ($apps['onlinestatus']=="" && $appname!="twitter") 
							$imgstatus="_off";
						else 
							$imgstatus="";
							
						$link["facebook"]='javascript: void(0)" 
											onclick="window.open(\''.$CFG->wwwroot.'/blocks/onlinepresence/fbapp.php?id='.$apps['accountname'].'&moodleuser='.$userrecord->id.'\', 
											\'fbmessage\', \'width=600, height=350\');return false;';
						$link["twitter"]=$apptoCheck.'/'.$apps['accountname'].'" target="_blank" ';
						
						$content->text .= '<a  href="'.$link[$appname].'"  style="line-height: 1.2em">';						
						$content->text .= '<img src="'.$CFG->wwwroot.'/blocks/onlinepresence/img/'.$appname.$imgstatus.'.gif" height="16" alt="'.$appname.'" title="status: '.$apps['customMessage']['content'].'">';//.$appname;
						$content->text .= '</a> ';
							
						
						$found[$appname]=true; // Set true when user has appl. info in JSON
						}
					}
						if (!$found[$appname])
						$content->text .= '<img src="'.$CFG->wwwroot.'/blocks/onlinepresence/img/'.$appname.'_noacc.gif" height="16" title="no account"> ';								
				}
				$content->text .= '<div class="user" style="display:inline;padding-left:2px"><a href="'.$CFG->wwwroot.'/user/view.php?id='.$userrecord->id.'&amp;course='.SITEID.'" title="user">'.print_user_picture($userrecord->id,  $COURSE->id, $userrecord->picture, 16, true, false, '', false);
				$content->text .=$userrecord->firstname . ' ' . $userrecord->lastname ; 
                 $content->text .='</a><div>';

			//	}
			}
			$content->text .= '</div></li>'; 
		$content->text .= '</ul>'; 
		} else {
            $content->text .= '<div class="info">';
            $content->text .= get_string('nousers','block_onlinepresence');
            $content->text .= '</div>'; 
		}
		$content->text.="<br/><center><a title=\"DEPTHS setup\"".
                               " href=\"$CFG->wwwroot/depths/setup/configureopos.php\"> Configure your account </a></center>";


echo $content->text;
?>
</body>
</html>
