// $Id: TabDepthsProps.java 50003 2008-04-13 21:58:15Z Zoran $ 
// Copyright (c) 2007-2008 Jeremic Zoran, University of Belgrade

package depths.configuration;

import java.beans.PropertyChangeEvent;

import org.argouml.configuration.ConfigurationKey;

public class DepthsConfigurationKeyImpl implements ConfigurationKey {
	/**
	 * The string value for the key.
	 */
	private String key = null;

	/**
	 * Create a single component configuration key.
	 * 
	 * @param k1
	 *            key component.
	 */
	public DepthsConfigurationKeyImpl(String k1) {
		key = "depths." + k1;
	}

	/**
	 * Create a sub-component of an existing configuration key.
	 * 
	 * @param ck
	 *            configuration key
	 * @param k1
	 *            additional key component.
	 */
	public DepthsConfigurationKeyImpl(ConfigurationKey ck, String k1) {
		key = ck.getKey() + "." + k1;
	}

	/**
	 * Create a two-component configuration key.
	 * 
	 * @param k1
	 *            key component 1.
	 * @param k2
	 *            key component 2.
	 */
	public DepthsConfigurationKeyImpl(String k1, String k2) {
		key = "depths." + k1 + "." + k2;
	}

	/**
	 * Create a three-component configuration key.
	 * 
	 * @param k1
	 *            key component 1.
	 * @param k2
	 *            key component 2.
	 * @param k3
	 *            key component 3.
	 */
	public DepthsConfigurationKeyImpl(String k1, String k2, String k3) {
		key = "depths." + k1 + "." + k2 + "." + k3;
	}

	/**
	 * Create a four-component configuration key.
	 * 
	 * @param k1
	 *            key component 1.
	 * @param k2
	 *            key component 2.
	 * @param k3
	 *            key component 3.
	 * @param k4
	 *            key component 4.
	 */
	public DepthsConfigurationKeyImpl(String k1, String k2, String k3, String k4) {
		key = "depths." + k1 + "." + k2 + "." + k3 + "." + k4;
	}

	/**
	 * Create a five-component configuration key.
	 * 
	 * @param k1
	 *            key component 1.
	 * @param k2
	 *            key component 2.
	 * @param k3
	 *            key component 3.
	 * @param k4
	 *            key component 4.
	 * @param k5
	 *            key component 5.
	 */
	public DepthsConfigurationKeyImpl(String k1, String k2, String k3,
			String k4, String k5) {
		key = "depths." + k1 + "." + k2 + "." + k3 + "." + k4 + "." + k5;
	}

	/**
	 * Return the actual key used to access the configuration.
	 * 
	 * @return the key
	 */
	public final String getKey() {
		return key;
	}

	/**
	 * Compare the configuration key to a string.
	 * 
	 * @param pce
	 *            PropertyChangeEvent to check
	 * @return true if the changed property is for the key.
	 */
	public boolean isChangedProperty(PropertyChangeEvent pce) {
		if (pce == null) {
			return false;
		}
		return pce.getPropertyName().equals(key);
	}

	/**
	 * Returns a formatted key.
	 * 
	 * @return a formatted key string.
	 */
	public String toString() {
		return "{ConfigurationKeyImpl:" + key + "}";
	}
}
