package depths.uml.ui.courses;

import java.awt.GridBagLayout;
import javax.swing.JPanel;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.GridBagConstraints;
import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JList;
import javax.swing.JTextField;
import javax.swing.JTextArea;
import javax.swing.ListSelectionModel;

import java.awt.ComponentOrientation;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.StyleSheet;

import depths.json.course.Course;
import depths.json.designProblem.DesignProblem;
import depths.json.project.Project;
import depths.json.service.DepthsJSONCourse;
import depths.json.service.DepthsJSONDesignProblem;
import depths.json.service.DepthsJSONProject;
import depths.utility.ConvertArrayListToJListModel;
import java.awt.Color;

public class CoursePanel extends JPanel {

	private String selCourseUri;
	private String selDesignProblemUri;
	private String selProjectUri;  //  @jve:decl-index=0:

	private static final long serialVersionUID = 1L;
	private JPanel coursePanel = null;
	private JPanel problemPanel = null;
	private JPanel projectPanel = null;
	private JPanel courseListPanel = null;
	private JPanel problemListPanel = null;
	private JPanel problemDescriptionPanel = null;
	private JPanel projectListPanel = null;
	private JPanel projectDescriptionPanel = null;
	private JLabel CourseLabel = null;
	private JScrollPane courseScrollPane = null;
	private JList courseList = null;
	private JLabel problemLabel = null;
	private JScrollPane problemScrollPane = null;
	private JList problemList = null;
	private JLabel projectLabel = null;
	private JScrollPane projectScrollPane = null;
	private JList projectList = null;
	private JLabel courseDescriptionLabel = null;
	private JPanel courseDescriptionFieldsPanel = null;
	private JLabel numberOfDiagramLabel = null;
	private JTextField numberOfDiagramTextField = null;
	private JLabel descriptionCourseLabel = null;
	private JScrollPane descriptionCourseScrollPane = null;
	private JTextArea descriptionCourseTextArea = null;
	private JLabel problemDateLabel = null;
	private JTextField problemDateTextField = null;
	private JLabel problemDescriptionLabel = null;
	private JScrollPane problemDescriptionScrollPane = null;
	private JTextArea problemDescriptionTextArea = null;
	private JLabel projectDateLabel = null;
	private JTextField projectDateTextField = null;
	private JLabel projectCreatorLabel = null;
	private JTextField projectCreatorTextField = null;
	private JLabel projectFileLabel = null;
	private JTextField projectFileTextField = null;
	private JButton openProjectButton = null;
	private JLabel sepLabel = null;
	private JLabel sep2Label = null;
	private JButton createProjectButton = null;

	private DefaultListModel courses = new DefaultListModel();
	private DefaultListModel problems = new DefaultListModel();
	private DefaultListModel projects = new DefaultListModel();

	/**
	 * This is the default constructor
	 */
	public CoursePanel() {
		super();
		initialize();
	}

	/**
	 * This method initializes this
	 * 
	 * @return void
	 */
	private void initialize() {
		courses = ConvertArrayListToJListModel.convert(DepthsJSONCourse
				.listAllCourse());

		GridLayout gridLayout = new GridLayout();
		gridLayout.setRows(1);
		this.setLayout(gridLayout);
		this.setSize(1006, 394);
		this.add(getCoursePanel(), null);
		this.add(getProblemPanel(), null);
		this.add(getProjectPanel(), null);

		courseList.setModel(courses);

	}

	/**
	 * This method initializes coursePanel
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getCoursePanel() {
		if (coursePanel == null) {
			GridLayout gridLayout1 = new GridLayout();
			gridLayout1.setRows(2);
			coursePanel = new JPanel();
			coursePanel.setLayout(gridLayout1);
			coursePanel.add(getCourseListPanel(), null);
			coursePanel.add(getCourseDescriptionFieldsPanel(), null);
		}
		return coursePanel;
	}

	/**
	 * This method initializes problemPanel
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getProblemPanel() {
		if (problemPanel == null) {
			GridLayout gridLayout2 = new GridLayout();
			gridLayout2.setRows(2);
			problemPanel = new JPanel();
			problemPanel.setLayout(gridLayout2);
			problemPanel.add(getProblemListPanel(), null);
			problemPanel.add(getProblemDescriptionPanel(), null);
		}
		return problemPanel;
	}

	/**
	 * This method initializes projectPanel
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getProjectPanel() {
		if (projectPanel == null) {
			GridLayout gridLayout3 = new GridLayout();
			gridLayout3.setRows(2);
			projectPanel = new JPanel();
			projectPanel.setLayout(gridLayout3);
			projectPanel.add(getProjectListPanel(), null);
			projectPanel.add(getProjectDescriptionPanel(), null);
		}
		return projectPanel;
	}

	/**
	 * This method initializes courseListPanel
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getCourseListPanel() {
		if (courseListPanel == null) {
			GridBagConstraints gridBagConstraints1 = new GridBagConstraints();
			gridBagConstraints1.fill = GridBagConstraints.BOTH;
			gridBagConstraints1.gridy = 1;
			gridBagConstraints1.weightx = 1.0;
			gridBagConstraints1.weighty = 1.0;
			gridBagConstraints1.gridx = 0;
			GridBagConstraints gridBagConstraints = new GridBagConstraints();
			gridBagConstraints.gridx = 0;
			gridBagConstraints.gridy = 0;
			CourseLabel = new JLabel();
			CourseLabel.setText("COURSES");
			courseListPanel = new JPanel();
			courseListPanel.setLayout(new GridBagLayout());
			courseListPanel.add(CourseLabel, gridBagConstraints);
			courseListPanel.add(getCourseScrollPane(), gridBagConstraints1);
		}
		return courseListPanel;
	}

	/**
	 * This method initializes courseDescriptionPanel
	 * 
	 * @return javax.swing.JPanel
	 */

	/**
	 * This method initializes problemListPanel
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getProblemListPanel() {
		if (problemListPanel == null) {
			GridBagConstraints gridBagConstraints3 = new GridBagConstraints();
			gridBagConstraints3.fill = GridBagConstraints.BOTH;
			gridBagConstraints3.gridy = 1;
			gridBagConstraints3.weightx = 1.0;
			gridBagConstraints3.weighty = 1.0;
			gridBagConstraints3.gridx = 0;
			GridBagConstraints gridBagConstraints2 = new GridBagConstraints();
			gridBagConstraints2.gridx = 0;
			gridBagConstraints2.gridy = 0;
			problemLabel = new JLabel();
			problemLabel.setText("DESING PROBLEMS FOR SELECTED COURSE");
			problemListPanel = new JPanel();
			problemListPanel.setLayout(new GridBagLayout());
			problemListPanel.add(problemLabel, gridBagConstraints2);
			problemListPanel.add(getProblemScrollPane(), gridBagConstraints3);
		}
		return problemListPanel;
	}

	/**
	 * This method initializes problemDescriptionPanel
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getProblemDescriptionPanel() {
		if (problemDescriptionPanel == null) {
			GridBagConstraints gridBagConstraints12 = new GridBagConstraints();
			gridBagConstraints12.fill = GridBagConstraints.BOTH;
			gridBagConstraints12.gridy = 3;
			gridBagConstraints12.weightx = 1.0;
			gridBagConstraints12.weighty = 1.0;
			gridBagConstraints12.gridx = 0;
			GridBagConstraints gridBagConstraints11 = new GridBagConstraints();
			gridBagConstraints11.gridx = 0;
			gridBagConstraints11.gridy = 2;
			problemDescriptionLabel = new JLabel();
			problemDescriptionLabel
					.setText("Description of selected design problem:");
			GridBagConstraints gridBagConstraints10 = new GridBagConstraints();
			gridBagConstraints10.fill = GridBagConstraints.VERTICAL;
			gridBagConstraints10.gridy = 1;
			gridBagConstraints10.ipadx = 330;
			gridBagConstraints10.weightx = 1.0;
			gridBagConstraints10.gridx = 0;
			GridBagConstraints gridBagConstraints9 = new GridBagConstraints();
			gridBagConstraints9.gridx = 0;
			gridBagConstraints9.gridy = 0;
			problemDateLabel = new JLabel();
			problemDateLabel.setText("Date created selected design problem:");
			problemDescriptionPanel = new JPanel();
			problemDescriptionPanel.setLayout(new GridBagLayout());
			problemDescriptionPanel.add(problemDateLabel, gridBagConstraints9);
			problemDescriptionPanel.add(getProblemDateTextField(),
					gridBagConstraints10);
			problemDescriptionPanel.add(problemDescriptionLabel,
					gridBagConstraints11);
			problemDescriptionPanel.add(getProblemDescriptionScrollPane(),
					gridBagConstraints12);
		}
		return problemDescriptionPanel;
	}

	/**
	 * This method initializes projectListPanel
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getProjectListPanel() {
		if (projectListPanel == null) {
			GridBagConstraints gridBagConstraints5 = new GridBagConstraints();
			gridBagConstraints5.fill = GridBagConstraints.BOTH;
			gridBagConstraints5.gridy = 1;
			gridBagConstraints5.weightx = 1.0;
			gridBagConstraints5.weighty = 1.0;
			gridBagConstraints5.gridx = 0;
			GridBagConstraints gridBagConstraints4 = new GridBagConstraints();
			gridBagConstraints4.gridx = 0;
			gridBagConstraints4.gridy = 0;
			projectLabel = new JLabel();
			projectLabel.setText("PROJECTS FOR SELECTED DESIGN PROBLEM");
			projectListPanel = new JPanel();
			projectListPanel.setLayout(new GridBagLayout());
			projectListPanel.add(projectLabel, gridBagConstraints4);
			projectListPanel.add(getProjectScrollPane(), gridBagConstraints5);
		}
		return projectListPanel;
	}

	/**
	 * This method initializes projectDescriptionPanel
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getProjectDescriptionPanel() {
		if (projectDescriptionPanel == null) {
			GridBagConstraints gridBagConstraints22 = new GridBagConstraints();
			gridBagConstraints22.gridx = 0;
			gridBagConstraints22.gridy = 7;
			GridBagConstraints gridBagConstraints21 = new GridBagConstraints();
			gridBagConstraints21.gridx = 0;
			gridBagConstraints21.gridy = 8;
			sep2Label = new JLabel();
			sep2Label.setText(" ");
			GridBagConstraints gridBagConstraints20 = new GridBagConstraints();
			gridBagConstraints20.gridx = 0;
			gridBagConstraints20.gridy = 6;
			sepLabel = new JLabel();
			sepLabel.setText(" ");
			GridBagConstraints gridBagConstraints19 = new GridBagConstraints();
			gridBagConstraints19.gridx = 0;
			gridBagConstraints19.gridy = 9;
			GridBagConstraints gridBagConstraints18 = new GridBagConstraints();
			gridBagConstraints18.fill = GridBagConstraints.VERTICAL;
			gridBagConstraints18.gridy = 5;
			gridBagConstraints18.weightx = 1.0;
			gridBagConstraints18.gridx = 0;
			gridBagConstraints18.ipadx = 330;
			GridBagConstraints gridBagConstraints17 = new GridBagConstraints();
			gridBagConstraints17.gridx = 0;
			gridBagConstraints17.gridy = 4;
			projectFileLabel = new JLabel();
			projectFileLabel.setText("File URL selected project:");
			GridBagConstraints gridBagConstraints16 = new GridBagConstraints();
			gridBagConstraints16.fill = GridBagConstraints.VERTICAL;
			gridBagConstraints16.gridy = 3;
			gridBagConstraints16.weightx = 1.0;
			gridBagConstraints16.gridx = 0;
			gridBagConstraints16.ipadx = 330;
			GridBagConstraints gridBagConstraints15 = new GridBagConstraints();
			gridBagConstraints15.gridx = 0;
			gridBagConstraints15.gridy = 2;
			projectCreatorLabel = new JLabel();
			projectCreatorLabel.setText("Creator name selected project:");
			GridBagConstraints gridBagConstraints14 = new GridBagConstraints();
			gridBagConstraints14.fill = GridBagConstraints.VERTICAL;
			gridBagConstraints14.gridy = 1;
			gridBagConstraints14.ipadx = 330;
			gridBagConstraints14.weightx = 1.0;
			gridBagConstraints14.gridx = 0;
			GridBagConstraints gridBagConstraints13 = new GridBagConstraints();
			gridBagConstraints13.gridx = 0;
			gridBagConstraints13.gridy = 0;
			projectDateLabel = new JLabel();
			projectDateLabel.setText("Date created selected project:");
			projectDescriptionPanel = new JPanel();
			projectDescriptionPanel.setLayout(new GridBagLayout());
			projectDescriptionPanel.add(projectDateLabel, gridBagConstraints13);
			projectDescriptionPanel.add(getProjectDateTextField(),
					gridBagConstraints14);
			projectDescriptionPanel.add(projectCreatorLabel,
					gridBagConstraints15);
			projectDescriptionPanel.add(getProjectCreatorTextField(),
					gridBagConstraints16);
			projectDescriptionPanel.add(projectFileLabel, gridBagConstraints17);
			projectDescriptionPanel.add(getProjectFileTextField(),
					gridBagConstraints18);
			projectDescriptionPanel.add(sepLabel, gridBagConstraints20);
			projectDescriptionPanel.add(getOpenProjectButton(),
					gridBagConstraints19);
			projectDescriptionPanel.add(sep2Label, gridBagConstraints21);
			projectDescriptionPanel.add(getCreateProjectButton(),
					gridBagConstraints22);
		}
		return projectDescriptionPanel;
	}

	/**
	 * This method initializes courseScrollPane
	 * 
	 * @return javax.swing.JScrollPane
	 */
	private JScrollPane getCourseScrollPane() {
		if (courseScrollPane == null) {
			courseScrollPane = new JScrollPane();
			courseScrollPane.setViewportView(getCourseList());
		}
		return courseScrollPane;
	}

	/**
	 * This method initializes courseList
	 * 
	 * @return javax.swing.JList
	 */
	private JList getCourseList() {
		if (courseList == null) {
			courseList = new JList();
			courseList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			courseList
					.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
						public void valueChanged(
								javax.swing.event.ListSelectionEvent e) {
							openProjectButton.setEnabled(false);
							createProjectButton.setEnabled(false);

							projectList.removeAll();
							problemList.removeAll();

							problemList.setModel(new DefaultListModel());
							projectList.setModel(new DefaultListModel());

							if (courseList.getSelectedIndex() > -1) {
								Course c = (Course) courseList
										.getSelectedValue();
								descriptionCourseTextArea.setText(c
										.getDescription());
								selCourseUri = c.getUri();
								ArrayList<DesignProblem> list = DepthsJSONDesignProblem
										.listAllDesignProblem(c.getUri());
								problems = ConvertArrayListToJListModel
										.convert(list);
								problemList.setModel(problems);
								if (list != null) {
									if (list.size() > 0) {
										createProjectButton.setEnabled(true);
									}
								}

								selCourseUri = c.getUri();
								problemDateTextField.setText("");
								problemDescriptionTextArea.setText("");
								projectCreatorTextField.setText("");
								projectDateTextField.setText("");
								projectFileTextField.setText("");

							}
						}
					});
		}
		return courseList;
	}

	/**
	 * This method initializes problemScrollPane
	 * 
	 * @return javax.swing.JScrollPane
	 */
	private JScrollPane getProblemScrollPane() {
		if (problemScrollPane == null) {
			problemScrollPane = new JScrollPane();
			problemScrollPane.setViewportView(getProblemList());
		}
		return problemScrollPane;
	}

	/**
	 * This method initializes problemList
	 * 
	 * @return javax.swing.JList
	 */
	private JList getProblemList() {
		if (problemList == null) {
			problemList = new JList();
			problemList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			problemList
					.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
						public void valueChanged(
								javax.swing.event.ListSelectionEvent e) {
							openProjectButton.setEnabled(false);
							projectList.removeAll();

							projectList.setModel(new DefaultListModel());

							if (problemList.getSelectedIndex() > -1) {
								DesignProblem d = (DesignProblem) problemList
										.getSelectedValue();
								selDesignProblemUri = d.getUri();
								problemDateTextField.setText(d.getDateCreated());
								problemDescriptionTextArea.setText(d
										.getDescription());

								projectCreatorTextField.setText("");
								projectDateTextField.setText("");
								projectFileTextField.setText("");

								selDesignProblemUri = d.getUri();
								ArrayList<Project> l = DepthsJSONProject
										.listAllProjectForDesignProblem(d
												.getUri());
								projects = ConvertArrayListToJListModel
										.convert(l);
								projectList.setModel(projects);
								if (l != null) {
									if (l.size() > 0) {
										openProjectButton.setEnabled(true);
									}
								}

							}
						}
					});

		}
		return problemList;
	}

	/**
	 * This method initializes projectScrollPane
	 * 
	 * @return javax.swing.JScrollPane
	 */
	private JScrollPane getProjectScrollPane() {
		if (projectScrollPane == null) {
			projectScrollPane = new JScrollPane();
			projectScrollPane.setViewportView(getProjectList());
		}
		return projectScrollPane;
	}

	/**
	 * This method initializes projectList
	 * 
	 * @return javax.swing.JList
	 */
	private JList getProjectList() {
		if (projectList == null) {
			projectList = new JList();
			projectList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			projectList
					.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
						public void valueChanged(
								javax.swing.event.ListSelectionEvent e) {
							if (projectList.getSelectedIndex() > -1) {
								Project p = (Project) projectList
										.getSelectedValue();

								selProjectUri = p.getProjecturi();
								projectDateTextField.setText(p
										.getDatetimecreated());
								projectCreatorTextField.setText(p
										.getCreatorname());
								projectFileTextField.setText(p.getFileurl());

								openProjectButton.setEnabled(true);
								createProjectButton.setEnabled(true);
							}
						}
					});
		}
		return projectList;
	}

	/**
	 * This method initializes courseDescriptionFieldsPanel
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getCourseDescriptionFieldsPanel() {
		if (courseDescriptionFieldsPanel == null) {
			GridBagConstraints gridBagConstraints8 = new GridBagConstraints();
			gridBagConstraints8.fill = GridBagConstraints.BOTH;
			gridBagConstraints8.gridy = 3;
			gridBagConstraints8.weightx = 1.0;
			gridBagConstraints8.weighty = 1.0;
			gridBagConstraints8.gridx = 0;
			GridBagConstraints gridBagConstraints7 = new GridBagConstraints();
			gridBagConstraints7.gridx = 0;
			gridBagConstraints7.gridy = 2;
			descriptionCourseLabel = new JLabel();
			descriptionCourseLabel.setText("Description of selected course:");
			GridBagConstraints gridBagConstraints6 = new GridBagConstraints();
			gridBagConstraints6.fill = GridBagConstraints.VERTICAL;
			gridBagConstraints6.gridy = 1;
			gridBagConstraints6.ipadx = 330;
			gridBagConstraints6.weightx = 1.0;
			gridBagConstraints6.gridx = 0;
			numberOfDiagramLabel = new JLabel();
			numberOfDiagramLabel
					.setText("Number of problem diagrams in selected course:");
			numberOfDiagramLabel.setVisible(false);
			courseDescriptionFieldsPanel = new JPanel();
			courseDescriptionFieldsPanel.setLayout(new GridBagLayout());
			courseDescriptionFieldsPanel.add(numberOfDiagramLabel,
					new GridBagConstraints());
			courseDescriptionFieldsPanel.add(getNumberOfDiagramTextField(),
					gridBagConstraints6);
			courseDescriptionFieldsPanel.add(descriptionCourseLabel,
					gridBagConstraints7);
			courseDescriptionFieldsPanel.add(getDescriptionCourseScrollPane(),
					gridBagConstraints8);
		}
		return courseDescriptionFieldsPanel;
	}

	/**
	 * This method initializes numberOfDiagramTextField
	 * 
	 * @return javax.swing.JTextField
	 */
	private JTextField getNumberOfDiagramTextField() {
		if (numberOfDiagramTextField == null) {
			numberOfDiagramTextField = new JTextField();
			numberOfDiagramTextField.setEnabled(false);
			numberOfDiagramTextField.setVisible(false);
		}
		return numberOfDiagramTextField;
	}

	/**
	 * This method initializes descriptionCourseScrollPane
	 * 
	 * @return javax.swing.JScrollPane
	 */
	private JScrollPane getDescriptionCourseScrollPane() {
		if (descriptionCourseScrollPane == null) {
			descriptionCourseScrollPane = new JScrollPane();
			descriptionCourseScrollPane
					.setViewportView(getDescriptionCourseTextArea());
		}
		return descriptionCourseScrollPane;
	}

	/**
	 * This method initializes descriptionCourseTextArea
	 * 
	 * @return javax.swing.JTextArea
	 */
	private JTextArea getDescriptionCourseTextArea() {
		if (descriptionCourseTextArea == null) {
			descriptionCourseTextArea=new JTextArea();
			/*
			descriptionCourseTextArea = new JEditorPane("text/html", "");
			StyleSheet css = ((HTMLEditorKit) descriptionCourseTextArea.getEditorKit())
					.getStyleSheet();
			Style style = css.getStyle("body");
			StyleConstants.setRightIndent(style, (float) (2.0));
			StyleConstants.setLeftIndent(style, (float) (2.0));
			StyleConstants.setSpaceBelow(style, (float) (-2.0));
			StyleConstants.setSpaceAbove(style, (float) (-2.0));
			// dscProblemReletedLinks.setRows(2);
			 * 
			 */
			descriptionCourseTextArea.setEditable(false);
			
		}
		return descriptionCourseTextArea;
	}

	/**
	 * This method initializes problemDateTextField
	 * 
	 * @return javax.swing.JTextField
	 */
	private JTextField getProblemDateTextField() {
		if (problemDateTextField == null) {
			problemDateTextField = new JTextField();
			problemDateTextField.setEnabled(false);
		}
		return problemDateTextField;
	}

	/**
	 * This method initializes problemDescriptionScrollPane
	 * 
	 * @return javax.swing.JScrollPane
	 */
	private JScrollPane getProblemDescriptionScrollPane() {
		if (problemDescriptionScrollPane == null) {
			problemDescriptionScrollPane = new JScrollPane();
			problemDescriptionScrollPane
					.setViewportView(getProblemDescriptionTextArea());
		}
		return problemDescriptionScrollPane;
	}

	/**
	 * This method initializes problemDescriptionTextArea
	 * 
	 * @return javax.swing.JTextArea
	 */
	private JTextArea getProblemDescriptionTextArea() {
		if (problemDescriptionTextArea == null) {
			problemDescriptionTextArea= new JTextArea();
		/*	problemDescriptionTextArea = new JEditorPane("text/html", "");
			StyleSheet css = ((HTMLEditorKit) problemDescriptionTextArea.getEditorKit())
					.getStyleSheet();
			Style style = css.getStyle("body");
			StyleConstants.setRightIndent(style, (float) (2.0));
			StyleConstants.setLeftIndent(style, (float) (2.0));
			StyleConstants.setSpaceBelow(style, (float) (-2.0));
			StyleConstants.setSpaceAbove(style, (float) (-2.0));
			// dscProblemReletedLinks.setRows(2);
			 * 
			 */
			problemDescriptionTextArea.setEditable(false);
		}
		return problemDescriptionTextArea;
	}

	/**
	 * This method initializes projectDateTextField
	 * 
	 * @return javax.swing.JTextField
	 */
	private JTextField getProjectDateTextField() {
		if (projectDateTextField == null) {
			projectDateTextField = new JTextField();
			projectDateTextField.setEnabled(false);
		}
		return projectDateTextField;
	}

	/**
	 * This method initializes projectCreatorTextField
	 * 
	 * @return javax.swing.JTextField
	 */
	private JTextField getProjectCreatorTextField() {
		if (projectCreatorTextField == null) {
			projectCreatorTextField = new JTextField();
			projectCreatorTextField.setEnabled(false);
		}
		return projectCreatorTextField;
	}

	/**
	 * This method initializes projectFileTextField
	 * 
	 * @return javax.swing.JTextField
	 */
	private JTextField getProjectFileTextField() {
		if (projectFileTextField == null) {
			projectFileTextField = new JTextField();
			projectFileTextField.setEnabled(false);
		}
		return projectFileTextField;
	}

	/**
	 * This method initializes openProjectButton
	 * 
	 * @return javax.swing.JButton
	 */
	private JButton getOpenProjectButton() {
		if (openProjectButton == null) {
			openProjectButton = new JButton();
			openProjectButton.setText("OPEN SELECTED PROJECT");
			openProjectButton.setEnabled(false);

		}
		return openProjectButton;
	}

	/**
	 * This method initializes createProjectButton
	 * 
	 * @return javax.swing.JButton
	 */
	private JButton getCreateProjectButton() {
		if (createProjectButton == null) {
			createProjectButton = new JButton();
			createProjectButton
					.setText("CREATE NEW PROJECT FOR SELECTED PROBLEM");
			createProjectButton.setEnabled(false);

		}
		return createProjectButton;
	}

	public String getSelCourseUri() {
		return selCourseUri;
	}

	public void setSelCourseUri(String selCourseUri) {
		this.selCourseUri = selCourseUri;
	}

	public String getSelDesignProblemUri() {
		return selDesignProblemUri;
	}

	public void setSelDesignProblemUri(String selDesignProblemUri) {
		this.selDesignProblemUri = selDesignProblemUri;
	}

	public String getSelProjectUri() {
		return selProjectUri;
	}

	public void setSelProjectUri(String selProjectUri) {
		this.selProjectUri = selProjectUri;
	}

	public JButton retrunJButtoNewProject() {
		return getCreateProjectButton();
	}

	public JButton retrunJButtoOpenProject() {
		return getOpenProjectButton();
	}

} // @jve:decl-index=0:visual-constraint="10,10"
