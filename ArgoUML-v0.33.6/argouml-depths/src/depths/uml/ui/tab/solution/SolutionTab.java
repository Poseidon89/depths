package depths.uml.ui.tab.solution;

import javax.swing.ImageIcon;

import org.apache.log4j.Logger;

import depths.i18n.DepthsTranslator;
import depths.uml.ui.tab.description.DescriptionTab;

public class SolutionTab extends SolutionTabModel {

	private static final long serialVersionUID = -792371915290105527L;

	private static final Logger LOG = Logger.getLogger(SolutionTab.class);

	public SolutionTab() {
		super(DepthsTranslator.localize("depths_label.solution"),
				lookupIcon("ClassDiagram"));
	}

	public String getDescription() {
		return description.getText();
	}

	public String getDesignRules() {
		return designRules.getText();
	}

	public String getDesignConstraints() {
		return designConstraints.getText();
	}

	public String getAdditionalRequirements() {
		return additionalRequirements.getText();
	}

	public String getConsequences() {
		return consequences.getText();
	}

	public String getPros() {
		return pros.getText();
	}

	public String getCons() {
		return cons.getText();
	}

	public void setDescription(String text) {
		description.setText(text);
	}

	public void setDesignRules(String text) {
		designRules.setText(text);
	}

	public void setDesignConstraints(String text) {
		designConstraints.setText(text);
	}

	public void setAdditionalRequirements(String text) {
		additionalRequirements.setText(text);
	}

	public void setConsequences(String text) {
		consequences.setText(text);
	}

	public void setPros(String text) {
		pros.setText(text);
	}

	public void setCons(String text) {
		cons.setText(text);
	}

}
