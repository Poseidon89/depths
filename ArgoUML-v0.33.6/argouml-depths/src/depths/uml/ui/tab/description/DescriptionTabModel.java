// Copyright (c) 2011 Dzenan Strujic, University "Mediterranean" , Podgorica Montenegro

package depths.uml.ui.tab.description;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import javax.swing.ImageIcon;
import javax.swing.JEditorPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.StyleSheet;

import org.argouml.configuration.Configuration;
import org.argouml.swingext.UpArrowIcon;
import org.argouml.uml.ui.PropPanel;
import org.argouml.uml.ui.UMLModelElementTaggedValueDocument;
import org.argouml.uml.ui.UMLTextArea2;
import org.tigris.swidgets.Horizontal;
import org.tigris.swidgets.Vertical;

import depths.application.api.DepthsArgo;
import depths.i18n.DepthsTranslator;

public class DescriptionTabModel extends PropPanel {

	private static String orientations = Configuration.getString(Configuration
			.makeKey("layouts", "tabdocumentations"));

	JTextArea dscProblem, dscProblemTitle, dscProblemAuthorName;
	JEditorPane dscProblemReletedLinks;

	/**
	 * Construct a property panel with a given name and icon.
	 * 
	 * @param diagramName
	 *            the diagram name to use as the title of the panel
	 * @param icon
	 *            an icon to display on the panel
	 */
	protected DescriptionTabModel(String diagramName, ImageIcon icon) {
		// super(diagramName, icon);
		super(diagramName, icon);
		//
		setOrientation((orientations.equals("West") || orientations
				.equals("East")) ? Vertical.getInstance() : Horizontal
				.getInstance());
		setIcon(new UpArrowIcon());

		dscProblemTitle = new JTextArea();
		dscProblemTitle.setRows(1);
		dscProblemTitle.setLineWrap(true);
		dscProblemTitle.setWrapStyleWord(true);
		JScrollPane spTitle = new JScrollPane();
		spTitle.getViewport().add(dscProblemTitle);
		addField(
				DepthsTranslator
						.localize("depths_problem_description.problem_title"),
				spTitle);

		dscProblem = new JTextArea();
		dscProblem.setRows(4);
		dscProblem.setLineWrap(true);
		dscProblem.setWrapStyleWord(true);
		JScrollPane spProblemDescription = new JScrollPane();
		spProblemDescription.getViewport().add(dscProblem);
		addField(DepthsTranslator.localize("depths_problem_description.title"),
				spProblemDescription);

		dscProblemAuthorName = new JTextArea();
		dscProblemAuthorName.setRows(1);
		dscProblemAuthorName.setLineWrap(true);
		dscProblemAuthorName.setWrapStyleWord(true);
		JScrollPane spProblemAuthorName = new JScrollPane();
		spProblemAuthorName.getViewport().add(dscProblemAuthorName);
		addField(
				DepthsTranslator.localize("depths_problem_description.author"),
				spProblemAuthorName);

		dscProblemReletedLinks = new JEditorPane("text/html", "");
		StyleSheet css = ((HTMLEditorKit) dscProblemReletedLinks.getEditorKit())
				.getStyleSheet();
		Style style = css.getStyle("body");
		StyleConstants.setRightIndent(style, (float) (2.0));
		StyleConstants.setLeftIndent(style, (float) (2.0));
		StyleConstants.setSpaceBelow(style, (float) (-2.0));
		StyleConstants.setSpaceAbove(style, (float) (-2.0));
		// dscProblemReletedLinks.setRows(2);
		// dscProblemReletedLinks.setLineWrap(true);
		dscProblemReletedLinks.setEditable(false);
//		dscProblemReletedLinks
//				.setText("");
		dscProblemReletedLinks.addHyperlinkListener(new HyperlinkListener() {
			public void hyperlinkUpdate(HyperlinkEvent e) {
				if (e.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
					URI uri;
					try {
						java.awt.Desktop desktop = java.awt.Desktop
								.getDesktop();
						uri = new URI(e.getURL().toString());
						desktop.browse(uri);
					} catch (URISyntaxException e1) {
						e1.printStackTrace();
					} catch (IOException e2) {
						e2.printStackTrace();
					}

				}
			}
		});
		// dscProblemReletedLinks.setWrapStyleWord(true);
		JScrollPane spProblemReletedLinks = new JScrollPane();
		spProblemReletedLinks.getViewport().add(dscProblemReletedLinks);
		addField(DepthsTranslator.localize("depths_problem_description.links"),
				spProblemReletedLinks);

		setButtonPanelSize(18);
	}

}
