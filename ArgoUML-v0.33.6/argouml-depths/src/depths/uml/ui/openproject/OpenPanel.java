// Copyright (c) 2011 Dzenan Strujic, University "Mediterranean" , Podgorica Montenegro

package depths.uml.ui.openproject;

import javax.swing.DefaultListModel;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.ListSelectionModel;

import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import javax.swing.BoxLayout;
import java.awt.GridLayout;
import javax.swing.JComboBox;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JCheckBox;
import java.awt.Dimension;
import javax.swing.JButton;
import java.awt.Insets;
import javax.swing.BorderFactory;

import depths.application.api.DepthsArgo;
import depths.configuration.DepthsConfiguration;
import depths.i18n.DepthsTranslator;
import depths.json.project.Project;
import depths.json.service.DepthsJSONProject;

import java.awt.Color;
import java.util.ArrayList;

public class OpenPanel extends JPanel{
	private String projectURI = ""; // @jve:decl-index=0:
	private String fileName = ""; // @jve:decl-index=0:

	private DefaultListModel listModelAllProjects = null;
	private DefaultListModel listModelMyProjects = null;

	private JPanel centralPanel = null; // @jve:decl-index=0:visual-constraint="4,63"
	private JPanel contentPane = null;
	private JPanel leftCPanel = null;
	private JPanel filterPanel = null;
	private JLabel projectFilterLabel = null;
	private JComboBox projectFilterComboBox = null;
	private JScrollPane projectListsScrollPane = null;
	private JList projectListList = null;
	private JPanel rightCPanel = null;
	private JPanel proDescriptionPanel = null;
	private JLabel titleLabel = null;
	private JTextField titleTextField = null;
	private JLabel dateLabel = null;
	private JTextField dateTextField = null;
	private JLabel idLabel = null;
	private JTextField idTextField = null;
	private JLabel creatorLabel = null;
	private JTextField creatorTextField = null;

	private JPanel proCustomPanel = null;
	private JCheckBox projectCustomCheckBox = null;
	private JTextField fileTextField = null;
	private JButton openProjectButton = null;

	/**
	 * This method initializes jContentPane
	 * 
	 * @return javax.swing.JPanel
	 */

	/**
	 * This method initializes leftPanel
	 * 
	 * @return javax.swing.JPanel
	 */

	/**
	 * This method initializes jPanel
	 * 
	 * @return javax.swing.JPanel
	 */

	/**
	 * This method initializes centralPanel
	 * 
	 * @return javax.swing.JPanel
	 */
	public OpenPanel() {
		GridLayout gridLayout = new GridLayout();
		gridLayout.setRows(1);
		this.setLayout(gridLayout);
		this.add(getCentralPanel(), null);
		this.setSize(858, 232);
	}
	private JPanel getCentralPanel() {
		if (centralPanel == null) {
			centralPanel = new JPanel();
			centralPanel.setLayout(new GridBagLayout());
			centralPanel.setSize(858, 232);
			centralPanel.add(getContentPane(), new GridBagConstraints());
		}
		return centralPanel;
	}

	/**
	 * This method initializes contentPane
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getContentPane() {
		if (contentPane == null) {
			GridLayout gridLayout31 = new GridLayout();
			gridLayout31.setRows(1);
			gridLayout31.setHgap(6);
			contentPane = new JPanel();
			contentPane.setLayout(gridLayout31);
			contentPane.add(getLeftCPanel(), null);
			contentPane.add(getRightCPanel(), null);
		}
		return contentPane;
	}

	/**
	 * This method initializes leftCPanel
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getLeftCPanel() {
		if (leftCPanel == null) {
			leftCPanel = new JPanel();
			leftCPanel.setLayout(new BoxLayout(getLeftCPanel(),
					BoxLayout.Y_AXIS));
			leftCPanel.add(getFilterPanel(), null);
			leftCPanel.add(getProjectListsScrollPane(), null);
		}
		return leftCPanel;
	}

	/**
	 * This method initializes filterPanel
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getFilterPanel() {
		if (filterPanel == null) {
			GridBagConstraints gridBagConstraints91 = new GridBagConstraints();
			gridBagConstraints91.fill = GridBagConstraints.VERTICAL;
			gridBagConstraints91.gridy = -1;
			gridBagConstraints91.ipadx = 171;
			gridBagConstraints91.ipady = 8;
			gridBagConstraints91.weightx = 1.0;
			gridBagConstraints91.gridx = 2;
			GridBagConstraints gridBagConstraints81 = new GridBagConstraints();
			gridBagConstraints81.fill = GridBagConstraints.VERTICAL;
			gridBagConstraints81.gridy = -1;
			gridBagConstraints81.ipadx = 78;
			gridBagConstraints81.ipady = 8;
			gridBagConstraints81.gridx = -1;
			projectFilterLabel = new JLabel();
			projectFilterLabel.setText(DepthsTranslator
					.localize("depths_openproject.filter_label"));
			filterPanel = new JPanel();
			filterPanel.setLayout(new GridBagLayout());
			filterPanel.add(projectFilterLabel, gridBagConstraints81);
			filterPanel.add(getProjectFilterComboBox(), gridBagConstraints91);
		}
		return filterPanel;
	}

	/**
	 * This method initializes projectFilterComboBox
	 * 
	 * @return javax.swing.JComboBox
	 */
	private JComboBox getProjectFilterComboBox() {
		if (projectFilterComboBox == null) {
			projectFilterComboBox = new JComboBox();
			projectFilterComboBox.addItem("select...");
			projectFilterComboBox.addItem("Available projects");
			projectFilterComboBox.addItem("My projects");
			projectFilterComboBox
					.addItemListener(new java.awt.event.ItemListener() {
						public void itemStateChanged(java.awt.event.ItemEvent e) {

							titleTextField.setText("");
							dateTextField.setText("");
							idTextField.setText("");
							creatorTextField.setText("");
							projectURI = "";
							fileName = "";
							String user = DepthsConfiguration
									.getString(DepthsArgo.KEY_USER_USERNAME);
							if (projectFilterComboBox.getSelectedIndex() == 0) {

								projectListList
										.setModel(new DefaultListModel());
							} else if (projectFilterComboBox.getSelectedIndex() == 1) {
								listModelAllProjects = new DefaultListModel();
								ArrayList<depths.json.project.Project> lists = DepthsJSONProject
										.listAllProjects(user);
								if (lists != null) {
									for (int i = 0; i < lists.size(); i++) {
										listModelAllProjects.addElement(lists
												.get(i));
									}
								}

								projectListList.setModel(listModelAllProjects);
							} else if (projectFilterComboBox.getSelectedIndex() == 2) {
								listModelMyProjects = new DefaultListModel();
								ArrayList<depths.json.project.Project> lists = DepthsJSONProject
										.listMyProjects(user);
								if (lists != null) {
									for (int i = 0; i < lists.size(); i++) {
										listModelMyProjects.addElement(lists
												.get(i));
									}
								}
								projectListList.setModel(listModelMyProjects);
							}
						}
					});
		}
		return projectFilterComboBox;
	}

	/**
	 * This method initializes projectListsScrollPane
	 * 
	 * @return javax.swing.JScrollPane
	 */
	private JScrollPane getProjectListsScrollPane() {
		if (projectListsScrollPane == null) {
			projectListsScrollPane = new JScrollPane();
			projectListsScrollPane.setViewportView(getProjectListList());
		}
		return projectListsScrollPane;
	}

	/**
	 * This method initializes projectListList
	 * 
	 * @return javax.swing.JList
	 */
	private JList getProjectListList() {
		if (projectListList == null) {
			projectListList = new JList();
			projectListList
					.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			projectListList
					.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
						public void valueChanged(
								javax.swing.event.ListSelectionEvent e) {
							if (projectListList.getSelectedValue() != null) {

								Project sel = (Project) projectListList
										.getSelectedValue();

								titleTextField.setText(sel.getProjectName());
								dateTextField.setText(sel.getDatetimecreated());
								idTextField.setText(sel.getProjectId());
								fileTextField.setText(sel.getFileurl());

								projectURI = sel.getProjecturi();
								creatorTextField.setText(sel.getCreatorname());
							}
						}
					});
		}
		return projectListList;
	}

	/**
	 * This method initializes rightCPanel
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getRightCPanel() {
		if (rightCPanel == null) {
			rightCPanel = new JPanel();
			rightCPanel.setLayout(new BoxLayout(getRightCPanel(),
					BoxLayout.Y_AXIS));
			rightCPanel.add(getProDescriptionPanel(), null);
			rightCPanel.add(getProCustomPanel(), null);
		}
		return rightCPanel;
	}

	/**
	 * This method initializes proDescriptionPanel
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getProDescriptionPanel() {
		if (proDescriptionPanel == null) {
			GridBagConstraints gridBagConstraints51 = new GridBagConstraints();
			gridBagConstraints51.fill = GridBagConstraints.VERTICAL;
			gridBagConstraints51.gridx = 1;
			gridBagConstraints51.gridy = 2;
			gridBagConstraints51.ipadx = 280;
			gridBagConstraints51.ipady = 9;
			gridBagConstraints51.weightx = 1.0;
			gridBagConstraints51.insets = new Insets(0, 0, 1, 1);

			GridBagConstraints gridBagConstraintsCr = new GridBagConstraints();
			gridBagConstraintsCr.fill = GridBagConstraints.VERTICAL;
			gridBagConstraintsCr.gridx = 1;
			gridBagConstraintsCr.gridy = 3;
			gridBagConstraintsCr.ipadx = 280;
			gridBagConstraintsCr.ipady = 9;
			gridBagConstraintsCr.weightx = 1.0;
			gridBagConstraintsCr.insets = new Insets(0, 0, 1, 1);

			GridBagConstraints gridBagConstraintsCreator = new GridBagConstraints();
			gridBagConstraintsCreator.gridx = 0;
			gridBagConstraintsCreator.gridy = 3;
			gridBagConstraintsCreator.ipadx = 33;
			gridBagConstraintsCreator.ipady = 10;
			// gridBagConstraintsCreator.insets = new Insets(0, 0, 1, 0);

			GridBagConstraints gridBagConstraints41 = new GridBagConstraints();
			gridBagConstraints41.insets = new Insets(0, 0, 1, 0);
			gridBagConstraints41.gridy = 2;
			gridBagConstraints41.ipadx = 33;
			gridBagConstraints41.ipady = 10;
			gridBagConstraints41.gridx = 0;
			idLabel = new JLabel();
			idLabel.setText(DepthsTranslator
					.localize("depths_openproject.id_project"));
			GridBagConstraints gridBagConstraints31 = new GridBagConstraints();
			gridBagConstraints31.fill = GridBagConstraints.VERTICAL;
			gridBagConstraints31.gridx = 1;
			gridBagConstraints31.gridy = 1;
			gridBagConstraints31.ipadx = 280;
			gridBagConstraints31.ipady = 9;
			gridBagConstraints31.weightx = 1.0;
			gridBagConstraints31.insets = new Insets(0, 0, 0, 1);
			GridBagConstraints gridBagConstraints21 = new GridBagConstraints();
			gridBagConstraints21.gridx = 0;
			gridBagConstraints21.ipadx = 33;
			gridBagConstraints21.ipady = 10;
			gridBagConstraints21.gridy = 1;
			dateLabel = new JLabel();
			dateLabel.setText(DepthsTranslator
					.localize("depths_openproject.date_project"));

			GridBagConstraints gridBagConstraints12 = new GridBagConstraints();
			gridBagConstraints12.fill = GridBagConstraints.VERTICAL;
			gridBagConstraints12.gridx = 1;
			gridBagConstraints12.gridy = 0;
			gridBagConstraints12.ipadx = 280;
			gridBagConstraints12.ipady = 9;
			gridBagConstraints12.weightx = 1.0;
			gridBagConstraints12.insets = new Insets(0, 0, 0, 1);
			GridBagConstraints gridBagConstraints10 = new GridBagConstraints();
			gridBagConstraints10.gridx = 0;
			gridBagConstraints10.ipadx = 33;
			gridBagConstraints10.ipady = 10;
			gridBagConstraints10.gridy = 0;
			titleLabel = new JLabel();
			titleLabel.setText(DepthsTranslator
					.localize("depths_openproject.title_project"));

			creatorLabel = new JLabel();
			creatorLabel.setText(DepthsTranslator
					.localize("depths_openproject.creator_project"));

			proDescriptionPanel = new JPanel();
			proDescriptionPanel.setLayout(new GridBagLayout());
			// proDescriptionPanel.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
			proDescriptionPanel.setBorder(BorderFactory.createLineBorder(
					Color.black, 1));
			proDescriptionPanel.add(titleLabel, gridBagConstraints10);
			proDescriptionPanel.add(getTitleTextField(), gridBagConstraints12);
			proDescriptionPanel.add(dateLabel, gridBagConstraints21);
			proDescriptionPanel.add(getDateTextField(), gridBagConstraints31);
			proDescriptionPanel.add(idLabel, gridBagConstraints41);
			proDescriptionPanel.add(getIdTextField(), gridBagConstraints51);
			proDescriptionPanel.add(creatorLabel, gridBagConstraintsCreator);
			proDescriptionPanel
					.add(getCreatorTextField(), gridBagConstraintsCr);
		}
		return proDescriptionPanel;
	}

	/**
	 * This method initializes titleTextField
	 * 
	 * @return javax.swing.JTextField
	 */
	private JTextField getTitleTextField() {
		if (titleTextField == null) {
			titleTextField = new JTextField();
			titleTextField.setEnabled(false);
		}
		return titleTextField;
	}

	/**
	 * This method initializes dateTextField
	 * 
	 * @return javax.swing.JTextField
	 */
	private JTextField getDateTextField() {
		if (dateTextField == null) {
			dateTextField = new JTextField();
			dateTextField.setEnabled(false);
		}
		return dateTextField;
	}

	/**
	 * This method initializes idTextField
	 * 
	 * @return javax.swing.JTextField
	 */
	private JTextField getIdTextField() {
		if (idTextField == null) {
			idTextField = new JTextField();
			idTextField.setEnabled(false);
		}
		return idTextField;
	}

	private JTextField getCreatorTextField() {
		if (creatorTextField == null) {
			creatorTextField = new JTextField();
			creatorTextField.setEnabled(false);
		}
		return creatorTextField;
	}

	/**
	 * This method initializes proCustomPanel
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getProCustomPanel() {
		if (proCustomPanel == null) {
			GridLayout gridLayout1 = new GridLayout();
			gridLayout1.setRows(3);
			gridLayout1.setColumns(2);
			proCustomPanel = new JPanel();
			proCustomPanel.setLayout(gridLayout1);
			proCustomPanel.add(getProjectCustomCheckBox(), null);
			proCustomPanel.add(getFileTextField(), null);
			proCustomPanel.add(getOpenProjectButton(), null);
		}
		return proCustomPanel;
	}

	/**
	 * This method initializes projectCustomCheckBox
	 * 
	 * @return javax.swing.JCheckBox
	 */
	private JCheckBox getProjectCustomCheckBox() {
		if (projectCustomCheckBox == null) {
			projectCustomCheckBox = new JCheckBox();
			projectCustomCheckBox.setText(DepthsTranslator
					.localize("depths_openproject.info_project"));
			projectCustomCheckBox.setVisible(false);
			projectCustomCheckBox
					.addItemListener(new java.awt.event.ItemListener() {
						public void itemStateChanged(java.awt.event.ItemEvent e) {
							if (projectCustomCheckBox.isSelected()) {
								fileTextField.setEnabled(true);
							} else {
								fileTextField.setEnabled(false);
							}
						}
					});

		}
		return projectCustomCheckBox;
	}

	/**
	 * This method initializes fileTextField
	 * 
	 * @return javax.swing.JTextField
	 */
	private JTextField getFileTextField() {
		if (fileTextField == null) {
			fileTextField = new JTextField();
			fileTextField.setEnabled(false);
		}
		return fileTextField;
	}

	/**
	 * This method initializes openProjectButton
	 * 
	 * @return javax.swing.JButton
	 */
	private JButton getOpenProjectButton() {
		if (openProjectButton == null) {
			openProjectButton = new JButton();
			openProjectButton.setText(DepthsTranslator
					.localize("depths_openproject.open_project"));
			fileName = fileTextField.getText();
		}
		return openProjectButton;
	}

	public JPanel retrunContainer() {
		return getCentralPanel();
	}

	public JTextField returnProjectFile() {
		return getFileTextField();
	}

	public JButton returnOpenButton() {
		return getOpenProjectButton();
	}

	public DefaultListModel getListModelAllProjects() {
		return listModelAllProjects;
	}

	public void setListModelAllProjects(DefaultListModel listModelAllProjects) {
		this.listModelAllProjects = listModelAllProjects;
	}

	public DefaultListModel getListModelMyProjects() {
		return listModelMyProjects;
	}

	public void setListModelMyProjects(DefaultListModel listModelMyProjects) {
		this.listModelMyProjects = listModelMyProjects;
	}

	public String getProjectURI() {
		return projectURI;
	}

	public void setProjectURI(String projectURI) {
		this.projectURI = projectURI;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public boolean isSelectedCustom() {
		return projectCustomCheckBox.isSelected();
	}

	public void setDefaultValue() {
		projectFilterComboBox.setSelectedIndex(0);
		titleTextField.setText("");
		dateTextField.setText("");
		idTextField.setText("");
		creatorTextField.setText("");
		fileTextField.setText("");
		projectURI = "";
		fileName = "";
		projectCustomCheckBox.setSelected(false);
		fileTextField.setEnabled(false);
	}
}
