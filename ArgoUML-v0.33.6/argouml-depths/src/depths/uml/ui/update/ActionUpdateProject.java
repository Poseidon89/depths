// $Id: TabDepthsProps.java 50003 2008-04-13 21:58:15Z Zoran $ 
// Copyright (c) 2007-2008 Jeremic Zoran, University of Belgrade 

package depths.uml.ui.update;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.TimerTask;

import javax.swing.AbstractAction;

import javax.swing.JOptionPane;

import org.apache.log4j.Logger;

import org.argouml.application.helpers.ResourceLoaderWrapper;
import org.argouml.cognitive.Designer;
import org.argouml.configuration.Configuration;
import org.argouml.kernel.Project;
import org.argouml.kernel.ProjectManager;
import org.argouml.model.Model;
import org.argouml.persistence.PersistenceManager;
import org.argouml.persistence.ProjectFilePersister;
import org.argouml.ui.ExceptionDialog;
import org.argouml.ui.ProjectBrowser;
import org.argouml.ui.targetmanager.TargetManager;
import org.argouml.uml.ui.SaveGraphicsManager;
import org.argouml.util.ArgoDialog;
import org.argouml.util.ArgoFrame;
import org.tigris.gef.base.SaveGraphicsAction;
import org.tigris.gef.base.Diagram;
import org.tigris.gef.util.Util;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.core.util.MultivaluedMapImpl;

import depths.application.api.DepthsArgo;
import depths.configuration.DepthsConfiguration;
import depths.i18n.DepthsTranslator;
import depths.json.configResources.ConfigURL;
import depths.json.project.ProjectUtility;
import depths.json.service.DepthsJSONProject;
import depths.json.service.DepthsJSONTabService;
import depths.ui.DepthsModule;
import depths.ui.ErrorOpenMenu;
import depths.uml.diagram.DepthsUMLActivityDiagram;
import depths.uml.diagram.DepthsUMLClassDiagram;
import depths.uml.diagram.DepthsUMLCollaborationDiagram;
import depths.uml.diagram.DepthsUMLDeploymentDiagram;
import depths.uml.diagram.DepthsUMLSequenceDiagram;
import depths.uml.diagram.DepthsUMLStatechartDiagram;
import depths.uml.diagram.DepthsUMLUseCaseDiagram;
import depths.uml.ui.FTPConnectionManager;

/**
 * 
 * 
 * @author Zoran
 */
public class ActionUpdateProject extends AbstractAction {

	private static final long serialVersionUID = 3062674953320109889L;
	private ArgoDialog popup = null;

	private String selUri = null;

	private static final Logger LOG = Logger
			.getLogger(ActionUpdateProject.class);

	/**
	 * Constructor for this action.
	 */
	public ActionUpdateProject() {

		super(DepthsTranslator.localize("action.update"), ResourceLoaderWrapper
				.lookupIcon("action.upate"));
	}

	/**
	 * 
	 * @see AbstractAction#actionPerformed(ActionEvent)
	 */
	public void actionPerformed(ActionEvent ae) {

		if (ErrorOpenMenu.showMessage()) {

			if (DepthsConfiguration.getString(DepthsArgo.KEY_USER_REMEMBER)
					.equals("true")
					|| !DepthsConfiguration.getString(
							DepthsArgo.KEY_USER_PASSWORD).equals("")) {

				if (ProjectUtility.getLastOpenProject() == null) {
					JOptionPane.showMessageDialog(null,
							"Please first open project in repository!");
					return;
				}
				boolean connect = FTPConnectionManager
						.createConnectionToFTPServer();

				if (connect == true) {

					String id = trySaveProject(false, true);
					if (id != null) {

						trySaveDiagrams();

						FTPConnectionManager.closeConnection();

						int resultBox = JOptionPane
								.showConfirmDialog(
										null,
										"Project has been successfully updated in repository. Do you want to continue working on this project?",
										"Working project",
										JOptionPane.YES_NO_OPTION);
						if (resultBox == 0) {
							DepthsJSONProject.getProject(id);
							DepthsModule.changeMenuStatus(true);
						} else {
							DepthsModule.changeMenuStatus(false);
							DepthsJSONTabService.setValueSolution(null);
							DepthsJSONTabService.setValueDescription(null);

							ProjectUtility.setLastOpenProject(null);
							ProjectUtility.setSelectedDesignProblemURI(null);

							Model.getPump().flushModelEvents();
							Model.getPump().stopPumpingEvents();
							Model.getPump().flushModelEvents();
							Project p = ProjectManager.getManager()
									.getCurrentProject();
							ProjectBrowser.getInstance().clearDialogs();
							Designer.disableCritiquing();
							Designer.clearCritiquing();
							// clean the history
							TargetManager.getInstance().cleanHistory();
							p.remove();
							p = ProjectManager.getManager().makeEmptyProject();
							TargetManager.getInstance().setTarget(
									p.getDiagramList().get(0));
							Designer.enableCritiquing();
							Model.getPump().startPumpingEvents();
						}
					}
				} else {
					JOptionPane.showMessageDialog(null,
							"FTP server is offline!");
				}
			} else {

				JOptionPane.showMessageDialog(null,
						"Please enter moodle user name and password!");

			}
		}

	}

	/**
	 * Method that does almost everything in this class.
	 * <p>
	 * 
	 * @return true if all went well.
	 */

	private boolean trySaveDiagrams() {
		ArrayList<DepthsUMLClassDiagram> classDiagrams = ProjectUtility
				.getCurrentProject().getClassDiagrams();

		for (DepthsUMLClassDiagram diagram : classDiagrams) {

			trySaveDiagram((Diagram) diagram.getUMLDiagram(),
					diagram.getFileUrl());
		}

		ArrayList<DepthsUMLUseCaseDiagram> useCaseDiagrams = ProjectUtility
				.getCurrentProject().getUseCaseDiagrams();

		for (DepthsUMLUseCaseDiagram diagram : useCaseDiagrams) {
			trySaveDiagram((Diagram) diagram.getUMLDiagram(),
					diagram.getFileUrl());

		}

		ArrayList<DepthsUMLActivityDiagram> activityDiagrams = ProjectUtility
				.getCurrentProject().getActivityDiagrams();

		for (DepthsUMLActivityDiagram diagram : activityDiagrams) {
			trySaveDiagram((Diagram) diagram.getUMLDiagram(),
					diagram.getFileUrl());

		}

		ArrayList<DepthsUMLCollaborationDiagram> collaborationDiagrams = ProjectUtility
				.getCurrentProject().getCollaborationDiagrams();

		for (DepthsUMLCollaborationDiagram diagram : collaborationDiagrams) {
			trySaveDiagram((Diagram) diagram.getUMLDiagram(),
					diagram.getFileUrl());

		}
		ArrayList<DepthsUMLDeploymentDiagram> deploymentDiagrams = ProjectUtility
				.getCurrentProject().getDeploymentDiagrams();

		for (DepthsUMLDeploymentDiagram diagram : deploymentDiagrams) {
			trySaveDiagram((Diagram) diagram.getUMLDiagram(),
					diagram.getFileUrl());

		}

		ArrayList<DepthsUMLSequenceDiagram> sequenceDiagrams = ProjectUtility
				.getCurrentProject().getSequenceDiagrams();

		for (DepthsUMLSequenceDiagram diagram : sequenceDiagrams) {
			trySaveDiagram((Diagram) diagram.getUMLDiagram(),
					diagram.getFileUrl());

		}
		ArrayList<DepthsUMLStatechartDiagram> stateDiagrams = ProjectUtility
				.getCurrentProject().getStatechartDiagrams();

		for (DepthsUMLStatechartDiagram diagram : stateDiagrams) {
			trySaveDiagram((Diagram) diagram.getUMLDiagram(),
					diagram.getFileUrl());

		}

		return true;
	}

	private boolean trySaveDiagram(Diagram target, String name) {

		TargetManager tm = TargetManager.getInstance();
		tm.setTarget(target);

		if (!(target instanceof Diagram)) {

			return false;
		}

		String defaultName = target.getName();
		defaultName = Util.stripJunk(defaultName);

		SaveGraphicsManager sgm = SaveGraphicsManager.getInstance();

		File picFile = null;
		try {
			picFile = File.createTempFile(name, ".png");
		} catch (IOException ioe) {
			LOG.debug("IOException in trySaveProject");
			JOptionPane.showMessageDialog(null,
					ioe.getMessage() + "\n\n" + ioe.getCause());
		}
		picFile.deleteOnExit();

		try {

			picFile = new File(picFile.getParentFile(), name);

			String suffix = sgm.getFilterFromFileName(picFile.getName())
					.getSuffix();

			return doSave(picFile, suffix, true);

		} catch (OutOfMemoryError e) {
			new ExceptionDialog(
					ArgoFrame.getInstance(),
					"You have run out of memory. "
							+ "Close down ArgoUML and restart with a larger heap size.",
					e);
			JOptionPane
					.showMessageDialog(
							null,
							"You have run out of memory. \n Close down ArgoUML and restart with a larger heap size.\n"
									+ e.getMessage() + "\n\n" + e.getCause());
		} catch (Exception e) {
			new ExceptionDialog(ArgoFrame.getInstance(), e);
			LOG.error("Got some exception" + e.getMessage() + " cause: "
					+ e.getCause());
			JOptionPane.showMessageDialog(null,
					e.getMessage() + "\n\n" + e.getCause());
		}

		return false;
	}

	/**
	 * Try to save the project.
	 * 
	 * @param overwrite
	 *            if true, then we overwrite without asking
	 * @param saveNewFile
	 *            if true, we'll ask for a new file even if the current project
	 *            already had one
	 */
	private String trySaveProject(boolean overwrite, boolean saveNewFile) {

		Project project = ProjectManager.getManager().getCurrentProject();
		PersistenceManager pm = PersistenceManager.getInstance();
		ProjectFilePersister persister = null;
		persister = pm.getSavePersister();

		File file = null;
		try {
			file = File.createTempFile("pattern", ".zargo");
		} catch (IOException ioe) {
			LOG.debug("IOException in trySaveProject");
			JOptionPane.showMessageDialog(null,
					ioe.getMessage() + "\n\n" + ioe.getCause());
			return null;
		}
		file.deleteOnExit();
		pm.setSavePersister(null);
		if (persister == null) {
			persister = pm.getPersisterFromFileName(file.getName());
		}
		if (persister == null) {
			throw new IllegalStateException("Filename " + project.getName()
					+ " is not of a known file type");
		}

		try {
			project.preSave();
			persister.save(project, file);
			project.postSave();
		} catch (InterruptedException se) {
			LOG.debug("InterruptedException at trySaveProject");
			JOptionPane.showMessageDialog(null,
					se.getMessage() + "\n\n" + se.getCause());
			return null;
		} catch (Exception se) {
			LOG.debug("Exception at trySaveProject " + se.getMessage());
			JOptionPane.showMessageDialog(null,
					se.getMessage() + "\n\n" + se.getCause());
			return null;
		}
		if (file == null) {
			return null;
		}

		ProjectUtility.getNewProject().setProjectValues();
		LOG.info("project name="
				+ ProjectUtility.getCurrentProject().getProjectName()
				+ " file name=" + file.getName());

		String idd = saveProjectUsingJSON();

		FTPConnectionManager.uploadFileToRepository(file, ".zargo");

		return idd;

	}

	public String saveProjectUsingJSON() {
		depths.json.project.Project p = ProjectUtility.getCurrentProject();
		String json = "";
		json = DepthsJSONProject.saveProject(p, "edit");

		String wsURL = ConfigURL.saveProject;
		Client c = Client.create();
		WebResource r = c.resource(wsURL);
		MultivaluedMapImpl queryParams = new MultivaluedMapImpl();
		Object of = json;
		queryParams.add("json", of);

		String cc = r.queryParams(queryParams).get(String.class);
		String id = "";
		id = (cc.toString());

		wsURL = ConfigURL.saveDiagrams;
		r = c.resource(wsURL);
		String rs = DepthsJSONProject.saveDiagrams(p, cc.toString());

		queryParams = new MultivaluedMapImpl();
		of = rs;
		queryParams.add("json", of);
		cc = r.queryParams(queryParams).get(String.class);

		p.setProjecturi(id);

		return id;
	}

	/**
	 * Actually do the saving of the graphics file.
	 * 
	 * @return true if it was successful.
	 * @param theFile
	 *            is the file that we are writing to
	 * @param suffix
	 *            is the suffix. Used for deciding what format the file shall
	 *            have.
	 * @param useUI
	 *            is true if we are supposed to use the UI e.g. to warn the user
	 *            that we are replacing an old file.
	 */
	private boolean doSave(File theFile, String suffix, boolean useUI)
			throws FileNotFoundException, IOException {

		SaveGraphicsManager sgm = SaveGraphicsManager.getInstance();
		SaveGraphicsAction cmd = null;

		cmd = sgm.getSaveActionBySuffix(suffix);
		if (cmd == null) {
			return false;
		}

		if (useUI) {
			// ProjectBrowser.getInstance().showStatus(
			// "Writing " + theFile + "...");
		}

		FileOutputStream fo = new FileOutputStream(theFile);
		cmd.setStream(fo);
		cmd.setScale(Configuration.getInteger(
				SaveGraphicsManager.KEY_GRAPHICS_RESOLUTION, 1));
		cmd.actionPerformed(null);
		fo.close();
		if (useUI) {
			// ProjectBrowser.getInstance().showStatus("Wrote " + theFile);
		}

		FTPConnectionManager.uploadFileToRepository(theFile, ".png");

		return true;
	}

	/**
	 * Execute this action from the command line.
	 * 
	 * TODO: The underlying GEF library relies on Acme that doesn't allow us to
	 * create these files unless there is a window showing. For this reason I
	 * have had to split the performing of commands in
	 * {@link org.argouml.application.Main#main(String[])} so that we can, by
	 * not supplying the -batch option, run these commands with the window
	 * showing. Hopefully this can eventually be fixed.
	 * 
	 * @see org.argouml.application.api.CommandLineInterface#doCommand(String)
	 * @param argument
	 *            is the file name that we save to.
	 * @return true if it is OK.
	 */

}
