// Copyright (c) 2011 Dzenan Strujic, University "Mediterranean" , Podgorica Montenegro

package depths.json.service;

import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;

import depths.application.api.DepthsArgo;
import depths.configuration.DepthsConfiguration;
import depths.json.configResources.ConfigURL;
import depths.json.user.User;
import depths.utility.StringUtility;

public class DepthsJSONUser {

	private static final Logger LOG = Logger.getLogger(DepthsJSONFTP.class);

	public static boolean checkAccessUser(String userName, String password) {
		String wsURL = ConfigURL.checkuser;

		System.out.println("C " + wsURL);
		if (ValidURL.valide(wsURL)) {
			try {
				LOG.debug("chack user.");
				Client c = Client.create();
				WebResource r = c.resource(wsURL);
				String encPass = StringUtility.md5(password);
				String jsonRes = r.queryParam("user", userName)
						.queryParam("pass", encPass)
						.accept(MediaType.APPLICATION_JSON).get(String.class);

				JSONArray jsonArr = new JSONArray(jsonRes);
				JSONObject json = jsonArr.getJSONObject(0);
				User user = new User();
				user.setAccess("");
				user.setAccess(json.getString("access"));

				if (user.getAccess().equals("true")) {
					return true;
				} else {
					return false;
				}

			} catch (JSONException e) {
				e.printStackTrace();
			}
		} else {

			return false;
		}
		return false;
	}

	public static User readUser(String userName, String password) {
		String wsURL = ConfigURL.checkuser;

		if (ValidURL.valide(wsURL)) {
			try {
				LOG.debug("readUserParameter");
				Client c = Client.create();
				WebResource r = c.resource(wsURL);
				String encPass = StringUtility.md5(password);

				String jsonRes = r.queryParam("user", userName)
						.queryParam("pass", encPass)
						.accept(MediaType.APPLICATION_JSON).get(String.class);

				JSONArray jsonArr = new JSONArray(jsonRes);
				JSONObject json = jsonArr.getJSONObject(0);
				User user = new User();
				user.setAccess(json.getString("access"));

				if (user.getAccess().equals("true")) {
					user.setFirstName(json.getString("firstname"));
					user.setLastName(json.getString("lastname"));
					user.setUri(json.getString("uri"));
				}

				user.setMessage(json.getString("message"));
				user.setUserName(json.getString("username"));
				user.setMessage(json.getString("message"));

				return user;
			} catch (JSONException e) {
				e.printStackTrace();
			}
		} else {

			return null;
		}
		return null;
	}

	public static User getUser() {
		return readUser(
				DepthsConfiguration.getString(DepthsArgo.KEY_USER_USERNAME),
				DepthsConfiguration.getString(DepthsArgo.KEY_USER_PASSWORD));
	}
}
