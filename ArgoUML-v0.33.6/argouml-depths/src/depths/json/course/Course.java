package depths.json.course;

public class Course {

	private String uri;
	private String title;
	private String description;
	private int numberOfDesignProblem;

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getNumberOfDesignProblem() {
		return numberOfDesignProblem;
	}

	public void setNumberOfDesignProblem(int numberOfDesignProblem) {
		this.numberOfDesignProblem = numberOfDesignProblem;
	}

	public String toString() {
		return this.title;
	}
}
