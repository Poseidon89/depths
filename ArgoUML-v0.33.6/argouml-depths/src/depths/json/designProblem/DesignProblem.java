package depths.json.designProblem;

import java.util.ArrayList;

public class DesignProblem {
	private String uri;
	private String title;
	private String description;
	private String dateCreated;
	private String author;
	private ArrayList<String> relatedLinkTitle = new ArrayList<String>();
	private ArrayList<String> relatedLinkHref = new ArrayList<String>();

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(String dateCreated) {
		this.dateCreated = dateCreated;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public ArrayList<String> getRelatedLinkTitle() {
		return relatedLinkTitle;
	}

	public void setRelatedLinkTitle(ArrayList<String> relatedLinkTitle) {
		this.relatedLinkTitle = relatedLinkTitle;
	}

	public ArrayList<String> getRelatedLinkHref() {
		return relatedLinkHref;
	}

	public void setRelatedLinkHref(ArrayList<String> relatedLinkHref) {
		this.relatedLinkHref = relatedLinkHref;
	}

	public String toString() {
		return this.title;
	}

}
