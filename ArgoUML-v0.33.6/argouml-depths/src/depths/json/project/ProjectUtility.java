// $Id: TabDepthsProps.java 50003 2008-04-13 21:58:15Z Zoran $ 
// Copyright (c) 2007-2008 Jeremic Zoran, University of Belgrade

package depths.json.project;

import org.apache.log4j.Logger;

public class ProjectUtility {
	private static final Logger LOG = Logger.getLogger(ProjectUtility.class);
	private static Project currentProject;
	private static Project lastOpenProject = null;
	private static String selectedDesignProblemURI = null;
	private static String lastUserUri = null;
	
	public static Project getNewProject() {
		currentProject = new Project();
		return currentProject;
	}

	public static Project getCurrentProject() {
		return currentProject;
	}

	public static Project getLastOpenProject() {
		return lastOpenProject;
	}

	public static String getLastUserUri() {
		return lastUserUri;
	}

	public static void setLastUserUri(String lastUserUri) {
		ProjectUtility.lastUserUri = lastUserUri;
	}

	public static void setLastOpenProject(Project lastOpenProject) {
		ProjectUtility.lastOpenProject = lastOpenProject;
	}

	public static void setCurrentProject(Project currProject) {
		ProjectUtility.currentProject = currProject;
	}

	public static String getSelectedDesignProblemURI() {
		return selectedDesignProblemURI;
	}

	public static void setSelectedDesignProblemURI(
			String selectedDesignProblemURI) {
		ProjectUtility.selectedDesignProblemURI = selectedDesignProblemURI;
	}

}
