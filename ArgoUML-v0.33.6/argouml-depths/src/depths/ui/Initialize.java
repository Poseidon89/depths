package depths.ui;

import javax.swing.JOptionPane;

import org.apache.log4j.Logger;

import depths.application.api.DepthsArgo;
import depths.configuration.DepthsConfiguration;
import depths.json.ftp.FtpParameter;
import depths.json.service.DepthsJSONCourse;
import depths.json.service.DepthsJSONFTP;
import depths.json.service.DepthsJSONUser;
import depths.json.user.User;
import depths.uml.ui.FTPConnectionManager;

public class Initialize {

	private static final Logger LOG = Logger.getLogger(Initialize.class);

	public static void initializeFtpParameters() {
		String wsURL = DepthsConfiguration.getString(DepthsArgo.KEY_WS);
		if (wsURL.equals("")) {
			JOptionPane
					.showMessageDialog(
							null,
							"Web Service is not initialize, please enter link WS.\nYou should set it in order to use DEPTHS (DEPTHS menu->DEPTHS settings).");
		} else {

			FtpParameter ftp = DepthsJSONFTP.readFtpParameter();
			if (ftp != null) {
				DepthsConfiguration.setString(DepthsArgo.KEY_FTP_HOST,
						ftp.getFtpUrl());

				DepthsConfiguration.setString(
						DepthsArgo.KEY_REPOSITORY_USERNAME,
						ftp.getFtpUsername());
				DepthsConfiguration.setString(
						DepthsArgo.KEY_REPOSITORY_PASSWORD,
						ftp.getFtpPassword());
				DepthsConfiguration.setString(
						DepthsArgo.SSL,
						ftp.getSecurityssl());
				
				DepthsConfiguration.setString(DepthsArgo.KEY_FTP_PORT,
						ftp.getFtpPort());
				DepthsConfiguration.setString(DepthsArgo.KEY_FTP_IMAGES_FOLDER,
						ftp.getFtpLocationImages());
				DepthsConfiguration.setString(
						DepthsArgo.KEY_FTP_PROJECTS_FOLDER,
						ftp.getFtpLocationProjects());

//				FTPConnectionManager.createStructureDirectory(ftp
//						.getFtpLocationImages());
//				FTPConnectionManager.createStructureDirectory(ftp
//						.getFtpLocationProjects());
				LOG.debug("initi ftp parameters");
				System.out.println(ftp.toString());

			} else {
				JOptionPane
						.showMessageDialog(
								null,
								"Please enter valid WS URL.\nYou should set it in order to use DEPTHS (DEPTHS menu->DEPTHS settings).");

			}

		}
	}
	
	public static boolean initializeUserData() {
		String wsURL = DepthsConfiguration.getString(DepthsArgo.KEY_WS);
		String userName = DepthsConfiguration
				.getString(DepthsArgo.KEY_USER_USERNAME);
		String password = DepthsConfiguration
				.getString(DepthsArgo.KEY_USER_PASSWORD);
		if (userName.equals("") || password.equals("")) {
			return false;
		}
		if (wsURL.equals("")) {
			JOptionPane
					.showMessageDialog(
							null,
							"Web Service is not initialize, please enter link WS.\nYou should set it in order to use DEPTHS (DEPTHS menu->DEPTHS settings).");
			return false;
		} else {

			if (DepthsJSONUser.checkAccessUser(userName, password)) {

				User user = DepthsJSONUser.readUser(userName, password);
				user.setPassword(password);

				if (user != null) {

					if (user.getAccess().equals("true")) {
						DepthsConfiguration.setString(
								DepthsArgo.KEY_USER_PASSWORD,
								user.getPassword());
						DepthsConfiguration.setString(
								DepthsArgo.KEY_USER_USERNAME,
								user.getUserName());
						DepthsConfiguration.setString(DepthsArgo.USER_URI,
								user.getUri());
						LOG.debug("initi user parameters");
						System.out.println(user.toString());
						return true;
					} else {
						JOptionPane.showMessageDialog(null,
								"Please enter valid user name and password!");
						return false;
					}

				} else {
					JOptionPane
							.showMessageDialog(
									null,
									"Please enter valid WS URL.\nYou should set it in order to use DEPTHS (DEPTHS menu->DEPTHS settings).");
					return false;
				}

			}
			return false;
		}
	}
	
}
