// $Id: TabDepthsProps.java 50003 2008-04-13 21:58:15Z Zoran $ 
// Copyright (c) 2007-2008 Jeremic Zoran, University of Belgrade 

package depths.ui;

import java.awt.Color;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.ImageIcon;
import javax.swing.JScrollPane;
import javax.swing.UIManager;

import org.argouml.application.api.Argo;
import org.argouml.configuration.Configuration;
import org.argouml.i18n.Translator;
import org.argouml.model.Model;

import org.argouml.swingext.UpArrowIcon;
import org.argouml.uml.ui.PropPanel;
import org.argouml.uml.ui.UMLDeprecatedCheckBox;
import org.argouml.uml.ui.UMLModelElementCommentDocument;
import org.argouml.uml.ui.UMLModelElementTaggedValueDocument;
import org.argouml.uml.ui.UMLTextArea2;
import org.argouml.uml.ui.UMLTextField2;
import org.tigris.gef.presentation.Fig;
import org.tigris.swidgets.Horizontal;
import org.tigris.swidgets.LabelledLayout;
import org.tigris.swidgets.Vertical;

/**
 * 
 * Deprecated
 * 
 * @author Zoran
 */
public class TabDepthsDocumentation extends PropPanel

{

	private static String orientation = Configuration.getString(Configuration
			.makeKey("layout", "tabdocumentation"));

	/**
	 * Construct new documentation tab
	 */
	public TabDepthsDocumentation() {

		// super(Translator.localize("tab.documentation"), (ImageIcon) null);
		super("DEPTHS documentation", (ImageIcon) null);

		setOrientation((orientation.equals("West") || orientation
				.equals("East")) ? Vertical.getInstance() : Horizontal
				.getInstance());
		setIcon(new UpArrowIcon());

		addField(Translator.localize("label.author"), new UMLTextField2(
				new UMLModelElementTaggedValueDocument(Argo.AUTHOR_TAG)));

		addField(Translator.localize("label.version"), new UMLTextField2(
				new UMLModelElementTaggedValueDocument(Argo.VERSION_TAG)));

		addField(Translator.localize("label.since"), new UMLTextField2(
				new UMLModelElementTaggedValueDocument(Argo.SINCE_TAG)));

		addField(Translator.localize("label.deprecated"),
				new UMLDeprecatedCheckBox());

		UMLTextArea2 see = new UMLTextArea2(
				new UMLModelElementTaggedValueDocument(Argo.SEE_TAG));
		see.setRows(2);
		see.setLineWrap(true);
		see.setWrapStyleWord(true);
		JScrollPane spSee = new JScrollPane();
		spSee.getViewport().add(see);
		addField(Translator.localize("label.see"), spSee);

		// make new column with LabelledLayout
		add(LabelledLayout.getSeperator());

		UMLTextArea2 doc = new UMLTextArea2(
				new UMLModelElementTaggedValueDocument(Argo.DOCUMENTATION_TAG));
		doc.setRows(2);
		doc.setLineWrap(true);
		doc.setWrapStyleWord(true);
		JScrollPane spDocs = new JScrollPane();
		spDocs.getViewport().add(doc);
		addField(Translator.localize("label.documentation"), spDocs);

		// Comment.name text field - editing disabled
		UMLTextArea2 comment = new UMLTextArea2(
				new UMLModelElementCommentDocument(false));
		comment.setRows(2);
		comment.setLineWrap(true);
		comment.setWrapStyleWord(true);
		comment.setEnabled(false);
		comment.setDisabledTextColor(comment.getForeground());
		// See http://bugs.sun.com/bugdatabase/view_bug.do?bug_id=4919687
		Color c = new Color(UIManager.getColor("TextField.inactiveBackground")
				.getRGB());
		comment.setBackground(c);
		JScrollPane spComment = new JScrollPane();
		spComment.getViewport().add(comment);
		addField(Translator.localize("label.comment.name"), spComment);

		// Comment.body text field - editing disabled
		UMLTextArea2 commentBody = new UMLTextArea2(
				new UMLModelElementCommentDocument(true));
		commentBody.setRows(2);
		commentBody.setLineWrap(true);
		commentBody.setWrapStyleWord(true);
		commentBody.setEnabled(false);
		commentBody.setDisabledTextColor(comment.getForeground());
		commentBody.setBackground(c);
		JScrollPane spCommentBody = new JScrollPane();
		spCommentBody.getViewport().add(commentBody);
		addField(Translator.localize("label.comment.body"), spCommentBody);

		/*
		 * Since there are no buttons on this panel, we have to set the size of
		 * the buttonpanel, otherwise the title would not be aligned right.
		 */
		setButtonPanelSize(18);
	}

	/**
	 * Checks if the tab should be enabled. Returns true if the target returned
	 * by getTarget is a modelelement or if that target shows up as Fig on the
	 * active diagram and has a modelelement as owner.
	 * 
	 * @return true if this tab should be enabled, otherwise false.
	 */
	public boolean shouldBeEnabled() {
		Object target = getTarget();
		return shouldBeEnabled(target);
	}

	/**
	 * @see org.argouml.uml.ui.PropPanel#shouldBeEnabled(java.lang.Object)
	 */

	public boolean shouldBeEnabled(Object target) {
		target = (target instanceof Fig) ? ((Fig) target).getOwner() : target;
		return Model.getFacade().isAModelElement(target);
	}

	/**
	 * for test purposes
	 * 
	 * @param s
	 */
	public void printToFile(String s) {
		try {
			BufferedWriter out = new BufferedWriter(new FileWriter(
					"C://_test/ArgoUML_TabDepthsDocumentation.txt", true));
			out.write(s);
			out.write("\r\n");
			out.close();
		} catch (IOException e) {
		}
	}

}