// $Id: TabDepthsProps.java 50003 2008-04-13 21:58:15Z Zoran $ 
// Copyright (c) 2007-2008 Jeremic Zoran, University of Belgrade

package depths.ui;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JTabbedPane;
import javax.swing.SwingConstants;

import org.argouml.application.api.GUISettingsTabInterface;
import org.argouml.configuration.Configuration;
import org.argouml.i18n.Translator;
import org.argouml.util.ArgoDialog;

import depths.i18n.DepthsTranslator;

/**
 * 
 * 
 * @author Zoran
 */
public class DepthsSettingsDialog extends ArgoDialog implements WindowListener {
	private boolean windowOpen;
	private JTabbedPane tabs;
	private JButton applyButton;
	private List<GUISettingsTabInterface> settingsTabs;

	DepthsSettingsDialog() {
		super(DepthsTranslator.localize("tab.depths_settings"),
				ArgoDialog.OK_CANCEL_OPTION, true);
		tabs = new JTabbedPane();

		applyButton = new JButton(Translator.localize("button.apply"));
		applyButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				handleSave();
			}
		});
		addButton(applyButton);

		settingsTabs = new ArrayList<GUISettingsTabInterface>();
		addSettingsTab(new DepthsSettingsTabWS());
		addSettingsTab(new DepthsSettingsTabUser());

		for (GUISettingsTabInterface stp : settingsTabs) {
			tabs.addTab(DepthsTranslator.localize(stp.getTabKey()),
					stp.getTabPanel());
		}

		// Increase width to accommodate all tabs on one row.
		final int minimumWidth = 480;
		tabs.setPreferredSize(new Dimension(Math.max(
				tabs.getPreferredSize().width, minimumWidth), tabs
				.getPreferredSize().height));

		tabs.setTabPlacement(SwingConstants.LEFT);
		setContent(tabs);
		addWindowListener(this);

	}

	private List<GUISettingsTabInterface> getSettingsTabs() {

		return Collections.unmodifiableList(settingsTabs);

	}

	/**
	 * Register a new SettingsTab.
	 * 
	 * @param panel
	 *            The GUISettingsTabInterface to add.
	 */
	public void addSettingsTab(final GUISettingsTabInterface panel) {
		settingsTabs.add(panel);
	}

	/**
	 * 
	 * @param e
	 * @see java.awt.event.WindowListener#windowDeiconified(java.awt.event.WindowEvent)
	 */
	public void windowDeiconified(WindowEvent e) {
		// ignored - we only care about open/closing
	}

	/**
	 * 
	 * @param show
	 * @see java.awt.Dialog#setVisible(boolean)
	 */
	public void setVisible(boolean show) {
		if (show) {
			handleRefresh();
			toFront();
		}
		super.setVisible(show);
		// windowOpen state will be changed when window is activated
	}

	/**
	 * @see java.awt.event.WindowListener#windowOpened(java.awt.event.WindowEvent)
	 */
	public void windowOpened(WindowEvent e) {
		handleOpen();
	}

	/**
	 * @see java.awt.event.WindowListener#windowClosing(java.awt.event.WindowEvent)
	 */
	public void windowClosing(WindowEvent e) {
		// Handle the same as an explicit cancel
		handleCancel();
	}

	/**
	 * @see java.awt.event.WindowListener#windowIconified(java.awt.event.WindowEvent)
	 */
	public void windowIconified(WindowEvent e) {
		// ignored - we only care about open/closing
	}

	/**
	 * @see java.awt.event.WindowListener#windowActivated(java.awt.event.WindowEvent)
	 */
	public void windowActivated(WindowEvent e) {
		handleOpen();
	}

	/**
	 * @see java.awt.event.WindowListener#windowDeactivated(java.awt.event.WindowEvent)
	 */
	public void windowDeactivated(WindowEvent e) {
		// ignored - we only care about open/closing
	}

	/**
	 * @see java.awt.event.WindowListener#windowClosed(java.awt.event.WindowEvent)
	 */
	public void windowClosed(WindowEvent e) {
		// ignored - we only care about open/closing
	}

	private void handleOpen() {
		// We only request focus the first time we become visible
		if (!windowOpen) {
			getOkButton().requestFocusInWindow();
			windowOpen = true;
		}
	}

	/**
	 * Called when the user has pressed Cancel. Performs "Cancel" in all Tabs.
	 */
	private void handleCancel() {
		for (GUISettingsTabInterface tab : settingsTabs) {
			tab.handleSettingsTabCancel();
		}
		windowOpen = false;
	}

	/**
	 * Perform "Refresh" in all Tabs.
	 */
	private void handleRefresh() {
		for (GUISettingsTabInterface tab : settingsTabs) {
			tab.handleSettingsTabRefresh();
		}
	}

	/**
	 * Called when the user has pressed Save. Performs "Save" in all Tabs.
	 */
	private void handleSave() {
		for (GUISettingsTabInterface tab : settingsTabs) {
			tab.handleSettingsTabSave();
		}
		windowOpen = false;
		Configuration.save();
	}

}
