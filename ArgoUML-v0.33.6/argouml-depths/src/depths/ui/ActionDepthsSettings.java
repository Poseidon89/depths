// $Id: TabDepthsProps.java 50003 2008-04-13 21:58:15Z Zoran $ 
// Copyright (c) 2007-2008 Jeremic Zoran, University of Belgrade 

package depths.ui;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import org.argouml.application.helpers.ResourceLoaderWrapper;
import org.argouml.util.ArgoDialog;

import depths.i18n.DepthsTranslator;

/**
 * Action for starting the Argo settings window.
 * 
 * @author Thomas N
 * @author Thierry Lach
 * @since 0.9.4
 */
public class ActionDepthsSettings extends AbstractAction {

	/**
	 * The settings dialog.
	 */
	private ArgoDialog dialog;

	/**
	 * Constructor.
	 */
	public ActionDepthsSettings() {
		super(DepthsTranslator.localize("action.depths_settings"),
				ResourceLoaderWrapper.lookupIcon("action.settings"));
	}

	/*
	 * @see java.awt.event.ActionListener#actionPerformed(
	 * java.awt.event.ActionEvent)
	 */
	public void actionPerformed(ActionEvent event) {

		if (dialog == null) {
			dialog = new DepthsSettingsDialog();
		}
		dialog.setVisible(true);

	}

	/**
	 * The serial version.
	 */
	private static final long serialVersionUID = -3646595772633674514L;
}
