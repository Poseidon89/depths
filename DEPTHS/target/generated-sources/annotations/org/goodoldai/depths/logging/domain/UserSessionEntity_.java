package org.goodoldai.depths.logging.domain;

import java.util.Date;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(UserSessionEntity.class)
public abstract class UserSessionEntity_ {

	public static volatile SingularAttribute<UserSessionEntity, Long> id;
	public static volatile SingularAttribute<UserSessionEntity, UserEntity> userEntity;
	public static volatile SingularAttribute<UserSessionEntity, String> sessionId;
	public static volatile SingularAttribute<UserSessionEntity, Date> sessionStarted;

}

