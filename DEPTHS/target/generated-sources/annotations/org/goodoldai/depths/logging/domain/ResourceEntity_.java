package org.goodoldai.depths.logging.domain;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(ResourceEntity.class)
public abstract class ResourceEntity_ {

	public static volatile SingularAttribute<ResourceEntity, Long> id;
	public static volatile SingularAttribute<ResourceEntity, String> resourceTitle;
	public static volatile SingularAttribute<ResourceEntity, String> resourceUri;
	public static volatile SingularAttribute<ResourceEntity, String> resourceType;

}

