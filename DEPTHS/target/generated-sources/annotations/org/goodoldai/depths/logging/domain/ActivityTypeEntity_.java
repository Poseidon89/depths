package org.goodoldai.depths.logging.domain;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(ActivityTypeEntity.class)
public abstract class ActivityTypeEntity_ {

	public static volatile SingularAttribute<ActivityTypeEntity, Long> id;
	public static volatile SingularAttribute<ActivityTypeEntity, String> type;

}

