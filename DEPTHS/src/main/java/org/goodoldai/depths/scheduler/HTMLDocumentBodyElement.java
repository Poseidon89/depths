package org.goodoldai.depths.scheduler;

import java.util.ArrayList;
import java.util.Collection;

public class HTMLDocumentBodyElement {
	
	private String title="";
	public void setTitle(String title) {
		this.title = title;
	}
	Collection<String> notifications=new ArrayList<String>();
	
	public String produceBodyElement(){
		String bodyElement="<tr class='subtitled'><td width='10%'>&nbsp;</td><td colspan='2'><br><strong>" +
				title.toUpperCase() +
				"</strong><br></td></tr>";
		
		int i=0;
		for(String message:notifications){
			i++;
		bodyElement=bodyElement+"<tr class='notifelement";
		
		if(notifications.size()==i){
	
			bodyElement=bodyElement+" bordered_bottom";
		}
		
		bodyElement=bodyElement+"'><td width='10%'>&nbsp;</td>" +
				"<td colspan='2'>" +
				message +
				"</td></tr>";
		
		}
		return bodyElement;
	}
	public void addNotification(String message){
		notifications.add(message);
	}

}
