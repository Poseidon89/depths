package org.goodoldai.depths.scheduler;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collection;

import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.goodoldai.depths.config.SMTPConfig;
import org.goodoldai.depths.config.Settings;

import org.goodoldai.depths.domain.user.User;

 

public class SendEmail {

	void sendEmailToUser(User user, String html, String subject) throws AddressException, MessagingException{
		 
		 
		 
			String em=user.getEmail();
			if(em.startsWith("mailto:")){
				int ind=em.lastIndexOf(":");
				em=em.substring(ind+1);
			}
			 
			if (Settings.getInstance().config.cronSchedulerConfig.debug==true){
				em="depths.webmaster@gmail.com";
			} 
			//email.add(em);
		 
		sendEmail(html,em,subject);
		 
		
	}
	private void sendEmail(String html,String email,String subject) throws AddressException, MessagingException{
		SMTPConfig smtpConfig = Settings.getInstance().config.cronSchedulerConfig.smtpConfig;
		String host = smtpConfig.host;
	    String user = smtpConfig.user;
	    String pass = smtpConfig.pass;
	    int port=smtpConfig.port;
	    boolean auth=smtpConfig.auth;
	    boolean starttls=smtpConfig.starttlsenable;
	    Properties props = System.getProperties();
	    props.put("mail.smtp.starttls.enable", String.valueOf(starttls));  
	    props.put("mail.smtp.host", host);
	    
	    props.put("mail.smtp.user", user);
	    props.put("mail.smtp.password", pass);
	    props.put("mail.smtp.port", port);
	    props.put("mail.smtp.auth", String.valueOf(auth));
 

	    Session session = Session.getDefaultInstance(props, null);
	    MimeMessage message = new MimeMessage(session);
	    message.setFrom(new InternetAddress(user));

	   Collection<InternetAddress> toAddress = new ArrayList<InternetAddress>();
 
	   // for( String to: emails) { // changed from a while loop
	        toAddress.add(new InternetAddress(email));
	        message.addRecipient(Message.RecipientType.TO, new InternetAddress(email));
	  //  }
	    
/*
	    for( InternetAdress) { // changed from a while loop
	        message.addRecipient(Message.RecipientType.TO, toAddress[i]);
	    }
	    */
	    message.setSubject(subject);
	    message.setDataHandler(new DataHandler(new HTMLDataSource(html)));

	    Transport transport = session.getTransport("smtp");
	    transport.connect(host, user, pass);
	    transport.sendMessage(message, message.getAllRecipients());
	    transport.close();


	}
 
/*
 * Inner class to act as a JAF datasource to send HTML e-mail content
 */
static class HTMLDataSource implements DataSource {
    private String html;

    public HTMLDataSource(String htmlString) {
        html = htmlString;
    }

    // Return html string in an InputStream.
    // A new stream must be returned each time.
    public InputStream getInputStream() throws IOException {
        if (html == null) throw new IOException("Null HTML");
        return new ByteArrayInputStream(html.getBytes());
    }

    public OutputStream getOutputStream() throws IOException {
        throw new IOException("This DataHandler cannot write HTML");
    }

    public String getContentType() {
        return "text/html";
    }

    public String getName() {
        return "JAF text/html dataSource to send e-mail only";
    }
}

} //End of class
