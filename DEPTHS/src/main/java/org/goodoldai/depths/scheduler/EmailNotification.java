package org.goodoldai.depths.scheduler;
 
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;

import org.apache.log4j.Logger;
import org.goodoldai.depths.domain.activity.Brainstorming;
import org.goodoldai.depths.domain.content.Brainstorm;
import org.goodoldai.depths.domain.content.Submission;
 
import org.goodoldai.depths.domain.user.User;

public class EmailNotification {
	private static final Logger LOGGER = Logger.getLogger(EmailNotification.class);
	public void ideaSubmittedNotification(User whoSubmitted,Brainstorm brainstorm, Brainstorming brainstorming){
		HTMLDocumentMainBody htmlMainBody=new HTMLDocumentMainBody();
		htmlMainBody.setBodyTitle("Notification about idea submitted");
		HTMLDocumentSimpleBodyElement htmlBodyEl1=new HTMLDocumentSimpleBodyElement();
		String message=" submitted new idea to ";
		String what=brainstorm.getTitle();
		 htmlBodyEl1.setBodyContentElements(whoSubmitted.getFirstname()+whoSubmitted.getLastname(), message, what);
		 
		htmlMainBody.addBodyElement(htmlBodyEl1);
		String subject="Notification about idea submitted by "+whoSubmitted.getFirstname()+" "+whoSubmitted.getLastname();
		sendNotification(whoSubmitted,htmlMainBody, subject);
		
	}
	public void fileUploadedNotification(User whoSubmitted, Submission task) {
		HTMLDocumentMainBody htmlMainBody=new HTMLDocumentMainBody();
		htmlMainBody.setBodyTitle("Notification about file uploaded");
		HTMLDocumentSimpleBodyElement htmlBodyEl1=new HTMLDocumentSimpleBodyElement();
		String message=" uploaded new file to ";
		String what=task.getTitle();
		 htmlBodyEl1.setBodyContentElements(whoSubmitted.getFirstname()+whoSubmitted.getLastname(), message, what);
		 
		htmlMainBody.addBodyElement(htmlBodyEl1);
		String subject="Notification about file uploaded by "+whoSubmitted.getFirstname()+" "+whoSubmitted.getLastname();
		sendNotification(whoSubmitted,htmlMainBody, subject);
		
	}
	private void sendNotification(User user, HTMLDocumentMainBody htmlMainBody, String subject){
		 String html="";
		 HTMLDocument htmlDoc=new HTMLDocument();
		 HTMLDocumentFooter htmlFooter=new HTMLDocumentFooter();
		 HTMLDocumentHeader htmlHeader=new HTMLDocumentHeader();
		 htmlHeader.setTitle("Notification");
		 
		 Calendar cal = Calendar.getInstance();
		   DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
		   cal.add(Calendar.DATE, 0);
		    String today=dateFormat.format(cal.getTime());
	  
		 htmlHeader.setPeriod(today);
		 addHTMLMainBodyContent(htmlMainBody);
		
		 htmlDoc.setDocFooter(htmlFooter);
		 htmlDoc.setDocHeader(htmlHeader);
		 htmlDoc.setDocMainBody(htmlMainBody);
		 html=htmlDoc.produceHTMLDocument();
		 SendEmail sEmail=new SendEmail();
		 try {
			sEmail.sendEmailToUser(user, html, subject);
		} catch (AddressException e) {
			// TODO Auto-generated catch block
			LOGGER.error("Error:"+e.getLocalizedMessage());
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			LOGGER.error("Error:"+e.getLocalizedMessage());
		}
	}
	private void addHTMLMainBodyContent(HTMLDocumentMainBody htmlMainBody){
		 
	}
	

}
