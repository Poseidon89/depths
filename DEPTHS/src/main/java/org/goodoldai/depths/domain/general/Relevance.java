package org.goodoldai.depths.domain.general;

import org.goodoldai.depths.config.Constants;
import org.goodoldai.depths.domain.skos.Concept;

import thewebsemantic.Namespace;
import thewebsemantic.RdfProperty;
import thewebsemantic.RdfType;


@Namespace(Constants.DEPTHS_NS)
@RdfType("Relevance")
public class Relevance extends Resource{

	private double hasValue;
	private double hasSocialRatingValue;
	private double hasOveralValue;
	private Concept isRelevantFor;
	private Resource isRelevanceOf;
	
	@RdfProperty(Constants.DEPTHS_NS+"hasSocialRatingValue")
	public double getHasSocialRatingValue() {
		return hasSocialRatingValue;
	}
	public void setHasSocialRatingValue(double hasSocialValue) {
		this.hasSocialRatingValue = hasSocialValue;
	}
	
	@RdfProperty(Constants.LOCO_NS+"hasValue")
	public double getHasValue() {
		return hasValue;
	}
	public void setHasValue(double hasValue) {
		this.hasValue = hasValue;
	}
	@RdfProperty(Constants.DEPTHS_NS+"isRelevantFor")
	public Concept getIsRelevantFor() {
		return isRelevantFor;
	}
	public void setIsRelevantFor(Concept isRelevantFor) {
		this.isRelevantFor = isRelevantFor;
	}
	@RdfProperty(Constants.DEPTHS_NS+"isRelevanceOf")
	public Resource getIsRelevanceOf() {
		return isRelevanceOf;
	}
	public void setIsRelevanceOf(Resource isRelevantFor) {
		this.isRelevanceOf = isRelevantFor;
	}
	@RdfProperty(Constants.DEPTHS_NS+"hasOveralValue")
	public double getHasOveralValue() {
		if(hasOveralValue==0){
			hasOveralValue=getHasValue();
		}
		return hasOveralValue;
	}
	public void setHasOveralValue(double hasOveralValue) {
		this.hasOveralValue = hasOveralValue;
	}
}
