package org.goodoldai.depths.domain.content;

import org.goodoldai.depths.config.Constants;

import thewebsemantic.Namespace;
import thewebsemantic.RdfType;

@Namespace(Constants.DEPTHS_NS)
@RdfType("ClassDiagram")
public class ClassDiagram extends Diagram {

	/**
	 * 
	 */
	private static final long serialVersionUID = 626129555487990815L;

}
