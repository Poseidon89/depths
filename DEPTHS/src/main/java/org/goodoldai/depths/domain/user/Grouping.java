package org.goodoldai.depths.domain.user;

import java.util.ArrayList;
import java.util.Collection;

import org.goodoldai.depths.config.Constants;
import org.goodoldai.depths.domain.content.Course;
import org.goodoldai.depths.domain.general.Resource;

import thewebsemantic.Namespace;
import thewebsemantic.RdfProperty;
import thewebsemantic.RdfType;

@Namespace(Constants.DEPTHS_NS)
@RdfType("Grouping")
public class Grouping extends Resource{
	private String groupingname="";
	private String description="";
	private Course courseRef;
	private Collection<Group> groupRefs;
	
	public Grouping(){
		groupRefs=new ArrayList<Group>();
	}
	
	@RdfProperty(Constants.DEPTHS_NS + "groupingname")
	public String getGroupingname() {
		return groupingname;
	}
	public void setGroupingname(String groupingname) {
		this.groupingname = groupingname;
	}
	@RdfProperty(Constants.DEPTHS_NS + "description")
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	@RdfProperty(Constants.DEPTHS_NS + "courseRef")
	public Course getCourseRef() {
		return courseRef;
	}
	public void setCourseRef(Course course) {
		this.courseRef = course;
	}
	@RdfProperty(Constants.DEPTHS_NS + "groupRef")
	public Collection<Group> getGroupRef() {
		return groupRefs;
	}
	public void setGroupRef(Collection<Group> groupRef) {
		this.groupRefs = groupRef;
	}
	public void addGroup(Group group) {
		if (null != group) {
			if (!getGroupRef().contains(group)) {
				getGroupRef().add(group);
			}
		}
	}
	public void removeGroup(Group group) {
		if (null != group) {
			if (!getGroupRef().contains(group)) {
				getGroupRef().remove(group);
			}
		}
	}

}
