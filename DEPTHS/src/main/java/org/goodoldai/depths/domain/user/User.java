package org.goodoldai.depths.domain.user;

import java.util.ArrayList;
import java.util.Collection;

import org.goodoldai.depths.domain.content.Course;
import org.goodoldai.depths.domain.content.Diagram;
import org.goodoldai.depths.domain.foaf.Person;

import thewebsemantic.Namespace;
import thewebsemantic.RdfProperty;
import thewebsemantic.RdfType;
import org.goodoldai.depths.config.Constants;
 
 

@Namespace(Constants.LOCO_NS)
@RdfType("User")
public class User extends Person{
	
	private String firstname="";
	private String lastname="";
	private String username="";
	private String password="";
	private String email="";
	
	private Collection<Course> subscribedToCourse;
	private Collection<Group> memberOfGroup;
	
	@RdfProperty(Constants.DEPTHS_NS + "subscribedToCourse")
	public Collection<Course> getSubscribedToCourse() {
		return subscribedToCourse;
	}

	public void setSubscribedToCourse(Collection<Course> subscribedToCourse) {
		this.subscribedToCourse = subscribedToCourse;
	}

	public void addCourse(Course course) {
		if (null != course) {
			if (!this.subscribedToCourse.contains(course)) {
				this.subscribedToCourse.add(course);
			}
		}
	}
	
	public User(){
		subscribedToCourse=new ArrayList<Course>();
		memberOfGroup=new ArrayList<Group>();
	}
	
	@RdfProperty(Constants.LOCO_NS + "firstname")
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	@RdfProperty(Constants.LOCO_NS + "lastname")
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	@RdfProperty(Constants.LOCO_NS + "username")
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	@RdfProperty(Constants.LOCO_NS + "password")
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	@RdfProperty(Constants.DEPTHS_NS + "memberOfGroup")
	public Collection<Group> getMemberOfGroup() {
		return memberOfGroup;
	}

	public void setMemberOfGroup(Collection<Group> memberOfGroup) {
		this.memberOfGroup = memberOfGroup;
	}
	public void addGroup(Group group) {
		if (null != group) {
			if (!getMemberOfGroup().contains(group)) {
				getMemberOfGroup().add(group);
			}
		}
	}
	public Collection<Group> getMembershipToGroupInCourse(Course course){
		Collection<Group> groups=new ArrayList<Group>();
		for(Group group:memberOfGroup){
			Collection<Course> courses=group.getSubscribedToCourse();
			if(courses.contains(course)){
				groups.add(group);
	 		}
		}
		return groups;
	}
	@RdfProperty(Constants.DEPTHS_NS + "email")
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	} 
	

}
