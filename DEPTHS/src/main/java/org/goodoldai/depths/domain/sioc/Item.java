package org.goodoldai.depths.domain.sioc;

import org.goodoldai.depths.domain.general.Resource;
import org.goodoldai.depths.config.Constants;

import thewebsemantic.Namespace;
import thewebsemantic.RdfProperty;
import thewebsemantic.RdfType;

@Namespace(Constants.SIOC_NS)
@RdfType("Item")
public class Item extends Resource {

	private static final long serialVersionUID = 115904640347422859L;
	
	private String content;

	public Item() {
		super();
	}
	
	public Item(String uri) {
		super(uri);
	}
	
	@RdfProperty(Constants.SIOC_NS + "content")
	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		if(content != null)
			this.content = content;
	}

}
