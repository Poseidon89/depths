package org.goodoldai.depths.domain.activity;

import org.goodoldai.depths.config.Constants;

import thewebsemantic.Namespace;
import thewebsemantic.RdfType;

@Namespace(Constants.LOCO_NS)
@RdfType("Quizzing")
public class Quizzing extends Activity{

}
