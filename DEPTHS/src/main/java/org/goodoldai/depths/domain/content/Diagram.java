package org.goodoldai.depths.domain.content;

import org.goodoldai.depths.config.Constants;

import thewebsemantic.Namespace;
import thewebsemantic.RdfProperty;
import thewebsemantic.RdfType;

@Namespace(Constants.DEPTHS_NS)
@RdfType("Diagram")
public class Diagram extends ContentItem{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7316191710723230671L;
	protected String fileUrl;
	protected String id;
	@RdfProperty(Constants.DEPTHS_NS + "id")
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@RdfProperty(Constants.LOCO_NS + "fileUrl")
	public String getFileUrl() {
		return fileUrl;
	}

	public void setFileUrl(String url) {
		this.fileUrl = url;
	}
}
