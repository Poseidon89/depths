package org.goodoldai.depths.domain.events;

import java.util.ArrayList;
import java.util.Collection;

import org.goodoldai.depths.config.Constants;
import org.goodoldai.depths.domain.content.Posting;

import thewebsemantic.Namespace;
import thewebsemantic.RdfProperty;
import thewebsemantic.RdfType;

@Namespace(Constants.LOCO_NS)
@RdfType("DiscussionEvent")
public class DiscussionEvent extends Event {
	protected  Posting  postingRef;
	

	public DiscussionEvent(){
		//postingRef=new ArrayList<Posting>();
	}
	
	@RdfProperty(Constants.LOCO_NS + "postingRef")
	public  Posting  getPostingRef() {
		return postingRef;
	}

	public void setPostingRef( Posting  postingRef) {
		this.postingRef = postingRef;
	}
	 


}
