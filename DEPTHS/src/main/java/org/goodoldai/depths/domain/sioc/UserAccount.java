package org.goodoldai.depths.domain.sioc;

import java.net.URI;
import java.net.URISyntaxException;

import org.apache.log4j.Logger;
import org.goodoldai.depths.domain.general.Resource;
import org.goodoldai.depths.config.Constants;

import thewebsemantic.Namespace;
import thewebsemantic.RdfProperty;
import thewebsemantic.RdfType;

@Namespace(Constants.SIOC_NS)
@RdfType("UserAccount")
public class UserAccount extends Resource {

	private static final long serialVersionUID = 3442326975448955738L;
	private static final Logger LOGGER = Logger.getLogger(UserAccount.class);
	private String accountName;
	private URI accountServiceHomepage;
	
	public UserAccount() {
		super();
	}

	public UserAccount(String uri) {
		super(uri);
	}

	@RdfProperty(Constants.FOAF_NS + "accountName")
	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		if(accountName != null)
			this.accountName = accountName;
	}

	@RdfProperty(Constants.FOAF_NS + "accountServiceHomepage")
	public URI getAccountServiceHomepage() {
		return accountServiceHomepage;
	}

	public void setAccountServiceHomepage(URI accountServiceHomepage) {
		if(accountServiceHomepage != null)
			this.accountServiceHomepage = accountServiceHomepage;
	}
	
	public void setAccountServiceHomepage(String accountServiceHomepage) {
		if(accountServiceHomepage != null)
			try {
				setAccountServiceHomepage(new URI(accountServiceHomepage));
			} catch (URISyntaxException e) {
				LOGGER.error("Error:"+e.getLocalizedMessage());
			}
	}

}
