package org.goodoldai.depths.domain.content;

import org.goodoldai.depths.config.Constants;
import org.goodoldai.depths.domain.general.Scale;

import thewebsemantic.Namespace;
import thewebsemantic.RdfProperty;
import thewebsemantic.RdfType;

@Namespace(Constants.DEPTHS_NS)
@RdfType("Assessment")
public class Assessment extends Task {

	private Scale usingScale;
	private Submission taskToAssess;
	@RdfProperty(Constants.LOCO_NS + "usingScale")
	public Scale getUsingScale() {
		return usingScale;
	}
	public void setUsingScale(Scale usingScale) {
		this.usingScale = usingScale;
	}
	@RdfProperty(Constants.DEPTHS_NS + "taskToAssess")
	public Submission getTaskToAssess() {
		return taskToAssess;
	}
	public void setTaskToAssess(Submission taskToAssess) {
		this.taskToAssess = taskToAssess;
	}
}
