package org.goodoldai.depths.domain.content;

import java.util.ArrayList;
import java.util.Collection;

import org.goodoldai.depths.config.Constants;
import org.goodoldai.depths.domain.annotation.Annotation;
import org.goodoldai.depths.domain.annotation.Metadata;
import org.goodoldai.depths.domain.general.Resource;

import thewebsemantic.Namespace;
import thewebsemantic.RdfProperty;
import thewebsemantic.RdfType;

@Namespace(Constants.LOCO_NS)
@RdfType("ContentItem")
public class ContentItem extends Resource{
	private Collection<Annotation> hasAnnotation;
	private Collection<Metadata> hasMetadata;
	
	public ContentItem(){
		hasAnnotation=new ArrayList<Annotation>();
		hasMetadata=new ArrayList<Metadata>();
		
	}
	@RdfProperty(Constants.LOCO_NS + "hasAnnotation")
	public Collection<Annotation> getHasAnnotation() {
		return hasAnnotation;
	}
	public void setHasAnnotation(Collection<Annotation> hasAnnotation) {
		this.hasAnnotation = hasAnnotation;
	}
	public void addHasAnnotation(Annotation annotation) {
		if (null != annotation) {
			if (!getHasAnnotation().contains(annotation))
				getHasAnnotation().add(annotation);
		}
	}

	public Collection<Metadata> getHasMetadata() {
		return hasMetadata;
	}
	public void setHasMetadata(Collection<Metadata> hasMetadata) {
		this.hasMetadata = hasMetadata;
	}
	public void addHasMetadata(Metadata metadata) {
		if (null != metadata) {
			if (!getHasMetadata().contains(metadata))
				getHasMetadata().add(metadata);
		}
	}
	

}
