package org.goodoldai.depths.domain.foaf;

import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;

import org.apache.log4j.Logger;
import org.goodoldai.depths.domain.sioc.UserAccount;
import org.goodoldai.depths.config.Constants;

import thewebsemantic.Namespace;
import thewebsemantic.RdfProperty;
import thewebsemantic.RdfType;

@Namespace(Constants.FOAF_NS)
@RdfType("Agent")
public class Agent extends FoafThing {

	private static final long serialVersionUID = -8188475351498473419L;
	private static final Logger LOGGER = Logger.getLogger(Agent.class);
	private String nick;
	private URI mbox;
	private Collection<UserAccount> accounts;

	public Agent() {
		super();
		accounts = new ArrayList<UserAccount>();
	}

	public Agent(String uri) {
		super(uri);
		accounts = new ArrayList<UserAccount>();
	}

	
	public String getNick() {
		return nick;
	}

	@RdfProperty(Constants.FOAF_NS + "nick")
	public void setNick(String nick) {
		if (nick != null)
			this.nick = nick;
	}

	@RdfProperty(Constants.FOAF_NS + "mbox")
	public URI getMbox() {
		return mbox;
	}

	public void setMbox(URI mbox) {
		this.mbox = mbox;
	}
	
	/**
	 * @return the accounts
	 */
	@RdfProperty(Constants.FOAF_NS + "holdsAccount")
	public Collection<UserAccount> getAccounts() {
		return accounts;
	}

	/**
	 * @param accounts the accounts to set
	 */
	public void setAccounts(Collection<UserAccount> accounts) {
		if (null != accounts) {
			this.accounts = accounts;
		} else {
			LOGGER.error("Error: accounts can not be null");
			//throw new RuntimeException("accounts can not be null.");
		}
	}
	
	public void addAccount(UserAccount account) {
		if (account != null) {
			if (!getAccounts().contains(account)){
				getAccounts().add(account);
			}
		} else
			LOGGER.error("Error: accounts must not be null");
		 
	}

}
