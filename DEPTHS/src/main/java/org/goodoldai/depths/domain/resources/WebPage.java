package org.goodoldai.depths.domain.resources;

import org.goodoldai.depths.config.Constants;

import thewebsemantic.Namespace;
import thewebsemantic.RdfType;

@Namespace(Constants.DEPTHS_NS)
@RdfType("WebPage")
public class WebPage extends WebResource {

}
