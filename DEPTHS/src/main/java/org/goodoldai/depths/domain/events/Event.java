package org.goodoldai.depths.domain.events;

import java.util.Date;

import org.goodoldai.depths.config.Constants;
import org.goodoldai.depths.domain.general.Resource;

import thewebsemantic.Namespace;
import thewebsemantic.RdfProperty;
import thewebsemantic.RdfType;

@Namespace(Constants.LOCO_NS)
@RdfType("Event")
public class Event extends Resource {
	private Date dateTimeStart;
	private Date dateTimeEnd;
	
	@RdfProperty(Constants.LOCO_NS + "dateTimeEnd")
	 
	public Date getDateTimeEnd() {
		return dateTimeEnd;
	}
	public void setDateTimeEnd(Date dateTimeEnd) {
		this.dateTimeEnd = dateTimeEnd;
	}
	@RdfProperty(Constants.LOCO_NS + "dateTimeStart")
	public void setDateTimeStart(Date dateTimeStart) {
		this.dateTimeStart = dateTimeStart;
	}
	public Date getDateTimeStart() {
		return dateTimeStart;
	}

}
