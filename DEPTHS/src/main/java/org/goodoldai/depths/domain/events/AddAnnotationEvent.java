package org.goodoldai.depths.domain.events;

import org.goodoldai.depths.config.Constants;

import thewebsemantic.Namespace;
import thewebsemantic.RdfType;

@Namespace(Constants.LOCO_NS)
@RdfType("AddAnnotationEvent")
public class AddAnnotationEvent extends AnnotationEvent {

}
