package org.goodoldai.depths.domain.annotation;

import org.goodoldai.depths.config.Constants;
import org.goodoldai.depths.domain.general.Resource;
import org.goodoldai.depths.domain.user.User;

import thewebsemantic.Namespace;
import thewebsemantic.RdfProperty;
import thewebsemantic.RdfType;

@Namespace(Constants.LOCO_NS)
@RdfType("Tag")
public class Tag extends Annotation {
	private Resource hasParentTag;
	private String hasContent;
	@RdfProperty(Constants.DEPTHS_NS + "hasParentTag")
	public Resource getHasParentTag() {
		return hasParentTag;
	}

	public void setHasParentTag(Resource hasParentTag) {
		this.hasParentTag = hasParentTag;
	}
	@RdfProperty(Constants.LOCO_NS + "hasContent")
	public String getHasContent() {
		return hasContent;
	}

	public void setHasContent(String hasContent) {
		this.hasContent = hasContent;
	}

}
