package org.goodoldai.depths.domain.content;

import java.util.Date;

import org.goodoldai.depths.config.Constants;
import org.goodoldai.depths.domain.general.Resource;
import org.goodoldai.depths.domain.user.User;

import thewebsemantic.Namespace;
import thewebsemantic.RdfProperty;
import thewebsemantic.RdfType;

@Namespace(Constants.LOCO_NS)
@RdfType("Message")
public class Message extends Resource {
private Date dateTimeSent;
private String hasContent;
private User sentBy;

@RdfProperty(Constants.LOCO_NS + "dateTimeSent")
public Date getDateTimeSent() {
	return dateTimeSent;
}

public void setDateTimeSent(Date dateTimeSent) {
	this.dateTimeSent = dateTimeSent;
}
@RdfProperty(Constants.LOCO_NS + "hasContent")
public String getHasContent() {
	return hasContent;
}
public void setHasContent(String hasContent) {
	this.hasContent = hasContent;
}
@RdfProperty(Constants.LOCO_NS + "sentBy")
public User getSentBy() {
	return sentBy;
}
public void setSentBy(User sentBy) {
	this.sentBy = sentBy;
}




}
