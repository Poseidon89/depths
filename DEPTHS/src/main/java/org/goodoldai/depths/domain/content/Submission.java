package org.goodoldai.depths.domain.content;

import org.goodoldai.depths.config.Constants;

import thewebsemantic.Namespace;
import thewebsemantic.RdfType;

@Namespace(Constants.DEPTHS_NS)
@RdfType("Submission")
public class Submission extends Task {

}
