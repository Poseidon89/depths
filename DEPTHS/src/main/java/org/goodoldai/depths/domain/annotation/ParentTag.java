package org.goodoldai.depths.domain.annotation;

import org.goodoldai.depths.config.Constants;
 

import thewebsemantic.Namespace;
import thewebsemantic.RdfProperty;
import thewebsemantic.RdfType;

@Namespace(Constants.DEPTHS_NS)
@RdfType("ParentTag")
public class ParentTag extends Annotation{
	private String hasContent;
	
	@RdfProperty(Constants.LOCO_NS + "hasContent")
	public String getHasContent() {
		return hasContent;
	}

	public void setHasContent(String hasContent) {
		this.hasContent = hasContent;
	}

}
