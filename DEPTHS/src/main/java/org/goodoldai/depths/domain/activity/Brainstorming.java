package org.goodoldai.depths.domain.activity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import org.goodoldai.depths.config.Constants;
import org.goodoldai.depths.domain.annotation.UserRating;
import org.goodoldai.depths.domain.content.Brainstorm;
import org.goodoldai.depths.domain.project.Project;
import org.goodoldai.depths.domain.user.User;
import org.goodoldai.depths.semanticstuff.util.StringUtils;

import thewebsemantic.Namespace;
import thewebsemantic.RdfProperty;
import thewebsemantic.RdfType;

@Namespace(Constants.DEPTHS_NS)
@RdfType("Brainstorming")
public class Brainstorming extends Activity{
	private String hasContent;
	private Date dateTimeSent;
	private Brainstorm inResponseTo;
	private User sentBy;
	private Collection<UserRating> hasUserEvaluation;
	
	public Brainstorming(){
		hasUserEvaluation=new ArrayList<UserRating>();
	}
	@RdfProperty(Constants.LOCO_NS + "hasContent")
	public String getHasContent() {
		if(hasContent!=null){
		return StringUtils.checkOutputContent(hasContent);
		}else{
			return null;
		}
	}
	public void setHasContent(String content) {
		 content=StringUtils.checkInputContent(content);
		this.hasContent = content;
	}
	@RdfProperty(Constants.LOCO_NS + "dateTimeSent")
	public Date getDateTimeSent() {
		return dateTimeSent;
	}

	public void setDateTimeSent(Date dateTimeSent) {
		this.dateTimeSent = dateTimeSent;
	}
	
	@RdfProperty(Constants.DEPTHS_NS + "inResponseTo")
	public Brainstorm getInResponseTo() {
		return inResponseTo;
	}
	public void setInResponseTo(Brainstorm inResponseTo) {
		this.inResponseTo = inResponseTo;
	}
	@RdfProperty(Constants.LOCO_NS + "sentBy")
	public User getSentBy() {
		return sentBy;
	}
	public void setSentBy(User sentBy) {
		this.sentBy = sentBy;
	}
	@RdfProperty(Constants.LOCO_NS + "hasUserEvaluation")
	public Collection<UserRating> getHasUserEvaluation() {
		return hasUserEvaluation;
	}
	public void setHasUserEvaluation(Collection<UserRating> hasUserEvaluation) {
		this.hasUserEvaluation = hasUserEvaluation;
	}
	public void addHasUserEvaluation(UserRating uRating) {
		if (uRating!=null) {
			if (!getHasUserEvaluation().contains(uRating)) {
				getHasUserEvaluation().add(uRating);
			}
		}
	}
	
}
