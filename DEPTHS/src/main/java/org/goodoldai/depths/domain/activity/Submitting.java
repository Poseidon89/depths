package org.goodoldai.depths.domain.activity;

import java.util.Date;

import org.goodoldai.depths.config.Constants;
import org.goodoldai.depths.domain.content.Brainstorm;
import org.goodoldai.depths.domain.content.Submission;
import org.goodoldai.depths.domain.project.Project;
import org.goodoldai.depths.domain.user.User;

import thewebsemantic.Namespace;
import thewebsemantic.RdfProperty;
import thewebsemantic.RdfType;

@Namespace(Constants.DEPTHS_NS)
@RdfType("Submitting")
public class Submitting extends Activity{

	private Date dateTimeSent;
	private Submission inResponseTo;
	private Project projectRef;
	private User sentBy;

	@RdfProperty(Constants.LOCO_NS + "dateTimeSent")
	public Date getDateTimeSent() {
		return dateTimeSent;
	}

	public void setDateTimeSent(Date dateTimeSent) {
		this.dateTimeSent = dateTimeSent;
	}
	@RdfProperty(Constants.DEPTHS_NS + "inResponseTo")
	public Submission getInResponseTo() {
		return inResponseTo;
	}
	public void setInResponseTo(Submission inResponseTo) {
		this.inResponseTo = inResponseTo;
	}
	@RdfProperty(Constants.DEPTHS_NS + "projectRef")
	public Project getProjectRef() {
		return projectRef;
	}
	public void setProjectRef(Project projectRef) {
		this.projectRef = projectRef;
	}
	@RdfProperty(Constants.LOCO_NS + "sentBy")
	public User getSentBy() {
		return sentBy;
	}
	public void setSentBy(User sentBy) {
		this.sentBy = sentBy;
	}

	
}
