package org.goodoldai.depths.domain.content;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import org.goodoldai.depths.config.Constants;
import org.goodoldai.depths.domain.general.Scale;
import org.goodoldai.depths.domain.project.Project;
import org.goodoldai.depths.domain.skos.Concept;
import org.goodoldai.depths.domain.user.Grouping;
import org.goodoldai.depths.domain.user.User;
 

import thewebsemantic.Namespace;
import thewebsemantic.RdfProperty;
import thewebsemantic.RdfType;

@Namespace(Constants.DEPTHS_NS)
@RdfType("DesignProblem")
public class DesignProblem extends ContentItem{
	/**
	 * 
	 */
	private static final long serialVersionUID = -2651694374178006486L;
	private String description;
	private User createdBy;
	private Scale usingScale;
	private Date startDate;
	private Date endDate;
	private String projectType;
	private int groupmode;

	private Grouping grouping;
	private int visible;
	private int groupmembersonly;






	private Collection<Task> taskRef;
	private Collection<Project> isDescribedByProject;
	private Collection<Course> courseRef;
	private Collection<Concept> hasRelevantConcept;
	

public DesignProblem(){
	 courseRef=new ArrayList<Course>();
	isDescribedByProject=new ArrayList<Project>();
	taskRef=new ArrayList<Task>();
	 hasRelevantConcept=new ArrayList<Concept>();
}
@RdfProperty(Constants.DEPTHS_NS+"projecttype")
public String getProjectType() {
	return projectType;
}
public void setProjectType(String projectType) {
	this.projectType = projectType;
}
@RdfProperty(Constants.DEPTHS_NS+"startDate")
public Date getStartDate() {
	return startDate;
}

public void setStartDate(Date startDate) {
	this.startDate = startDate;
}
@RdfProperty(Constants.DEPTHS_NS+"endDate")
public Date getEndDate() {
	return endDate;
}

public void setEndDate(Date endDate) {
	this.endDate = endDate;
}
	/**
	 * @return the description
	 */
	@RdfProperty(Constants.DC_TERMS_NS+"description")
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	
	 
	@RdfProperty(Constants.DEPTHS_NS+"createdBy")
	public User getCreator() {
		return createdBy;
	}

	 
	public void setCreator(User creator) {
		this.createdBy = creator;
	}
	@RdfProperty(Constants.DEPTHS_NS + "isDescribedByProject")
	 	public Collection<Project> getIsDescribedByProject() {
		return isDescribedByProject;
	}

	public void setIsDescribedByProject(Collection<Project> isDescribedByProject) {
		this.isDescribedByProject = isDescribedByProject;
	}

	public void addIsDescribedByProject(Project project) {
		if (null != project) {
			if (!getIsDescribedByProject().contains(project)) {
				getIsDescribedByProject().add(project);
			}
		}
	}
	@RdfProperty(Constants.DEPTHS_NS + "courseRef")
	public Collection<Course> getCourseRef() {
		return courseRef;
	}
	public void setCourseRef(Collection<Course> courseRef) {
		this.courseRef = courseRef;
	}

	
	public void addCourseRef(Course course) {
		if (null != course) {
			if (!getCourseRef().contains(course)) {
				getCourseRef().add(course);
			}
		}
	}
	@RdfProperty(Constants.LOCO_NS + "usingScale")
	public Scale getUsingScale() {
		return usingScale;
	}
	public void setUsingScale(Scale usingScale) {
		this.usingScale = usingScale;
	}
	@RdfProperty(Constants.DEPTHS_NS + "taskRef")
	public Collection<Task> getTaskRef() {
		return taskRef;
	}
	public void setTaskRef(Collection<Task> taskRef) {
		this.taskRef = taskRef;
	}

	
	public void addTaskRef(Task task) {
		if (null != task) {
			if (!getTaskRef().contains(task)) {
				getTaskRef().add(task);
			}
		}
	}
	@RdfProperty(Constants.DEPTHS_NS + "hasRelevantConcept")
	public Collection<Concept> getHasRelevantConcept() {
		return hasRelevantConcept;
	}
	public void setHasRelevantConcept(Collection<Concept> hasRelevantConcept) {
		this.hasRelevantConcept = hasRelevantConcept;
	}
	public void removeAllRelevantConcepts(){
		this.hasRelevantConcept.clear();
	}
	public void addHasRelevantConcept(Concept concept) {
		if (null != concept) {
			if (!getHasRelevantConcept().contains(concept)) {
				hasRelevantConcept.add(concept);
			}
		}
	}
	@RdfProperty(Constants.DEPTHS_NS + "groupmode")
	public int getGroupmode() {
		return groupmode;
	}
	public void setGroupmode(int groupmode) {
		this.groupmode = groupmode;
	}
	@RdfProperty(Constants.DEPTHS_NS + "grouping")
	public Grouping getGrouping() {
		return grouping;
	}
	public void setGrouping(Grouping grouping) {
		this.grouping = grouping;
	}
	@RdfProperty(Constants.DEPTHS_NS + "visible")
	public int getVisible() {
		return visible;
	}
	public void setVisible(int visible) {
		this.visible = visible;
	}
	@RdfProperty(Constants.DEPTHS_NS + "groupmembersonly")
	public int getGroupmembersonly() {
		return groupmembersonly;
	}
	public void setGroupmembersonly(int groupmembersonly) {
		this.groupmembersonly = groupmembersonly;
	}

	
}
