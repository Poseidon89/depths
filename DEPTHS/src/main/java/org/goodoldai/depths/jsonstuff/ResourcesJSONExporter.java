package org.goodoldai.depths.jsonstuff;

import java.util.Collection;
import java.util.LinkedList;

import org.apache.log4j.Logger;

import org.codehaus.jettison.json.JSONStringer;
import org.goodoldai.depths.domain.activity.Brainstorming;
import org.goodoldai.depths.domain.general.Relevance;
import org.goodoldai.depths.domain.general.Resource;
import org.goodoldai.depths.domain.resources.WebResource;
import org.goodoldai.depths.domain.skos.Concept;
import org.goodoldai.depths.semanticstuff.services.annotations.AnnotationManagament;
import org.goodoldai.depths.utility.StringUtility;
 

public class ResourcesJSONExporter {
	private static final Logger LOGGER = Logger.getLogger(ResourcesJSONExporter.class);
	private static final class ResourcesJSONExporterHolder{
		private static final ResourcesJSONExporter INSTANCE=new ResourcesJSONExporter();
	}
	public static ResourcesJSONExporter getInstance(){
		return ResourcesJSONExporterHolder.INSTANCE;
	}
	public String exportRecommendedResourcesToJSON(Collection<Relevance> webPageRelevances,
			Collection<Relevance> contentRelevances, String conceptUri) throws Exception{
		Collection<String> addedItems=new LinkedList<String>();
		AnnotationManagament annMan = AnnotationManagament.getInstance();
		Concept concept = annMan.loadResourceByURI(Concept.class,
				conceptUri, false);
		JSONStringer js=new JSONStringer();
		js.array();
		js.object();
		js.key("concepturi").value(conceptUri);
		js.key("concepttitle").value(concept.getTitle());
		js.endObject();
		js.object().key("onlineresources");
		
		 js.array();
		
		for(Relevance wpRel:webPageRelevances){
			 
			Resource relRes=wpRel.getIsRelevanceOf();
			if(!addedItems.contains(relRes.getUri().toString())){
				addedItems.add(relRes.getUri().toString());
			 
			String href=((WebResource) relRes).getHref();
			js.object();
			js.key("href").value(href);
			js.key("uri").value(relRes.getUri());
			js.key("title").value(relRes.getTitle());
			if(wpRel.getHasOveralValue()>1){
				js.key("relevance").value(1);
			}else{
				js.key("relevance").value(wpRel.getHasOveralValue());
			}
			
			 
			js.endObject();
			}
		}
		 js.endArray();
		js.endObject();
		js.object().key("contentresources");
		  js.array();
		 for(Relevance contRel:contentRelevances){
			Resource relRes=contRel.getIsRelevanceOf();
			if(!addedItems.contains(relRes.getUri().toString())){
				addedItems.add(relRes.getUri().toString());
				 
			if(relRes instanceof Brainstorming){
				try{
					
				js.object();
				 
				
				js.key("resourcetype").value("brainstorming");
				if(((Brainstorming) relRes).getSentBy()!=null){
				js.key("sentby").value(((Brainstorming) relRes).getSentBy().getUsername());
				}
				js.key("uri").value(relRes.getUri().toString());
				if(((Brainstorming) relRes).getInResponseTo()!=null){
				js.key("inresponseto").value(((Brainstorming) relRes).getInResponseTo().getUri().toString());
				}
				if(((Brainstorming) relRes).getHasContent()!=null){
				String content=((Brainstorming) relRes).getHasContent();
				content=StringUtility.getShortContent(content,70);
				
				js.key("content").value(content);
				}
				 
				js.key("relevance").value(contRel.getHasOveralValue());
				js.endObject();
				
				}catch(Exception e){
					LOGGER.info("Exception:"+e.getMessage());
				}
			}
		}
		}
		  js.endArray();
		js.endObject();
		
		js.endArray();
	 
		return js.toString();
	}

}
