package org.goodoldai.depths.jsonstuff;


import java.util.Collection;


import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONStringer;
import org.goodoldai.depths.config.Constants;
import org.goodoldai.depths.domain.content.Diagram;
import org.goodoldai.depths.domain.project.Project;
import org.goodoldai.depths.domain.project.Solution;
import org.goodoldai.depths.domain.user.User;
import org.goodoldai.depths.semanticstuff.rdfpersistance.DataModelManager;
import org.goodoldai.depths.semanticstuff.rdfpersistance.query.ontmodel.OntModelQueryService;
import org.goodoldai.depths.semanticstuff.rdfpersistance.query.ontmodel.OntModelQueryServiceImpl;
import org.goodoldai.depths.semanticstuff.rdfpersistance.query.results.QueryResult;
import org.goodoldai.depths.semanticstuff.rdfpersistance.query.results.ResultsCollection;
import org.goodoldai.depths.semanticstuff.services.project.ProjectManagament;
import org.goodoldai.depths.services.datalayer.SolutionsDataLayer;
import org.goodoldai.depths.utility.DateTimeUtility;
import org.goodoldai.depths.utility.StringUtility;

 
 

 

public class ProjectJSONExporter {
	private OntModelQueryService queryService = new OntModelQueryServiceImpl();
	private static final Logger LOGGER = Logger.getLogger(ProjectJSONExporter.class
			.getName());
	private static class ProjectJSONExporterHolder{
		private static final ProjectJSONExporter INSTANCE=new ProjectJSONExporter();
	}
	public static ProjectJSONExporter getInstance(){
		return ProjectJSONExporterHolder.INSTANCE;
	}
	public String getProjectsAsJSON(Collection<Project> projects, User user) throws JSONException{
		JSONStringer js=new JSONStringer();
		js.array();
		if(projects!=null){
		for (Project project:projects){
			boolean availability=ProjectManagament.getInstance().checkProjectAvailabilityByDate(project, user);

			if(availability){
				this.serializeProjectSolution(js, project);
			}
			 
		}
		}
		
		js.endArray();
	 
		return js.toString();
	}
	public String getProjectsAsJSON(Collection<Project> projects) throws JSONException{
		JSONStringer js=new JSONStringer();
		js.array();
		if(projects!=null){
		for (Project project:projects){
				this.serializeProjectSolution(js, project);
			}
		}
		
		js.endArray();
		
		return js.toString();
	}
	public String getProjectAsJSON(Project project) throws JSONException{
		 
		JSONStringer js=new JSONStringer();
		js.array();
		js.object();
		js.key("projecturi").value(project.getUri().toString());
		js.key("project");
		js.object();
		js.key("title").value(project.getTitle());
		js.key("fileurl").value(project.getFileUrl());
		js.key("datetimecreated").value(project.getDateCreated());
		js.key("datetimecreatedE").value(DateTimeUtility.convertDateTimeToEpoch(project.getDateCreated()));
		User creator=(User) project.getCreatedBy();
		if(creator!=null){
		js.key("createdBy").value(creator.getUri().toString());
		js.key("creatorname").value(creator.getFirstname()+" "+creator.getLastname());
		}else{
			js.key("createdBy").value("");
			js.key("creatorname").value("");
		}
		if(project.getParentProject()!=null){
			js.key("parentProject").value(project.getParentProject().getUri().toString());
		}
		js.endObject();
		js.endObject();
		js.endArray();
		 
		return js.toString();
	}
	public  ResultsCollection getDiagramsForProject(String projectUri){
		ResultsCollection result=null;
		String queryString = "PREFIX rdf: <" + Constants.RDF_NS + "> \n"
				+ "PREFIX sioc: <" + Constants.SIOC_NS + "> \n"
				+ "PREFIX foaf: <" + Constants.FOAF_NS + "> \n"  
				+ "PREFIX dc: <" + Constants.DC_TERMS_NS + "> \n" 
				+ "PREFIX loco: <" + Constants.LOCO_NS + "> \n" 
				+"PREFIX depths: <" + Constants.DEPTHS_NS + "> \n"
				+ "SELECT  DISTINCT (str(?dtitle) AS ?title) ?diagram ?diagramtype (str(?dfileUrl) AS ?fileUrl) \n" + "WHERE  {\n"
				+ "<"+projectUri+"> rdf:type depths:Project.\n"
				+ "<"+projectUri+">  depths:containsDiagram ?diagram.\n"
				 
				+"?diagram depths:id ?id.\n"
				+"?diagram rdf:type ?diagramtype.\n"
				+"?diagram loco:fileUrl ?dfileUrl.\n"
				+"?diagram dc:title ?dtitle.\n"
			
				+"}\n ";
				                 
	 
		result = queryService.executeSelectSparqlQuery(queryString, DataModelManager.getInstance().getDataModel());	
		 
		 
		return result;
	}
	public  ResultsCollection getProjectAndSolution(String projectUri){
		ResultsCollection result=null;
		String queryString = "PREFIX rdf: <" + Constants.RDF_NS + "> \n"
				+ "PREFIX sioc: <" + Constants.SIOC_NS + "> \n"
				+ "PREFIX foaf: <" + Constants.FOAF_NS + "> \n"  
				+ "PREFIX dc: <" + Constants.DC_TERMS_NS + "> \n" 
				+ "PREFIX loco: <" + Constants.LOCO_NS + "> \n" 
				+"PREFIX depths: <" + Constants.DEPTHS_NS + "> \n"
				+ "SELECT  DISTINCT ?creator (str(?pprojectid) AS ?projectid)  (str(?pfileurl) AS ?fileurl) " +
				" ?datetimecreated (str(?ptitle) AS ?title) ?creator (str(?ufirstname) AS ?firstname) (str(?ulastname) AS ?lastname)" +
				" ?parentproject" +
				" ?solution (str(?pdescription) AS ?description) (str(?phasAdditionalRequirements) AS ?hasAdditionalRequirements) (str(?phasCons) AS ?hasCons) " +
				"(str(?phasConsequences) AS ?hasConsequences) (str(?phasDesignConstraints) AS ?hasDesignConstraints)" +
				" (str(?phasDesignRules) AS  ?phasDesignRules) (str(?phasPros) AS ?hasPros)\n" + "WHERE  {\n"
				+ "<"+projectUri+"> rdf:type depths:Project.\n"
				+ "<"+projectUri+">  depths:projectid ?pprojectid.\n"
				+ "<"+projectUri+">  dc:title ?ptitle.\n"
				+ "<"+projectUri+">  loco:fileUrl ?pfileurl.\n"
				+ "<"+projectUri+">  dc:created ?datetimecreated.\n"
			
				+"OPTIONAL{"
				+ "<"+projectUri+">  depths:hasParentProject ?parentproject.\n"
				+"}"
				+ "<"+projectUri+">  depths:createdBy ?creator.\n"
				+" ?creator loco:firstname ?ufirstname.\n"
				+"?creator loco:lastname ?ulastname.\n"
				+ "<"+projectUri+">  depths:hasSolution ?solution.\n"
				+"?solution depths:description ?dpescription.\n"
				+"?solution depths:hasAdditionalRequirements ?phasAdditionalRequirements.\n"
				+"?solution depths:hasCons ?phasCons.\n"
				+"?solution depths:hasConsequences ?phasConsequences.\n"
				+"?solution depths:hasDesignConstraints ?phasDesignConstraints.\n"
				+"?solution depths:hasDesignRules ?phasDesignRules.\n"
				+"?solution depths:hasPros ?phasPros.\n"
			
				+"}\n ";
				                 
 
		result = queryService.executeSelectSparqlQuery(queryString, DataModelManager.getInstance().getDataModel());	
		 
		 
		return result;
	}
	public void serializeProjectSolutionForProjectUri(JSONStringer js, String projectUri) throws JSONException {
		js.array();
		js.object();
		LOGGER.info("serializeProjectSolutionForProjectUri KT-1");
		ResultsCollection result=getProjectAndSolution(projectUri);
		LOGGER.info("serializeProjectSolutionForProjectUri KT-2");
		if (result != null && !result.getResultsCollection().isEmpty()){
			LOGGER.info("serializeProjectSolutionForProjectUri KT-3");
			for (QueryResult queryResult : result.getResultsCollection()) {
			
				LOGGER.info("serializeProjectSolutionForProjectUri KT-4");
				String projectid=queryResult.getVariableValue("projectid");
				String title=queryResult.getVariableValue("title");
				String fileurl=queryResult.getVariableValue("fileurl");
				String datetimecreated=queryResult.getVariableValue("datetimecreated");
				datetimecreated=StringUtility.replaceLiteralType(datetimecreated);
				String creator=queryResult.getVariableValue("creator");
				String creatorFN=queryResult.getVariableValue("firstname");
				String creatorLN=queryResult.getVariableValue("lastname");
				String parentproject=queryResult.getVariableValue("parentproject");
		
				js.key("projecturi").value(projectUri);
				js.key("projectid").value(projectid);
				js.key("project");
		
				js.object();
				js.key("title").value(title);
				js.key("fileurl").value(fileurl);
				js.key("datetimecreated").value(datetimecreated);
	
				 
				//Date dateC=null;
				//try {
					//dateC = DateFormat.getInstance().parse(datetimecreated);
					//dateC=new SimpleDateFormat("yyyy-mm-ddTHH:mm:ss.mmmz").parse(datetimecreated);

				//} catch (ParseException e) {
					// TODO Auto-generated catch block
				//	LOGGER.error("Error:"+e.getLocalizedMessage());
				//}
				//js.key("datetimecreatedE").value(DateTimeUtility.convertDateTimeToEpoch(dateC));
				js.key("datetimecreatedE").value(datetimecreated);
		
				if(creator!=null){
				js.key("createdBy").value(creator);
				js.key("creatorname").value(creatorFN+" "+creatorLN);
			}else{
				js.key("createdBy").value("");
				js.key("creatorname").value("");
			}
			
					js.key("parentProject").value(parentproject);
				 
				js.endObject();
	
				String solution=queryResult.getVariableValue("solution");
				String description=queryResult.getVariableValue("description");
				String hasAdditionalRequirements=queryResult.getVariableValue("hasAdditionalRequirements");
				String hasCons=queryResult.getVariableValue("hasCons");
				String hasConsequences=queryResult.getVariableValue("hasConsequences");
				String hasDesignConstraints=queryResult.getVariableValue("hasDesignConstraints");
				String hasDesignRules=queryResult.getVariableValue("hasDesignRules");
				String hasPros=queryResult.getVariableValue("hasPros");
	
					
					js.key("solution");
					js.object();
					js.key("solutionuri").value(solution);
					js.key("description").value(description);
					js.key("hasAdditionalRequirements").value(hasAdditionalRequirements);
					js.key("hasCons").value(hasCons);
					js.key("hasConsequences").value(hasConsequences);
					js.key("hasDesignConstraints").value(hasDesignConstraints);
					js.key("hasDesignRules").value(hasDesignRules);
					js.key("hasPros").value(hasPros);
					js.endObject();
					LOGGER.info("serializeProjectSolutionForProjectUri KT-5");
					ResultsCollection dResult=this.getDiagramsForProject(projectUri);
					if (dResult != null && !dResult.getResultsCollection().isEmpty()){
						js.key("diagrams");
						js.array();
						for (QueryResult dQueryResult : dResult.getResultsCollection()) {
							LOGGER.info("serializeProjectSolutionForProjectUri KT-6");
							String diagramid=dQueryResult.getVariableValue("id");
							String diagramuri=dQueryResult.getVariableValue("diagram");
							String diagramtype=dQueryResult.getVariableValue("diagramtype");
							String dfileUrl=dQueryResult.getVariableValue("fileUrl");
							String dTitle=dQueryResult.getVariableValue("title");
							
								js.object();
								js.key("id").value(diagramid);
								js.key("diagramuri").value(diagramuri);
								js.key("type").value(diagramtype);
								js.key("fileUrl").value(dfileUrl);
								js.key("title").value(dTitle);
								 
								js.endObject();
								
								LOGGER.info("serializeProjectSolutionForProjectUri KT-7");
							
						}
						js.endArray();
					}
					 
						 
						 
						
						
					 
			}
		}
	 
		js.endObject();
		js.endArray();
		
	}
	public void serializeProjectSolution(JSONStringer js, Project project) throws JSONException {
		js.array();
		js.object();
		js.key("projecturi").value(project.getUri().toString());
		js.key("projectid").value(project.getProjectId().toString());
		js.key("project");
		js.object();
		js.key("title").value(project.getTitle());
		js.key("fileurl").value(project.getFileUrl());
		js.key("datetimecreated").value(project.getDateCreated());
		
		js.key("datetimecreatedE").value(DateTimeUtility.convertDateTimeToEpoch(project.getDateCreated()));
		User creator= project.getCreatedBy();
		if(creator!=null){
		js.key("createdBy").value(creator.getUri().toString());
		js.key("creatorname").value(creator.getFirstname()+" "+creator.getLastname());
	}else{
		js.key("createdBy").value("");
		js.key("creatorname").value("");
	}
		User modificator= project.getModifiedBy();
		if(modificator!=null){
		js.key("modifiedBy").value(modificator.getUri().toString());
		js.key("modifiedByName").value(modificator.getFirstname()+" "+modificator.getLastname());
	}else{
		js.key("modifiedBy").value("");
		js.key("modifiedByName").value("");
	}
		if(project.getParentProject()!=null){
			js.key("parentProject").value(project.getParentProject().getUri().toString());
		}
		js.endObject();
		if(project.getHasSolution()!=null){
			Solution pSolution=project.getHasSolution();
			
			js.key("solution");
			js.object();
			js.key("solutionuri").value(pSolution.getUri().toString());
			js.key("description").value(pSolution.getDescription());
			js.key("hasAdditionalRequirements").value(pSolution.getHasAdditionalRequirements());
			js.key("hasCons").value(pSolution.getHasCons());
			js.key("hasConsequences").value(pSolution.getHasConsequences());
			js.key("hasDesignConstraints").value(pSolution.getHasDesignConstraints());
			js.key("hasDesignRules").value(pSolution.getHasDesignRules());
			js.key("hasPros").value(pSolution.getHasPros());
			js.endObject();
		}
		if(!project.getContainsDiagrams().isEmpty()){
		 
			Collection<Diagram> diagrams=project.getContainsDiagrams();
			js.key("diagrams");
			js.array();
			for(Diagram diagram:diagrams){
				js.object();
				js.key("id").value(diagram.getId());
				js.key("diagramuri").value(diagram.getUri().toString());
				js.key("type").value(diagram.getClass().getSimpleName());
				js.key("fileUrl").value(diagram.getFileUrl());
				js.key("title").value(diagram.getTitle());
				 
				js.endObject();
				
			}
			js.endArray();
			
		}
		 
		js.endObject();
		js.endArray();
	}
	public String getProjectSolutionAsJSON(Project project) throws JSONException{
		 
		JSONStringer js=new JSONStringer();
		 
			this.serializeProjectSolution(js, project);
	 
		return js.toString();
	}
	public String getProjectSolutionAsJSON(String projectUri) throws Exception{
	 	String jsonResult=SolutionsDataLayer.getDataLayerInstance().getProjectJsonByUri(projectUri);
	 	if(jsonResult==null){
	 	JSONStringer js=new JSONStringer();
		 
			this.serializeProjectSolutionForProjectUri(js, projectUri);
		 	SolutionsDataLayer.getDataLayerInstance().setProjectJson(projectUri, js.toString());
		 	return js.toString();
		}else return jsonResult;
	}

}
