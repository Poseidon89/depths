package org.goodoldai.depths.jsonstuff;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONStringer;
import org.goodoldai.depths.config.Settings;
 

public class ConfigurationJSONExporter {
	private static class ConfigurationJSONExporterHolder{
		private static final ConfigurationJSONExporter INSTANCE=new ConfigurationJSONExporter();
	}
	public static ConfigurationJSONExporter getInstance(){
		return ConfigurationJSONExporterHolder.INSTANCE;
	}
	public  String getWebServicesConfigurationJson() throws JSONException{
		JSONStringer js = new JSONStringer();
		// Begin array
		js.array();
		js.object().key("ftp-repository");
		js.array();
		js.object(); 
		js.key("ftp-username").value(Settings.getInstance().config.ftpRepositoryConfig.ftpUser.ftpUserUsername);
		js.key("ftp-password").value(Settings.getInstance().config.ftpRepositoryConfig.ftpUser.ftpUserPassword);
		js.key("ftp-url").value(Settings.getInstance().config.ftpRepositoryConfig.ftpLocations.ftpUrl);
		js.key("ftp-port").value(Settings.getInstance().config.ftpRepositoryConfig.ftpLocations.ftpPort);
		js.key("security-ssl").value(Settings.getInstance().config.ftpRepositoryConfig.ftpLocations.securitySSL);
		js.key("ftp-location-projects").value(Settings.getInstance().config.ftpRepositoryConfig.ftpLocations.ftpLocationProjects);
		js.key("ftp-location-images").value(Settings.getInstance().config.ftpRepositoryConfig.ftpLocations.ftpLocationImages);
		js.endObject();
		js.endArray();
		js.endObject();
		js.endArray();
		 
		return js.toString();
	}
	 
}
