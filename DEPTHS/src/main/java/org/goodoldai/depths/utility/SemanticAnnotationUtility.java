package org.goodoldai.depths.utility;

public class SemanticAnnotationUtility {
private static int maxNumberOfLinks;
private static boolean limitHost;
private static String RMI_HOST=null;
private static int RMI_PORT = 0;
private static double  TFIDFMinAcceptableValue=0.00;

/**
 * @return the maxNumberOfLinks
 */
public static int getMaxNumberOfLinks() {
	return maxNumberOfLinks;
}

/**
 * @param maxNumberOfLinks the maxNumberOfLinks to set
 */
public static void setMaxNumberOfLinks(int maxNumberOfLinks) {
	SemanticAnnotationUtility.maxNumberOfLinks = maxNumberOfLinks;
}

/**
 * @return the limitHost
 */
public static boolean isLimitHost() {
	return limitHost;
}

/**
 * @param limitHost the limitHost to set
 */
public static void setLimitHost(boolean limitHost) {
	SemanticAnnotationUtility.limitHost = limitHost;
}

/**
 * @return the rMI_HOST
 */
public static String getRMI_HOST() {
	return RMI_HOST;
}

/**
 * @param rmi_host the rMI_HOST to set
 */
public static void setRMI_HOST(String rmi_host) {
	RMI_HOST = rmi_host;
}

/**
 * @return the rMI_PORT
 */
public static int getRMI_PORT() {
	return RMI_PORT;
}

/**
 * @param rmi_port the rMI_PORT to set
 */
public static void setRMI_PORT(int rmi_port) {
	RMI_PORT = rmi_port;
}

/**
 * @return the tFIDFMinAcceptableValue
 */
public static double getTFIDFMinAcceptableValue() {
	return TFIDFMinAcceptableValue;
}

/**
 * @param minAcceptableValue the tFIDFMinAcceptableValue to set
 */
public static void setTFIDFMinAcceptableValue(double minAcceptableValue) {
	TFIDFMinAcceptableValue = minAcceptableValue;
}

}
