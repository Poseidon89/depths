package org.goodoldai.depths.utility;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.goodoldai.depths.config.Settings;
//import org.goodoldai.depths.rest.json.SolutionServiceUtilityTest;



public class PropertiesManager {
	private static final Logger LOGGER = Logger.getLogger(PropertiesManager.class);
	static boolean isSetProperties=false;
	
	public PropertiesManager(){
		
	}
	private static void loadParams() {
	   	try{
	 
			InputStream is= (InputStream) PropertiesManager.class.getClassLoader().getResource(Settings.getInstance().config.weightFactorsConfig).getContent();
		 
			Properties props=new Properties();
			  props.load(is);
			 is.close();
			
			setWeightParams(props);
			setThreadParams(props);
			setSemanticAnnotationParams(props);
		    } catch (IOException e) {
               LOGGER.error("IOException in loadParams "+e.getStackTrace().toString());
           }
	 
	}
	private static void setWeightParams(Properties props){
		 	WeightFactorsUtility.setAssessingActivityWeightPeers(Double.parseDouble(props.getProperty("problem_activity_assessing")));
		WeightFactorsUtility.setBrainstormingActivityWeightPeers(Double.parseDouble(props.getProperty("problem_activity_brainstorming")));
		WeightFactorsUtility.setSubmittingActivityWeightPeers(Double.parseDouble(props.getProperty("problem_activity_submitting")));
		
		WeightFactorsUtility.setAssessingCourseActivityWeightPeers(Double.parseDouble(props.getProperty("problem_course_activity_assessing")));
		WeightFactorsUtility.setBrainstormingCourseActivityWeightPeers(Double.parseDouble(props.getProperty("problem_course_activity_brainstorming")));
		WeightFactorsUtility.setSubmittingCourseActivityWeightPeers(Double.parseDouble(props.getProperty("problem_course_activity_submitting")));
		
		WeightFactorsUtility.setActivityCompetenceWeightPeers(Double.parseDouble(props.getProperty("problem_activity_competence")));
		
		WeightFactorsUtility.setBrainstormingSkillWeightPeers(Double.parseDouble(props.getProperty("problem_skill_brainstorming")));
		WeightFactorsUtility.setSubmittingSkillWeightPeers(Double.parseDouble(props.getProperty("problem_skill_submitting")));
		
		WeightFactorsUtility.setBrainstormingCourseSkillWeightPeers(Double.parseDouble(props.getProperty("problem_course_skill_brainstorming")));
		WeightFactorsUtility.setSubmittingCourseSkillWeightPeers(Double.parseDouble(props.getProperty("problem_course_skill_submitting")));
		
		WeightFactorsUtility.setSkillCompetenceWeightPeers(Double.parseDouble(props.getProperty("problem_skill_competence")));
		
		//WeightFactorsUtility.setSubmittingActivityWeightPeers(Double.parseDouble(props.getProperty("peers_activity_submitting")));
		//WeightFactorsUtility.setSubmittingActivityWeightPeers(Double.parseDouble(props.getProperty("peers_activity_submitting")));
		WeightFactorsUtility.setRatingRelevancePercent(Double.parseDouble(props.getProperty("rating_relevance_percent")));
		WeightFactorsUtility.setPatternRelevancePercent(Double.parseDouble(props.getProperty("pattern_relevance_percent")));
	}
	private static void setThreadParams(Properties props){
		ThreadsUtility.setSetFieldThreadDelayTime(Integer.parseInt(props.getProperty("set_field_thread_delay_time")));
		ThreadsUtility.setDeleteRecordThreadDelayTime(Integer.parseInt(props.getProperty("delete_record_thread_delay_time")));
		ThreadsUtility.setFindPeersThreadDelayTime(Integer.parseInt(props.getProperty("find_peers_thread_delay_time")));
		ThreadsUtility.setFindWebPagesThreadDelayTime(Integer.parseInt(props.getProperty("find_web_pages_thread_delay_time")));
		ThreadsUtility.setRateWebPagesThreadDelayTime(Integer.parseInt(props.getProperty("rate_web_pages_thread_delay_time")));
		ThreadsUtility.setRateWebPagesThreadDelayTime(Integer.parseInt(props.getProperty("rate_content_thread_delay_time")));
		ThreadsUtility.setRateWebPagesThreadDelayTime(Integer.parseInt(props.getProperty("find_content_thread_delay_time")));
	}
	private static void setSemanticAnnotationParams(Properties props){
		//SemanticAnnotationUtility.setRMI_HOST(props.getProperty("RMI_HOST"));
		//SemanticAnnotationUtility.setRMI_PORT(Integer.parseInt(props.getProperty("RMI_PORT")));
		SemanticAnnotationUtility.setMaxNumberOfLinks(Integer.parseInt(props.getProperty("max_links_number")));
		SemanticAnnotationUtility.setLimitHost(Boolean.parseBoolean(props.getProperty("limit_host")));
		
		SemanticAnnotationUtility.setTFIDFMinAcceptableValue(Double.parseDouble(props.getProperty("tfidf_min_acceptable_value")));
		
		
	}
	/**
	 * @return the isSetProperties
	 */
	public static boolean isSetProperties() {
		return isSetProperties;
	}
	/**
	 * @param isSetProperties the isSetProperties to set
	 */
	public static void setSetProperties(boolean isSetProp) {
	 
		isSetProperties = isSetProp;
		 
	}
	public static void setFactors(){
	 
		if (!isSetProperties){
 
			loadParams();
 
			setSetProperties(true);
 
		 
		}
 
	}

}
