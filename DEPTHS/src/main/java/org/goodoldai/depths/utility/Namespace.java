package org.goodoldai.depths.utility;

import java.io.Serializable;

public class Namespace implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String prefix;
	private String namespaceURI;
	/**
	 * This class keeps data about specific namespace
	 * @param prefix
	 * @param namespace
	 */
	public Namespace(String prefix, String namespace)
	{
		this.setPrefix(prefix);
		this.setNamespaceURI(namespace);
	}
	/**
	 * @return the prefix
	 */
	public String getPrefix() {
		return prefix;
	}
	/**
	 * @param prefix the prefix to set
	 */
	private void setPrefix(String prefix) {
		this.prefix = prefix;
	}
	/**
	 * @return the namespaceURI
	 */
	public String getNamespaceURI() {
		return namespaceURI;
	}
	/**
	 * @param namespaceURI the namespaceURI to set
	 */
	private void setNamespaceURI(String namespaceURI) {
		this.namespaceURI = namespaceURI;
	}

}
