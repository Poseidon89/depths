package org.goodoldai.depths.utility;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.jsoup.Jsoup;

public class StringUtility {

	public static String hex(byte[] array) {
		  StringBuffer sb = new StringBuffer();
		  for (int i = 0; i < array.length; ++i) {
		    sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).toUpperCase().substring(1,3));
		  }
		  return sb.toString();
		}
		 
		 
		public static String md5(String message) { 
		  try { 
		    MessageDigest md = MessageDigest.getInstance("MD5"); 
		    return hex (md.digest(message.getBytes("CP1252"))); 
		  } catch (NoSuchAlgorithmException e) { } catch (UnsupportedEncodingException e) { } 
		  return null;
		}
		public static String[] commaSeparatedStringToStringArray(String aString){
		    String[] splittArray = null;
		    if (aString != null || !aString.equalsIgnoreCase("")){
		         splittArray = aString.split(",");
		     
		    }
		    return splittArray;
		}
		public static String getShortContent(String content, int length) {
			 
			String contentToRemove[]={"<br/>","<br />","<br>","</br>"};
			 int arraySize=contentToRemove.length;
			 for (int i=0;i<arraySize;i++){
				String cont= contentToRemove[i];
			if (content.contains(cont)){
				content=content.replace(cont, "");
			 
			}
			 }
			int len=content.length();
			 
			if (len>length)
				len=length;
			String shortContent=content.substring(0, len)+"...";
			return shortContent;
		}
		public static String replaceLiteralType(String content){
			
			String res=content;
			if(content.contains("^^")){
				int index=content.indexOf("^^");
				int length=content.length();
			
				 res=content.substring(0,index);
			}
			return res;
		}
		public static String html2text(String html) {
		    return Jsoup.parse(html).text();
		}
		public static String replace(String str, String pattern, String replace) {
		    int s = 0;
		    int e = 0;
		    StringBuffer result = new StringBuffer();

		    while ((e = str.indexOf(pattern, s)) >= 0) {
		        result.append(str.substring(s, e));
		        result.append(replace);
		        s = e+pattern.length();
		    }
		    result.append(str.substring(s));
		    return result.toString();
		}

}
