package org.goodoldai.depths.annotation;

import java.util.ArrayList;
import java.util.HashMap;

class DomainConceptSemAnnotation {
	private ArrayList<String> webPageLocations = new ArrayList<String>();
	private ArrayList<PageHits> sortedWebPages = new ArrayList<PageHits>();
	private String patternName = null;
	private String patternForm = null;
	String patternInstanceURI = null;

	/**
	 * @return the webPageLocations
	 */
	public ArrayList<String> getWebPageLocations() {
		return webPageLocations;
	}

	public ArrayList<PageHits> getSortedWebPages() {
		return sortedWebPages;
	}

	/**
	 * @param webPageLocations
	 *            the webPageLocations to set Sort the pages that hits one
	 *            pattern based on the number of hits, so the first place will
	 *            be the page that has at most instances of the pattern name.
	 */
	void sortWebPages(OnlineRepositoryManager onlineRepManager) {
	 
		HashMap<String, PageHits> pagesHits = onlineRepManager.getPagesHits();
		 
		for (int i = 0; i < webPageLocations.size(); i++) {
			 
			String currPageURL = webPageLocations.get(i);
		 
			PageHits pageHits = pagesHits.get(currPageURL);
		 
			if (pageHits != null) {
				 
				String dominantPattern = (pageHits.getDominantPattern() != null) ? pageHits
						.getDominantPattern() : "";
			 
				if (dominantPattern.equals(patternInstanceURI)) {
				 
					pageHits.setPatternRelevance(onlineRepManager,
							patternInstanceURI);
					 
					addPageOnRightPosition(pageHits);
				 
				}
			}
			 
		}
		 

	}

	/**
	 * 
	 * @param pageHitsToAdd
	 *            Finds the appropriate place for page in list based on the
	 *            number of hits.
	 */
	private void addPageOnRightPosition(PageHits pageHitsToAdd) {
		double pageRelevanceToAdd = pageHitsToAdd.getPageRelevance();
		boolean pageAdded = false;

		if (sortedWebPages.size() > 0) {
			for (int i = 0; i < sortedWebPages.size(); i++) {
				PageHits pageToCheck = sortedWebPages.get(i);

				int pageRelevanceToCheck = pageToCheck.getDominantPatternHits();

				if (pageRelevanceToAdd > pageRelevanceToCheck) {
					sortedWebPages.add(i, pageHitsToAdd);
					pageAdded = true;
					break;
				}
			}
		}
		if (!pageAdded) {
			sortedWebPages.add(pageHitsToAdd);
		}

	}

	/**
	 * 
	 * @param webPageLocations
	 */
	public void setWebPageLocations(ArrayList<String> webPageLocations) {
		this.webPageLocations = webPageLocations;
	}

	/**
	 * Add new web page location if it does not exists
	 * 
	 * @param url
	 */
	void addWebPageLocation(String url) {
 
		boolean webPageExists = false;
		for (int i = 0; i < webPageLocations.size(); i++) {
			String currentUrl = webPageLocations.get(i);
			if (currentUrl.equals(url)) {
				webPageExists = true;
			}
		}
		if (!webPageExists) {

			webPageLocations.add(url);
		}
	}

	/**
	 * @return the patternName
	 */
	public String getPatternName() {
		return patternName;
	}

	/**
	 * @param patternName
	 *            the patternName to set
	 */
	public void setPatternName(String patternName) {

		this.patternName = patternName;
	}

	/**
	 * @return the patternForm
	 */
	public String getPatternForm() {
		return patternForm;
	}

	/**
	 * @param patternForm
	 *            the patternForm to set
	 */
	public void setPatternForm(String patternForm) {

		this.patternForm = patternForm;
	}

	/**
	 * @return the patternInstanceURI
	 */
	public String getPatternInstanceURI() {
		return patternInstanceURI;
	}

	/**
	 * 
	 * @param patternInstanceURI
	 *            the patternInstanceURI to set
	 */
	public void setPatternInstanceURI(String patternInstanceURI) {

		this.patternInstanceURI = patternInstanceURI;
	}

}
