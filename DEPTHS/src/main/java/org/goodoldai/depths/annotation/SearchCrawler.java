package org.goodoldai.depths.annotation;

import java.io.*;
import java.net.*;
import java.util.*;
import java.util.regex.*;
 
//import depths.moodle.SemanticAnnotationFactory;

import org.apache.log4j.Logger;
import org.goodoldai.depths.domain.resources.OnlineRepository;
import org.goodoldai.depths.semanticstuff.services.content.ContentManagament;
import org.goodoldai.depths.services.resources.ResourceQueries;
import org.goodoldai.depths.utility.PropertiesManager;
import org.goodoldai.depths.utility.SemanticAnnotationUtility;
//import depths.moodle.annotation.utility.AnnotationFacility;
// The Search Web Crawler
class SearchCrawler implements Serializable
{
	private static final Logger LOGGER = Logger.getLogger(SearchCrawler.class);
  /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
// Cache of robot disallow lists.
  private HashMap<String, ArrayList<String>> disallowListCache = new HashMap<String, ArrayList<String>>();
 
  private HashMap<String,PageHits> setOfRepositoryLinks=new HashMap<String,PageHits>();
  
  private boolean crawling;
  // Matches log file print writer.
  private PrintWriter logFileWriter;
  // Constructor for Search Web Crawler.
  public SearchCrawler()
  {
	   
	 PropertiesManager.setFactors();
     
  }
  
  // Handle Search/Stop button being clicked.
  void startsearching(String url,String name, int maxUrlNumber, String ontologyUri) {
	 LOGGER.info("startSearching..."+url+" maxUrlNumber:"+maxUrlNumber);
	  // Validate that start URL has been entered.
  String startUrl = url;
   
  if (startUrl.length() < 1) {
    LOGGER.error("Missing Start URL." );
  }
  // Verify start URL.
  else if (verifyUrl(startUrl) == null) {
	  LOGGER.error("Invalid Start URL.");
  }
  // Validate that Max URLs is either empty or is a number.
  int maxUrls=0;
  if (maxUrlNumber==0){
  maxUrls = SemanticAnnotationUtility.getMaxNumberOfLinks();
  }else{
	  maxUrls=maxUrlNumber;
  }
  if (maxUrls < 1) {
    	LOGGER.error("Invalid Max URLs value.");
    }
   
  // Validate that matches log file has been entered. 
  String logFile = "someLogFile.txt";
  if (logFile.length() < 1) {
	  LOGGER.error("Missing Matches Log File.");
  }
  // Remove "www" from start URL if present.
  startUrl = removeWwwFromUrl(startUrl);
  
  // Start the Search Crawler.
  try {
	search(logFile, startUrl,name, maxUrls, ontologyUri);
} catch (Exception e) {
	// TODO Auto-generated catch block
	LOGGER.error("Error:"+e.getLocalizedMessage());
}
 LOGGER.info("finished searching..."+url);
}
private void search( final String logFile, final String startUrl,final String name,
  final int maxUrls, String ontologyUri ) throws Exception
{
 
	
   //OnlineRepository onlineRepository=new OnlineRepository();
   OnlineRepository onlineRepository=ResourceQueries.getInstance().getOnlineRepository(startUrl);
   onlineRepository.setTitle(name);
   onlineRepository.setHref(startUrl);
   ContentManagament.getInstance().saveResource(onlineRepository,false);
    OnlineRepositoryManager onlineRepositoryManager=new OnlineRepositoryManager(onlineRepository);
    onlineRepositoryManager.repositoryUrl=startUrl;
 
      // Open matches log file.
    	boolean limitHost=SemanticAnnotationUtility.isLimitHost();
      try {
        logFileWriter = new PrintWriter(new FileWriter(logFile));
      } catch (Exception e) {
    	  LOGGER.error("Unable to open matches log file."); 
        return;
      }
      // Turn crawling flag on.
      crawling = true;
   
       // Perform the actual crawling.
      crawl(startUrl, maxUrls, limitHost);
        // Turn crawling flag off.
      crawling = false;
      // Close matches log file.
      try {
        logFileWriter.close();
        onlineRepositoryManager.setPagesHits(setOfRepositoryLinks);
          
      } catch (Exception e) {
    	  LOGGER.error("Unable to close matches log file."+e.getCause());
      }
      try{
     	   AnnotationFacility.annotateOnlineRepository(onlineRepositoryManager, ontologyUri);
                
      }catch(Exception e){
    	  LOGGER.error("Unable to annotate online repository."+e.getCause());
      }
     
     
 
}
 
 private boolean urlDocType(String url){
	 boolean valid=true;
	 String[] notAllowedLinkContent={"action=edit","action=history","%","�","�","�","�","�","�","�","�","�",
			 ".jpeg",".jpg",".png",".bmp",".gif",".ps",".svg",".tif","&amp;",".vsd",".pptx",".ppt",".ogg"};
	 if (url.toLowerCase().endsWith("/")){
	 
		 valid=true;
	 }
	 for(int i=0;i<notAllowedLinkContent.length;i++){
		 String checkContent=notAllowedLinkContent[i];
		 if(url.toLowerCase().contains(checkContent)){
		
			 valid=false;
		 }
	 }

	 return valid;
 }
// Verify URL format.
private URL verifyUrl(String url) {
	
  // Only allow HTTP URLs.
  if ((!url.toLowerCase().startsWith("http://"))){
  if (!url.toLowerCase().startsWith("www.")){
		  return null;
	  }
  }

  if (!urlDocType(url)){
	  return null;
  }
  
  // Verify format of URL.
  URL verifiedUrl = null;
  try {
    verifiedUrl = new URL(url);
  } catch (Exception e) {
	  LOGGER.error("verifyUrl Error:"+verifiedUrl);
    return null;
  }
   
  return verifiedUrl;
}
// Check if robot is allowed to access the given URL. 
@SuppressWarnings("unused")
private boolean isRobotAllowed(URL urlToCheck) {
	 
  String host = urlToCheck.getHost().toLowerCase();
  // Retrieve host's disallow list from cache.
   
  ArrayList<String> disallowList =
    disallowListCache.get(host);
  
  // If list is not in the cache, download and cache it.
  if (disallowList == null) {
    disallowList = new ArrayList<String>();
    try {
      URL robotsFileUrl =
        new URL("http://" + host + "/robots.txt");
      // Open connection to robot file URL for reading.
      BufferedReader reader =
        new BufferedReader(new InputStreamReader(
          robotsFileUrl.openStream()));
      // Read robot file, creating list of disallowed paths.
      String line;
      while ((line = reader.readLine()) != null) {
        if (line.indexOf("Disallow:") == 0) {
          String disallowPath =
            line.substring("Disallow:".length());
          // Check disallow path for comments and remove if present.
          int commentIndex = disallowPath.indexOf("#");
          if (commentIndex != -1) {
            disallowPath =
              disallowPath.substring(0, commentIndex);
          }
          // Remove leading or trailing spaces from disallow path.
          disallowPath = disallowPath.trim();
          // Add disallow path to list.
          disallowList.add(disallowPath);
        }
      }
      // Add new disallow list to cache.
      disallowListCache.put(host, disallowList);
      
    }
    catch (Exception e) {
      /* Assume robot is allowed since an exception
         is thrown if the robot file doesn't exist. */ 
      return true;
    }
  }
  
  /* Loop through disallow list to see if
     crawling is allowed for the given URL. */
  String file = urlToCheck.getFile();
 
  for (int i = 0; i < disallowList.size(); i++) {
    String disallow = disallowList.get(i);
    if (file.startsWith(disallow)) {
    	 
      return false;
    }
  }
   
  return true;
}
 
// Download page at given URL.
private String downloadPage(URL pageUrl) {
	 
	
  try {
	   
    // Open connection to URL for reading.
	InputStream is=pageUrl.openStream();
	 
	InputStreamReader isr=new InputStreamReader(is);
	  
    BufferedReader reader =new BufferedReader(isr);
     
    // Read page into buffer.
    String line;
    StringBuffer pageBuffer = new StringBuffer();
     
     while ((line = reader.readLine()) != null) {
    	 
      pageBuffer.append(line);
    }
   
    return pageBuffer.toString();
  } catch (Exception e) {
	  LOGGER.error("Error while downloading page:"+pageUrl);
	  LOGGER.error("Error:"+e.getMessage()+" cause:" +e.getCause());
  }
   
  return null;
}
// Remove leading "www" from a URL's host if present.
private String removeWwwFromUrl(String url) {
  int index = url.indexOf("://www.");
  if (index != -1) {
    return url.substring(0, index + 3) +
      url.substring(index + 7);
  }
  return (url);
}
// Parse through page contents and retrieve links.
private ArrayList<String> retrieveLinks(
  URL pageUrl, String pageContents, HashSet<String> crawledList, 
  boolean limitHost)
{
 
	
  // Compile link matching pattern.
  Pattern p =
    Pattern.compile("<a\\s+href\\s*=\\s*\"?(.*?)[\"|>]",
      Pattern.CASE_INSENSITIVE);
  Matcher m = p.matcher(pageContents);
  // Create list of link matches.
  ArrayList<String> linkList = new ArrayList<String>();
  while (m.find()) {
    String link = m.group(1).trim();
    // Skip empty links.
    if (link.length() < 1) {
      continue;
    }
    // Skip links that are just page anchors.
    if (link.charAt(0) == '#') {
      continue;
    }
    // Skip mailto links.
    if (link.indexOf("mailto:") != -1) {
      continue;
    }
    // Skip JavaScript links.
    if (link.toLowerCase().indexOf("javascript") != -1) {
      continue;
    }
    // Prefix absolute and relative URLs if necessary.
    if (link.indexOf("://") == -1) {
      // Handle absolute URLs.
      if (link.charAt(0) == '/') {
        link = "http://" + pageUrl.getHost() + link;
      // Handle relative URLs.
      } else {
        String file = pageUrl.getFile();
        if (file.indexOf('/') == -1) {
          link = "http://" + pageUrl.getHost() + "/" + link;
        } else {
          String path =
            file.substring(0, file.lastIndexOf('/') + 1); 
          link = "http://" + pageUrl.getHost() + path + link;
        }
      }
    }
    // Remove anchors from link.
    int index = link.indexOf('#');
    if (index != -1) {
      link = link.substring(0, index);
    }
    // Remove leading "www" from URL's host if present. 
    link = removeWwwFromUrl(link);
    // Verify link and skip if invalid.
    
    URL verifiedLink = verifyUrl(link);
   
    if (verifiedLink == null) {
    	     continue;
    }
      /* If specified, limit links to those
      having the same host as the start URL. */
    if (limitHost &&
        !pageUrl.getHost().toLowerCase().equals(
          verifiedLink.getHost().toLowerCase()))  
    {
      continue;
    }
    // Skip link if it has already been crawled.
    if (crawledList.contains(link)) {
      continue;
    }
    // Add link to list.
   
    linkList.add(link);
    
  }
 
  return (linkList);
}
 
// Perform the actual crawling, searching for the search string.
private void crawl(
  String startUrl, int maxUrls, boolean limitHost )
{

  // Set up crawl lists.
  HashSet<String> crawledList = new HashSet<String>();
  LinkedHashSet<String> toCrawlList = new LinkedHashSet<String>();
  // Add start URL to the to crawl list.
  toCrawlList.add(startUrl);
  /* Perform actual crawling by looping
    through the To Crawl list. */
  while (crawling && toCrawlList.size() > 0)
  {
    /* Check to see if the max URL count has
       been reached, if it was specified.*/
    if (maxUrls != -1) {
      if (crawledList.size() == maxUrls) {
        break;
    }
  }
  // Get URL at bottom of the list.
  String url = toCrawlList.iterator().next();
  // Remove URL from the To Crawl list.
  toCrawlList.remove(url);
  // Convert string url to URL object.
   URL verifiedUrl = verifyUrl(url);
 
  // Skip URL if robots are not allowed to access it.
   
   if (verifiedUrl == null) {
  	 LOGGER.info("verifiedUrl==null:");
    continue;
  }
   
  // Add page to the crawled list.
  crawledList.add(url);
  // Download the page at the given URL.
 
  String pageContents = downloadPage(verifiedUrl);
   String pageTitle=getPageTitleFromContents(pageContents);
 
   setOfRepositoryLinks.put(verifiedUrl.toString(), new PageHits(verifiedUrl.toString(),pageTitle));
 
  //repositoryLinks.add(verifiedUrl);
   /* If the page was downloaded successfully, retrieve all its
     links and then see if it contains the search string. */
  if (pageContents != null && pageContents.length() > 0)
  {
	   
    // Retrieve list of valid links from page.
    ArrayList<String> links =
      retrieveLinks(verifiedUrl, pageContents, crawledList,
        limitHost);
     // Add links to the To Crawl list.
    toCrawlList.addAll(links);
    }
    }
  }
private String getPageTitleFromContents(String pageContents){
	String title="";
	  
	 if(pageContents!=null){
	int sI=pageContents.indexOf("<title>");
 
	int sI2=pageContents.indexOf("</title>");
	 
	if (sI>=0 && sI2>=7){
	 
	 title=pageContents.substring(sI+7,sI2).toString();
	  
	}
	 }
	 
	return title;
	
}

}