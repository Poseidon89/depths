package org.goodoldai.depths.annotation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.log4j.Logger;
import org.goodoldai.depths.domain.resources.OnlineRepository;

//

class OnlineRepositoryManager {
	private static final Logger LOGGER = Logger.getLogger(OnlineRepositoryManager.class);
	private ArrayList<String> links=new ArrayList<String>();
 	private OnlineRepository onlineRepository;
 	
 	Map<String, DomainConceptSemAnnotation> resourceAnnotations 
	= new HashMap<String, DomainConceptSemAnnotation>();
 	
	public OnlineRepository getOnlineRepository() {
		return onlineRepository;
	}
	public void setOnlineRepository(OnlineRepository onlineRepository) {
		this.onlineRepository = onlineRepository;
	}
	//number of all documents
	private  int numberOfAllDocuments=0;
	//number of documents containing the term - document frequency
	 private HashMap<String, Integer> docNumbContTerm=new HashMap<String, Integer>();
	//private  HashMap<String ,PageHits> pagesHits=new HashMap<String, PageHits>();
	private  HashMap<String ,PageHits> _pagesHits=new HashMap<String, PageHits>();
	public String repositoryUrl;
	
	OnlineRepositoryManager(OnlineRepository onlineRepository){
		this.onlineRepository=onlineRepository;
	}
	void addDocContinuingTerm(String patternURI){
	 if(docNumbContTerm.containsKey(patternURI)){
			
		Integer numbOfOccurrence=docNumbContTerm.get(patternURI);
		numbOfOccurrence=numbOfOccurrence+1;
		docNumbContTerm.put(patternURI, numbOfOccurrence);
	 	}else{
		 
			docNumbContTerm.put(patternURI, new Integer(1));
		}
	}

	/**
	 * Keeps the Map of pages with data about how many times the page is hit.
	 */
	

	
	private   void addNewDocument(){
		numberOfAllDocuments++;
	}
	/**
	 * @return the repositoryName
	 */
 
	void addFoundConcepts(String url,Collection<String> conceptsUris){
 
		//PageHits pageHitsInst= pagesHits.get(url);
		PageHits _pageHitsInst= _pagesHits.get(url);
		 	addNewDocument();
		 	
		 	HashMap<String,Integer> instancesNumber=new HashMap<String,Integer>();
		 	for(String instanceStr:conceptsUris){
		 		if(instancesNumber.containsKey(instanceStr)){
		 			Integer val=instancesNumber.get(instanceStr);
		 			val++;
		 			instancesNumber.put(instanceStr, val);
		 		}else{
		 			instancesNumber.put(instanceStr,new Integer(1));
		 		}
		 	}
		 	Iterator<Entry<String, Integer>> it = instancesNumber.entrySet().iterator();
		    while (it.hasNext()) {
		        Map.Entry entries= (Map.Entry)it.next();
		        String instanceStrPatt=(String) entries.getKey();
		        Integer value=(Integer) entries.getValue();
		   
		       if(value>2){
		    	 
		    	   _pageHitsInst.addPatternHit(this,instanceStrPatt);
					DomainConceptSemAnnotation patternAnn = null;
				if ( resourceAnnotations.containsKey( instanceStrPatt ) ) {
					patternAnn = (DomainConceptSemAnnotation) resourceAnnotations.get( instanceStrPatt );
				}else {
						patternAnn = new DomainConceptSemAnnotation();
						patternAnn.setPatternInstanceURI(instanceStrPatt);
						resourceAnnotations.put( instanceStrPatt, patternAnn );
					
				}
				patternAnn.addWebPageLocation(url.toString());
		       }
		      //  it.remove(); // avoids a ConcurrentModificationException
		    }
 
	}
	
	
	/**
	 * analyse the page hits and calculate dominant pattern within each page
	 */
	void analisePagesHits(){
		 
		 
		Set<String> set=_pagesHits.keySet();
		@SuppressWarnings("unchecked")
		HashMap<String,PageHits> pagesHitsClone=(HashMap<String, PageHits>) _pagesHits.clone();
		 Iterator<String> iter=set.iterator();
		  while (iter.hasNext()){
			String pageURL=(String) iter.next();
			 PageHits pageHits=(PageHits) pagesHitsClone.get(pageURL);
			 LOGGER.info("analyseHits for page:"+pageURL);
		 	boolean keepThisPage= pageHits.analyseHits(this);
	 		if(keepThisPage==false){
				if(pagesHitsClone.containsKey(pageURL)){
					pagesHitsClone.remove(pageURL.toString());
				}
			}
		 }
	 	 _pagesHits.clear();
		 _pagesHits=pagesHitsClone;
	}
 

	/**
	 * @return the links
	 */
	public ArrayList<String> getLinks() {
		return links;
	}

	/**
	 * @param links the links to set
	 */
	public void setLinks(ArrayList<String> links) {
		this.links = links;
		for (int i=0;i<links.size();i++){
			String link=links.get(i);
			_pagesHits.put(link, new PageHits(link));
		}
	}
 
	/**
	 * @return the pagesHits
	 */
	public   HashMap<String, PageHits> getPagesHits() {
		return _pagesHits;
	}
	/**
	 * @param pagesHits the pagesHits to set
	 */
	public   void setPagesHits(HashMap<String, PageHits> pagesHits) {
		this._pagesHits = pagesHits;
	}
	/**
	 * @return the numberOfAllDocuments
	 */
	public int getNumberOfAllDocuments() {
		return this.numberOfAllDocuments;
	}
	/**
	 * @param numberOfAllDocuments the numberOfAllDocuments to set
	 */
	public   void setNumberOfAllDocuments(int numberOfAllDocuments) {
		this.numberOfAllDocuments = numberOfAllDocuments;
	}
	/**
	 * @return the docNumbContTerm
	 */
	int getDocNumbContTerm(String pattern) {
		Integer docNumbContPattern=docNumbContTerm.get(pattern);
		 return docNumbContPattern.intValue();
	}
	/**
	 * @param docNumbContTerm the docNumbContTerm to set
	 */
	public   void setDocNumbContTerm(HashMap<String, Integer> docNumbContTerm) {
		this.docNumbContTerm = docNumbContTerm;
	}

	 
}
