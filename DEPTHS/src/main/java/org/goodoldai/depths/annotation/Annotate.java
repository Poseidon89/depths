package org.goodoldai.depths.annotation;
 
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;
 
import java.util.Iterator;

import org.apache.log4j.Logger;
import org.goodoldai.depths.config.Constants;
import org.goodoldai.depths.config.Settings;
import org.goodoldai.depths.domain.general.Relevance;
import org.goodoldai.depths.domain.general.Resource;
import org.goodoldai.depths.domain.skos.Concept;
import org.goodoldai.depths.semanticstuff.services.moodle.RDFDataManagement;
import org.goodoldai.depths.utility.MoodleDatabaseManager;
 

 

import com.ontotext.kim.client.GetService;
import com.ontotext.kim.client.KIMService;
import com.ontotext.kim.client.corpora.CorporaAPI;
import com.ontotext.kim.client.corpora.KIMAnnotation;
import com.ontotext.kim.client.corpora.KIMAnnotationSet;
import com.ontotext.kim.client.corpora.KIMCorporaException;
import com.ontotext.kim.client.corpora.KIMDocument;
import com.ontotext.kim.client.corpora.KIMFeatureMap;
import com.ontotext.kim.client.model.FeatureConstants;
import com.ontotext.kim.client.semanticannotation.SemanticAnnotationAPI;
 



class Annotate {
	private static final Logger LOGGER = Logger.getLogger(Annotate.class);
	//public static final String PROTONT_TOPIC = "http://www.w3.org/2004/02/skos/core#Concept";
	//public static final String KIM_KB_NAMESPACE = "http://www.ontotext.com/kim/2006/05/wkb#";
	
	private static final String RMI_HOST = Settings.getInstance().config.kimConfig.server;
	private static final int RMI_PORT = Settings.getInstance().config.kimConfig.port;
	
	private static CorporaAPI apiCorpora = null;
	//private static KIMAnnotationSet kimASet=null;
    //private static KIMService serviceKim = null;
    private static SemanticAnnotationAPI apiSemAnn1=null;
	
	

    
	
	public Annotate()
	{
		
	}
	private static void connectToKIM(){
		
		try{
			// connect to KIMService (deployed on specific host)
			KIMService serviceKim = GetService.from(RMI_HOST, RMI_PORT);
			 
			// obtain CorporaAPI and NercAPI components
			apiCorpora = serviceKim.getCorporaAPI();
			apiSemAnn1 = serviceKim.getSemanticAnnotationAPI();
			
		} catch (Exception e) {
			
			//LOGGER.error("Error:"+e.getLocalizedMessage());
			LOGGER.info("An error occured while trying to connect to KIM!"+e.getLocalizedMessage());
			//throw new FeedbackProcessingException("KIM services are not available!");
		}
			  
	}
	
	private ArrayList<String> getAnnotations(String urlDoc,String ontologyUrl, Collection<String> domainConcepts) {
		ArrayList<String> annotations = new ArrayList<String>();
		
		//Map<String, SemAnnotation> annotations = new HashMap<String, SemAnnotation>();
	 
		try {
			 
			URL url=null;
			try {
				url = new URL(urlDoc);
			} catch (MalformedURLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			 
			KIMDocument kdoc=null;
	 
			if (apiCorpora!=null)
			{
				try {
					kdoc = apiCorpora.createDocument(url,"UTF-8");
				}catch (KIMCorporaException e) {
					
					// TODO Auto-generated catch block
					LOGGER.info("Error in create document for:"+url+" message:"+e.getLocalizedMessage());
				} 
				 
				kdoc = apiSemAnn1.execute(kdoc);
				 
				KIMAnnotationSet kimASet=null;
				try{
				kimASet= kdoc.getAnnotations();
				}catch(Exception ex){
					 
					LOGGER.info("Exception in getting annotations from page:"+url.toString() +" ERROR TRACE:"+ex.getStackTrace());
					 
				}
				 
				if ( kimASet != null ) {
					 
				Iterator<?> annIterator = kimASet.iterator();
			 
				while (annIterator.hasNext()) {
					 
					KIMAnnotation kimAnnotation = (KIMAnnotation) annIterator.next();
					KIMFeatureMap kimFeatures = kimAnnotation.getFeatures();
					if (kimFeatures == null) 
						continue;
					Object instanceAnn = kimFeatures.get(FeatureConstants.INSTANCE);
					Object classAnn = kimFeatures.get(FeatureConstants.CLASS);
					Object annOrigin = kimFeatures.get(FeatureConstants.FEATURE_ORIGINAL_NAME);
					if ( instanceAnn == null || classAnn == null ) 
						continue;
					 if ( classAnn.equals( Constants.SKOS_NS+"Concept" ) ) {
						 String instanceStr = (String)instanceAnn;
						
						if(domainConcepts.contains(instanceStr)){
						if (annOrigin.toString().length()>1)
							annotations.add(instanceStr);
						 
						}
												
					}
				}
			
			} 
			}
		} catch (RemoteException re) {
			LOGGER.error( re.getMessage() );
			 
		} catch ( Exception e ) {
			LOGGER.error(e.getCause().getMessage());
		 
		} 
		return annotations;		
		
	}
	ArrayList<String> getResourceAnnotation(String urlDoc,String ontologyURL, Collection<String> domainConcepts){
		 ArrayList<String> conceptList=new ArrayList<String>();
		connectToKIM();
		conceptList=getAnnotations(urlDoc, ontologyURL,domainConcepts);
		return conceptList;
	}
	void getContentAnnotation(String mdlTableName,
			String fieldName, String fieldValue, String contentFieldName,
			String content, Resource resource){
		connectToKIM();
		getContentAnnotations(mdlTableName,
				fieldName, fieldValue, contentFieldName,
				content, resource);
	}
	
	private void getContentAnnotations(String mdlTableName,
			String fieldName, String fieldValue, String contentFieldName,
			String content, Resource resource){
	ArrayList<String> annotations = new ArrayList<String>();
		
	
			 
			KIMAnnotationSet kimASet = null;
			ArrayList<String> matchStack=new ArrayList<String>(); 
			try {
				kimASet=apiSemAnn1.execute(content);
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				LOGGER.info("RemoteException while SemanticAnnotationAPI have tried to execute annotation on content"+e.getLocalizedMessage() );
			}catch(Exception ex){
				LOGGER.info("Exception while SemanticAnnotationAPI have tried to execute annotation on content"+ex.getLocalizedMessage() );
			}
            	
            	if ( kimASet != null ) {
					 
    				Iterator<?> annIterator = kimASet.iterator();
    			 
    				while (annIterator.hasNext()) {
    					 
    					KIMAnnotation kimAnnotation = (KIMAnnotation) annIterator.next();
    					KIMFeatureMap kimFeatures = kimAnnotation.getFeatures();
    					 
    					if (kimFeatures == null) 
    						continue;
    					Object instanceAnn = kimFeatures.get(FeatureConstants.INSTANCE);
    					Object classAnn = kimFeatures.get(FeatureConstants.CLASS);
    					Object annOrigin = kimFeatures.get(FeatureConstants.FEATURE_ORIGINAL_NAME);
    					 
    					if ( instanceAnn == null || classAnn == null ) 
    						continue;
    					 
    					 if ( classAnn.equals( Constants.SKOS_NS+"Concept" ) ) {
    						 
    						 if(!matchStack.contains(annOrigin.toString())){
    						 	 matchStack.add(annOrigin.toString());
    						 	 	content=checkContentClassType(mdlTableName,
    										fieldName, fieldValue, contentFieldName,instanceAnn,classAnn,annOrigin,content);
    						 	}
    					 		try {
									processContentAnnotation(instanceAnn, resource);
								} catch (Exception e) {
									// TODO Auto-generated catch block
									LOGGER.error("Error:"+e.getLocalizedMessage());
								}
    					 		}
    					//	if(domainConcepts.contains(instanceStr)){
    						if (annOrigin.toString().length()>1)
    							annotations.add(instanceAnn.toString());
    						 
    					//	}
    												
    					}
    				}
            }
	 
			
		//return annotations;	
			
		 
		 
	 
private void processContentAnnotation( Object instanceAnn, Resource resource) throws Exception{
	Concept concept=RDFDataManagement.getInstance().loadResourceByURI(Concept.class,instanceAnn.toString(),false);
	Relevance relevance=new Relevance();
	relevance.setIsRelevantFor(concept);
	 
	relevance.setIsRelevanceOf(resource);
	
	RDFDataManagement.getInstance().saveResource(relevance,false);
	resource.addRelevance(relevance);
	
	RDFDataManagement.getInstance().updateResource(resource,false);
}
	private String checkContentClassType(String mdlTableName,
			String fieldName, String fieldValue, String contentFieldName,Object instanceAnn,Object classAnn, Object annOrigin,String content){
		 
		String newContent=content;
	 	//if ( AnnotationFacility.ifClassDesignPatternForm(classAnn.toString()) ) {
	 		try {
				newContent=replaceSubstring(content,annOrigin.toString(),instanceAnn.toString());
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				LOGGER.error("Error:"+e.getLocalizedMessage());
			}
		 	//updateContentInDatabase(mdlTableName,
				//	fieldName, fieldValue, contentFieldName,newContent);
		  
		 	//DesignPatternSemAnnotation patternAnn = null;		
		//}
	 	 return newContent;
		 
	}
	private String replaceSubstring(String content,String match,String classURI) throws UnsupportedEncodingException{
	 
	String someString = content;
	 
	String uri=URLEncoder.encode(classURI.toString(),"UTF-8");
 
	String replace="<a title='View recommended resources' href='#' onclick='return showDEPTHSRecommendationDialog(\\\\\"[[[wwwroot]]]/depths/services/online_resources/view_suggestions.php?conceptUri="+uri+"\\\\\"); return false;'>"+match+"</a>";
 
	String resultString = null;
	
	 
resultString=someString.replaceAll(match,replace);

 	return resultString;
	}
	
	private void updateContentInDatabase(String mdlTableName,String fieldName, 
			String fieldValue,String contentFieldName,String newContent){
		String mdlTable=mdlTableName;
		String mdlTblPrefix=Settings.getInstance().config.moodleConfig.tablePrefix;
		String mdlFullTblName=mdlTblPrefix+mdlTable;
		String contentToAnnotateFieldName=contentFieldName;
		String mdlRefFieldName=fieldName;
		String mdlRefFieldValue=fieldValue;
		
		String sql="UPDATE "+mdlFullTblName+" SET "+contentToAnnotateFieldName+"=\""+newContent+"\" WHERE "+mdlRefFieldName+"="+mdlRefFieldValue;
	 	
		MoodleDatabaseManager.getInstance().performUpdateTransaction(sql);
	 	}
}
