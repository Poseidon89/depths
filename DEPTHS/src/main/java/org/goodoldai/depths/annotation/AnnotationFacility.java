package org.goodoldai.depths.annotation;

import java.io.Serializable;
//import java.util.ArrayList;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set; 
import org.apache.log4j.Logger;
import org.goodoldai.depths.domain.general.Relevance;
import org.goodoldai.depths.domain.resources.OnlineRepository;
import org.goodoldai.depths.domain.resources.WebPage;
import org.goodoldai.depths.domain.skos.Concept;
import org.goodoldai.depths.semanticstuff.services.annotations.AnnotationManagament;
import org.goodoldai.depths.semanticstuff.services.moodle.RDFDataManagement;
import org.goodoldai.depths.services.resources.ResourceQueries;
import org.goodoldai.depths.utility.WeightFactorsUtility;

class AnnotationFacility implements Serializable{
	private static final Logger LOGGER = Logger.getLogger(AnnotationFacility.class);
 
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
 

	 static void annotateOnlineRepository(OnlineRepositoryManager onlineRepManager, String ontologyUri) throws Exception{
	 
 
	HashMap<String,PageHits> pageLinks=onlineRepManager.getPagesHits();
	Collection<String> domainConcepts=new ArrayList<String>();
	domainConcepts=AnnotationManagament.getInstance().getDomainConceptsUrisForOntology(ontologyUri);
 
	if(domainConcepts.isEmpty()){
		AnnotationManager.getInstance().readAllDomainOntologies();
		domainConcepts=AnnotationManagament.getInstance().getDomainConceptsUrisForOntology(ontologyUri);
	}
 
	 Set<String> set=pageLinks.keySet();
	 Iterator<String> iter=set.iterator();
	 while (iter.hasNext()){
		
		 PageHits currPageHits=(PageHits) pageLinks.get(iter.next()); 
		 //currPageHits.getPageTitle();
		 String url=currPageHits.getPageUrl();
 
		 ArrayList<String> semanticAnnotations=new ArrayList<String>();
	 
		 Annotate a = new Annotate();
		 semanticAnnotations=a.getResourceAnnotation(url.toString(),ontologyUri, domainConcepts);
 
		 onlineRepManager.addFoundConcepts(url.toString(),semanticAnnotations);
	 
	 	
	}
 
	 onlineRepManager.analisePagesHits();
	processResourceAnnotations(onlineRepManager );

		 
}
 private static void processResourceAnnotations(OnlineRepositoryManager onlineRepManager ) throws Exception{
	//Should be processed using JenaBean instead the commented code
 
	Set<String> set=onlineRepManager.resourceAnnotations.keySet();
	 Iterator<String> iter=set.iterator();

	OnlineRepository onlRepository= onlineRepManager.getOnlineRepository();
 
	 
	 while (iter.hasNext()){
	 
		 DomainConceptSemAnnotation currPatternAnn=(DomainConceptSemAnnotation) onlineRepManager.resourceAnnotations.get(iter.next()); 
	 
		 currPatternAnn.sortWebPages(onlineRepManager);
	 
		 ArrayList<PageHits> sortedWebPages=currPatternAnn.getSortedWebPages();
		 
		 String conceptURI=currPatternAnn.patternInstanceURI;
		 
		 
		 Concept currConcept=RDFDataManagement.getInstance().loadResourceByURI(Concept.class, conceptURI,false);
	 
			 	 for (int i=0;i<sortedWebPages.size();i++){
			  
			 		 PageHits pageHit=(PageHits) sortedWebPages.get(i);
			 		 	 
			 		   if (pageHit.getDominantPattern().equals(conceptURI.toString())){
			 			   
			 	 
			 		   
			 		   
			 		 String webPageUrl=pageHit.getPageUrl();
			 		WebPage wPage=ResourceQueries.getInstance().getWebPageByPageUrl(webPageUrl);
			 		// WebPage wPage=new WebPage();
			 		 wPage.setHref(webPageUrl);
			 		 wPage.setTitle(pageHit.getPageTitle());
			 		 
			 		Relevance relevance=ResourceQueries.getInstance().getPatternWebPageRelevance(currConcept.getUri().toString(),wPage.getUri().toString());
			 		 
			 		relevance.setHasValue(pageHit.getPageRelevance());
			 		double overalRelevance=pageHit.getPageRelevance()*WeightFactorsUtility.getPatternRelevancePercent();
			 		LOGGER.info("pageRelevance:"+pageHit.getPageRelevance());
			 		LOGGER.info("WF:"+WeightFactorsUtility.getPatternRelevancePercent());
			 		LOGGER.info("overal relevance:"+overalRelevance);
			 	 
		 		    relevance.setHasOveralValue(overalRelevance);
		 		    relevance.setIsRelevantFor(currConcept);
		 		   relevance.setIsRelevanceOf(wPage);
		 		  
		 		 RDFDataManagement.getInstance().saveResource(relevance,false);
			 		 wPage.addRelevance(relevance);
			 		RDFDataManagement.getInstance().saveResource(wPage,false);
			 		 onlRepository.addWebResource(wPage);
			 		;
		 
			 		  }
		 
			   }	 
		 	 
    }
 
	 RDFDataManagement.getInstance().saveResource(onlRepository, false);
 
} 
 

}
