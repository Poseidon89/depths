package org.goodoldai.depths.config;

import org.simpleframework.xml.Element;

public class DomainOntologyConfig {

	@Element(name = "id", required = true)
	public String id;
	
	@Element(name = "title", required = true)
	public String title;
	
	@Element(name = "uri", required = true)
	public String uri;
	
	@Element(name = "file-location", required = true)
	public String filelocation;
	
	 
}
