package org.goodoldai.depths.config;

import java.util.ArrayList;

import org.simpleframework.xml.ElementList;

public class DomainOntologiesConfig {
	 @ElementList(entry = "domain-ontology", inline = true)
	    public ArrayList<DomainOntologyConfig> domOntConf;
}
