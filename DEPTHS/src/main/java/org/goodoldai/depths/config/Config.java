package org.goodoldai.depths.config;

 
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root
public class Config {
	
	@org.simpleframework.xml.Transient
	public static String H2 = "H2";
	@org.simpleframework.xml.Transient
	public static String MySQL = "MySQL";
	
	@Element(name = "rdf-repository", required = true)
	public RdfRepositoryConfig rdfRepositoryConfig;
	
	@Element(name="ftp-repository", required=true)
	public FtpRepositoryConfig ftpRepositoryConfig;
	 
	 
	
	@Element(name = "weightfactors-config", required = true)
	public String weightFactorsConfig;
	
	@Element(name = "kim-config", required = true)
	public KIMConfig kimConfig;
	
	@Element(name = "web-repositories", required = true)
	public WebRepositoriesConfig webRepsConfig;
	
	@Element(name = "domain-ontologies", required = true)
	public DomainOntologiesConfig domainOntologiesConfig;
	
	@Element(name = "moodle-configuration", required = true)
	public MoodleConfig moodleConfig;
	
	@Element(name = "cron-scheduler", required = true)
	public CronSchedulerConfig cronSchedulerConfig;
	
	@Element(name = "logging-config", required = true)
	public LoggingConfig loggingConfig;
	
	@Element(name = "caching", required = true)
	public Boolean caching;
	
}
