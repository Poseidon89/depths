package org.goodoldai.depths.config;

import org.simpleframework.xml.Element;

public class WebRepositoryConfig {
	@Element(name = "ontology-id", required = true)
	public String ontologyId;
	
	@Element(name = "name", required = true)
	public String name;
	
	@Element(name = "url", required = true)
	public String url;
	
	@Element(name = "number-of-links", required = true)
	public int numbOfLinks;
	
	@Element(name = "annotate", required = true)
	public boolean annotate;

}
