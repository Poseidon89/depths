package org.goodoldai.depths.config;

import org.simpleframework.xml.Element;

public class LoggingConfig {

	public static final String UPDATE = "update";
	public static final String CREATE = "create";
	public static final String CREATEDROP = "create-drop";
	public static Object dbDDLAuto=UPDATE;
	
	@Element(name = "eval-config-dir", required = false)
	public String evalConfigDir;
	
	@Element(name = "log4j-config", required = true)
	public String log4jConfig;
	
	@Element(name = "enable", required = true)
	public boolean enable = false;
	
	@Element(name = "dbdialect", required = true)
	public String dbDialect;
	
	@Element(name = "dbdriver", required = true)
	public String dbDriver;
	
	@Element(name = "dburl", required = true)
	public String dbURL;
	 
	 @Element(name = "dbuser", required = true)
	 public String dbUser;
	 
	@Element(name = "dbpassword", required = false)
	public String dbPassword = "";
	
 
}
