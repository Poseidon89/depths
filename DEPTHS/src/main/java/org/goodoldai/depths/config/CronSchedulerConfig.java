package org.goodoldai.depths.config;

import org.simpleframework.xml.Element;

public class CronSchedulerConfig {

	@Element(name = "debug", required = true)
	public boolean debug = false;
	
	@Element(name = "activated", required = true)
	public boolean activated = false;
	
	@Element(name = "hourly", required = true)
	public boolean hourly = false;
	
	@Element(name = "daily", required = true)
	public boolean daily = false;
	
	@Element(name = "weekly", required = true)
	public boolean weekly = false;
	
	@Element(name = "monthly", required = true)
	public boolean monthly = false;
	
	@Element(name = "smtp-config", required = true)
	public SMTPConfig smtpConfig;
}
