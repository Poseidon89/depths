package org.goodoldai.depths.config;

import java.util.ArrayList;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;

public class WebRepositoriesConfig {

	 @ElementList(entry = "web-repository", inline = true)
	    public ArrayList<WebRepositoryConfig> webRepConf;
		@Element(name = "annotate-repositories", required = true)
		public boolean annotateRepositories;

}
