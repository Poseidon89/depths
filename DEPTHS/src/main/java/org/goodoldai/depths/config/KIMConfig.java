package org.goodoldai.depths.config;

import org.simpleframework.xml.Element;

public class KIMConfig {
	@Element(name = "server", required = true)
	public String server;
	
	@Element(name = "port", required = true)
	public int port = 0;
}
