/*
 * Settings.java
 * by s.ziplies
 */
package org.goodoldai.depths.config;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
 

 
import org.apache.log4j.Logger;
import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

public class Settings {

	/**
	 * The logger.
	 */
	private static final Logger LOGGER = Logger.getLogger(Settings.class.getName());

	/**
	 * Config file name.
	 */
	public static final String CONFIGFILE = "config.xml";

	/**
	 * Relative path to folder in which the default config file is distributed.
	 */
	public static final String defaultConfigDir = "config/";

	/**
	 * Config is a singleton.
	 */
	public Config config;

	public static class SettingsHolder {
		public static final Settings INSTANCE = new Settings();
	}

	/**
	 * 
	 * @return
	 */
	public static Settings getInstance() {
		return SettingsHolder.INSTANCE;
	}

	/**
	 * @throws Exception
	 * 
	 */
	public Settings() {
	 	try {
	 		loadDefaultConfig();
			
		} catch (Exception e) {
			LOGGER.error( "Could not load settings: ");
		}
	}

	/**
	 * @throws Exception
	 * 
	 */
	public void loadDefaultConfig() throws Exception {

		InputStream is = null;
		Serializer serializer = new Persister();
		try {
			// get path to config file
			URL url = Thread.currentThread().getContextClassLoader()
					.getResource(defaultConfigDir + CONFIGFILE);
		
			if (url != null) {
				String path = url.getFile();
					// remove white spaces encoded with %20
				path = path.replaceAll("%20", " ");
				is = new FileInputStream(new File(path));
				// read config file into Java
				config = serializer.read(Config.class, is);
				//
			}
		
		} catch (FileNotFoundException fnfe) {
			// TODO
			throw new Exception("Could not find the configuration file: "
					+ CONFIGFILE, fnfe);
		} catch (Exception e) {
			throw new Exception("Could not serialize the configuration file: "
					+ CONFIGFILE, e);
		} finally {
			if (null != is) {
				try {
					is.close();
				} catch (IOException e) {
					LOGGER.error("Could not close InputStream!", e);
				}
			}
		}
	}

}
