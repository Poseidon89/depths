package org.goodoldai.depths.config;

import org.simpleframework.xml.Element;

public class FtpUserConfig {

	@Element(name = "ftp-user-username", required = true)
	public String ftpUserUsername;
	
	@Element(name = "ftp-user-password", required = true)
	public String ftpUserPassword;
}
