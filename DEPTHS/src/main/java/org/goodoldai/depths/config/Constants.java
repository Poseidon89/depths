package org.goodoldai.depths.config;

public class Constants {

 
	
	
	
	
	public static final String FOAF_NS = "http://xmlns.com/foaf/0.1/";
	public static final String DC_TERMS_NS = "http://purl.org/dc/terms/";
	public static final String RDFS_NS = "http://www.w3.org/2000/01/rdf-schema#";
	public static final String RDF_NS = "http://www.w3.org/1999/02/22-rdf-syntax-ns#";
	public static final String SIOC_NS = "http://rdfs.org/sioc/ns#";
	public static final String SKOS_NS = "http://www.w3.org/2004/02/skos/core#";
	public static final String PARTICIPATION_NS = "http://purl.org/vocab/participation/schema#";
	
	public static final String OWL_NS = "http://www.w3.org/2002/07/owl";
 
	public static final String DEPTHS_NS = "http://www.depths.org/zjeremic/software-solution/2008/depths.owl#";
	public static final String LOCO_NS="http://www.lornet.org/loco-cite/2007/.owl#";
	public static final String XMLSCHEMA_NS="http://www.w3.org/2001/XMLSchema#";
}
