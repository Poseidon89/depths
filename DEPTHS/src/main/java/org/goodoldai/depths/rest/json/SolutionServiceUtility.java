package org.goodoldai.depths.rest.json;

import java.util.Date;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONStringer;

import org.codehaus.jettison.json.JSONObject;

import org.goodoldai.depths.domain.activity.UploadingSolution;
import org.goodoldai.depths.domain.content.ActivityDiagram;
import org.goodoldai.depths.domain.content.ClassDiagram;
import org.goodoldai.depths.domain.content.CollaborationDiagram;
import org.goodoldai.depths.domain.content.DeploymentDiagram;
import org.goodoldai.depths.domain.content.DesignProblem;
import org.goodoldai.depths.domain.content.Diagram;
import org.goodoldai.depths.domain.content.SequenceDiagram;
import org.goodoldai.depths.domain.content.StatechartDiagram;
import org.goodoldai.depths.domain.content.UseCaseDiagram;
import org.goodoldai.depths.domain.project.Project;
import org.goodoldai.depths.domain.project.Solution;
import org.goodoldai.depths.domain.user.User;
import org.goodoldai.depths.jsonstuff.ProjectJSONExporter;
import org.goodoldai.depths.semanticstuff.services.AbstractDAOManager;
import org.goodoldai.depths.semanticstuff.services.content.ContentManagament;
import org.goodoldai.depths.semanticstuff.services.project.ProjectManagament;
import org.goodoldai.depths.semanticstuff.services.user.UserManagament;
import org.goodoldai.depths.services.datalayer.SolutionsDataLayer;

import thewebsemantic.NotFoundException;

public class SolutionServiceUtility {
	private static final Logger LOGGER = Logger
			.getLogger(SolutionServiceUtility.class);

	// private static UserManagament um=new UserManagament();

	public static String saveNewSuggestedSolution(String projectJson)
			throws Exception {

		JSONObject projectJsonObject = new JSONObject(projectJson);
		// Get user
		String useruri = projectJsonObject.getString("useruri");
		User user = null;
		if ((!useruri.equals("")) && (useruri != null)) {
			user = UserManagament.getInstance().getUserByUri(useruri);
		} else {
			LOGGER.error("There is no user assigned to this solution");
		}

		String designProblemUri = projectJsonObject.getString("designproblem");
		DesignProblem dProblem = null;

		String mode = projectJsonObject.getString("mode");

		Project project = null;
		JSONObject projectObject = projectJsonObject.getJSONObject("project");
		// JSONObject projectObject=projectObject.getJSONObject(0);
		String projecturi = null;
		if (mode.equals("create")) {
			project = new Project();
			AbstractDAOManager.getAbstractDAOInstance().saveResource(project,
					false);
			UploadingSolution uplSolution = new UploadingSolution();
			uplSolution.setDateTimeSent(new Date());
			uplSolution.setSentBy(user);
			uplSolution.setProjectRef(project);
			uplSolution.setSubmitted(false);
			AbstractDAOManager.getAbstractDAOInstance().saveResource(
					uplSolution, false);

		} else if (mode.equals("edit")) {

			projecturi = projectObject.getString("projecturi");
			project = SolutionsDataLayer.getDataLayerInstance()
					.getProjectByUri(projecturi);
			// project=
			// AbstractDAOManager.getAbstractDAOInstance().loadResourceByURI(Project.class,
			// projecturi,false);
			LOGGER.info("uploading solution should be updated by new time");
		} else if (mode.equals("reuse")) {
			project = new Project();
			AbstractDAOManager.getAbstractDAOInstance().saveResource(project,
					false);
			String parentprojecturi = projectObject
					.getString("parentprojecturi");
			if (parentprojecturi != "") {
				// Project parentproject=
				// AbstractDAOManager.getAbstractDAOInstance().loadResourceByURI(Project.class,
				// parentprojecturi,false);
				Project parentproject = SolutionsDataLayer
						.getDataLayerInstance().getProjectByUri(
								parentprojecturi);
				if (parentproject != null) {
					project.setParentProject(parentproject);
				}
			}
			UploadingSolution uplSolution = new UploadingSolution();
			uplSolution.setDateTimeSent(new Date());
			uplSolution.setSentBy(user);
			uplSolution.setProjectRef(project);
			uplSolution.setSubmitted(false);
			AbstractDAOManager.getAbstractDAOInstance().saveResource(
					uplSolution, false);
		} else {
			LOGGER.info("Unknown mode is used:" + mode);
		}

		// dProblem.addIsDescribedByProject(project);
		if (!designProblemUri.equals("")) {
			dProblem = ContentManagament.getInstance().getDesignProblemByUri(
					designProblemUri);
			project.addHasDesignProblemRef(dProblem);
			ContentManagament.getInstance().updateResource(dProblem, false);
		}

		String projectid = projectObject.getString("projectid");
		project.setProjectId(projectid);
		String title = projectObject.getString("title");
		project.setTitle(title);
		String dateTimeSent = projectObject.getString("dateTimeSent");
		project.setDateCreated(new Date());
		String fileUrl = projectObject.getString("fileUrl");
		project.setFileUrl(fileUrl);
		String createdBy = projectObject.getString("createdBy");
		if (!createdBy.equals("") && (createdBy != null)) {
			User creator = UserManagament.getInstance().getUserByUri(createdBy);
			if (creator != null) {
				project.setCreatedBy(creator);
			}
		}

		String modifiedBy = projectObject.getString("modifiedBy");
		LOGGER.info("modifiedBy:" + modifiedBy);
		if (!modifiedBy.equals("") && (modifiedBy != null)) {
			User modificator = UserManagament.getInstance().getUserByUri(
					modifiedBy);
			if (modificator != null) {
				project.setModifiedBy(modificator);
			}
		}

		JSONObject solutionObject = projectJsonObject.getJSONObject("solution");

		Solution solution = null;
		if (mode.equals("create") || (mode.equals("reuse"))) {
			solution = new Solution();
			AbstractDAOManager.getAbstractDAOInstance().saveResource(solution,
					false);
		} else if (mode.equals("edit")) {
			solution = project.getHasSolution();
			if (solution == null) {
				solution = new Solution();
				AbstractDAOManager.getAbstractDAOInstance().saveResource(
						solution, false);
			}
			// solution=AbstractDAOManager.getAbstractDAOInstance().loadResourceByURI(Solution.class,
			// ,false);
		}

		String description = solutionObject.getString("description");
		solution.setDescription(description);
		String hasAdditionalRequirements = solutionObject
				.getString("hasAdditionalRequirements");
		solution.setHasAdditionalRequirements(hasAdditionalRequirements);
		String hasCons = solutionObject.getString("hasCons");
		solution.setHasCons(hasCons);
		String hasConsequences = solutionObject.getString("hasConsequences");
		solution.setHasConsequences(hasConsequences);
		String hasDesignConstraints = solutionObject
				.getString("hasDesignConstraints");
		solution.setHasDesignConstraints(hasDesignConstraints);
		String hasDesignRules = solutionObject.getString("hasDesignRules");
		solution.setHasDesignRules(hasDesignRules);
		String hasPros = solutionObject.getString("hasPros");
		solution.setHasPros(hasPros);

		project.setHasSolution(solution);
		AbstractDAOManager.getAbstractDAOInstance().updateResource(solution,
				false);
		AbstractDAOManager.getAbstractDAOInstance().updateResource(project,
				false);

		return project.getUri().toString();
	}

	public static String getProjectSolutionJsonByUri(String projectUri)
			throws Exception {
		String resultsJson = "[]";
		resultsJson = SolutionsDataLayer.getDataLayerInstance()
				.getProjectJsonByUri(projectUri);
		///////
		LOGGER.info("getProjectSolutionJsonByUri started for:"+projectUri);
		//resultsJson=ProjectJSONExporter.getInstance().getProjectSolutionAsJSON(projectUri);
		
		if (resultsJson == null) {
			Project project = null;
			try {
				project = SolutionsDataLayer.getDataLayerInstance()
						.getProjectByUri(projectUri);
				// project=ProjectManagament.getInstance().loadResourceByURI(Project.class,
				// projectUri, false);
			} catch (NotFoundException ex) {
				LOGGER.error("Exception in getProjectSolutionJsonByUri for projectUri:"
						+ projectUri);
				return resultsJson;
			}
			if (project != null) {
				resultsJson = ProjectJSONExporter.getInstance()
						.getProjectSolutionAsJSON(project);
				SolutionsDataLayer.getDataLayerInstance().setProjectJson(projectUri, resultsJson);
			}
		}	
		
		LOGGER.info("getProjectSolutionJsonByUri outputJSON:" + resultsJson);
		return resultsJson;
	}

	public static String saveDiagrams(String diagramsJson) throws Exception {

		JSONObject projectJsonObject = new JSONObject(diagramsJson);
		JSONStringer jsOutput = new JSONStringer(); // this is resulting json
													// that will be send to
													// ArgoUML
		String useruri = projectJsonObject.getString("useruri");
		User currentUser = UserManagament.getInstance().getUserByUri(useruri);

		String projecturi = projectJsonObject.getString("projecturi");
		Project currentProject = ProjectManagament.getInstance()
				.getProjectByUri(projecturi);

		JSONArray diagrams = projectJsonObject.getJSONArray("diagrams");
		jsOutput.array();
		for (int i = 0; i < diagrams.length(); i++) {
			jsOutput.object();

			JSONObject diagramObject = (JSONObject) diagrams.get(i);
			String mode = diagramObject.getString("mode");

			String type = diagramObject.getString("type");
			String fileUrl = diagramObject.getString("fileUrl");
			String title = diagramObject.getString("title");
			String id = diagramObject.getString("id");
			jsOutput.key("id").value(id);
			Diagram diagram = null;
			if (mode.equals("edit")) {
				String uri = diagramObject.getString("uri");
				diagram = ProjectManagament.getInstance().getDiagramByUri(uri);
				jsOutput.key("uri").value(uri);

				diagram.setFileUrl(fileUrl);
				diagram.setTitle(title);
				diagram.setId(id);
				ProjectManagament.getInstance().updateResource(diagram, false);
			} else if (mode.equals("create")) {
				if (type.equals("ActivityDiagram")) {
					diagram = new ActivityDiagram();
				} else if (type.equals("ClassDiagram")) {
					diagram = new ClassDiagram();
				} else if (type.equals("CollaborationDiagram")) {
					diagram = new CollaborationDiagram();
				} else if (type.equals("DeploymentDiagram")) {
					diagram = new DeploymentDiagram();
				} else if (type.equals("SequenceDiagram")) {
					diagram = new SequenceDiagram();
				} else if (type.equals("StatechartDiagram")) {
					diagram = new StatechartDiagram();
				} else if (type.equals("UseCaseDiagram")) {
					diagram = new UseCaseDiagram();
				}

				ProjectManagament.getInstance().saveResource(diagram, false);
				jsOutput.key("uri").value(diagram.getUri().toString());
				currentProject.addDiagram(diagram);
				ProjectManagament.getInstance().updateResource(currentProject,
						false);

				diagram.setFileUrl(fileUrl);
				diagram.setTitle(title);
				diagram.setId(id);
				ProjectManagament.getInstance().updateResource(diagram, false);
			} else if (mode.equals("delete")) {
				String uri = diagramObject.getString("uri");
				Class clazz = null;
				if (type.equals("ActivityDiagram")) {
					clazz = ActivityDiagram.class;
				} else if (type.equals("ClassDiagram")) {
					clazz = ClassDiagram.class;
				} else if (type.equals("CollaborationDiagram")) {
					clazz = CollaborationDiagram.class;
				} else if (type.equals("DeploymentDiagram")) {
					clazz = DeploymentDiagram.class;
				} else if (type.equals("SequenceDiagram")) {
					clazz = SequenceDiagram.class;
				} else if (type.equals("StatechartDiagram")) {
					clazz = StatechartDiagram.class;
				} else if (type.equals("UseCaseDiagram")) {
					clazz = UseCaseDiagram.class;
				}
				ProjectManagament.getInstance().deleteResourceByURI(clazz, uri,
						false);
			}

			jsOutput.endObject();
		}
		jsOutput.endArray();

		return jsOutput.toString();

	}
}
