package org.goodoldai.depths.rest;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.goodoldai.depths.domain.general.Resource;
import org.goodoldai.depths.domain.user.User;
import org.goodoldai.depths.logging.ActivityLogging;
import org.goodoldai.depths.semanticstuff.services.content.ContentManagament;
import org.goodoldai.depths.semanticstuff.services.user.UserManagament;

 

@Path("logging")
public class ServiceUseLoggingService {
	private static final Logger LOGGER = Logger.getLogger(ServiceUseLoggingService.class);
	@GET
	@Path("sendmessage")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ "application/x-javascript", MediaType.APPLICATION_JSON })
	public String sendMessageToPeer(@QueryParam("sender") String sender, @QueryParam("receiver") String receiver,
			@QueryParam("fromapp") String fromApp, @QueryParam("toapp") String toApp, @QueryParam("message") String content,
			@QueryParam("sessionid") String sessionId,
			@QueryParam("relatedto") String relatedToUri){
		User senderUser=UserManagament.getInstance().getUserByUsername(sender);
		//User receiverUser=UserManagament.getInstance().getUserByUsername(receiver);
		Resource resource=null;
		try {
			resource = ContentManagament.getInstance().loadResourceByURI(Resource.class, relatedToUri,false);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			LOGGER.info("Resource not found:"+relatedToUri+" error:"+e.getMessage());
			
		}
		Map<String, String> properties=new HashMap<String,String>();
		properties.put("fromapp", fromApp);
		properties.put("toapp", toApp);
		properties.put("receiver", receiver);
		properties.put("message", content);
		 
		ActivityLogging.getInstance().storeActivityRelatedLogData("SendMessageToPeer", sessionId, new Date(), senderUser, resource, properties);
		return "Success";
	}
	@GET
	@Path("redirect")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ "application/x-javascript", MediaType.APPLICATION_JSON })
	public String redirectFromTool(@QueryParam("username") String username, @QueryParam("caller") String caller,
			@QueryParam("fromapp") String fromApp, @QueryParam("toapp") String toApp, 
			@QueryParam("sessionid") String sessionId,
			@QueryParam("relatedto") String relatedToUri){
		User user=UserManagament.getInstance().getUserByUsername(username);
		//User receiverUser=UserManagament.getInstance().getUserByUsername(receiver);
		Resource resource=null;
		try {
			resource = ContentManagament.getInstance().loadResourceByURI(Resource.class, relatedToUri,false);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			LOGGER.error("Error:"+e.getLocalizedMessage());
		}
		Map<String, String> properties=new HashMap<String,String>();
		properties.put("fromapp", fromApp);
		properties.put("toapp", toApp);
		properties.put("caller", caller);
		 
		 
		ActivityLogging.getInstance().storeActivityRelatedLogData("RedirectUserFromExtApp", sessionId, new Date(), user, resource, properties);
		return "Success";
	}
	@GET
	@Path("pagevisit")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ "application/x-javascript", MediaType.APPLICATION_JSON })
	public String webPageVisit(
			@QueryParam("username") String username, 
			@QueryParam("webpageuri") String webPageUri,
			@QueryParam("webpagelink") String webPageLink,
			@QueryParam("relatedconcept") String relatedConcept,
			@QueryParam("pagerelevance") String pageRelevance,
			@QueryParam("sessionid") String sessionId 
			){
		User user=UserManagament.getInstance().getUserByUsername(username);
		//User receiverUser=UserManagament.getInstance().getUserByUsername(receiver);
		Resource resource=null;
		try {
			resource = ContentManagament.getInstance().loadResourceByURI(Resource.class, webPageUri,false);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			LOGGER.error("Error:"+e.getLocalizedMessage());
		}
		Map<String, String> properties=new HashMap<String,String>();
		properties.put("webpagelink", webPageLink);
		properties.put("relatedconcept", relatedConcept);
		properties.put("pagerelevance", pageRelevance);
		 
		 
		ActivityLogging.getInstance().storeActivityRelatedLogData("SuggestedWebPageVisit", sessionId, new Date(), user, resource, properties);
		return "Success";
	}

}
