package org.goodoldai.depths.rest;

import java.io.InputStream;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONException;
import org.goodoldai.depths.domain.activity.Brainstorming;
import org.goodoldai.depths.domain.user.User;
import org.goodoldai.depths.rest.json.TagsServiceUtility;
import org.goodoldai.depths.semanticstuff.services.annotations.AnnotationManagament;
import org.goodoldai.depths.semanticstuff.services.user.UserManagament;
import org.goodoldai.depths.utility.ServicesUtility;


@Path("tags")
public class TagsService {
	private static final Logger LOGGER = Logger.getLogger(TagsService.class);
	@GET
	@Path("getmytags")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ "application/x-javascript", MediaType.APPLICATION_JSON })
	public String getMyTagsByUsername(@QueryParam("username") String username){
		User user=UserManagament.getInstance().getUserByUsername(username);
		String myTagsJson="";
		try {
			myTagsJson = TagsServiceUtility.getListOfMyTagsByUriJSON(user.getUri().toString());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			LOGGER.error("Error:"+e.getLocalizedMessage());
		}
 LOGGER.info("getMyTags for username:"+username+" returning:"+myTagsJson);
		return myTagsJson;
	}
	@GET
	@Path("getmypeerstags")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ "application/x-javascript", MediaType.APPLICATION_JSON })
	public String getMyPeersTags(@QueryParam("username") String username){
		User user=UserManagament.getInstance().getUserByUsername(username);
		String myTagsJson="";
		try {
			myTagsJson = TagsServiceUtility.getListOfPeersTagsByUriJSON(user.getUri().toString());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			LOGGER.error("Error:"+e.getLocalizedMessage());
		}
		LOGGER.info("getmypeerstags for username:"+username+" returning:"+myTagsJson);
		return myTagsJson;
	}
	@GET
	@Path("getalldomainconcepts")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ "application/x-javascript", MediaType.APPLICATION_JSON })
	public String getAllDomainConcepts(@QueryParam("username") String username){
		User user=UserManagament.getInstance().getUserByUsername(username);
		String myTagsJson="";
		try {
			String userUri="";
			if(user!=null){
				user.getUri().toString();
			}
			myTagsJson = TagsServiceUtility.getAllDomainConceptsAsTagsJSON(userUri);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			LOGGER.error("Error:"+e.getLocalizedMessage());
		}
		LOGGER.info("getAllDomainConcepts for username:"+username+" returns:"+ myTagsJson); 
		return myTagsJson;
	}
	@GET
	@Path("getmypeersnotes")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ "application/x-javascript", MediaType.APPLICATION_JSON })
	public String getMyPeersNotes(@QueryParam("username") String username, @QueryParam("ideauri") String ideaUri){
		User user=UserManagament.getInstance().getUserByUsername(username);
		String myPeersNotesForIdeaJson="[]";
		Brainstorming idea=null;
		 
		 try {
			 idea=AnnotationManagament.getInstance().loadResourceByURI(Brainstorming.class,ideaUri,false);
			 if(idea!=null){
			myPeersNotesForIdeaJson = TagsServiceUtility.getListOfPeersNotesForIdeaUriJSON(user,idea);
			 }
		} catch (Exception e) {
			// TODO Auto-generated catch block
			LOGGER.info("Exception happened in getMyPeersNotes for idea:"+ideaUri+" error:"+e.getMessage());
			//LOGGER.error("Error:"+e.getLocalizedMessage());
		}
		 LOGGER.info("getAllDomainConcepts for username:"+username+" idea:"+ideaUri+" returns:"+ myPeersNotesForIdeaJson);
		return myPeersNotesForIdeaJson;
	}
	@GET
	@Path("getmyannotationforidea")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ "application/x-javascript", MediaType.APPLICATION_JSON })
	public String getMyAnnotationForIdea(@QueryParam("username") String username, @QueryParam("ideauri") String ideauri){
	 	User user=UserManagament.getInstance().getUserByUsername(username);
		String myAnnotationsJson="[]";
		Brainstorming idea=null;
		 	 try {
			 idea=AnnotationManagament.getInstance().loadResourceByURI(Brainstorming.class,ideauri,false);
			 if(idea!=null){
			 myAnnotationsJson=TagsServiceUtility.getMyAnnotationOfIdeaJSON(user,idea);
			 }
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			LOGGER.info("getMyAnnotationForIdea not finished because resource not found:"+ideauri);
		}
		 	 LOGGER.info("getmyannotationforidea for username:"+username+" idea:"+ideauri+" returns:"+ myAnnotationsJson);
	 	return myAnnotationsJson;
	}
	
	@POST
	@Path("addtagsforidea")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ "application/x-javascript", MediaType.APPLICATION_JSON })
	public String addTagsForIdea(InputStream is) {
	 
		 String response="";
		String jsonString= ServicesUtility.convertInputStreamToString(is);
		 
	     try {
			 response=TagsServiceUtility.addIdeaAnnotation(jsonString);
		 } catch (JSONException e) {
			// TODO Auto-generated catch block
		 	LOGGER.error("Error:"+e.getLocalizedMessage());
		 }catch(Exception ex){
			 ex.printStackTrace();
		 }
	     LOGGER.info("addtagsforidea for input:"+jsonString+" returns:"+ response);
		return response;
	}
	
	@POST
	@Path("addcommentforidea")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ "application/x-javascript", MediaType.APPLICATION_JSON })
	public String addCommentForIdea(InputStream is) {
	 
		 String response="";
		String jsonString= ServicesUtility.convertInputStreamToString(is);
		 
	     try {
			 response=TagsServiceUtility.addCommentAnnotation(jsonString);
		 } catch (JSONException e) {
			// TODO Auto-generated catch block
		 	LOGGER.error("Error:"+e.getLocalizedMessage());
		 }catch(Exception ex){
			 ex.printStackTrace();
		 }
	     LOGGER.info("addcommentforidea for input:"+jsonString+" returns:"+ response);
		return response;
	}
}
