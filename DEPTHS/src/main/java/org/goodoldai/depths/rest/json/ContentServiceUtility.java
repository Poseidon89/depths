package org.goodoldai.depths.rest.json;

import java.util.Collection;

import org.apache.log4j.Logger;
import org.goodoldai.depths.domain.content.Course;
import org.goodoldai.depths.domain.content.DesignProblem;
 
import org.goodoldai.depths.jsonstuff.ContentJSONExporter;
import org.goodoldai.depths.semanticstuff.services.content.ContentManagament;
import org.goodoldai.depths.services.datalayer.SolutionsDataLayer;
 

public class ContentServiceUtility {
	private static final Logger LOGGER = Logger.getLogger(ContentServiceUtility.class);
	
	public static String getListOfMyCoursesByUriJSON(String userUri) throws Exception{
		Collection<Course> myCourses=ContentManagament.getInstance().getCoursesForUser(userUri);
		String coursesJson="[]";
		if(myCourses!=null){
		 coursesJson=ContentJSONExporter.getInstance().getCoursesAsJSON(myCourses);
		} 
		LOGGER.info("user:"+userUri);
		LOGGER.info(coursesJson);
		return coursesJson;
	}

	public static String getListOfDesignProblemsForCourseJSON(String courseUri) throws Exception {
		// TODO Auto-generated method stub
		Collection<DesignProblem> dProblems=ContentManagament.getInstance().getDesignProblemsForCourse(courseUri);
		String dProblemsJson=ContentJSONExporter.getInstance().getDesignProblemsAsJSON(dProblems);
		LOGGER.info("course:"+courseUri);
		LOGGER.info(dProblemsJson);
		return dProblemsJson;
	}

	public static String getProblemDescriptionJSON(String problemUri) throws Exception {
		// TODO Auto-generated method stub
		//DesignProblem dProblem=ContentManagament.getInstance().loadResourceByURI(DesignProblem.class, problemUri,false);
		//DesignProblem dProblem=ProjectManagament.getInstance().loadResourceByURI(DesignProblem.class,problemUri,false);
		DesignProblem dProblem=SolutionsDataLayer.getDataLayerInstance().getDesignProblemByUri(problemUri);
		String dProblemDescriptionJson=ContentJSONExporter.getInstance().getDesignProblemDescriptionAsJSON(dProblem);
		LOGGER.info(dProblemDescriptionJson);
		return dProblemDescriptionJson;
	}

	public static String getListOfRelatedConceptsForProblemJSON(
			String problemUri) throws Exception {
		// TODO Auto-generated method stub
		//DesignProblem dProblem=ContentManagament.getInstance().loadResourceByURI(DesignProblem.class, problemUri,false);
		LOGGER.info("getListOfRelatedConceptsForProblemJSON for problem:"+problemUri);
		DesignProblem dProblem=SolutionsDataLayer.getDataLayerInstance().getDesignProblemByUri(problemUri);

		String dProblemRelatedConcepts=ContentJSONExporter.getInstance().getListOfRelatedConceptsForProblem(dProblem);
		LOGGER.info(dProblemRelatedConcepts);
		return dProblemRelatedConcepts;
	}

	public static String getListOfRecommendedConceptsForProblemJSON(
			String problemUri) throws Exception {
		LOGGER.info("getListOfRecommendedConceptsForProblemJSON for problem:"+problemUri);
		DesignProblem dProblem=SolutionsDataLayer.getDataLayerInstance().getDesignProblemByUri(problemUri);
	
		String dProblemRelatedConcepts=ContentJSONExporter.getInstance().getListOfRecommendedConceptsForProblem(dProblem);
		LOGGER.info(dProblemRelatedConcepts);
		return dProblemRelatedConcepts;
	}
}
