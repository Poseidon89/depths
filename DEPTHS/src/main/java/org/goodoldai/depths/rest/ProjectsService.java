package org.goodoldai.depths.rest;

import java.io.InputStream;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.goodoldai.depths.domain.user.User;
import org.goodoldai.depths.rest.json.ProjectServiceUtility;
//import org.goodoldai.depths.semanticstuff.services.project.ProjectManagament;
import org.goodoldai.depths.semanticstuff.services.user.UserManagament;
import org.goodoldai.depths.utility.ServicesUtility;

@Path("projects")
public class ProjectsService {
	private static final Logger LOGGER=Logger.getLogger(ProjectsService.class);
	@GET
	@Path("listbyusername")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ "application/x-javascript", MediaType.APPLICATION_JSON })
	public String getListOfMyProjectsByUsername(@QueryParam("username") String username){
		User user=UserManagament.getInstance().getUserByUsername(username);
		String projectsJson="";
		try {
			projectsJson = ProjectServiceUtility.getListOfMyProjectsJSON(user);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			LOGGER.error("Error:"+e.getLocalizedMessage());
		}
		LOGGER.info("getListOfMyProjectsByUsername service called for:"+username+" returning:"+projectsJson);
		return projectsJson;
	}
	@GET
	@Path("listallprojects")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ "application/x-javascript", MediaType.APPLICATION_JSON })
	public String getListOfAllProjects(@QueryParam("username") String username){
		User user=UserManagament.getInstance().getUserByUsername(username);
		if(user==null){
			 return null;
		}
		String projectsJson="";
		try {
			projectsJson = ProjectServiceUtility.getListOfAllProjectsJSON(user);
			 
		} catch (Exception e) {
			// TODO Auto-generated catch block
			LOGGER.error("Error:"+e.getLocalizedMessage());
		}
		LOGGER.info("getListOfAllProjects service called for:"+username+" returning:"+projectsJson);
		return projectsJson;
	}
	@POST
	@Path("listprojectsbyuris")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ "application/x-javascript", MediaType.APPLICATION_JSON })
	public String getListOfProjectsByUris(InputStream is){
		 
		String projectsJson="";
		String jsonString= ServicesUtility.convertInputStreamToString(is);
		 
		try {
			 projectsJson = ProjectServiceUtility.getListOfProjectsByUris(jsonString);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			LOGGER.error("Error:"+e.getLocalizedMessage());
		}
		LOGGER.info("getListOfAllProjects service called for input:"+jsonString+" returning:"+projectsJson);
		return projectsJson;
	}
	@GET
	@Path("listavailableprojects")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ "application/x-javascript", MediaType.APPLICATION_JSON })
	public String getListOfAllAllowedProjects(@QueryParam("username") String username){
		User user=UserManagament.getInstance().getUserByUsername(username);
		if(user==null){
			 return null;
		}
		String projectsJson="";
		try {
			projectsJson = ProjectServiceUtility.getListOfAllAllowedProjectsJSON(user);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			LOGGER.error("Error:"+e.getLocalizedMessage());
		}
		LOGGER.info("getListOfAllAllowedProjects for user:"+username+" returning:"+projectsJson);
		return projectsJson;
	}
	@GET
	@Path("listprojectsbyuseruri")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ "application/x-javascript", MediaType.APPLICATION_JSON })
	public String getListOfMyProjectsByUri(@QueryParam("useruri") String userUri){
		String projectsJson="";
		try {
			User user=UserManagament.getInstance().loadResourceByURI(User.class, userUri,false);
			projectsJson = ProjectServiceUtility.getListOfMyProjectsJSON(user);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			LOGGER.error("Error:"+e.getLocalizedMessage());
		}
		LOGGER.info("getListOfMyProjectsByUri for user:"+userUri+" returning:"+projectsJson);
		return projectsJson;
	}
	@GET
	@Path("listprojectsforproblembyusername")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ "application/x-javascript", MediaType.APPLICATION_JSON })
	public String getListOfProjectsForProblemByUsername(@QueryParam("username") String username,
			@QueryParam("designproblemuri") String designProblemUri){
		 		String projectsJson="";
		try {
			User user=UserManagament.getInstance().getUserByUsername(username);
			projectsJson = ProjectServiceUtility.getListOfrojectsForDesignProblemJSON(user,designProblemUri);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			LOGGER.error("Error:"+e.getLocalizedMessage());
		}
		LOGGER.info("listprojectsforproblembyusername for user:"+username+" and problem:"+designProblemUri+" returning:"+projectsJson);
		return projectsJson;
	}
	@GET
	@Path("listprojectsforproblem")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ "application/x-javascript", MediaType.APPLICATION_JSON })
	public String getListOfProjectsForProblem(@QueryParam("useruri") String userUri,
			@QueryParam("designproblemuri") String designProblemUri){
	 	String projectsJson="";
		try {
			User user=UserManagament.getInstance().loadResourceByURI(User.class, userUri,false);
			projectsJson = ProjectServiceUtility.getListOfrojectsForDesignProblemJSON(user,designProblemUri);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			LOGGER.error("Error:"+e.getLocalizedMessage());
		}
		LOGGER.info("listprojectsforproblem for user:"+userUri+" and problem:"+designProblemUri+" returning:"+projectsJson);
		return projectsJson;
	}
	@GET
	@Path("getprojectnotsubmitted")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ "application/x-javascript", MediaType.APPLICATION_JSON })
	public String getListOfProjectsForProblemNotSubmitted(@QueryParam("username") String username,
			@QueryParam("designproblemuri") String designProblemUri){
		User user=UserManagament.getInstance().getUserByUsername(username);
		 
		String projectsJson="";
		try {
			projectsJson = ProjectServiceUtility.getListOfProjectsForProblemNotSubmitted(user,designProblemUri);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			LOGGER.error("Error:"+e.getLocalizedMessage());
		}
		LOGGER.info("getprojectnotsubmitted for user:"+username+" and problem:"+designProblemUri+" returning:"+projectsJson);
		return projectsJson;
	}
	@GET
	@Path("setprojectassubmitted")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ "application/x-javascript", MediaType.APPLICATION_JSON })
	public String setProjectAsSubmitted(@QueryParam("username") String username,
			@QueryParam("projecturi") String projectUri,
			@QueryParam("submissionuri") String submissionUri){
		User user=UserManagament.getInstance().getUserByUsername(username);
		 
		String response="";
		try {
			response = ProjectServiceUtility.setProjectAsSubmitted(user.getUsername().toString(),projectUri,submissionUri);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			LOGGER.error("Error:"+e.getLocalizedMessage());
		}
		LOGGER.info("setprojectassubmitted for user:"+username+" and problem:"+projectUri+" submissionuri:"+submissionUri+" response:"+response);

		return response;
	}
	

}
