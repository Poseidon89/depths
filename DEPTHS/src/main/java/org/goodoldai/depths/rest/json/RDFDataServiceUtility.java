package org.goodoldai.depths.rest.json;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.goodoldai.depths.eventsprocessor.DeleteMoodleTable;
import org.goodoldai.depths.eventsprocessor.InsertMoodleTable;

public class RDFDataServiceUtility {
	private static final Logger LOGGER = Logger.getLogger(RDFDataServiceUtility.class);
	
	public static String addMoodleTableToRDF(String jsonTable) throws JSONException{
	 	   String response=""; 
	 	 
	 	 
			JSONObject tableJsonObject=new JSONObject(jsonTable);
			 String action=tableJsonObject.getString("action");
			 String sessionId="";
			 if(tableJsonObject.has("sessionid")){
			  sessionId=tableJsonObject.getString("sessionid");
			 }
			if(tableJsonObject.has("tablename")){
				String tableName=tableJsonObject.getString("tablename");
			 
				if(tableName.equals("modelling_crit_name")){
			 		String jCriteria=tableJsonObject.getString("criteria");
			 		JSONObject criteria=new JSONObject(jCriteria);
					String jRatings=tableJsonObject.getString("ratings");
		 			JSONArray ratings=new JSONArray(jRatings);
					InsertMoodleTable imt=new InsertMoodleTable();
					try {
						response= imt.addCriteriaRatings(criteria,ratings,action);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						LOGGER.error("Error L001:"+e.getLocalizedMessage());
					}
				}else if(tableName.equals("modelling_as_rating")){
					String jRating=tableJsonObject.getString("rating");
			 		JSONObject rating=new JSONObject(jRating);
					String jCriterias=tableJsonObject.getString("criterias");
		 		JSONArray criterias=new JSONArray(jCriterias);
					InsertMoodleTable imt=new InsertMoodleTable();
					try {
			 			response= imt.addAssessingData(rating,criterias,action, sessionId);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						LOGGER.error("Error L002:"+e.getLocalizedMessage());
					}
				}else{
				int tableId=tableJsonObject.getInt("id");
				String jTable=tableJsonObject.getString("dataobject");
				JSONObject table=new JSONObject(jTable);
				String jAddOn=tableJsonObject.getString("addon");
			 
				JSONObject addOnData=new JSONObject(jAddOn);
				
				//JSONObject table=tableJsonObject.getJSONObject("dataobject");
			 		InsertMoodleTable imt=new InsertMoodleTable();
				try {
					response= imt.checkInsertTableTypeAndProcess(tableName, table,addOnData, tableId, action, sessionId);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					LOGGER.error("Error L003:"+e.getLocalizedMessage());
				}
			} 
			}
			 
			return response;
		 
	}
	public static String deleteMoodleTableRow(String jsonTable) throws Exception{
		String response=""; 
		JSONObject tableJsonObject=new JSONObject(jsonTable);
		 String table=tableJsonObject.getString("table");
		 
		 DeleteMoodleTable dmt=new DeleteMoodleTable();
		 dmt.checkTableAndDeleteRow(table,tableJsonObject);
		
		return response;
	}
}
