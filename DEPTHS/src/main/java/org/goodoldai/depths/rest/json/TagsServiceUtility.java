package org.goodoldai.depths.rest.json;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import org.codehaus.jettison.json.JSONObject;
import org.goodoldai.depths.domain.activity.Brainstorming;
import org.goodoldai.depths.domain.annotation.ParentTag;
import org.goodoldai.depths.domain.annotation.Tag;
import org.goodoldai.depths.domain.annotation.UserNote;

import org.goodoldai.depths.domain.general.Relevance;
import org.goodoldai.depths.domain.general.Resource;

import org.goodoldai.depths.domain.skos.Concept;
import org.goodoldai.depths.domain.user.User;
import org.goodoldai.depths.jsonstuff.TagsJSONExporter;
import org.goodoldai.depths.semanticstuff.services.annotations.AnnotationManagament;

import org.goodoldai.depths.semanticstuff.services.user.UserManagament;

public class TagsServiceUtility {
	private static final Logger LOGGER = Logger.getLogger(TagsServiceUtility.class);
	public static String getListOfMyTagsByUriJSON(String userUri) throws Exception {
		Collection<Tag> annotatedItems=AnnotationManagament.getInstance().getTagsForUser(userUri);
		String tagsJson=TagsJSONExporter.getInstance().getTagsAsJSON(annotatedItems);
		 
		return tagsJson;
	}
	public static String getListOfPeersTagsByUriJSON(String userUri) throws Exception {
		Collection<Tag> annotatedItems=AnnotationManagament.getInstance().getAllPeersTags(userUri);
		String tagsJson=TagsJSONExporter.getInstance().getTagsAsJSON(annotatedItems);
		 
		return tagsJson;
	}
	public static String addIdeaAnnotation(String jsonString) throws Exception {
		JSONObject jsonObject=new JSONObject(jsonString);
		 String username=jsonObject.getString("username");
		 User user=UserManagament.getInstance().getUserByUsername(username);
		 String ideauri=jsonObject.getString("ideauri");
		 Brainstorming idea=AnnotationManagament.getInstance().loadResourceByURI(Brainstorming.class,ideauri,false);
		 //String notes=jsonObject.getString("notes");
		 String visibility=jsonObject.getString("visibility");
		 String tagsString=jsonObject.getString("tags");
		 if(idea!=null){
			JSONObject tagsObj=new JSONObject(tagsString);
			for (int i = 0; i < tagsObj.length(); i++) {
				String tagStr = (String) tagsObj.getString(String.valueOf(i+1));
				
				Concept concept=AnnotationManagament.getInstance().getDomainConceptByTitle(tagStr);
				if(concept==null){
				 ParentTag pTag=AnnotationManagament.getInstance().getParentTag(user,tagStr);
				 Tag tag=AnnotationManagament.getInstance().setTagForIdeaByUser(user,idea,pTag);
				 idea.addHasAnnotation(tag);
				}else{
					Tag tag=AnnotationManagament.getInstance().setTagForIdeaByUser(user,idea,concept);
					 idea.addHasAnnotation(tag);
					idea.addTopic(concept);
				}
			 
				 
			}
			AnnotationManagament.getInstance().saveResource(idea,false);
		 }
		 
		return null;
	}
	public static String getMyAnnotationOfIdeaJSON(User user, Brainstorming idea) {
		
		 String myAnnotationOfIdeaJSON="";
		 try{
		 myAnnotationOfIdeaJSON=AnnotationManagament.getInstance().getMyAnnotationOfIdea(user,idea);
		 }catch(Exception e){
			 LOGGER.error("Error:"+e.getLocalizedMessage());
		 }
		return myAnnotationOfIdeaJSON;
	}
	public static String getListOfPeersNotesForIdeaUriJSON(User user,
			Brainstorming idea) throws Exception {
		String myPeersNotesForIdeaJson="";
		myPeersNotesForIdeaJson=AnnotationManagament.getInstance().getPeersNotesForIdeaUriJSON(user,idea);
		
		return myPeersNotesForIdeaJson;
	}
	public static String getAllDomainConceptsAsTagsJSON(String string) throws Exception {
		Collection<Resource> annotatedItems=AnnotationManagament.getInstance().getAllAnnotatedResources();
		Map<Concept, Integer> conceptFrequency=new HashMap<Concept,Integer>();
		if(annotatedItems!=null){
		for(Resource annItem:annotatedItems){
			if(annItem instanceof Relevance){
				Concept c=((Relevance) annItem).getIsRelevantFor();
				if(!conceptFrequency.containsKey(c)){
					conceptFrequency.put(c, Integer.valueOf(1));
				}else{
					Integer value=conceptFrequency.get(c);
					value=value+1;
					conceptFrequency.put(c, value);
				}
			}
		}
	}
		String tagsJson=TagsJSONExporter.getInstance().getConceptsAsTagsJSON(conceptFrequency);
		 
		return tagsJson;
	}
	public static String addCommentAnnotation(String jsonString) throws Exception {
	
		JSONObject jsonObject=new JSONObject(jsonString);
		 String username=jsonObject.getString("username");
		 User user=UserManagament.getInstance().getUserByUsername(username);
		 String ideauri=jsonObject.getString("ideauri");
		 Brainstorming idea=AnnotationManagament.getInstance().loadResourceByURI(Brainstorming.class,ideauri,false);
		 String comment=jsonObject.getString("comment");
		 String visibility=jsonObject.getString("visibility");
		
		 String commentType=jsonObject.getString("commenttype");
		
		 if(idea!=null){
		 UserNote uNote=AnnotationManagament.getInstance().getUserNoteForIdeaByUser(user,idea,comment,visibility,commentType);
		 }
		 return null;
	}

}
