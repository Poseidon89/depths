package org.goodoldai.depths.logging.domain;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.goodoldai.depths.domain.general.Resource;
 

@Entity
@Table(name = "Activity")
public class ActivityEntity {

	private long id;
	protected Date datetime;
	private Set<ActivityPropertyEntity> properties =new HashSet<ActivityPropertyEntity>();
	private UserSessionEntity usersession;
	private ActivityTypeEntity activitytype;
	private ResourceEntity resource;
	
	public ActivityEntity(Date date, UserSessionEntity userSession2,
			ActivityTypeEntity activityTypeEnt, ResourceEntity resource2) {
		datetime=date;
		usersession=userSession2;
		activitytype=activityTypeEnt;
		resource=resource2;
	}

	@Id
	@Column(name = "id", unique = true, nullable = false, insertable=false, updatable=false)
	@GeneratedValue(strategy=GenerationType.AUTO)
	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "datetime", length = 19)
	public Date getDatetime() {
		return this.datetime;
	}

	public void setDatetime(Date datetime) {
		this.datetime = datetime;
	}
	@OneToMany(cascade=CascadeType.ALL, mappedBy="activity")
	public Set<ActivityPropertyEntity> getProperties() {
		return properties;
	}

	public void setProperties(Set<ActivityPropertyEntity> properties) {
		this.properties = properties;
	}
	
	public void addProperty(ActivityPropertyEntity property){
		this.properties.add(property);
	}
	
	@OneToOne(cascade = CascadeType.ALL)
	public UserSessionEntity getUsersession() {
		return usersession;
	}

	public void setUsersession(UserSessionEntity usersession) {
		this.usersession = usersession;
	}
	@OneToOne(cascade = CascadeType.ALL)
	public ActivityTypeEntity getActivitytype() {
		return activitytype;
	}
	
	public void setActivitytype(ActivityTypeEntity activitytype) {
		this.activitytype = activitytype;
	}
	@OneToOne(cascade = CascadeType.ALL)
	public ResourceEntity getResource() {
		return resource;
	}

	public void setResource(ResourceEntity resource) {
		this.resource = resource;
	}
}
