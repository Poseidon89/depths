package org.goodoldai.depths.logging.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Index;

@Entity
@Table(name = "ServiceType")
public class ServiceTypeEntity {
	private long id;
	protected String type;
	
	public ServiceTypeEntity(){ }

	public ServiceTypeEntity(String type){
		this.type=type;
	}
	
	@Id
	@Column(name = "id", unique = true, nullable = false, insertable=false, updatable=false)
	@GeneratedValue(strategy=GenerationType.AUTO)
	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}
	
	@Column(name = "type", nullable = false)
	@Index(name="type")
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	 
	
}
