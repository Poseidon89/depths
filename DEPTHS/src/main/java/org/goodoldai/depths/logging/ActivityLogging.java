package org.goodoldai.depths.logging;

import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;


import org.apache.log4j.Logger;
import org.goodoldai.depths.config.LoggingConfig;
import org.goodoldai.depths.config.Settings;
import org.goodoldai.depths.domain.general.Resource;
import org.goodoldai.depths.domain.user.User;

import org.goodoldai.depths.logging.domain.ActivityPropertyEntity;
import org.goodoldai.depths.logging.domain.ActivityEntity;
import org.goodoldai.depths.logging.domain.ActivityTypeEntity;
import org.goodoldai.depths.logging.domain.ResourceEntity;
import org.goodoldai.depths.logging.domain.UserEntity;
import org.goodoldai.depths.logging.domain.UserSessionEntity;


public class ActivityLogging extends AbstractQueryService {
	
	private static final Logger LOGGER = Logger.getLogger(ActivityLogging.class);
	@SuppressWarnings("unused")
	private LoggingConfig loggConfig = Settings.getInstance().config.loggingConfig;

	public static class ActivityLoggingHolder {
		private static final ActivityLogging INSTANCE = new ActivityLogging();
	}

	public static ActivityLogging getInstance() {
		return ActivityLoggingHolder.INSTANCE;
	}
	private ActivityTypeEntity getActivityType(String activityType) {
		String stringQuery = "from ActivityTypeEntity where type =:type";
		try {
			synchronized (activityType) {

				ActivityTypeEntity activityTypeEntity = (ActivityTypeEntity) ServiceUseLogging.getInstance()
						.getUniqueResultForOneParamQuery(stringQuery, "type",
								activityType);
				if (activityTypeEntity == null) {
					activityTypeEntity = new ActivityTypeEntity(activityType);
					ServiceUseLogging.getInstance().saveEntity(activityTypeEntity);
				}
				return activityTypeEntity;
			}
		} catch (NullPointerException npe) {
			LOGGER.info("Service type is null");
			return null;
		}
	}

	/*
	public UserEntity getUserEntity(String userUri, String name) {
		String stringQuery = "from UserEntity where uri =:uri";
	 
		try {
			synchronized (userUri) {
				UserEntity userEntity = (UserEntity) this
						.getUniqueResultForOneParamQuery(stringQuery, "uri", userUri);
				
				if (userEntity == null) {
					userEntity = new UserEntity(userUri, name);
					this.saveEntity(userEntity);
				}

				return userEntity;
			}
		} catch (NullPointerException npe) {
			LOGGER.info("User entity is null");
			return null;
		}
	}
	*/
	public void makeTestConnection(){
		String activityType="SendMessageToPeer";
		String stringQuery = "from ActivityTypeEntity where type =:type";
		try {
			synchronized (activityType) {

				ActivityTypeEntity activityTypeEntity = (ActivityTypeEntity) ServiceUseLogging.getInstance()
						.getUniqueResultForOneParamQuery(stringQuery, "type",
								activityType);
			}
		}catch(Exception ex){
			LOGGER.info("Error in makeTestConnection:"+ex.getMessage());
		}
	}
	public ResourceEntity getResourceEntity(String resourceUri,
			String resourceType, String resourceTitle) {

		String stringQuery = "from ResourceEntity where uri =:uri";
		try {
			synchronized (resourceUri) {
				ResourceEntity resEntity = (ResourceEntity) this
						.getUniqueResultForOneParamQuery(stringQuery, "uri",
								resourceUri);

				if (resEntity == null) {
					resEntity = new ResourceEntity(resourceUri, resourceType,
							resourceTitle);
					this.saveEntity(resEntity);
				} else if ((!resEntity.getResourceTitle().equals(resourceTitle) && (!resourceType
						.equals(User.class.getSimpleName())))) {
					resEntity.setResourceTitle(resourceTitle);
					this.updateEntity(resEntity);
				}
				return resEntity;
			}
		} catch (NullPointerException npe) {
			LOGGER.info("Resource entity is null");
			return null;
		}
	}
	/*
	public UserSessionEntity getUserSessionEntity(UserEntity user,
			String sessionId) {

		String stringQuery = "from UserSessionEntity where userEntity_id=:userId and sessionId=:sessionId";

		try {
			synchronized (sessionId) {
				Map<String, Object> params = new HashMap<String, Object>();
				params.put("userId", user.getId());
				params.put("sessionId", sessionId);

				UserSessionEntity usSessEntity = (UserSessionEntity) this
						.getUniqueResultForMoreParamQuery(stringQuery, params);

				if (usSessEntity == null) {
					usSessEntity = new UserSessionEntity(user, sessionId,
							new Date());
					this.saveEntity(usSessEntity);
				}
				return usSessEntity;
			}
		} catch (NullPointerException npe) {
			LOGGER.info("UserSession entity is null:" + npe.getMessage());
			return null;
		}
	}
	*/
 
	public void storeActivityRelatedLogData(String activityType, String sessionId,
			Date activityDate, User user, Resource resource,
			Map<String, String> properties) {

		UserEntity userEntity = ServiceUseLogging.getInstance().getUserEntity(user.getUri().toString(),
				user.getUsername());
		
		UserSessionEntity userSession = ServiceUseLogging.getInstance().getUserSessionEntity(userEntity,
				sessionId);
		ResourceEntity resEntity=null;
		if(resource!=null){
		 resEntity=getResourceEntity(resource.getUri().toString(),resource.getClass().getSimpleName(),resource.getTitle());
		}
		ActivityTypeEntity activityTypeEnt = getActivityType(activityType);
		ActivityEntity activityEnt = new ActivityEntity(activityDate, userSession,
				  activityTypeEnt, resEntity);
	 

		activityEnt.setUsersession(userSession);
		if(activityEnt!=null){
		ServiceUseLogging.getInstance().saveEntity(activityEnt);
		}
		if (properties!=null){
		Iterator<Entry<String, String>> it = properties.entrySet().iterator();
		Set<ActivityPropertyEntity> propertiesSet = new HashSet<ActivityPropertyEntity>();

		while (it.hasNext()) {
			Entry<String, String> pairs = it.next();
		 if (pairs.getValue()==null){
			 pairs.setValue("");
		 }
			ActivityPropertyEntity pEntity = new ActivityPropertyEntity(pairs.getKey(),
					pairs.getValue());
			pEntity.setActivity(activityEnt);
			ServiceUseLogging.getInstance().saveEntity(pEntity);
			activityEnt.addProperty(pEntity);
			propertiesSet.add(pEntity);

		}
		}
		ServiceUseLogging.getInstance().updateEntity(activityEnt);
	}
}
