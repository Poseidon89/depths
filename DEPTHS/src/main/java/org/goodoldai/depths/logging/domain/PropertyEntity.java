package org.goodoldai.depths.logging.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "Property")
public class PropertyEntity  implements java.io.Serializable  {
	private static final long serialVersionUID = 3005983455665015133L;
	
	private long id;
	protected String key;
	protected String value;
	protected ServiceEntity service;

	public PropertyEntity(){ }
	
	public PropertyEntity(String key, String value){
		this.key=key;
		this.value=value;
		 
	}
	
	@Id
	@Column(name = "id", unique = true, nullable = false, insertable=false, updatable=false)
	@GeneratedValue(strategy=GenerationType.AUTO)
	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}
	
	@Column(name = "propkey", nullable = false)
	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}
	
	@Column(name = "propvalue", nullable = false)
	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	 
	@ManyToOne(targetEntity=ServiceEntity.class)
	@JoinColumn(name="service_id", nullable = false)
	public ServiceEntity getService() {
		return service;
	}

	public void setService(ServiceEntity service) {
		this.service = service;
	}
 
}
