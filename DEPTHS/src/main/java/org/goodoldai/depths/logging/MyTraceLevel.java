package org.goodoldai.depths.logging;

import org.apache.log4j.Level;

public class MyTraceLevel extends Level{
	 
    /**
     * Value of my trace level. This value is lesser than {@link org.apache.log4j.Priority#DEBUG_INT}
     * and higher than {@link org.apache.log4j.Level#TRACE_INT}
     */
    public static final int MY_TRACE_INT = DEBUG_INT - 10;
    /**
     * {@link Level} representing my log level
     */
    public static final Level MY_TRACE = new MyTraceLevel(MY_TRACE_INT,"MY_TRACE",3);
    /**
     * Constructor
     *
     * @param arg0
     * @param arg1
     * @param arg2
     */
	protected MyTraceLevel(int level, String levelStr, int syslogEquivalent) {
		super(level, levelStr, syslogEquivalent);
		// TODO Auto-generated constructor stub
	}
	 /**
     * Checks whether <code>sArg</code> is "MY_TRACE" level. If yes then returns {@link MyTraceLevel#MY_TRACE},
     * else calls {@link MyTraceLevel#toLevel(String, Level)} passing it {@link Level#DEBUG} as the defaultLevel
     *
     * @see Level#toLevel(java.lang.String)
     * @see Level#toLevel(java.lang.String, org.apache.log4j.Level)
     *
     */
    public static Level toLevel(String sArg) {
        if (sArg != null && sArg.toUpperCase().equals("MY_TRACE")) {
            return MY_TRACE;
        }
        return (Level) toLevel(sArg, Level.DEBUG);
    }

    /**
     * Checks whether <code>val</code> is {@link MyTraceLevel#MY_TRACE_INT}. If yes then returns {@link MyTraceLevel#MY_TRACE},
     * else calls {@link MyTraceLevel#toLevel(int, Level)} passing it {@link Level#DEBUG} as the defaultLevel
     *
     * @see Level#toLevel(int)
     * @see Level#toLevel(int, org.apache.log4j.Level)
     *
     */
    public static Level toLevel(int val) {
        if (val == MY_TRACE_INT) {
            return MY_TRACE;
        }
        return (Level) toLevel(val, Level.DEBUG);
    }
    /**
     * Checks whether <code>val</code> is {@link MyTraceLevel#MY_TRACE_INT}. If yes then returns {@link MyTraceLevel#MY_TRACE},
     * else calls {@link Level#toLevel(int, org.apache.log4j.Level)}
     *
     * @see Level#toLevel(int, org.apache.log4j.Level)
     */
    public static Level toLevel(int val, Level defaultLevel) {
        if (val == MY_TRACE_INT) {
            return MY_TRACE;
        }
        return Level.toLevel(val,defaultLevel);
    }

    /**
     * Checks whether <code>sArg</code> is "MY_TRACE" level. If yes then returns {@link MyTraceLevel#MY_TRACE},
     * else calls {@link Level#toLevel(java.lang.String, org.apache.log4j.Level)}
     *
     * @see Level#toLevel(java.lang.String, org.apache.log4j.Level)
     */
    public static Level toLevel(String sArg, Level defaultLevel) {                 
    if(sArg != null && sArg.toUpperCase().equals("MY_TRACE")) {
        return MY_TRACE;
    }
    return Level.toLevel(sArg,defaultLevel);
 }

}
