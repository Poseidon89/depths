package org.goodoldai.depths.services.peers;

import org.apache.log4j.Logger;
import org.goodoldai.depths.semanticstuff.services.content.ContentManagament;
import org.goodoldai.depths.semanticstuff.services.user.UserManagament;
import org.goodoldai.depths.utility.EvaluationUtility;

public class PeersServiceManager {

private static final Logger LOGGER = Logger.getLogger(PeersServiceManager.class);
	
	private static class PeersServiceManagerHolder{
		private static final PeersServiceManager INSTANCE=new PeersServiceManager();
	}
	public static PeersServiceManager getInstance(){
		return PeersServiceManagerHolder.INSTANCE;
	}
	public  void startFindPeersThread(String courseId){
		String designProblemId=null;
		//LOGGER.info("should be processed here");
	 	//FindPeersThread peersThread=new FindPeersThread(myRepository,courseId,designProblemId);
		 
		//peersThread.start();
	}
	public  String processPeersForDesignProblem(String currUserUri,String courseUri,String designProblemUri){
		//LOGGER.info(" calling start find peers thread here should be processed here");
			String json="";
		PeersProcessing peersProcessing=new PeersProcessing();
		try {
		
			json=peersProcessing.processPeersForDesignProblem(currUserUri,courseUri, designProblemUri);
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			LOGGER.error("Error:"+e.getLocalizedMessage());
		}
		return json;
		 //FindPeersThread peersThread=new FindPeersThread(courseId,designProblemId);
		// peersThread.start();
	}
}
