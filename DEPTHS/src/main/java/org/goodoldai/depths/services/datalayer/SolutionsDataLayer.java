package org.goodoldai.depths.services.datalayer;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.goodoldai.depths.domain.content.DesignProblem;
import org.goodoldai.depths.domain.project.Project;
import org.goodoldai.depths.semanticstuff.services.project.ProjectManagament;

import thewebsemantic.NotFoundException;
 
public class SolutionsDataLayer {
	private Map<String, Project> loadedProjects=new HashMap<String,Project>();
	private Map<String,String> loadedProjectsJsons=new HashMap<String,String>();
	private Map<String, DesignProblem> loadedProblems=new HashMap<String,DesignProblem>();
	private static final Logger LOGGER = Logger.getLogger(SolutionsDataLayer.class
			.getName());
	private static class SolutionsDataLayerHolder{
		private static final SolutionsDataLayer INSTANCE=new SolutionsDataLayer();
	}
	public static SolutionsDataLayer getDataLayerInstance(){
		return SolutionsDataLayerHolder.INSTANCE;
	}
	
	public Project getProjectByUri(String projectUri) throws Exception{
		Project project=null;
		LOGGER.info("getProjectByUri:"+projectUri+"  size:"+loadedProjects.size());
		if(loadedProjects.containsKey(projectUri)){
			LOGGER.info("FOUND");
			project=loadedProjects.get(projectUri);
			}
		else{
			try{
				project=ProjectManagament.getInstance().loadResourceByURI(Project.class, projectUri, false);
				
				if(project!=null){
					loadedProjects.put(projectUri, project);	
				}
			}catch(NotFoundException ex){
				LOGGER.error("Exception in getProjectSolutionJsonByUri for projectUri:"+projectUri);
				return null;
			}
		}
		return project;
	}
	public Collection<DesignProblem> getAllDesignProblems(){
		return loadedProblems.values();
	}
	public DesignProblem getDesignProblemByUri(String dProblemUri) throws Exception{
		DesignProblem dProblem=null;
		if(loadedProblems.containsKey(dProblemUri)){
			LOGGER.info("DesignProblem Found");
			dProblem=loadedProblems.get(dProblemUri);
		}else{
			dProblem=ProjectManagament.getInstance().loadResourceByURI(DesignProblem.class,dProblemUri,false);
			//dProblem=SolutionsDataLayer.getDataLayerInstance().getDesignProblemByUri(dProblemUri);
			if(dProblem!=null){
				loadedProblems.put(dProblemUri,dProblem);
			}
		}
		return dProblem;
	}
	public String getProjectJsonByUri(String projectUri) throws Exception{
		String json=null;
		LOGGER.info("getProjectJsonByUri started for :"+projectUri);
		if(loadedProjectsJsons.containsKey(projectUri)){
			LOGGER.info("project already exists in cache");
			json=loadedProjectsJsons.get(projectUri);
			}
		 
		return json;
	}
	public void setProjectJson(String projectUri, String json){
		loadedProjectsJsons.put(projectUri, json);
	}
}
