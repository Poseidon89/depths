package org.goodoldai.depths.services.datalayer;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.goodoldai.depths.domain.user.User;
import org.goodoldai.depths.services.peers.UserModel;
 
public class RecommendationDataLayer {
	private Map<String,UserModelData> userModelData=new HashMap<String,UserModelData>();
	//Map<dproblem,Map<user, usermodel>>
	private  Map<String, HashMap<String, UserModel>> peersForRecommendation=new HashMap<String,HashMap<String,UserModel>>();
	private static final Logger LOGGER = Logger.getLogger(RecommendationDataLayer.class
			.getName());
	private static class RecommendationDataLayerHolder{
		private static final RecommendationDataLayer INSTANCE=new RecommendationDataLayer();
	}
	public static RecommendationDataLayer getDataLayerInstance(){
		return RecommendationDataLayerHolder.INSTANCE;
	}
	public HashMap<String, UserModel> getPeersForRecommendationForProblem(String dProblemUri){
		if(peersForRecommendation.containsKey(dProblemUri)){
			return peersForRecommendation.get(dProblemUri);
		}else{
			HashMap<String,UserModel> dProblemListOfPeers=new HashMap<String,UserModel>();
			peersForRecommendation.put(dProblemUri, dProblemListOfPeers);
			return dProblemListOfPeers;
		}
	}
	public Collection<UserModel> getPeersForRecommendationForProblemAsCollection(String dProblemUri){
		return getPeersForRecommendationForProblem(dProblemUri).values();
	}
	public UserModel getPeerUserModel(Map<String, UserModel> dProblemListOfPeers,User user){
		LOGGER.info("getPeersUserModel:");
		LOGGER.info(user);
		if(user==null){
			return null;
		}
		if(dProblemListOfPeers.containsKey(user.getUri().toString())){
			return dProblemListOfPeers.get(user.getUri().toString());
		}else{
			UserModel newUserModel=new UserModel(user);
			dProblemListOfPeers.put(user.getUri().toString(), newUserModel);
			return newUserModel;
		}
	}
	public UserModelData getUserModelData(String userUri){
		 
		if(!userModelData.containsKey(userUri)){
			UserModelData newData=new UserModelData(userUri);
			userModelData.put(userUri, newData);
			return newData;
		}else{
			return userModelData.get(userUri);
		}
	}
	
	public void deleteUserCache(String userUri) {
		 
		if (userModelData.containsKey(userUri)) {
			LOGGER.info("User found. Deleting User Model cache for user:"
					+ userUri);
			userModelData.remove(userUri);
		}
	}
	public void deleteAllUsersCache() {
		if(!userModelData.isEmpty()){
			userModelData.clear();
		}
		
	}
	
}
