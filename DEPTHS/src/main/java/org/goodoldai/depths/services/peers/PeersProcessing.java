package org.goodoldai.depths.services.peers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.goodoldai.depths.domain.activity.Activity;
import org.goodoldai.depths.domain.activity.Assessing;
import org.goodoldai.depths.domain.activity.Brainstorming;
import org.goodoldai.depths.domain.activity.Submitting;
import org.goodoldai.depths.domain.content.Assessment;
import org.goodoldai.depths.domain.content.Brainstorm;
import org.goodoldai.depths.domain.content.DesignProblem;
import org.goodoldai.depths.domain.content.Submission;
import org.goodoldai.depths.domain.content.Task;
import org.goodoldai.depths.domain.general.LearningContext;

import org.goodoldai.depths.domain.user.Group;
import org.goodoldai.depths.domain.user.User;

import org.goodoldai.depths.rest.json.PeersServiceUtility;

import org.goodoldai.depths.semanticstuff.services.content.ContentManagament;
import org.goodoldai.depths.semanticstuff.services.context.LearningContextManagament;
import org.goodoldai.depths.semanticstuff.services.user.UserManagament;

import org.goodoldai.depths.services.datalayer.RecommendationDataLayer;
import org.goodoldai.depths.services.datalayer.SolutionsDataLayer;
import org.goodoldai.depths.services.datalayer.UserModelData;

import org.goodoldai.depths.utility.EvaluationUtility;
import org.goodoldai.depths.utility.PropertiesManager;
import org.goodoldai.depths.utility.WeightFactorsUtility;

public class PeersProcessing {
	String designProblemUri = null;
	String courseUri = null;

	private static final Logger LOGGER = Logger.getLogger(PeersProcessing.class);

	private static class PeersProcessingHolder {
		private static final PeersProcessing INSTANCE = new PeersProcessing();
	}

	public static PeersProcessing getInstance() {
		return PeersProcessingHolder.INSTANCE;
	}

	String processPeersForDesignProblem(String currUserUri, String courseUri,
			String dProblemUri) throws Exception {
		designProblemUri = dProblemUri;
		String json = "[]";
		// DesignProblem dProblem = ContentManagament.getInstance()
		// .getDesignProblemByUri(dProblemUri);
		// if (dProblem != null) {
		/*
		 * String projectType = dProblem.getProjectType(); Collection<String>
		 * allowedPeers = null; if (projectType.equals("teacher_groups")) {
		 * Collection<Group> currentUserGroups = UserManagament
		 * .getInstance().getCurrentUserCourseGroups(currUserUri, courseUri);
		 * allowedPeers = new ArrayList<String>(); if (currentUserGroups !=
		 * null) { for (Group group : currentUserGroups) { Collection<User>
		 * possibleUser = group.getMembers(); for (User us : possibleUser) { if
		 * (!allowedPeers.contains(us.getUri().toString())) {
		 * allowedPeers.add(us.getUri().toString()); } } } } }
		 * PropertiesManager.setFactors(); processLearningContext(dProblem,
		 * allowedPeers);
		 */
		
		// UserModelData
		// uModelData=RecommendationDataLayer.getDataLayerInstance().getUserModelData(currUserUri);
 
		HashMap<String, UserModel> dProblemListOfPeers = RecommendationDataLayer
				.getDataLayerInstance().getPeersForRecommendationForProblem(
						dProblemUri);
	 
		// userModel=RecommendationDataLayer.getDataLayerInstance().getPeerUserModel(dProblemListOfPeers,user);

	
		List<String> sortedListOfPeers = getSortedListOfPeersForDesignProblem(
				currUserUri, dProblemUri);
 
		LOGGER.info(sortedListOfPeers);
		// List<String> sortedListOfPeers =
		// getSortedListOfPeersForDesignProblem(uModelData,currUserUri,courseUri,dProblemUri);
	

		// json = PeersServiceUtility.getRecommendedPeersListAsJSON(
		// currUserUri, uModelData,courseUri,dProblemUri);
		json = PeersServiceUtility.getRecommendedPeersListAsJSON(currUserUri,
				sortedListOfPeers, dProblemUri);
 
		
		// }

		return json;
	}

	private List<String> getSortedListOfPeersForDesignProblem(
			String currUserUri, String dProblemUri) throws Exception {
		DesignProblem dProblem = SolutionsDataLayer.getDataLayerInstance()
				.getDesignProblemByUri(dProblemUri);
		HashMap<String, UserModel> dProblemListOfPeers = RecommendationDataLayer
				.getDataLayerInstance().getPeersForRecommendationForProblem(
						dProblemUri);
		if (dProblem != null) {
			String projectType = dProblem.getProjectType();

			Collection<User> allowedPeers = null;
			if (projectType.equals("teacher_groups")) {

				Collection<Group> currentUserGroups = UserManagament
						.getInstance().getCurrentUserCourseGroups(currUserUri,
								courseUri);

				allowedPeers = new ArrayList<User>();
				if (currentUserGroups != null) {

					for (Group group : currentUserGroups) {
						Collection<User> possibleUser = group.getMembers();
						for (User us : possibleUser) {
							if (!allowedPeers.contains(us.getUri().toString())) {
								allowedPeers.add(us);
							}
						}
					}

				}
			}
		}
		List<String> sortedListOfPeers = PeersProcessing.getInstance()
				.processRelevantPeers(dProblem);

		return sortedListOfPeers;
	}

	public void processPeersForDesignProblem(DesignProblem dProblem) {
		try {
			PropertiesManager.setFactors();
			processLearningContext(dProblem);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			LOGGER.error("Error:"+e.getLocalizedMessage());
		}
	}

	public void processLearningContext(DesignProblem dProblem) throws Exception {
		Collection<Task> tasks = dProblem.getTaskRef();

		if (tasks != null) {
			for (Task task : tasks) {

				if (task instanceof Brainstorm) {
					processingBrainstormTask(dProblem, task);
				} else if (task instanceof Submission) {
					processingSubmissionTask(dProblem, task);

				} else if (task instanceof Assessment) {
					processingAssessmentTask(dProblem, task);
				}

			}
		}
	}

	public void processLearningContext(final LearningContext lContext,final Task task) {
		LOGGER.info("processLearningContext:"+lContext.getUri()+" task:"+task.getUri());
		Thread thread = new Thread(new Runnable() {
		    public void run() {
		DesignProblem dProblem;
		try {
		 	PropertiesManager.setFactors();
			dProblem = ContentManagament.getInstance().getDesignProblemForTask(
					task.getUri().toString());
		 	if (task instanceof Brainstorm) {
		 		 
		 		processingBrainstormingTask(lContext,dProblem,task);
			} else if (task instanceof Submission) {
				processingSubmittingsTask(lContext,dProblem, task);

			} else if (task instanceof Assessment) {
				processingAssessingTask(lContext,dProblem, task);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			LOGGER.error("Error PP001:"+e.getLocalizedMessage());
		}
		    }
		    
		});
		thread.start();
		

	}

	private void processingBrainstormTask(DesignProblem dProblem, Task task)
			throws Exception {
		Collection<LearningContext> brainstormLContexts = LearningContextManagament
				.getInstance().getLearningContextForTask(
						task.getUri().toString());
		if (brainstormLContexts != null) {
			for (LearningContext brLearningContext : brainstormLContexts) {
				processingBrainstormingTask(brLearningContext,dProblem,task);
			}
		}

	}
	private void processingBrainstormingTask(LearningContext brLearningContext,
			DesignProblem dProblem, Task task) throws Exception{
	 	User user = brLearningContext.getUserRef();
		UserModel userModel = null;
		HashMap<String, UserModel> dProblemListOfPeers = RecommendationDataLayer
				.getDataLayerInstance()
				.getPeersForRecommendationForProblem(
						dProblem.getUri().toString());
		userModel = RecommendationDataLayer.getDataLayerInstance()
				.getPeerUserModel(dProblemListOfPeers, user);
		if(userModel!=null){
		if (brLearningContext.getActivityRef().getClass()
				.equals(Brainstorming.class)) {
			LOGGER.info("brainstorming found here");
		 	userModel.addActivityCompetence(WeightFactorsUtility
					.getBrainstormingActivityWeightPeers());
			Collection<String> evalValues = LearningContextManagament
					.getInstance().getEvaluationsForBrainstorming(
							brLearningContext.getActivityRef().getUri()
									.toString());
			for (String value : evalValues) {
				Double selValue = Double.valueOf(value);
				userModel.addSumOfIdeaSkillCompetenceValues(selValue);
			}
		} else if (brLearningContext.getActivityRef().getClass()
				.equals(Assessing.class)) {
			userModel.addActivityCompetence(WeightFactorsUtility
					.getAssessingActivityWeightPeers());
		}
		}
	}
	
	private void processingAssessingTask(LearningContext assLearningContext,DesignProblem dProblem, Task task){
		
		User user = assLearningContext.getUserRef();
		UserModel userModel = null;
		Map<String, UserModel> dProblemListOfPeers = RecommendationDataLayer
				.getDataLayerInstance()
				.getPeersForRecommendationForProblem(
						dProblem.getUri().toString());
		userModel = RecommendationDataLayer.getDataLayerInstance()
				.getPeerUserModel(dProblemListOfPeers, user);
		if (assLearningContext.getActivityRef().getClass()
				.equals(Brainstorming.class)) {
			if (assLearningContext.getActivityRef().getClass()
					.equals(Assessing.class)) {
				userModel.addActivityCompetence(WeightFactorsUtility
						.getAssessingActivityWeightPeers());
			}
		}
	}
	private void processingAssessmentTask(DesignProblem dProblem, Task task)
			throws Exception {
		Collection<LearningContext> assessmentLContexts = LearningContextManagament
				.getInstance().getLearningContextForTask(
						task.getUri().toString());
		if (assessmentLContexts != null) {
			for (LearningContext assLearningContext : assessmentLContexts) {
				processingAssessingTask(assLearningContext,dProblem,task);
			}
		}
		Collection<Submitting> submittingsForTask = LearningContextManagament
				.getInstance().getEvaluatedSubmittionsForTask(
						task.getUri().toString());
		if (submittingsForTask != null) {
			for (Submitting submitting : submittingsForTask) {
				User sentBy = submitting.getSentBy();
				UserModel sentByUserModel = null;
				Map<String, UserModel> dProblemListOfPeers = RecommendationDataLayer
						.getDataLayerInstance()
						.getPeersForRecommendationForProblem(
								dProblem.getUri().toString());
				sentByUserModel = RecommendationDataLayer
						.getDataLayerInstance().getPeerUserModel(
								dProblemListOfPeers, sentBy);
				Collection<String> evalValues = LearningContextManagament
						.getInstance().getEvaluationsForSubmitting(
								submitting.getUri().toString());

				for (String value : evalValues) {
					Double selValue = Double.valueOf(value);
					sentByUserModel
							.addSumOfSubmitionSkillCompetenceValues(selValue);
				}
			}
		}
	}

	 

	private void processingSubmissionTask(DesignProblem dProblem, Task task)
			throws Exception {
		Collection<LearningContext> submissionLContexts = LearningContextManagament
				.getInstance().getLearningContextForTask(
						task.getUri().toString());
		if (submissionLContexts != null) {

			for (LearningContext submLearningContext : submissionLContexts) {
				processingSubmittingsTask(submLearningContext, dProblem,task);
			}
		}
	}
private void processingSubmittingsTask(LearningContext submLearningContext, DesignProblem dProblem, Task task){
	User user = submLearningContext.getUserRef();
	UserModel userModel = null;
	
	HashMap<String, UserModel> dProblemListOfPeers = RecommendationDataLayer
			.getDataLayerInstance()
			.getPeersForRecommendationForProblem(
					dProblem.getUri().toString());
	userModel = RecommendationDataLayer.getDataLayerInstance()
			.getPeerUserModel(dProblemListOfPeers, user);
	if(userModel!=null){
	if (submLearningContext.getActivityRef() != null) {
		if (submLearningContext.getActivityRef().getClass()
				.equals(Submitting.class)) {
			userModel.addActivityCompetence(WeightFactorsUtility
					.getSubmittingActivityWeightPeers());
		}
	}
}
}
 

	 

	public List<String> processRelevantPeers(DesignProblem dProblem) {

		List<String> relPeersUrisSorted = new ArrayList<String>();
		HashMap<String, UserModel> recPeersForProblem = RecommendationDataLayer
				.getDataLayerInstance().getPeersForRecommendationForProblem(
						dProblem.getUri().toString());
		for (Map.Entry<String, UserModel> entry : recPeersForProblem.entrySet()) {
			UserModel tempUser = entry.getValue();

			double activityComp = tempUser.getActivityCompetence();
			double courseActivityComp = tempUser.getCourseActivityCompetence();
			double ideaSkillComp = tempUser.getIdeaSkillCompetence();
			double ideaCourseSkillComp = tempUser
					.getCourseIdeaSkillCompetence();

			double submitionSkillComp = tempUser.getSubmitionSkillCompetence();
			double courseSubmitionSkillComp = tempUser
					.getCourseSubmitionSkillCompetence();

			double totalActivityComp = (activityComp + courseActivityComp)
					* WeightFactorsUtility.getActivityCompetenceWeightPeers();
			double totalIdeaSkillComp = (ideaSkillComp + ideaCourseSkillComp)
					* WeightFactorsUtility.getSkillCompetenceWeightPeers();
			double totalSubmitionSkillComp = (submitionSkillComp + courseSubmitionSkillComp)
					* WeightFactorsUtility.getSkillCompetenceWeightPeers();

			double totalComp = totalActivityComp + totalIdeaSkillComp
					+ totalSubmitionSkillComp;

			LOGGER.info("_____________________________");
			LOGGER.info("ACTIVITY COMPETENCE");
			LOGGER.info("Problem Level: Brainstorming (WF:"
					+ WeightFactorsUtility
							.getBrainstormingActivityWeightPeers() + ")");
			LOGGER.info(" Submitting (WF:"
					+ WeightFactorsUtility.getSubmittingActivityWeightPeers()
					+ ")");
			LOGGER.info(" Assessing (WF:"
					+ WeightFactorsUtility.getAssessingActivityWeightPeers()
					+ ")");
			LOGGER.info("Course Level: Brainstorming (WF:"
					+ WeightFactorsUtility
							.getBrainstormingCourseActivityWeightPeers() + ")");
			LOGGER.info(" Submitting (WF:"
					+ WeightFactorsUtility
							.getSubmittingCourseActivityWeightPeers() + ")");
			LOGGER.info(" Assessing (WF:"
					+ WeightFactorsUtility
							.getAssessingCourseActivityWeightPeers() + ")");
			LOGGER.info("Total activity (WF:"
					+ WeightFactorsUtility.getActivityCompetenceWeightPeers()
					+ ")");
			LOGGER.info("Problem level=" + activityComp);
			LOGGER.info("Course level=" + courseActivityComp);
			LOGGER.info("Total=" + totalActivityComp);
			LOGGER.info("_____________________________");
			LOGGER.info("SKILL COMPETENCE (IDEAS RATING)");
			LOGGER.info("WF (Problem level:"
					+ WeightFactorsUtility.getBrainstormingSkillWeightPeers()
					+ ")");
			LOGGER.info(" (Course level:"
					+ WeightFactorsUtility
							.getBrainstormingCourseSkillWeightPeers() + ")");
			LOGGER.info("WF:"
					+ WeightFactorsUtility.getSkillCompetenceWeightPeers());
			LOGGER.info("Problem level=" + ideaSkillComp);
			LOGGER.info("Course level=" + ideaCourseSkillComp);

			LOGGER.info("Total=" + totalIdeaSkillComp);
			LOGGER.info("_____________________________");
			LOGGER.info("SKILL COMPETENCE (SOLUTIONS RATING)");
			LOGGER.info("WF (Problem level:"
					+ WeightFactorsUtility.getSubmittingSkillWeightPeers()
					+ ")");
			LOGGER.info(" (Course level:"
					+ WeightFactorsUtility
							.getSubmittingCourseSkillWeightPeers() + ")");
			LOGGER.info("WF:"
					+ WeightFactorsUtility.getSkillCompetenceWeightPeers());
			LOGGER.info("Problem level=" + submitionSkillComp);
			LOGGER.info("Course level=" + courseSubmitionSkillComp);

			LOGGER.info("Total=" + totalSubmitionSkillComp);
			LOGGER.info("_____________________________");
			LOGGER.info("TOTAL=" + totalComp);
			LOGGER.info("*****************************");

			tempUser.setTotalCompetence(totalComp);

			relPeersUrisSorted.add(tempUser.user.getUri().toString());

			CustComparatorByTotalCompetence comp = new CustComparatorByTotalCompetence();
			comp.setMap(RecommendationDataLayer.getDataLayerInstance()
					.getPeersForRecommendationForProblem(
							dProblem.getUri().toString()));
			Collections.sort(relPeersUrisSorted, comp);

		}
		return relPeersUrisSorted;
	}

	@Deprecated
	public List<String> processRelevantPeers(UserModelData uModelData,
			DesignProblem dProblem) {

		List<String> relPeersUrisSorted = new ArrayList<String>();
		for (Map.Entry<String, UserModel> entry : uModelData
				.getRelevantPeersForDesignProblem(dProblem.getUri().toString())
				.entrySet()) {

			UserModel tempUser = entry.getValue();

			double activityComp = tempUser.getActivityCompetence();
			double courseActivityComp = tempUser.getCourseActivityCompetence();
			double ideaSkillComp = tempUser.getIdeaSkillCompetence();
			double ideaCourseSkillComp = tempUser
					.getCourseIdeaSkillCompetence();

			double submitionSkillComp = tempUser.getSubmitionSkillCompetence();
			double courseSubmitionSkillComp = tempUser
					.getCourseSubmitionSkillCompetence();

			double totalActivityComp = (activityComp + courseActivityComp)
					* WeightFactorsUtility.getActivityCompetenceWeightPeers();
			double totalIdeaSkillComp = (ideaSkillComp + ideaCourseSkillComp)
					* WeightFactorsUtility.getSkillCompetenceWeightPeers();
			double totalSubmitionSkillComp = (submitionSkillComp + courseSubmitionSkillComp)
					* WeightFactorsUtility.getSkillCompetenceWeightPeers();

			double totalComp = totalActivityComp + totalIdeaSkillComp
					+ totalSubmitionSkillComp;
			/*
			 * LOGGER.info("_____________________________");
			 * LOGGER.info("ACTIVITY COMPETENCE");
			 * LOGGER.info("Problem Level: Brainstorming (WF:"
			 * +WeightFactorsUtility.getBrainstormingActivityWeightPeers()+")");
			 * LOGGER.info(" Submitting (WF:"+WeightFactorsUtility.
			 * getSubmittingActivityWeightPeers()+")");
			 * LOGGER.info(" Assessing (WF:"
			 * +WeightFactorsUtility.getAssessingActivityWeightPeers()+")");
			 * logger
			 * .info("Course Level: Brainstorming (WF:"+WeightFactorsUtility
			 * .getBrainstormingCourseActivityWeightPeers()+")");
			 * LOGGER.info(" Submitting (WF:"
			 * +WeightFactorsUtility.getSubmittingCourseActivityWeightPeers
			 * ()+")"); LOGGER.info(" Assessing (WF:"+WeightFactorsUtility.
			 * getAssessingCourseActivityWeightPeers()+")");
			 * LOGGER.info("Total activity (WF:"
			 * +WeightFactorsUtility.getActivityCompetenceWeightPeers()+")");
			 * LOGGER.info("Problem level="+activityComp);
			 * LOGGER.info("Course level="+courseActivityComp);
			 * LOGGER.info("Total="+totalActivityComp);
			 * LOGGER.info("_____________________________");
			 * LOGGER.info("SKILL COMPETENCE (IDEAS RATING)");
			 * LOGGER.info("WF (Problem level:"
			 * +WeightFactorsUtility.getBrainstormingSkillWeightPeers()+")" );
			 * LOGGER.info(" (Course level:"+WeightFactorsUtility.
			 * getBrainstormingCourseSkillWeightPeers()+")" );
			 * LOGGER.info("WF:"+
			 * WeightFactorsUtility.getSkillCompetenceWeightPeers());
			 * LOGGER.info("Problem level="+ideaSkillComp);
			 * LOGGER.info("Course level="+ideaCourseSkillComp);
			 * 
			 * LOGGER.info("Total="+totalIdeaSkillComp);
			 * LOGGER.info("_____________________________");
			 * LOGGER.info("SKILL COMPETENCE (SOLUTIONS RATING)");
			 * LOGGER.info("WF (Problem level:"
			 * +WeightFactorsUtility.getSubmittingSkillWeightPeers()+")" );
			 * LOGGER.info(" (Course level:"+WeightFactorsUtility.
			 * getSubmittingCourseSkillWeightPeers()+")" );
			 * LOGGER.info("WF:"+WeightFactorsUtility
			 * .getSkillCompetenceWeightPeers());
			 * LOGGER.info("Problem level="+submitionSkillComp);
			 * LOGGER.info("Course level="+courseSubmitionSkillComp);
			 * 
			 * LOGGER.info("Total="+totalSubmitionSkillComp);
			 * LOGGER.info("_____________________________");
			 * LOGGER.info("TOTAL="+totalComp);
			 * LOGGER.info("*****************************");
			 */
			tempUser.setTotalCompetence(totalComp);

			relPeersUrisSorted.add(tempUser.user.getUri().toString());

			CustComparatorByTotalCompetence comp = new CustComparatorByTotalCompetence();
			comp.setMap(uModelData.getRelevantPeersForDesignProblem(dProblem
					.getUri().toString()));
			Collections.sort(relPeersUrisSorted, comp);

		}
		return relPeersUrisSorted;
	}

	public class CustComparatorByTotalCompetence implements Comparator<String> {
		private Map map;

		@Override
		public int compare(String um1, String um2) {
			if (map == null)
				return 0;
			UserModel umodel1 = (UserModel) map.get(um1);
			UserModel umodel2 = (UserModel) map.get(um2);
			// handle the case where either one is null
			return umodel1.compareTo(umodel2);
		}

		public void setMap(Map map) {
			this.map = map;
		}

	}

}
