package org.goodoldai.depths.semanticstuff.services.annotations;

import java.util.Collection;
import java.util.Date;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONStringer;
import org.goodoldai.depths.config.Constants;
import org.goodoldai.depths.domain.activity.Brainstorming;
import org.goodoldai.depths.domain.annotation.ParentTag;
import org.goodoldai.depths.domain.annotation.Tag;
import org.goodoldai.depths.domain.annotation.UserNote;
import org.goodoldai.depths.domain.general.Resource;

import org.goodoldai.depths.domain.skos.Concept;
import org.goodoldai.depths.domain.user.User;
import org.goodoldai.depths.jsonstuff.TagsJSONExporter;

import org.goodoldai.depths.semanticstuff.rdfpersistance.query.ontmodel.OntModelQueryService;
import org.goodoldai.depths.semanticstuff.rdfpersistance.query.ontmodel.OntModelQueryServiceImpl;
import org.goodoldai.depths.semanticstuff.rdfpersistance.urigenerator.URIBuilder;
import org.goodoldai.depths.semanticstuff.services.AbstractDAOImpl;
 
import org.goodoldai.depths.semanticstuff.services.user.UserManagament;
import org.goodoldai.depths.semanticstuff.util.StringUtils;

public class AnnotationManagament extends AbstractDAOImpl {
	private OntModelQueryService queryService = new OntModelQueryServiceImpl();
	private static final Logger LOGGER = Logger.getLogger(AnnotationManagament.class);
	
	private static class AnnotationManagamentHolder{
		private static final AnnotationManagament INSTANCE=new AnnotationManagament();
	}
	public static AnnotationManagament getInstance(){
		return AnnotationManagamentHolder.INSTANCE;
	}
	private AnnotationManagament(){
		if(AnnotationManagamentHolder.INSTANCE!=null){
			throw new IllegalStateException("Already instantiated");
		}
	}
	public Collection<Tag> getTagsForUser(String userUri) throws Exception {
		String queryString = 
				"PREFIX rdf: <"+Constants.RDF_NS+"> \n" + 
				"PREFIX sioc: <"+Constants.SIOC_NS+"> \n" + 
				"PREFIX loco: <"+Constants.LOCO_NS+"> \n" + 
				
				"PREFIX depths: <"+Constants.DEPTHS_NS+"> \n" + 
				"SELECT  DISTINCT ?tag \n" + 
				"WHERE  {\n" +
					"?tag rdf:type loco:Tag.\n"+
					"?tag  loco:madeBy <"+userUri+">\n " +
					"}";
			 
			Collection<String> tagsUris = queryService
					.executeOneVariableSelectSparqlQuery(queryString, "tag",
							getDataModel());

			if (tagsUris != null && !tagsUris.isEmpty()){
				return loadResourcesByURIs(Tag.class,tagsUris, false);
			}
			return null;
	}
	public Collection<String> getDomainConceptsUrisForOntology(String ontologyUri) throws Exception {
		String queryString = 
				"PREFIX rdf: <"+Constants.RDF_NS+"> \n" + 
				"PREFIX skos: <"+Constants.SKOS_NS+"> \n" + 
				"PREFIX loco: <"+Constants.LOCO_NS+"> \n" + 
				"PREFIX dc: <"+Constants.DC_TERMS_NS+"> \n" + 
				
				"PREFIX depths: <"+Constants.DEPTHS_NS+"> \n" + 
				"SELECT  DISTINCT ?domainConcept \n" + 
				"WHERE  {\n" +
					"?domainConcept rdf:type skos:Concept.\n"+
					"<"+ ontologyUri +"> dc:subject ?domainConcept.\n " +
					"}";
			 
			Collection<String> conceptsUris = queryService
					.executeOneVariableSelectSparqlQuery(queryString, "domainConcept",
							getDataModel());

			//if (conceptsUris != null && !conceptsUris.isEmpty()){
				return conceptsUris;
			//}
			//return null;
	}
	public ParentTag getParentTag(User user,String tagStr) throws Exception {
		// TODO Auto-generated method stub
		ParentTag pTag=null;
		String queryString = 
				"PREFIX rdf: <"+Constants.RDF_NS+"> \n" + 
				"PREFIX loco: <"+Constants.LOCO_NS+"> \n" + 
				"PREFIX depths: <"+Constants.DEPTHS_NS+"> \n" + 
				"SELECT  DISTINCT ?parentTag \n" + 
				"WHERE  {\n" +
					"?parentTag rdf:type depths:ParentTag.\n"+
					"?parentTag  loco:hasContent \""+tagStr+"\"^^<http://www.w3.org/2001/XMLSchema#string>.\n " +
					"}";
			 
			Collection<String> tagsUris = queryService
					.executeOneVariableSelectSparqlQuery(queryString, "parentTag",
							getDataModel());
			if (tagsUris != null && !tagsUris.isEmpty()){
			 
				pTag= loadResourceByURI(ParentTag.class,tagsUris.iterator().next(), false);
				 
			}else{
				pTag=new ParentTag();
				String pTagUri=URIBuilder.getInstance().uriGenerator.generateInstanceURI(pTag);
				pTag.setUri(pTagUri);
				pTag.setHasContent(tagStr);
				//User user=loadResourceByURI(User.class,userUri,false);
				pTag.setMadeBy(user);
				saveResource(pTag,false);
			}
		return pTag;
	}
	public Tag setTagForIdeaByUser(User user, Brainstorming idea, Resource pTag) throws Exception {
		// TODO Auto-generated method stub
		Tag tag=null;
		String queryString = 
				"PREFIX rdf: <"+Constants.RDF_NS+"> \n" + 
				"PREFIX loco: <"+Constants.LOCO_NS+"> \n" + 
				"PREFIX depths: <"+Constants.DEPTHS_NS+"> \n" + 
				"SELECT  DISTINCT ?tag \n" + 
				"WHERE  {\n" +
					"?tag rdf:type loco:Tag.\n"+
					"?tag depths:hasParentTag <"+pTag.getUri().toString()+">.\n"+
					"?tag loco:madeBy <"+user.getUri().toString()+">.\n"+
					"<"+idea.getUri().toString()+"> loco:hasAnnotation ?tag.\n"+
					"}";
			 
			Collection<String> tagsUris = queryService
					.executeOneVariableSelectSparqlQuery(queryString, "tag",
							getDataModel());
			if (tagsUris != null && !tagsUris.isEmpty()){
				
				tag= loadResourceByURI(Tag.class,tagsUris.iterator().next(), false);
		 	}else{
				
				tag=new Tag();
				String tagUri=URIBuilder.getInstance().uriGenerator.generateInstanceURI(tag);
				tag.setUri(tagUri);
				if(pTag instanceof ParentTag){
				tag.setHasContent(((ParentTag) pTag).getHasContent());
				}else{
					tag.setHasContent(pTag.getTitle());
				}
		 		tag.setMadeBy(user);
				tag.setHasParentTag(pTag);
				saveResource(tag,false);
			}
		return tag;
	}
	public Collection<Tag> getAllPeersTags(String userUri) throws Exception {
		String queryString = 
				"PREFIX rdf: <"+Constants.RDF_NS+"> \n" + 
				"PREFIX sioc: <"+Constants.SIOC_NS+"> \n" + 
				"PREFIX loco: <"+Constants.LOCO_NS+"> \n" + 
				
				"PREFIX depths: <"+Constants.DEPTHS_NS+"> \n" + 
				"SELECT  DISTINCT ?tag \n" + 
				"WHERE  {\n" +
					"?tag rdf:type loco:Tag.\n"+
					 
					"}";
			 
			Collection<String> tagsUris = queryService
					.executeOneVariableSelectSparqlQuery(queryString, "tag",
							getDataModel());

			if (tagsUris != null && !tagsUris.isEmpty()){
				return loadResourcesByURIs(Tag.class,tagsUris, false);
			}
			return null;
	}
	public UserNote getUserNoteForIdeaByUser(User user, Brainstorming idea, String noteContent, String visibility, String commentType) throws Exception {
		// TODO Auto-generated method stub
		 
		UserNote uNote=null;
 
				
				uNote=new UserNote();
				String uNoteUri=URIBuilder.getInstance().uriGenerator.generateInstanceURI(uNote);
				uNote.setUri(uNoteUri);
				String content=StringUtils.checkInputContentSize(noteContent,450);
				content=StringUtils.checkInputContent(content);
				
				uNote.setHasContent(content);
				
				uNote.setMadeBy(user);
				uNote.setDateCreated(new Date());
				uNote.setNoteType(commentType);
				idea.addHasAnnotation(uNote);
		//	}
			//uNote.setHasContent(noteContent);
			uNote.setVisibility(visibility);
			saveResource(uNote,false);
			updateResource(idea,false);
			 
		return uNote;
	}
 
	public String getMyAnnotationOfIdea(User user, Brainstorming idea) throws Exception {
		UserNote uNote=null;
		Collection<Tag> tags=null;
		String annotationJson="[]";
		String queryString = 
				"PREFIX rdf: <"+Constants.RDF_NS+"> \n" + 
				"PREFIX loco: <"+Constants.LOCO_NS+"> \n" + 
				"PREFIX depths: <"+Constants.DEPTHS_NS+"> \n" + 
				"SELECT  DISTINCT ?userNote \n" + 
				"WHERE  {\n" +
					"?userNote rdf:type loco:UserNote.\n"+
					"<"+idea.getUri().toString()+"> loco:hasAnnotation ?userNote.\n"+
					"?userNote loco:madeBy <"+user.getUri().toString()+">.\n"+
					 
					"}";
			 
			Collection<String> uNotesUris = queryService
					.executeOneVariableSelectSparqlQuery(queryString, "userNote",
							getDataModel());
			if (uNotesUris != null && !uNotesUris.isEmpty()){
				
				uNote= loadResourceByURI(UserNote.class,uNotesUris.iterator().next(), false);
			 	
				String queryString2 = 
						"PREFIX rdf: <"+Constants.RDF_NS+"> \n" + 
						"PREFIX sioc: <"+Constants.SIOC_NS+"> \n" + 
						"PREFIX loco: <"+Constants.LOCO_NS+"> \n" + 
						
						"PREFIX depths: <"+Constants.DEPTHS_NS+"> \n" + 
						"SELECT  DISTINCT ?tag \n" + 
						"WHERE  {\n" +
							"?tag rdf:type loco:Tag.\n"+
							"?tag  loco:madeBy <"+user.getUri().toString()+">.\n " +
							"<"+idea.getUri().toString()+"> loco:hasAnnotation ?tag.\n"+
							"}";
					Collection<String> tagsUris = queryService
							.executeOneVariableSelectSparqlQuery(queryString2, "tag",
									getDataModel());
					
					if (tagsUris != null && !tagsUris.isEmpty()){
						 tags= loadResourcesByURIs(Tag.class,tagsUris, false);
					} 
					annotationJson=TagsJSONExporter.getInstance().getIdeaAnnotationsAsJSON(uNote,tags);
			} 
		
		
		
		 
		return annotationJson;
	}
	public String getPeersNotesForIdeaUriJSON(User user, Brainstorming idea) throws Exception {
		 
		Collection<UserNote> uNotes=null;
		String peersNotesForIdeaJSON="[]";
		String queryString = 
				"PREFIX rdf: <"+Constants.RDF_NS+"> \n" + 
				"PREFIX loco: <"+Constants.LOCO_NS+"> \n" + 
				"PREFIX depths: <"+Constants.DEPTHS_NS+"> \n" + 
				"PREFIX dc: <"+Constants.DC_TERMS_NS+"> \n"+
				"SELECT  DISTINCT ?userNote \n" + 
				"WHERE  {\n" +
					"?userNote rdf:type loco:UserNote.\n"+
					"<"+idea.getUri().toString()+"> loco:hasAnnotation ?userNote.\n"+
				 	 "?userNote dc:created ?created"+
					"}"+
				 	 "ORDER BY (?created)"	;
			 
			Collection<String> uNotesUris = queryService
					.executeOneVariableSelectSparqlQuery(queryString, "userNote",
							getDataModel());
			if (uNotesUris != null && !uNotesUris.isEmpty()){
				
				uNotes= loadResourcesByURIs(UserNote.class,uNotesUris, false);
				peersNotesForIdeaJSON=TagsJSONExporter.getInstance().getPeersNotesForIdeaJSON(user,uNotes);
			}else{
				JSONStringer js=new JSONStringer();
				js.object().key("success").value("false");
				js.endObject();
				
				return "("+js.toString()+")";
			}
		
		
		
		 
		return peersNotesForIdeaJSON;
	}
	public Collection<Resource> getAllAnnotatedResources() throws Exception {
		String queryString = 
				"PREFIX rdf: <"+Constants.RDF_NS+"> \n" + 
				"PREFIX sioc: <"+Constants.SIOC_NS+"> \n" + 
				"PREFIX loco: <"+Constants.LOCO_NS+"> \n" + 
				"PREFIX skos: <"+Constants.SKOS_NS+"> \n"+
				
				"PREFIX depths: <"+Constants.DEPTHS_NS+"> \n" + 
				"SELECT  DISTINCT ?resource \n" + 
				"WHERE  {\n" +
					"?concept rdf:type skos:Concept.\n"+
					"?resource depths:isRelevantFor ?concept.\n"+
					 
					"}";
		 
			Collection<String> resourcesUris = queryService
					.executeOneVariableSelectSparqlQuery(queryString, "resource",
							getDataModel());

			if (resourcesUris != null && !resourcesUris.isEmpty()){
				return loadResourcesByURIs(Resource.class,resourcesUris, false);
			}
			return null;
	}
	public Concept getDomainConceptByTitle(String title) throws Exception {
		Concept concept=null;
		String queryString = 
				"PREFIX rdf: <"+Constants.RDF_NS+"> \n" + 
				"PREFIX loco: <"+Constants.LOCO_NS+"> \n" + 
				"PREFIX depths: <"+Constants.DEPTHS_NS+"> \n" + 
				"PREFIX skos: <"+Constants.SKOS_NS+"> \n"+
				"PREFIX xsd: <"+Constants.XMLSCHEMA_NS+">\n"+
				"PREFIX dc: <"+Constants.DC_TERMS_NS+">\n"+
				"SELECT  DISTINCT ?concept \n" + 
				"WHERE  {\n" +
					"?concept rdf:type skos:Concept.\n"+
					"?concept dc:title \""+title+"\"^^xsd:string.\n"+
					"}";
		 
			Collection<String> conceptsUris = queryService
					.executeOneVariableSelectSparqlQuery(queryString, "concept",
							getDataModel());
			if (conceptsUris != null && !conceptsUris.isEmpty()){
				
				concept= loadResourceByURI(Concept.class,conceptsUris.iterator().next(), false);
		 	}
			return concept;
		
	}

}
