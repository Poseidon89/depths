package org.goodoldai.depths.semanticstuff.services.moodle;

import java.util.Collection;

import org.apache.log4j.Logger;
import org.goodoldai.depths.config.Constants;
 
import org.goodoldai.depths.semanticstuff.rdfpersistance.query.ontmodel.OntModelQueryService;
import org.goodoldai.depths.semanticstuff.rdfpersistance.query.ontmodel.OntModelQueryServiceImpl;
import org.goodoldai.depths.semanticstuff.services.AbstractDAOImpl;
 

public class RDFDataManagement extends AbstractDAOImpl {
	private OntModelQueryService queryService = new OntModelQueryServiceImpl();
	private static final Logger LOGGER = Logger.getLogger(RDFDataManagement.class);

	private static class RDFDataManagementHolder {
		private static final RDFDataManagement INSTANCE = new RDFDataManagement();
	}

	public static RDFDataManagement getInstance() {
		return RDFDataManagementHolder.INSTANCE;
	}

	private RDFDataManagement() {
		if (RDFDataManagementHolder.INSTANCE != null) {
			throw new IllegalStateException("Already instantiated");
		}
	}

	public boolean checkIfResourceExists(String uri) {
		String queryString = "PREFIX rdf: <" + Constants.RDF_NS + ">\n"

		+ "SELECT  DISTINCT ?resource\n" + "WHERE {\n" + "<" + uri
				+ "> rdf:type ?resource.\n" + "}";
 		Collection<String> resUris = queryService
				.executeOneVariableSelectSparqlQuery(queryString, "resource",
						getDataModel());

		if (resUris != null && !resUris.isEmpty()) {
			if (resUris.size() == 0) {
				return false;
			} else {
				return true;
			}
		}
		return false;
	}
}
