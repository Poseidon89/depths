package org.goodoldai.depths.semanticstuff.rdfpersistance;

import org.apache.log4j.Logger;

public class DataProviderFactory {

	@SuppressWarnings("unused")
	private static final Logger LOGGER = Logger.getLogger(DataProviderFactory.class
			.getName());

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static DataProvider getDataProvider() {
		return new RepositoryDataProvider();
	}

}
