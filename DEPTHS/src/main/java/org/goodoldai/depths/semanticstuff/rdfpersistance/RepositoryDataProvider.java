package org.goodoldai.depths.semanticstuff.rdfpersistance;

import java.sql.SQLException;


import org.apache.log4j.Logger;
import org.goodoldai.depths.config.Settings;

import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.sdb.SDBFactory;
import com.hp.hpl.jena.sdb.Store;
import com.hp.hpl.jena.sdb.StoreDesc;
import com.hp.hpl.jena.sdb.sql.SDBConnection;
import com.hp.hpl.jena.sdb.store.DatabaseType;
import com.hp.hpl.jena.sdb.store.LayoutType;

public class RepositoryDataProvider implements DataProvider {

	private static final Logger LOGGER =  Logger.getLogger(RepositoryDataProvider.class);

	private StoreDesc storeDesc;
	private SDBConnection conn;
	private Store store;

	public RepositoryDataProvider() {
		// get DB settings from config file
				String dbType =  Settings.getInstance().config.rdfRepositoryConfig.sdbConfig.dbType;

				// MySQL
				if (Settings.getInstance().config.MySQL.equalsIgnoreCase(dbType)) {
					// create a data store description for MySQL database
					storeDesc = new StoreDesc(LayoutType.LayoutTripleNodesHash,
							DatabaseType.MySQL);
					
					  
				}
				// H2
				else if (Settings.getInstance().config.H2.equalsIgnoreCase(dbType)) {
					// create a data store description for H2 database
					storeDesc = new StoreDesc(LayoutType.LayoutSimple,
							DatabaseType.H2);
				}
		// get the connection
		establishConnection();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.goodoldai.depths.services.datacentral.DataProvider#getDataModel()
	 */
	public Model getDataModel(boolean reconnect) {
		Model dataModel = null;

		try {
			// create the necessary tables - cleans the db
			if (Settings.getInstance().config.rdfRepositoryConfig.sdbConfig.format && !reconnect) {
				LOGGER.debug("Formatting db tables.1..");
				store.getTableFormatter().format();
				LOGGER.debug("Formatting db tables.2..");
				store.getTableFormatter().create();
			 
				LOGGER.debug("Formatting db tables.3..");
			}
			// get the data model
			establishConnection();
			LOGGER.debug("Connecting default data model...");
			dataModel = SDBFactory.connectDefaultModel(store);
			//dataModel=ModelFactory.createDefaultModel();

		} catch (Exception e) {
			LOGGER.error("Could not load DataModel from database!", e);
		}

		return dataModel;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.goodoldai.depths.services.datacentral.DataProvider#flushDataModel(com.hp.
	 * hpl.jena.rdf.model.Model)
	 */
	public void flushDataModel(Model dataModel) {
		// nothing to do here. data is automatically flushed to database.
		// well, should be...
		store.close();
		conn.close();
	}

	/**
	 * 
	 */
	private void establishConnection() {
		try {
			conn = new SDBConnection(ConnectionPool.getInstance().getConnection());
			LOGGER.debug("Got SQL connection (is closed? : " + conn.getSqlConnection().isClosed() + ")");
			store = SDBFactory.connectStore(conn, storeDesc);
		} catch (SQLException e) {
			LOGGER.error("Could not get JDBC Connection for SDB store", e);
			// TODO: throw exception? retry?
		}
	}
}
