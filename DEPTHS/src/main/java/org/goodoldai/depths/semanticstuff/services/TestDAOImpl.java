package org.goodoldai.depths.semanticstuff.services;

import java.util.Collection;

import org.goodoldai.depths.domain.sioc.UserAccount;
import org.goodoldai.depths.semanticstuff.rdfpersistance.query.ontmodel.OntModelQueryService;
import org.goodoldai.depths.semanticstuff.rdfpersistance.query.ontmodel.OntModelQueryServiceImpl;
import org.goodoldai.depths.config.Constants;

public class TestDAOImpl extends AbstractDAOImpl {

	private OntModelQueryService queryService = new OntModelQueryServiceImpl();
	
	public UserAccount getAccountsOfUser(String personUri) throws Exception {
		
		String queryString = 
			"PREFIX rdf: <"+Constants.RDF_NS+"> \n" + 
			"PREFIX sioc: <"+Constants.SIOC_NS+"> \n" + 
			"PREFIX foaf: <"+Constants.FOAF_NS+"> \n" + 
			"SELECT ?account \n" + 
			"WHERE  {\n" +
				"<"+personUri+"> foaf:holdsAccount ?account ." +
				"?account rdf:type sioc:UserAccount ." +
			"}";
		
		Collection<String> accountUris = queryService
				.executeOneVariableSelectSparqlQuery(queryString, "account",
						getDataModel());

		if (accountUris != null && !accountUris.isEmpty()){
			return loadResourceByURI(UserAccount.class, accountUris.iterator().next(), false);
		}
		return null;
	}
}
