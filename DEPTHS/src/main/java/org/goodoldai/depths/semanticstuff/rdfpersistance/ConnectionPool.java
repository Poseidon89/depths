/**
 * 
 */
package org.goodoldai.depths.semanticstuff.rdfpersistance;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.apache.commons.dbcp.ConnectionFactory;
import org.apache.commons.dbcp.DriverManagerConnectionFactory;
import org.apache.commons.dbcp.PoolableConnectionFactory;
import org.apache.commons.dbcp.PoolingDataSource;
import org.apache.commons.pool.impl.GenericObjectPool;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;
import org.goodoldai.depths.config.Config;
import org.goodoldai.depths.config.Settings;
 

import com.hp.hpl.jena.sdb.sql.JDBC;

public class ConnectionPool {

	private static class ConnectionPoolHolder {
		private static final ConnectionPool INSTANCE = new ConnectionPool();
	}

	private static final Logger LOGGER = Logger.getLogger(ConnectionPool.class);
	

	private DataSource ds;
	private GenericObjectPool pool;

	private ConnectionPool() {
		
		Settings settings = Settings.getInstance();
		if (settings.config == null) {
			return;
		}
		//PropertyConfigurator.configure(ConnectionPool.class.getClassLoader()
			//	.getResource(settings.config.log4jConfig));
		DOMConfigurator.configure(ConnectionPool.class.getClassLoader()
				.getResource(settings.config.loggingConfig.log4jConfig));
		 
		LOGGER.info("Initializing JDBC connection pool");
		LOGGER.debug("Initializing JDBC connection pool");
		
		// get DB settings from config file
		String dbType = Settings.getInstance().config.rdfRepositoryConfig.sdbConfig.dbType;
		String jdbcURL = Settings.getInstance().config.rdfRepositoryConfig.sdbConfig.dbURL;
		String user = Settings.getInstance().config.rdfRepositoryConfig.sdbConfig.dbUser;
		String pw = Settings.getInstance().config.rdfRepositoryConfig.sdbConfig.dbPassword;
 

		// MySQL
				if (Config.MySQL.equalsIgnoreCase(dbType)) {
					JDBC.loadDriverMySQL();
				}
				// H2
				else if (Config.H2.equalsIgnoreCase(dbType)) {
					JDBC.loadDriverH2();
				}
	 
		pool = new GenericObjectPool();
		pool.setMinIdle(5);
		pool.setMaxActive(15);
		pool.setTestOnBorrow(true);
		ConnectionFactory connectionFactory = new DriverManagerConnectionFactory(
				jdbcURL, user, pw);
	 
		@SuppressWarnings("unused")
		PoolableConnectionFactory poolableConnectionFactory = new PoolableConnectionFactory(
				connectionFactory, pool, null, "SELECT 1;", false, true);

		ds = new PoolingDataSource(pool);
	}

	/**
	 * 
	 * @return
	 */
	public static ConnectionPool getInstance() {
		return ConnectionPoolHolder.INSTANCE;
	}

	/**
	 * 
	 * @return
	 * @throws SQLException
	 */
	public Connection getConnection() throws SQLException {
		
		return ds.getConnection();
	}

}
