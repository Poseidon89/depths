package org.goodoldai.depths.semanticstuff.services.content;

import static org.junit.Assert.*;

import java.util.Collection;

import org.apache.log4j.Logger;
import org.goodoldai.depths.domain.content.Course;
import org.goodoldai.depths.domain.content.DesignProblem;
import org.goodoldai.depths.testutility.TestUtility;
import org.junit.Test;

public class ContentManagamentTest {
	private static final Logger LOGGER = Logger.getLogger(ContentManagamentTest.class);
	@Test
	public void testGetInstance() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetDesignProblemByUri() {
		try{
			 DesignProblem  dp=  ContentManagament.getInstance().getDesignProblemByUri(TestUtility.testproblem);
		LOGGER.info("get dp title:"+dp.getTitle());
		}catch(Exception e){
			LOGGER.error("Error:"+e.getLocalizedMessage());
		}
		//fail("Not yet implemented");
	}

	@Test
	public void testGetCoursesForUser() {
		 try {
			Collection<Course> cs=ContentManagament.getInstance().getCoursesForUser(TestUtility.testuser);
		LOGGER.info("cs: size:"+cs.size());
		 } catch (Exception e) {
			// TODO Auto-generated catch block
			LOGGER.error("Error:"+e.getLocalizedMessage());
		}
	//	fail("Not yet implemented");
	}
	
	@Test
	public void testGetDesignProblemsForCourse(){
		try{
			Collection<DesignProblem> dps=ContentManagament.getInstance().getDesignProblemsForCourse(TestUtility.testcourse);
		for(DesignProblem dp:dps){
			LOGGER.info("Design problem in course:"+dp.getTitle());
		}
		}catch(Exception e){
			LOGGER.error("Error:"+e.getLocalizedMessage());
		}
	}

}
