package org.goodoldai.depths.datamapping;



import java.util.Collection;

import org.apache.log4j.Logger;
import org.goodoldai.depths.semanticstuff.rdfpersistance.DataModelManager;
import org.goodoldai.depths.semanticstuff.rdfpersistance.query.ontmodel.OntModelQueryService;
import org.goodoldai.depths.semanticstuff.rdfpersistance.query.ontmodel.OntModelQueryServiceImpl;

import org.goodoldai.depths.config.Constants;
 
 
import org.junit.Test;

public class D2RQMapperTest {
	private static final Logger LOGGER = Logger.getLogger(D2RQMapperTest.class);
	@Test
	public void testParseAllMoodleDataToDatabase() {
		LOGGER.info("starting test");
		//D2RQMapper dm=new D2RQMapper();
		//dm.parseAllMoodleDataToDatabase();
		testData();
		LOGGER.info("finished test");
		//fail("Not yet implemented");
	}
	public void testData(){
		
		OntModelQueryService queryService = new OntModelQueryServiceImpl();
	String queryString = "PREFIX rdf: <" + Constants.RDF_NS + ">\n"
	+"PREFIX rdfs: <" + Constants.RDFS_NS + ">\n"
	 
	+ "SELECT DISTINCT ?s\n" + "WHERE {\n" 
 
	+" ?s ?p ?o. \n"
 
	+"}";
	
	
	 
 
	Collection<String> accountUris = queryService
	.executeOneVariableSelectSparqlQuery(queryString, "s",
			DataModelManager.getInstance().getDataModel());
 
 for(String uri:accountUris){
	 LOGGER.info("found uri:"+uri);
 }
	}
}
